# JAM2: a Monte-Carlo event generator for high-energy nuclear collisions

JAM2 is the new version of JAM1 (https://github.com/ynara305/jam1).

This code simulates high energy nuclear collisions based on the transport theoretical models. 
Models include hadronic cascade model, relativistic quantum molecular dynamics (RQMDs,RQMDv,RQMD.RMF,RQMD.PDM), and hydrodynamics.
Hybrid simulation of hydrodynamics + cascade model is available.

First download pythia8.307 code from
https://pythia.org/download/pythia83/pythia8307.tar.bz2
from https://pythia.org/releases/

Then Install Pythia8 to your preferred directory:

./configure --prefix=$HOME/lib/pythia8

make install

Then go to JAM source directory and in case there is no file 'configure' do

autoreconf -i

./configure PYTHIA8=$HOME/lib/pythia8 --prefix=$HOME/lib/jam2 CXXFLAGS="-march=native -O3 -pipe -std=c++14"

make

make install

In case you set the environment variable 'PYTHIA8DATA', it should be identical to
the directory:'$HOME/lib/pythia8/share/Pythia8/xmldoc'.

JAM can be compiled with the CERN ROOT library by adding the configure option --with-root

If you do not need the default phase space output file 'phase.dat.gz', execute JAM2 with ./jam -p 0
