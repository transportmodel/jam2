#include <jam2/meanfield/MeanField.h>
#include <jam2/hadrons/JamStdlib.h>

namespace jam2 {

using namespace std;

bool MeanField::firstCall=true;

void gaussLegendre(std::vector<double>& x, std::vector<double>& w,int np) {
  double eps=3.0e-14;
  int m=int((np+1)/2);
  for (int i=0; i<m;i++) {
    double t=cos(M_PI*(i+1-0.25)/(np+0.5));
    double t1= t+1000.;
    double pp=1.0;
    while(abs(t-t1) > eps) {
      double p1=1.0, p2=0.0, aj=0.0;
      for (int j=0;j<np;j++) {
        double p3=p2;
        p2=p1;
        aj += 1.0;
        p1=((2.0*aj-1.0)*t*p2-(aj-1.0)*p3)/aj;
      }
      pp=np*(t*p1-p2)/(t*t-1.0);
      t1=t;
      t=t1-p1/pp;
    };

    x[i]=-t;
    x[np-1-i]=t;
    w[i]=2.0/((1.0-t*t)*pp*pp);
    w[np-1-i]=w[i];
  }
}

void infInterval(double A,std::vector<double>& xg,std::vector<double>& wg,int n) {
  for(int i=0;i<n;i++) {
    double xg2=xg[i]*xg[i];
    wg[i]=A*(1.0+xg2)/pow2(1.0-xg2)*wg[i];
    xg[i]=A*xg[i]/(1.0-xg2);
  }
}

void intervalAB(double A,double B,std::vector<double>& xg,std::vector<double>& wg,int n) {
  for(int i=0;i<n;i++) {
    xg[i]=(B+A)/2+(B-A)/2*xg[i];
    wg[i]=(B-A)/2*wg[i];
  }
}

MeanField::MeanField(Pythia8::Settings* s)
{
  settings = s;

  firstEng=true;
  //dT= settings->parm("Cascade:TimeStepSize");
  //widG = settings->parm("MeanField:gaussWidth");
  //facG = 1.0/pow(4.0*M_PI*widG, 1.5);
  //wG = 1.0/(4*widG);

  eosType=settings->mode("MeanField:EoS");
  optPotential=settings->mode("MeanField:optPotential");
  isDebug = settings->mode("Check:Debug");
  int self = settings->mode("MeanField:selfInteraction");
  selfInt = 1 - self;

  optScalarDensity=settings->mode("MeanField:optScalarDensity");
  optP0dev=settings->mode("MeanField:optP0dev");
  optTwoBodyDistance=settings->mode("MeanField:twoBodyDistance");
  optVectorPotential=settings->mode("MeanField:optVectorPotential");
  optVectorDensity=settings->mode("MeanField:optVectorDensity");
  optPV=settings->mode("MeanField:optMomPotential");
  optPotentialArg=settings->mode("MeanField:optPotentialArg");
  optPotentialArgMD=settings->mode("MeanField:optPotentialArgMD");
  optRecoverEnergy=settings->mode("MeanField:optRecoverEnergy");
  gamCM = settings->parm("Cascade:gamCM");
  gamCM2 = gamCM*gamCM;
  optDerivative = settings->flag("MeanField:optDerivative");
  facMesonPot = settings->parm("MeanField:factorMesonPotential");
  optVelocity=settings->mode("MeanField:optVelocity");

  rho0=settings->parm("MeanField:rho0");
  optLambdaPot = settings->mode("MeanField:optLambdaPotential");
  optPotentialDensity = settings->mode("MeanField:optPotentialDensity");
  optCollisionOrdering = settings->mode("Cascade:optCollisionOrder");
  optRQMDevolution= settings->mode("MeanField:optRQMDevolution");
  lambdaLag = settings->parm("Cascade:LagrangeMultiplier");


  if(optCollisionOrdering>100) optP0dev=0;

  // RQMDv
  optVdot=settings->mode("MeanField:optVdot");
  // In case only time component of vector potential is included,
  // we do not need to distinguish between kinetic momenta and canonical momenta
  if(optVectorPotential !=1) optVdot=0;

  optPropagate=0;
  if(optVectorPotential==1 && optVdot==0) optPropagate=1;
    
  if(optTwoBodyDistance==1) {
    distance = new NonRelDistance(settings);
  } else if(optTwoBodyDistance==2) {
    distance = new TwoBodyCM(settings);
  } else {
    distance = new RestFrame(settings);
  }

  optMDarg3=settings->mode("MeanField:twoBodyDistanceMD");
  if(optMDarg3==1) {
    distanceP = new NonRelDistance(settings);
  } else if(optMDarg3==2) {
    distanceP = new TwoBodyCM(settings);
  } else {
    distanceP = new RestFrame(settings);
  }

  optPotentialEval = settings->mode("MeanField:optPotentialEvaluation"); 
  optMomDerKernel = settings->flag("MeanField:optMomentumDerivativeKernel");
  if(optPotentialEval==3) optMomDerKernel=false;
  nGaussPoint=settings->mode("MeanField:nGaussPoint"); 
  nMCPoint=settings->mode("MeanField:nMonteCarloPoint"); 

  if(optPotentialEval>4 && optTwoBodyDistance !=3) {
    cout << "MeanField:: optPotentialEval cannot be used without optTwoBdoyDistance=3"<<endl;
    exit(1);
  }

  // integral by the Gauss points
  if(optPotentialEval==5) {
    xG.resize(nGaussPoint);
    wG.resize(nGaussPoint);
    gaussLegendre(xG,wG,nGaussPoint);
    infInterval(1.0,xG,wG,nGaussPoint);
  }

  // Covariant propagation.
  /*
  if(optCollisionOrdering>100) {
    distance->setOptP0dev(0);
    distanceP->setOptP0dev(0);
  }
  */
  //if(outputForce) outputFOfs.open("force.dat");

}

void MeanField::rescaleForce(Pythia8::Vec4& qdot)
{
  double q2=qdot.pAbs();
  static const double eps=1e-12;
  qdot[1] *= qdot[0]*(1.0-eps)/q2;
  qdot[2] *= qdot[0]*(1.0-eps)/q2;
  qdot[3] *= qdot[0]*(1.0-eps)/q2;
}

} // namespace jam2


