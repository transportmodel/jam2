#ifndef jam2_meanfield_ParityDoublet_h
#define jam2_meanfield_ParityDoublet_h

//#include <fstream>
#include <Pythia8/Settings.h>
#include <jam2/hadrons/JamParticleData.h>
#include <jam2/meanfield/PotentialType.h>
#include <jam2/hadrons/JamStdlib.h>

namespace jam2 {

class ParamPDM
{
public:
  std::array<double,12> fp;
  int  opt;
  int  potid;
  double uv;
  int id2;
  double m2;
};

class ParityDoublet
{
private:
  //std::ofstream ofs;
  Pythia8::Settings* settings;
  JamParticleData* jamtable;
  Pythia8::ParticleData* pTable;
  int eosType,transportModel;
  int optRMF=1; //=1:Walecka sigma-omega model,=2:PDM 3term =3:singlet model,=4:PDM 4term
  int optHyperonCoupling=5,optDeltaCoupling=3;
  int optNStarCoupling=3;
  int optDeltaStarCoupling=2;
  int optLambdaStarCoupling=2;
  int optSigmaStarCoupling=2;
  int optXiStarCoupling=2,optOmegaCoupling=2;
  std::unordered_map<int,ParamPDM> pBaryon;
  double M0=0.7,Md0=0.7,Ml0=0.7,Ms0=0.7,Mx0=0.7,Mo0=0.7;
  double Un=-0.06,Ud=-0.03, Ul=-0.03,Us=0.02,Ux=-0.02,Uo=0.02;
  double Uns=-0.06,Uls=-0.03,Uss=0.02,Uxs=-0.02;
  double Meff0; // baryon mass at the saturation density
  double M_nucl=0.939;
  double M_omega= 0.783;
  double M_omega2=M_omega*M_omega;
  double M_omega3=M_omega2*M_omega;
  double M_phi=1.02, M_phi2=M_phi*M_phi;
  double M_pion= 0.138;
  double M_rho= 0.776;
  double f_pion= 0.093;
  double f_pion2= f_pion*f_pion;
  double EpsPi=M_pion*M_pion*f_pion;
  double rho0= 0.168;      // saturation density
  double BE=-0.016;        // binding energy
  double K=0.24;
  double CONV=HBARC*HBARC*HBARC;
  double rho0gev=rho0*CONV;
  double pF0,muS0;
  double A,B,C,D; // parameters of the sigma field
  double C4=0.0; // omega self coupling;quartic term
  double lam6fPion;
  double Sigma0;  // sigma-field at saturation density rho0
  double Omega0,Phi0;
  double M_sigma;            // sigma meson mass in GeV
  double g_omega=0.0,Cv2=0.0;        // omega coupling
  double g_rho=0.0,g_rho0=0.0,Cr2=0.0;  // rho coupling
  double g_sigma,g_phi=0.0;
  double gDo=1.0, gDp=0.0, gDr=0.0; // delta coupling
  double gLo=1.0, gSo=1.0, gXo=1.0, gOo=1.0; // Hyperon-omega coupling
  double gLp=0.0, gSp=0.0, gXp=0.0, gOp=0.0; // Hyperon-phi coupling
  double gLr=0.0, gSr=0.0, gXr=0.0, gOr=0.0; // Hyperon-rho coupling
  double gLso=1.0, gSso=1.0, gXso=1.0, gOso=1.0; // Hyperon*-omega coupling
  double gLsp=0.0, gSsp=0.0, gXsp=0.0, gOsp=0.0; // Hyperon*-phi coupling
  double gLsr=0.0, gSsr=0.0, gXsr=0.0, gOsr=0.0; // Hyperon*-rho coupling
  int    optDeltaSigmaCoupling, optHyperonSigmaCoupling,optVectorCoupling;
  double xDSigma=1.0, xYSigma=1.0;
  double Esym=0.031; // nuclear symmetry energy in GeV
  int optPhiMeson=0, optRhoMeson=0;
  int optMassPDM=1;
  double zG81=1.0/sqrt(6.0); // = g_8/g_1
  double zG101=1.0/sqrt(6.0); // = g_10/g_1
  double alphaV=1.0; // F and D type coupling
  double tanV=1.0/sqrt(2); // omega-phi mixing
  double sigmaMax,sigmaIni=f_pion+1e-4;
  double rhosMax; // maximum scalar density which has a solution of gap equation.
  double facG=1.0,facGv=1.0;

  bool   withMomDep=false;
  static bool firstCall;
  double C1=0.0, C2=0.0, mu1=1.0, mu2=1.0;
  double gsp=0.0, gvp=0.0;
  double vex1=0.0,vex2=0.0,pmu1=1.0,pmu2=1.0;

  //double A3,B3,C3,D3; // parameters of the sigma field

public:
  ParityDoublet(Pythia8::Settings* s,JamParticleData* jpt);
  ~ParityDoublet() { };
  void setSigmaOmegaModel();
  void setSingletModel();
  void setPDM1Model();
  void setPDM2Model();
  void setParamPDM(int potid,std::unordered_map<int,ParamPDM>& p,int id1,int id2,double m0,double xv,double uv,double gphi,double grho,int opt,double m1=0.0,double m2=0.0);
  void setParamPDM2(int id1, double m1, ParamPDM& pdm);
  bool  setPotentialParam(EventParticle* p);
  bool  setParticle1(EventParticle* p,double* fp);
  void  setHyperonVectorCoupling();
  double getCv2() const {return Cv2;}
  double gOmega() const {return g_omega;}
  double getC4() const {return C4;}
  double getFPion() const {return f_pion;}
  bool isMomDep() const {return withMomDep;}
  int  rhoMeson() const {return optRhoMeson;}
  int isRMF() const {return optRMF;}
  double sigma_bisec(double rhos);
  double bisecGomega(double r2);
  double bisec1(double s0, double s1,double r0);
  double bisec2(double s0, double s1);
  double bisec3(double s);
  double sigma_bisecRMF(double rhos,double g,double smax);
  double newton(double s0, double rhos,int opt);
  void printInfo();
  double mOmega2() const {return M_omega2;}
  double mOmega3() const {return M_omega3;}
  double mPhi2() const {return M_phi2;}
  double mRho2() const {return M_rho*M_rho;}
  double mRho3() const {return M_rho*M_rho*M_rho;}
  double getL();
  double getFacCoupling() const {return facG;}
  double getFacCouplingV() const {return facGv;}

  // compute sigma coupling with masses m+ and m-.
  // m_{+-}=sqrt(a^2sigma^2+m0^2) +- bsigma
  pair<double,double> paramMass(double mp, double mm,double m0) {
    double gs1=sqrt( pow2(mp+mm) - 4*m0*m0)/(2*f_pion);
    double gs2= (mp - mm)/(2*f_pion);
    return {gs1,gs2};
  }

  // compute sigma coupling with particle ids.
  pair<double,double> paramMass(int id1, int id2,double m0) {
    ParticleDataEntryPtr pd1=jamtable->find(id1); // positive parity 
    ParticleDataEntryPtr pd2=jamtable->find(id2); // negative parity
    if(optMassPDM==1) {
      return paramMass(pd1->m0(),pd2->m0(),m0);
    } else {
      return paramMass(pd1->mMin(),pd2->mMin(),m0);
    }
  }

  // compute coefficients of sigma potential.
  std::tuple<double,double,double> coefScalarPot(double ms,double lam6f) {
    double a = (ms*ms - 3*M_pion*M_pion)/2. + lam6f*f_pion*f_pion;
    double b = (ms*ms -   M_pion*M_pion)/(2.*f_pion*f_pion) + 2*lam6f;
    double c = lam6f/(f_pion*f_pion);
    return {a,b,c};
  }

  // sigma potential U(sigma) for sigma-omega model
  double SigmaPotentialRMF(double sigma) {
    return sigma*sigma*(0.5*A + B*sigma/3.0 + C*sigma*sigma/4.0)/HBARC3;
  }
  // sigma potential U(sigma) for parity doublet model I
  double SigmaPotentialPDM1(double x) {
    return (-x*x*(A/2.0 - B*x*x/4.0 + C*pow4(x)/6.0) - EpsPi*x)/HBARC3;
  }
  // sigma potential U(sigma) for parity doublet model II
  double SigmaPotentialPDM2(double sigma) {
    double x = 0.5*(pow2(sigma/f_pion) - 1.0);
    double ds2 = A*x + B*x*x/2.0 + C*x*x*x/6.0 + D*x*x*x*x/24.0 - EpsPi*(sigma-f_pion);
    return ds2/HBARC3;
  }

  // first derivative with respect to sigma
  double dPotDSigmaRMF(double x) {
    //return scalarDens + A*x + B*x*x + C*pow3(x);
    return x*(A + B*x + C*x*x)/HBARC3;
  }

  double dPotDSigmaPDM1(double x) {
    return (-x*(A - B*x*x + C*pow4(x)) - EpsPi)/HBARC3;
  }
  double dPotDSigmaPDM2(double sigma) {
    double x = 0.5*(pow2(sigma/f_pion) - 1.0);
    //return scalarDens + sigma/f_pion2*(A + B*x + 0.5*C*x*x +1.0/6.0*D*x*x*x) - EpsPi;
    //return -scalarDens - sigma/f_pion2*(A + x*(B + 0.5*C*x +1.0/6.0*D*x*x)) + EpsPi;
    return (sigma/f_pion2*(A + x*(B + 0.5*C*x +1.0/6.0*D*x*x)) - EpsPi)/HBARC3;
  }

  // V_sigma/n_s^2 = SigmaPotentialRMF/dPotDSigmaRMF^2
  double dSigmaRMF2(double x) {
    return HBARC3*(0.5*A + B*x/3.0 + C*x*x/4.0)
                  /pow2(A + B*x + C*x*x);
  }

  // V_sigma/n_s^2 for PDM1 = SigmaPotentialDPM1/dPotDSigmaPDM1^2
  double dSigmaPDM1(double x) {
    return HBARC3*(-x*x*(A/2.0 - B*x*x/4.0 + C*pow4(x)/6.0) - EpsPi*x)/
           pow2(-x*(A - B*x*x + C*pow4(x)) - EpsPi);
  }
  // V_sigma/n_s^2 for PDM2 = SigmaPotentialDPM2/dPotDSigmaPDM2^2
  double dSigmaPDM2(double sigma) {
    double x = 0.5*(pow2(sigma/f_pion) - 1.0);
    return HBARC3*(A*x + B*x*x/2.0 + C*x*x*x/6.0 + D*x*x*x*x/24.0
       	- EpsPi*(sigma-f_pion))/
      pow2(sigma/f_pion2*(A + x*(B + 0.5*C*x +1.0/6.0*D*x*x)) - EpsPi);
  }

  // dsigma/drho_s (GeV fm^3)
  double dSigma(double sigma) {
    return HBARC3/(A - 3*B*sigma*sigma + 5*C*pow4(sigma));
  }
  double dSigmaRMF(double sigma) {
    return -HBARC3/(A + 2*B*sigma + 3*C*sigma*sigma);
  }
  double dSigma2(double sigma) {
    double x = 0.5*(pow2(sigma/f_pion) - 1.0);
    double ds2 = (A + B*x + x*(0.5*C*x +1.0/6.0*D*x*x))/f_pion2
               + pow2(sigma/f_pion2)*( B + x*(C +0.5*D*x));
    return -HBARC3/ds2;
  }

  // function used for bisec method in GeV^4.
  double funcSigma(double x,double scalarDens) {
    return -scalarDens + x*(A - B*x*x + C*pow4(x)) + EpsPi;
  }
  double dfuncSigma(double sigma) {
    return A - 3*B*sigma*sigma + 5*C*pow4(sigma);
  }
  double funcSigma2(double sigma,double scalarDens) {
    double x = 0.5*(pow2(sigma/f_pion) - 1.0);
    //return scalarDens + sigma/f_pion2*(A + B*x + 0.5*C*x*x +1.0/6.0*D*x*x*x) - EpsPi;
    //return -scalarDens - sigma/f_pion2*(A + x*(B + 0.5*C*x +1.0/6.0*D*x*x)) + EpsPi;
    return scalarDens + sigma/f_pion2*(A + x*(B + 0.5*C*x +1.0/6.0*D*x*x)) - EpsPi;
  }
  double dfuncSigma2(double sigma) {
    double x = 0.5*(pow2(sigma/f_pion) - 1.0);
    return (A + B*x + x*(0.5*C*x +1.0/6.0*D*x*x))/f_pion2
               + pow2(sigma/f_pion2)*( B + x*(C +0.5*D*x));
  }

  double funcSigmaRMF(double x,double scalarDens) {
    //return scalarDens + A*x + B*x*x + C*pow3(x);
    return scalarDens + x*(A + B*x + C*x*x);
  }
  double dfuncSigmaRMF(double x) {
    return A + x*(2*B + 3*C*x);
  }

  // effective mass
  double mEff(double sigma, double m0, double a1, double a2, double a) {
    return sqrt(sigma*sigma*a1*a1 + m0*m0) + a*a2*sigma;
  }

  // dM/dsigma
  double dMdSigma(double x, double m0, double a1, double a2, double a) {
    return x*a1*a1/sqrt(x*x*a1*a1+m0*m0) + a*a2;
  }

  // d^2M/dsigma^2
  double d2MdSigma2(double x, double m0, double a1, double a) {
    double tmp=sqrt(x*x*a1*a1+m0*m0);
    return a1*a1/tmp - x*x*a1*a1*a1*a1/(tmp*tmp*tmp);
  }

  // minimum sigma field that corresponds to the minimum mass:dm_+/dsigma=0
  double sigmaMin(double m0, double a1,double a2) {
    return 2*m0*abs(a2)/(2*a1*sqrt(a1*a1-a2*a2));
  }

  // minimum mass of positive parity baryon
  double mEffMin(double m0, double a1,double a2,double a) {
    return mEff(sigmaMin(m0,a1,a2),m0,a1,a2,a);
  }

  double funcGomega(double x, double r2) {
    double tmp=muS0 - x*r2/M_phi2*rho0gev;
    return rho0gev*x*x - M_omega2*tmp*x - C4*tmp*tmp*tmp;
  }

  double sol3(double x) {
    if (x < 1e-8) return 1.0 - 4.*x*x/27.;
    double y=pow(sqrt(1+x*x)-x,1.0/3.0);
    return 3./(2*x)*(1.-y*y)/y;
  }


  // g: coupling constant, n:baryon density in GeV^3
  double Omega(double g,double n) {
    double x=3*sqrt(3*C4)*g*n/(2*M_omega3);
    return sol3(x)*g*n/M_omega2;
  }
  double Omega(double m2,double c,double gn) {
    double x=3*sqrt(3*c)*gn/(2*pow(m2,3.0/2.0));
    return sol3(x)*gn/m2;
  }

  // compute sigma coupling from U=m^* - m_0 + gv*omega.
  double getGSigma(double b,double m0,double sigma,double gv,double u,double omega,double gp,double phi) {
    double tmp=b*(sigma-f_pion) + gv*omega + gp*phi - u;
    double tmp2 = tmp*tmp;
    double fs1=f_pion2-sigma*sigma;
    double fs2=f_pion2+sigma*sigma;
    double d=sqrt(tmp2*(tmp2*fs2*fs2-fs1*fs1*(tmp2-4*m0*m0)));
    double asq1=(tmp2*fs2+d)/(fs1*fs1);
    return sqrt(asq1);
  }

  double cubicEquation(double a0, double a1,double a2,double a3) {
    double b1=-2*a2*a2*a2 + 9*a1*a2*a3 - 27*a0*a3*a3;
    double b4=-a2*a2+3*a1*a3;
    double b3=b1*b1+4*b4*b4*b4;
    double b2=b1+sqrt(b3);
    double b23=std::cbrt(b2);
    double cr2=std::cbrt(2);
    return -a2/(3*a3) + b23/(3*cr2*a3) - cr2*b4/(3*b23*a3);
  }

  // momentum-dependent scalar potential
  double Vmds(double psq1) {
    return vex1/(1.0-psq1/pmu1);
  }
  // momentum-dependent vector potential
  double Vmdv(double psq) {
   return vex2/(1.0-psq/pmu2);
  }

  double devVmds(double psq) {
    return vex1/(1.0 - psq/pmu1);
  }
  double devVmdv(double psq) {
    return vex2/(1.0 - psq/pmu2);
  }
  double devVmes(double psq) {
    double fac1 = 1.0 - psq/pmu1;
    return -vex1/(pmu1*fac1*fac1);
  } 
  double devVmev(double psq) {
    double fac2 = 1.0 - psq/pmu2;
    return -vex2/(pmu2*fac2*fac2);
  } 




  /*
  double funcSigma3(double x,double scalarDens) {
    return -scalarDens + x*(A3 - B3*x*x + C3*pow4(x)) + EpsPi;
  }
  double dSigma3(double sigma) {
    return HBARC3/(A3 - 3*B3*sigma*sigma + 5*C3*pow4(sigma));
  }
  double dfuncSigma3(double x) {
    return A3 - 3*B3*x*x + 5*C3*pow4(x);
  }

  double dSigma3(double sigma) {
    double x = 0.5*(pow2(sigma/f_pion) - 1.0);
    double ds2 = (A3 + B3*x + x*(0.5*C3*x +1.0/6.0*D3*x*x))/f_pion2
               + pow2(sigma/f_pion2)*( B3 + x*(C3 +0.5*D3*x));
    return -HBARC3/ds2;
  }
  double funcSigma3(double sigma,double scalarDens) {
    double x = 0.5*(pow2(sigma/f_pion) - 1.0);
    return scalarDens + sigma/f_pion2*(A3 + x*(B3 + 0.5*C3*x +1.0/6.0*D3*x*x)) - EpsPi;
  }


  // dsigma/drho_s (GeV fm^3)
  double dSigma(double sigma,double a,double b,double c) {
    return HBARC3/(a - 3*b*sigma*sigma + 5*c*pow4(sigma));
  }
  double dSigma2(double sigma,double a,double b,double c) {
    double d=0.0;
    double x = 0.5*(pow2(sigma/f_pion) - 1.0);
    double ds2 = (a + b*x + 0.5*c*x*x +1.0/6.0*d*x*x*x)/f_pion2
               + pow2(sigma/f_pion2)*( b + c*x +0.5*D*x*x);
    return -HBARC3/ds2;
  }
  // function used for bisec method in GeV^4.
  double funcSigma(double x,double scalarDens,double a,double b,double c) {
    //return -scalarDens + A*x - B*x*x*x + C*pow5(x) + EpsPi;
    return -scalarDens + x*(a - b*x*x + c*pow4(x)) + EpsPi;
  }
  double funcSigma2(double sigma,double scalarDens,double a,double b,double c) {
    double d=0.0;
    double x = 0.5*(pow2(sigma/f_pion) - 1.0);
    //return scalarDens + sigma/f_pion2*(A + B*x + 0.5*C*x*x +1.0/6.0*D*x*x*x) - EpsPi;
    return scalarDens + sigma/f_pion2*(a + x*(b + 0.5*c*x +1.0/6.0*d*x*x)) - EpsPi;
  }
  */

};

} // end namespace jam2
#endif
