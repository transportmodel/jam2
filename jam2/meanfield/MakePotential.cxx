#include <jam2/meanfield/MakePotential.h>
#include <jam2/hadrons/JamStdlib.h>

namespace jam2 {

bool MakePotential::firstCall=true;

using namespace std;

MakePotential::MakePotential(Pythia8::Settings* s): settings(s)
{
  eosType=settings->mode("MeanField:EoS");
  transportModel = settings->mode("MeanField:transportModel"); 
  rho0=settings->parm("MeanField:rho0");
  cutOffPot=settings->parm("MeanField:cutOffPotential") * rho0;
  optPotentialType=2;
  C1=0.0; C2=0.0; mu1=1.0; mu2=1.0;
  withMomDep=1;

//------------------ CMF --------------------------------------------------
  // CMF from Jan Steinheimer 2022/9/1
  if(eosType==51) {
    string dataPath=PREFIX;
    //string fname=dataPath+"/"+"CMF_skyrme.dat";
    string fname= dataPath+"/"+settings->word("MeanField:eosFileName");
    readPotentialParam(fname,1.0,0.0);
    withMomDep=0;

  // CMF+MD1 2022/9/4 rho0=0.16 is used for non.rel. MD pot
  // Schroedinger equivalent potential fit.
  } else if(eosType==52) {
    string dataPath=PREFIX;
    //string fname=dataPath+"/"+"CMF_skyrme.dat";
    string fname= dataPath+"/"+settings->word("MeanField:eosFileName");

    // rho0=0.16 BE=-16.MeV
    //C1= -0.1661270741543753;
    //mu1= 3.2621575461030505*HBARC;

    // rho0=0.16 BE=-16.3MeV
    C1= -0.16665950120627837;
    mu1= 3.248020503473469*HBARC;

    readPotentialParam(fname,mu1,C1);

  // CMF+MD2 2022/10/9 experimental Uopt is defined as the difference of the single-particle energy
  // free energy.  (not Schroedinger equivalent potential)
  // See H. Feldmeier and J. Lindner, Z. Phys. A - Hadrons and Nuclei 341, 83-88 (1991)
  // U( Pinf= 1.3 )= 0.03  Einf= 0.665073298386571 Pinf= 1.3
  // U( 0.65 )= 0.0 Elab= 0.20320287416392357
  // BE=-0.0153 rho0=0.16
  } else if(eosType==53) {
    string dataPath=PREFIX;
    //string fname=dataPath+"/"+"CMF_skyrme.dat";
    string fname= dataPath+"/"+settings->word("MeanField:eosFileName");
    C1= -0.14845072399566206;
    mu1= 2.1665240217804356 *HBARC;
    readPotentialParam(fname,mu1,C1);

  // CMF+MD3 2022/9/19 
  } else if(eosType==54) {
    string dataPath=PREFIX;
    //string fname=dataPath+"/"+"CMF_skyrme.dat";
    string fname= dataPath+"/"+settings->word("MeanField:eosFileName");

    // Giessen
    C1    = -2*63.6e-3;
    mu1   = 2.13*HBARC; 
    readPotentialParam(fname,mu1,C1);

  } else {
    cout << "MakePotential:EoStype not implemented " << eosType << endl;
    exit(1);
  }
}

/*
// make momentum-dependent potential for the VDF potential.
void MakePotential::makeMDPotVDF(double mu1, double c1)
{
  useTable=true;
  double rmax=30.0;
  double dn=rmax/600;
  rhoMin=1e-5;
  for(int i=0; i<600;i++) {
    double n=(rhoMin+dn*i)*rho0;
    double ncut = min(n,cutOffPot);

    //auto [vm,dvm] =  dVmdn(n, mu1, c1);
    pair<double,double> a = dVmdn(n*rho0, mu1, c1);
    double vm = a.first;
    double dvm = a.second;

    double v=vVDF(ncut)-vm;
    double dv=dvVDF(ncut)-dvm;
    Vpot.push_back(v);
    dVdn.push_back(dv);
    //cout << n/rho0 << " V= "<< v*1e+3 << " dV= " << dv*1e+3*rho0 << " vm= "<< vm*1e+3 <<endl;
  }
  rhoMax=rmax*rho0;
  dRho=dn*rho0;
  maxN=Vpot.size();
}
*/

// MD single-particle potential divided by 4pi
double MakePotential::singleParticlePotMD(double p, double pf, double lambda)
{
  if (p==0.0) return lambda*lambda*(pf-lambda*atan(pf/lambda));
  double x=p/lambda;
  double y=pf/lambda;
  return 1.0/4.0*pow3(lambda)*( 2*y + 0.5/x*(y*y - x*x +1.0)*log((pow2(x+y)+1)/(pow2(x-y)+1))
        - 2*(atan(x+y) - atan(x-y)) );
}

// divided by (4pi)^2
double MakePotential::potMomdepT0(double pf, double lam)
{
  double x=pf/lam;
  return 2.0/3.0*pow4(pf)*lam*lam*(3.0/8.0 - 0.5/x*atan(2*x)
      -1.0/(16.0*x*x) + (3.0/(16.0*x*x)+1.0/(64.0*pow4(x)))*log(1.0+4*x*x));
}

double MakePotential::dVmdpf(double pf, double lam)
{
  double x=pf/lam;
  double x2=x*x;
  double x3=x2*x;
  double x4=x3*x;

  /*
  double A=2.0/3.0*pow4(pf)*lam*lam;
  double dA=8.0/3.0*pow3(pf)*lam*lam;
  double a= -0.5/x*atan(2*x);
  double b= -1.0/(16.0*x2);
  double c= 3.0/(16.0*x2)+1.0/(64.0*x4);
  double d = log(1.0+4*x2);
  double da = -a/x - 1.0/(x + 4*x3);
  double db = -b*2/x;
  double dc = -3.0/(8.0*x3) - 1.0/(16*x4*x);
  double dd = 8*x/(1.0+4*x2);
  double B=(3.0/8.0 + a + b + c*d);
  double dB= da + db + dc*d + c*dd;
  return A*dB/lam + dA*B;
  */

  double b=3.0/(16.0*x2)+1.0/(64.0*x4);
  double c=log(1.0+4*x2);
  double db = -3.0/(8.0*x3) - 1.0/(16*x4*x);
  double dc=8.0*x/(1.0+4*x2);
  double A = 3.0/8.0 - 0.5/x*atan(2*x)-1.0/(16.0*x2) + b*c;
  double dA = atan(2*x)/(2*x2)-1.0/(x*(1.0+4*x2)) + 1.0/(8*x3) + db*c + b*dc;
  return 8.0/3.0*pow3(pf)*lam*lam*A + 2.0/3.0*pow4(pf)*lam*lam * dA/lam;
}

// Derivative of the momentum-dependent potential per baryon density.
pair<double,double> MakePotential::dVmdn(double rho, double lam, double c)
{
  if(c==0.0) return {0.0,0.0};
  double d=4/(2*M_PI*M_PI*HBARC*HBARC*HBARC);
  double pf=pow(3/d*rho,1.0/3.0);

  // the momentum-dependent potential per baryon density
  double deg=d*d*c/(rho0*2);
  double MDPot=deg*potMomdepT0(pf,lam)/rho;

  // derivative of the momentum-dependent potential per baryon density
  //double dMDPot = deg*pf/(3*rho)*dVmdpf(pf,lam)/rho - MDPot/rho;
  double dMDPot = d*c/rho0*singleParticlePotMD(pf,pf,lam)/rho - MDPot/rho;

  return {MDPot,dMDPot};
}

void MakePotential::readPotentialParam(string fname,double mu,double c1)
{
  ifstream in(fname.c_str(), ios::in);
  if(!in) {
    cout << "MakePotential::readPotentialParam fail to open file "<< fname<<endl;
    exit(1);
  }

  // read the values of the potential and its derivative from the file.
  useTable=true;
  double nsave=0.0, dn=0.0;
  string templine;
  rhoMin=-1.0;
  while(getline(in,templine)) {
    if(templine.find('#')<0) continue;
    istringstream is(templine);

    //  n [n_0] V [MeV]  dV/dn*n_0 [MeV]
    double n,v,dvdn;
    is >> n >> v >> dvdn;
    if(n==0.0) continue;
    if(rhoMin<0.0) rhoMin=n*rho0;
    Vpot.push_back(v*1e-3);
    dVdn.push_back(dvdn*1e-3/rho0);
    dn = n-nsave;
    nsave=n;
  }
  in.close();
  rhoMax=nsave*rho0;
  dRho=dn*rho0;
  maxN=Vpot.size();

  // Add phase transition.
  if(settings->flag("MeanField:CMF-PT")) makePT();

  ofstream ofs;
  bool outputCMF = settings->flag("MeanField:outputCMF");
  if(outputCMF) ofs.open("cmf.out");
  if(!outputCMF && c1==0) return;

  // momentum-dependent potential.
  for(int i=0;i<maxN;i++) {
    double n = rhoMin + i*dRho;
    //auto [vm,dvm] =  dVmdn(n*rho0, mu, c1);
    pair<double,double> a = dVmdn(n, mu, c1);
    double vm = a.first;
    double dvm = a.second;
    //double V=v*1e-3-vm;
    //double dV=dvdn*1e-3/rho0-dvm;
    Vpot[i] -= vm;
    dVdn[i] -= dvm;

    if(outputCMF)
    ofs << n/rho0 << "  " << Vpot[i] << " " << dVdn[i] << " "<< vm << " " << dvm <<endl;

  }

  if(outputCMF) ofs.close();

}

// make a phase transition between the density n0 and n0+dn.
void MakePotential::makePT()
{
  double n0 = settings->parm("MeanField:rhoCutPT") * rho0;
  double dn  = settings->parm("MeanField:rhoShiftPT") * rho0;
  double dv1 = settings->parm("MeanField:VShiftPT") * 1e-3; // MeV -> GeV

  // copy original potential.
  std::vector<double> vpot=Vpot, dvdn=dVdn;

  double n1 = n0 + dn;
  double v0=getV(n0);
  double dv0=getdVdn(n0);
  //cout << " n0= "<< n0 << " v0= "<< v0 << " dv0= "<< dv0 <<endl;
  for(int i=0;i<maxN;i++) {
    double n=rhoMin + i*dRho;
    if(n<=n0){
    } else if (n>=n1) {
      vpot[i]=getV(n-dn) + dv1;
      dvdn[i]=getdVdn(n-dn);
    } else {
      //vpot[i] = v0 + dv0*(n-n0) - 3*dv0/dn*(n-n0)*(n-n0) + 2*dv0/(dn*dn)*pow3(n-n0);
      //dvdn[i] = dv0 - 6*dv0/dn*(n-n0) + 6*dv0/(dn*dn)*(n-n0)*(n-n0);
      double vv=dv1 - dv0*dn;
      vpot[i] = v0 + dv0*(n-n0) + 3/(dn*dn)*vv*pow2(n-n0) - 2/pow3(dn)*vv*pow3(n-n0);
      dvdn[i] = dv0 + 6/(dn*dn)*vv*(n-n0) - 6/pow3(dn)*vv*pow2(n-n0);

    }
  }
  Vpot=vpot;
  dVdn=dvdn;
}

} // namespace jam2
