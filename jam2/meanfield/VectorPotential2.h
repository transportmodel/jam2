#ifndef jam2_meanfield_VectorPotential2_h
#define jam2_meanfield_VectorPotential2_h

#include <jam2/hadrons/JamStdlib.h>
#include <Pythia8/Settings.h>
#include <jam2/collision/EventParticle.h>
//#include <jam2/meanfield/PotentialType.h>

namespace jam2 {

class VectorPotential2
{
private:
  Pythia8::Settings* settings;
  std::vector<double> bFac, cFac, tFac, pFac, sFac;
  int optPotentialEval;
  double rho0,cutOffPot;
  double vex1=0.0,vex2=0.0,pmu1=1.0,pmu2=1.0;
  double uex1=0.0,uex2=0.0;
  double C1=0.0, C2=0.0, mu1=1.0, mu2=1.0;
  int nV=4;
  bool withMomDep;
  int optVectorPotential,eosType,transportModel,optPotentialType;
  std::vector<double> Vpot={}, dVdn={}, Upot={};
  std::vector<double> dVdnV={},VpotR={};
  //std::array<double,601> Vpot={}, dVdn={};
  double rhoMax=0.0,dRho=0.0,rhoMin=0.0;
  int maxN=0;
  static bool firstCall;
  int useTable=0;
  double compK;
public:
  VectorPotential2(Pythia8::Settings* s);
  ~VectorPotential2() { };
  int  nv() const {return nV;}
  bool isMomDep() const {return withMomDep;}
  double potcut() const {return cutOffPot;}
  void setPotentialParam(EventParticle* p);
  void setLambdaPotential(double* fp);
  void setSigmaPotential(double* fp);
  std::vector<double> bfac() {return bFac;}
  std::vector<double> cfac() {return cFac;}
  std::vector<double> tfac() {return tFac;}
  std::vector<double> sfac() {return sFac;}
  std::vector<double> pfac() {return pFac;}
  void readPotentialParam(std::string fn,double mu1,double c1);
  void makePTMDPotSKM(int eostype,double mu1, double c1);
  void makeMDPotVDF(double mu1, double c1);
  void makePT();
  int isPotTable() const {return useTable;}
  double getRhoMin() const {return rhoMin;}
  void setMDParam(int opt);
  void setMDParam();

  // VDF single-particle potential
  double uVDF(double n) {
    double u=0.0;
    for(int j=0;j<nV;j++)
     u += pFac[j]*sFac[j]*pow(n,bFac[j]);
    return u;
  }

  // VDF one-particle potential
  double vVDF(double n) {
    double v=0.0;
    for(int j=0;j<nV;j++)
     v += pFac[j]*tFac[j]*pow(n,bFac[j]);
    return v;
  }

  double vrVDF(double n) {
    double v=0.0;
    for(int j=0;j<nV;j++)
     v += pFac[j]*tFac[j]*pow(n,bFac[j]-1);
    return v;
  }

  // the derivative of the VDF one-particle potential
  double dvVDF(double n) {
    double dv=0.0;
    for(int j=0;j<nV;j++)
     dv += pFac[j]*tFac[j]*bFac[j]*pow(n,bFac[j]-1);
    return dv;
  }

  // dV/drho - V/rho for VDF one-particle potential
  double dvvVDF(double n) {
    double dv=0.0;
    for(int j=0;j<nV;j++)
     dv += pFac[j]*tFac[j]*(bFac[j]-1)*pow(n,bFac[j]-1);
    return dv;
  }

  // Skyrme single-particle potential
  double uSKM(double n,int i) {
    return pFac[i]*sFac[i]*pow(n,bFac[i]) + pFac[i+1]*sFac[i+1]*pow(n,bFac[i+1]);
  }
  // Skyrme one-particle potential
  double vSKM(double n,int i) {
    return pFac[i]*tFac[i]*pow(n,bFac[i]) + pFac[i+1]*tFac[i+1]*pow(n,bFac[i+1]);
  }

  // derivative of the Skyrme one-particle potential
  double dvSKM(double n,int i) {
    return pFac[i]*tFac[i]*bFac[i]*pow(n,bFac[i]-1)
          +pFac[i+1]*tFac[i+1]*bFac[i+1]*pow(n,bFac[i+1]-1);
  }

  // Skyrme one-particle potential  del(V) - V/rho
  double dvvSKM(double n,int i) {
    return  pFac[i+1]*tFac[i+1]*(bFac[i+1]-1)*pow(n,bFac[i+1]-1);
  }

  // Skyrme one-particle potential V/rho
  double vrSKM(double n,int i) {
    return pFac[i]*tFac[i]*pow(n,bFac[i]-1) + pFac[i+1]*tFac[i+1]*pow(n,bFac[i+1]-1);
  }

  double Vrho(std::vector<double>& rhog, double rho) {
    if(useTable) {
      return getV(rho)/rho;
    } else {
      double vv=0.0;
      for(int j=0;j<nV;j++)
       vv += pFac[j]*tFac[j]*rhog[j];  // V/rho_B
      return vv;
    }
  }
  double delV(std::vector<double>& rhog, double rho) {
    if(useTable) {
      return (rho*getdVdn(rho)-getV(rho))/(rho*rho*rho);
    } else {
      double dv=0.0;
      for(int j=0;j<nV;j++)
      dv += (bFac[j]-1.0)*pFac[j]*tFac[j]*rhog[j]/(rho*rho);  // del(V/rho)/rho
      return dv;
    }
  }

  // n in the unit of 1/fm^3
  double getU(double n) {
    //if(n<0.014) return 0.0;
    int i=min(max(0,int((n-rhoMin)/dRho)),maxN-2);
    double x=(n-(rhoMin+i*dRho))/dRho;
    return Upot[i]*(1.0-x) + Upot[i+1]*x;
  }

  // n in the unit of 1/fm^3
  double getV(double n) {
    //if(n<0.014) return 0.0;
    int i=min(max(0,int((n-rhoMin)/dRho)),maxN-2);
    double x=(n-(rhoMin+i*dRho))/dRho;
    return Vpot[i]*(1.0-x) + Vpot[i+1]*x;
  }
  // n in the unit of 1/fm^3
  double getdVdn(double n) {
    //if(n<0.014) return 0.0;
    int i=min(max(0,int((n-rhoMin)/dRho)),maxN-2);
    double x=(n-(rhoMin+i*dRho))/dRho;
    return dVdn[i]*(1.0-x) + dVdn[i+1]*x;
  }

  double getdVdnV(double n) {
    //if(n<0.014) return 0.0;
    int i=min(max(0,int((n-rhoMin)/dRho)),maxN-2);
    double x=(n-(rhoMin+i*dRho))/dRho;
    return dVdnV[i]*(1.0-x) + dVdnV[i+1]*x;
  }

  double getVpotR(double n) {
    //if(n<0.014) return 0.0;
    int i=min(max(0,int((n-rhoMin)/dRho)),maxN-2);
    double x=(n-(rhoMin+i*dRho))/dRho;
    return VpotR[i]*(1.0-x) + VpotR[i+1]*x;
  }

  double singleParticlePotMD(double p, double pf, double lambda);
  double potMomdepT0(double pf, double lam);
  double dVmdpf(double pf, double lam);
  std::pair<double,double> dVmdn(double rho, double lam, double c);
  double uMD(double rho, double lam, double c);

  // Momentum-dependent single-particle potential
  double Umd(double psq1,double vfac1,double vfac2,double pfi1,double pfi2,double pfj1,double pfj2) {
    return vfac1*uex1/(1.0-psq1/(pfi1*pmu1)) + vfac2*uex2/(1.0-psq1/(pfi2*pmu2));
  }

  // Momentum-dependent one-particle potential
  double Vmd(double psq1,double vfac1,double vfac2,double pfi1,double pfi2,double pfj1,double pfj2) {
    return vfac1*vex1/(1.0-psq1/(pfi1*pmu1)) + vfac2*vex2/(1.0-psq1/(pfi2*pmu2));
  }

  double devVmd2(double psq,double vf1,double vf2, double pf1,double pf2) {
    return vf1*vex1/(1.0 - psq/(pf1*pmu1)) + vf2*vex2/(1.0 - psq/(pf2*pmu2));
  }
  double devVme2(double psq,double vf1,double vf2,double pf1,double pf2) {
    double fac1 = 1.0 - psq/(pf1*pmu1);
    double fac2 = 1.0 - psq/(pf2*pmu2);
    return -vf1*vex1/(pmu1*fac1*fac1) - vf2*vex2/(pmu2*fac2*fac2);
  }

  // kinetic part of pressure at T=0 in GeV/fm^3
  double Pkin(double pf,double m) {
    double ef=sqrt(pf*pf+m*m);
    double pmin = 2.0/3.0*ef*pf*pf*pf - m*m*ef*pf + m*m*m*m*log((ef+pf)/m);
    return 4./(16.*M_PI*M_PI)*pmin/HBARC3;
  }

  // K:incompressibility in GeV, be: binding energy in GeV, mnucl: nucleon mass in GeV
  std::tuple<double,double,double> SkyrmeParam(double K,double be,double mnucl) {
    double d=4/(2*M_PI*M_PI);
    double pf=pow(3/d*rho0,1.0/3.0)*HBARC;
    double pk=Pkin(pf,mnucl)/rho0;
    double ef=sqrt(pf*pf+mnucl*mnucl);
    double C= K/9 - pf*pf/(3*ef);
    double A= be + mnucl - ef;
    double gamma=-(C + 2*pk)/(A + 2*pk);
    double beta= ( C - A)/(gamma-1);
    double alpha= A - beta;
    return {alpha,beta,gamma};
  }

};

} // end namespace jam2
#endif
