#include <jam2/meanfield/VectorPotential2.h>

namespace jam2 {

bool VectorPotential2::firstCall=true;

using namespace std;

VectorPotential2::VectorPotential2(Pythia8::Settings* s): settings(s)
{
  //optP0dev=settings->mode("MeanField:optP0dev");
  //optPV=settings->mode("MeanField:optMomPotential");
  //optScalarDensity=settings->mode("MeanField:optScalarDensity");
  //optTwoBodyDistance=settings->mode("MeanField:twoBodyDistance");
  eosType=settings->mode("MeanField:EoS");
  //optBaryonCurrent = settings->mode("MeanField:optBaryonCurrent"); 
  optVectorPotential=settings->mode("MeanField:optVectorPotential");
  transportModel = settings->mode("MeanField:transportModel"); 
  rho0=settings->parm("MeanField:rho0");
  cutOffPot=settings->parm("MeanField:cutOffPotential") * rho0;
  compK=settings->parm("MeanField:incompressibility") * 1e-3;
  optPotentialEval = settings->mode("MeanField:optPotentialEvaluation");

  optPotentialType=2;
  withMomDep=1;

  if(eosType==1) {     // Skyrme Hard Nara2019 K=380 
    nV=2;
    cFac.resize(nV);
    bFac.resize(nV);
    nV=2;
    cFac[0] = -0.124838736062805;
    cFac[1] =  0.0707373967100532;
    bFac[0] =  1.0;
    bFac[1] =  2.00261915156202;
    withMomDep=0;

  } else if(eosType==2) { // Skyrme Soft Nara2019 K=210
    nV=2;
    cFac.resize(nV);
    bFac.resize(nV);
    cFac[0] = -0.310514700691548;
    cFac[1] =  0.256413361338797;
    bFac[0] =  1.0;
    bFac[1] =  1.20292928384297;
    withMomDep=0;

  } else if(eosType==3 || eosType==4 || eosType==5) { // Skyrme parameters from incompressibility K
    double Mnucl=0.938;
    double BE=-0.016;
    std::tuple<double,double,double> t1=SkyrmeParam(compK,BE,Mnucl);
    nV=2;
    cFac.resize(nV);
    bFac.resize(nV);
    cFac[0]=std::get<0>(t1);
    cFac[1]=std::get<1>(t1);
    bFac[0]=1.0;
    bFac[1]=std::get<2>(t1);
    withMomDep=0;

  } else if(eosType==6 || eosType==7 || eosType==8) { // Skyrme parameters from two incompressibility K
    double Mnucl=0.938;
    double BE=-0.016;
    std::tuple<double,double,double> t1=SkyrmeParam(0.38,BE,Mnucl); // K=380 MeV
    std::tuple<double,double,double> t2=SkyrmeParam(0.21,BE,Mnucl); // K=210 MeV
    nV=4;
    cFac.resize(nV);
    bFac.resize(nV);
    cFac[0]=std::get<0>(t1);
    cFac[1]=std::get<1>(t1);
    bFac[0]=1.0;
    bFac[1]=std::get<2>(t1);

    cFac[2]=std::get<0>(t2);
    cFac[3]=std::get<1>(t2);
    bFac[2]=1.0;
    bFac[3]=std::get<2>(t2);
    withMomDep=0;

  // optical potential is defined by sqrt{(m_N+S)^2+p^2} - sqrt{m_N^2 + p^2}
  } else if(eosType==11) {  // MH2 Skyrme Nara2021 K=380
    // U( Pinf= 1.7 )= 0.06  Einf= 1.0036086114353737 Pinf= 1.7
    // U( 0.65 )= 0.0 Elab= 0.20320287416392357
    nV=2;
    cFac.resize(nV);
    bFac.resize(nV);
    cFac[0] = -0.013119515535259911;
    cFac[1] = 0.08885442751779084;
    bFac[0] = 1.0;
    bFac[1] = 1.674140687543709;
    C1 = -0.3989613178044121;
    C2= 0.36728513480692454;
    mu1 = 2.02*HBARC;
    mu2= 1.0*HBARC;
    setMDParam();

  } else if(eosType==12) {  // MS2 Skyrme Nara2021 K=210
    // U( Pinf= 1.7 )= 0.06  Einf= 1.0036086114353737 Pinf= 1.7
    // U( 0.65 )= 0.0 Elab= 0.20320287416392357
    nV=2;
    cFac.resize(nV);
    bFac.resize(nV);
    cFac[0] = -0.5157475041588349;
    cFac[1] = 0.5906455475692723;
    bFac[0] = 1.0;
    bFac[1] = 1.0708570434690778;
    C1 = -0.3989613178044121;
    C2= 0.36728513480692454;
    mu1 = 2.02*HBARC;
    mu2= 1.0*HBARC;
    setMDParam();

  } else if(eosType==13) {  // MH1 Skyrme Nara2021 K=380
    // U( Pinf= 1.7 )= 0.06  Einf= 1.0036086114353737 Pinf= 1.7
    // U( 0.65 )= 0.0 Elab= 0.20320287416392357
    nV=2;
    cFac.resize(nV);
    bFac.resize(nV);
    cFac[0] = 0.03894524543017354;
    cFac[1] = 0.04170816920028206;
    bFac[0] = 1.0;
    bFac[1] = 2.2732990944321685;
    C1 = -0.16975960664857545;
    mu1 = 3.2272077792680443*HBARC;
    setMDParam();

  } else if(eosType==14) {  // MS1 Skyrme Nara2021 K=210
    // U( Pinf= 1.7 )= 0.06  Einf= 1.0036086114353737 Pinf= 1.7
    // U( 0.65 )= 0.0 Elab= 0.20320287416392357
    nV=2;
    cFac.resize(nV);
    bFac.resize(nV);
    cFac[0] = -0.23308866850927748;
    cFac[1] =  0.31374208313973306;
    bFac[0] = 1.0;
    bFac[1] = 1.1090643781089917;
    C1 = -0.16975960664857545;
    mu1 = 3.2272077792680443*HBARC;
    setMDParam();

  } else if(eosType==15) {  // Nara 2022/10/9 K=380MeV Uopt fit (not Usep)
    nV=2;
    cFac.resize(nV);
    bFac.resize(nV);
    cFac[0] = 0.012770230973753667;
    cFac[1] = 0.03169261559586007;
    bFac[0] = 1.0;
    bFac[1] = 2.5516181617195963;
    C1 = -0.15365423270898002;
    mu1 = 2.138132222773216*HBARC;
    setMDParam();

  } else if(eosType==16) {  // Nara 2022/10/9 K=210MeV Uopt fit (not Usep)
    nV=2;
    cFac.resize(nV);
    bFac.resize(nV);
    cFac[0] = -0.11716443480629699;
    cFac[1] = 0.16162728137591073;
    bFac[0] = 1.0;
    bFac[1] = 1.1873814173216382;
    C1 = -0.15365423270898002;
    mu1 = 2.138132222773216*HBARC;
    setMDParam();

//--------------Vector Density Functional (VDF) model --------------------------------------------------
  } else if(eosType==21 || eosType==24 || eosType==27 || eosType==30) {  // VDF1  IV

    double b[]={1.7681391, 3.5293515, 5.4352788, 6.3809822};
    double c[]={-8.450948e+1, 3.843139e+1, -7.958557, 1.552594};
    cFac.resize(nV);
    bFac.resize(nV);
    for(int i=0;i<nV;i++) {
      bFac[i]= b[i]-1.0;
      cFac[i]=c[i]*1e-3; // GeV
    }
    withMomDep=0;
    //cutOffPot=6.0;

  } else if(eosType==22 || eosType==25 || eosType==28 || eosType==31) {  // VDF2
    double b[]={1.8025297, 3.0777209, 6.4303869, 11.4003161};
    double c[]={-9.1843484e+1, 3.9574869e+1, -2.1547320e-1, 4.5187616e-5};
    cFac.resize(nV);
    bFac.resize(nV);
    for(int i=0;i<nV;i++) {
      bFac[i]= b[i]-1.0;
      cFac[i]=c[i]*1e-3;
    }
    withMomDep=0;
    //cutOffPot=6.0;

  } else if(eosType==23 || eosType==26 || eosType==29 || eosType==32) {  // VDF3
    double b[]={2.2138613, 2.5261557, 5.3081105,7.4901532};
    double c[]={-3.130530e+2, 2.611963e+2, -6.317680e-1, 4.450564e-3};
    cFac.resize(nV);
    bFac.resize(nV);
    for(int i=0;i<nV;i++) {
      bFac[i]= b[i]-1.0;
      cFac[i]=c[i]*1e-3;
    }
    withMomDep=0;
    //cutOffPot=10.0;

//------------------ RMF  --------------------------------------------------
  // RMF (CMF from Jan Steinheimer 2022/9/1)
  } else if(eosType==51) {
    string dataPath=PREFIX;
    string fname= dataPath+"/"+settings->word("MeanField:eosFileName");
    readPotentialParam(fname,1.0,0.0);
    withMomDep=0;

  // RMF+MD1 2022/9/4 rho0=0.16 is used for non.rel. MD pot
  // Schroedinger equivalent potential fit.
  } else if(eosType==52) {
    string dataPath=PREFIX;
    string fname= dataPath+"/"+settings->word("MeanField:eosFileName");

    if(rho0==0.168) setMDParam(1);
    else setMDParam(3);
    readPotentialParam(fname,mu1,C1);

  // RMF+MD2 2022/10/9 experimental Uopt is defined as the difference of the single-particle energy
  // free energy.  (not Schroedinger equivalent potential)
  // See H. Feldmeier and J. Lindner, Z. Phys. A - Hadrons and Nuclei 341, 83-88 (1991)
  // U( Pinf= 1.3 )= 0.03  Einf= 0.665073298386571 Pinf= 1.3
  // U( 0.65 )= 0.0 Elab= 0.20320287416392357
  // BE=-0.0153 rho0=0.16
  } else if(eosType==53) {
    string dataPath=PREFIX;
    string fname= dataPath+"/"+settings->word("MeanField:eosFileName");
    if(rho0==0.168) setMDParam(3);
    else setMDParam(4);
    readPotentialParam(fname,mu1,C1);

  // RMF+MD3 2022/9/19 
  } else if(eosType==54) {
    string dataPath=PREFIX;
    string fname= dataPath+"/"+settings->word("MeanField:eosFileName");
    setMDParam(7);

  } else {
    cout << "VectorPotential2:EoStype not implemented " << eosType << endl;
    exit(1);
  }

  // currently all baryons feel the same potential.
  pFac.assign(nV,1.0);

  // Set potential parameters, if it is defined by the polynomial function.
  // (RMF potential is provided by the numerical table.)
  tFac.resize(nV);
  sFac.resize(nV);
  if(eosType<=50) {
    // QMD simulation
    if(transportModel==1) {
      for(int i=0;i<nV;i++) tFac[i]=cFac[i]/(bFac[i]+1.0)/pow(rho0,bFac[i]);
      if(optPotentialEval<2) {
         for(int i=0;i<nV;i++) sFac[i]=tFac[i];
      } else {
         for(int i=0;i<nV;i++) sFac[i]=cFac[i]/pow(rho0,bFac[i]);
      }
    // BUU simulation
    } else {
      for(int i=0;i<nV;i++) {
	tFac[i]=cFac[i]/pow(rho0,bFac[i]);
	sFac[i]=tFac[i];
      }
    }
  }

  // make phase transition and Skyrme-MD potential rho0=0.168 B=-16MeV
  if(eosType==3 || eosType==6) { // Skyrme without MD
    makePTMDPotSKM(eosType,1.0, 0.0);

  } else if(eosType==4 || eosType==7) { // Skyrme + MD1
    withMomDep=1;
    if(rho0==0.168) setMDParam(1);
    else setMDParam(2);
    makePTMDPotSKM(eosType,mu1, C1);

  } else if(eosType==5 || eosType==8) { // Skyrme + MD2
    withMomDep=1;
    if(rho0==0.168) setMDParam(3);
    else setMDParam(4);
    makePTMDPotSKM(eosType,mu1, C1);

  // make VDF-MD potential
  } else if(eosType>=24 && eosType<=32) {
    withMomDep=1;

    // MD1
    if(eosType>=24 && eosType<=26) {
      setMDParam(2);

    // MD2
    } else if(eosType>=27 && eosType<=29) {
      setMDParam(4);

    // MD3 Giessen 
    } else {
      setMDParam(7);
    }
    makeMDPotVDF(mu1,C1);
  }


  if(firstCall) {
  cout << "# RQMDvec mode eosType= "<< eosType  << " rho0= " << rho0 <<endl;
  if(eosType<30) {
    for(int i=0;i<nV;i++)
    cout << "b= "<< bFac[i] << " c= " << cFac[i] << " t= " << tFac[i]<<endl;
  }
    firstCall=false;
  }
}

// make PT and momentum-dependent potential for the Skyrme potential.
void VectorPotential2::makePTMDPotSKM(int eostype,double lam1, double c1)
{
  useTable=1;
  double rmax=30.0;
  double dn=rmax/600;
  rhoMin=1e-8;
  //rhoMin=1e-5;
  rhoMax=rmax*rho0;
  dRho=dn*rho0;

  if(nV==2) {

    // make table for the potential and its derivative.
    for(int i=0; i<600;i++) {
      double n=(rhoMin+dn*i)*rho0;
      Upot.push_back(uSKM(n,0));
      Vpot.push_back(vSKM(n,0));
      dVdn.push_back(dvSKM(n,0));
      dVdnV.push_back(dvvSKM(n,0));
      VpotR.push_back(vrSKM(n,0));
    }

  // Hard + Soft
  } else {

    // Find the difference of the potential between hard and soft EoS at the transition point.
    double vdiff=0.0, udiff=0.0;
    double ncut = settings->parm("MeanField:rhoCutPT") * rho0;
    for(int i=0; i<600;i++) {
      double n=(rhoMin+dn*i)*rho0;
      if(n > ncut) {
        double n1=(rhoMin+dn*(i-1))*rho0;
        vdiff = vSKM(n1,0)-vSKM(n1,2);
        udiff = uSKM(n1,0)-uSKM(n1,2);
        break;
      }
    }

    // make table for the potential and its derivative.
    for(int i=0; i<600;i++) {
      double n=(rhoMin+dn*i)*rho0;
      //int j= n < ncut ? 0 : 2;
      int j= 0;
      double ud=0,vd=0;
      if(n > ncut) {
	ud=udiff;
	vd=vdiff;
	j=2;
      }
      Upot.push_back(uSKM(n,j) + ud);
      Vpot.push_back(vSKM(n,j) + vd);
      dVdn.push_back(dvSKM(n,j));
      dVdnV.push_back(dvvSKM(n,j) - vd/n*rho0);
      VpotR.push_back(vrSKM(n,j) - vd/n*rho0);
    }

  }

  maxN=Vpot.size();

  // Add phase transition.
  if(settings->flag("MeanField:PhaseTransition")) makePT();

  ofstream ofs;
  bool outputRMF = settings->flag("MeanField:outputPotential");
  if(outputRMF) {
    ofs.open("skm.out");
    ofs << "# n [rho0]    V(rho)  dVdn(rho)   V_m   dV_m  U(rho)" <<endl;
  }
  if(!outputRMF && c1==0) return;

  // include momentum-dependent potential.
  for(int i=0;i<maxN;i++) {
    double n = rhoMin + i*dRho;
    pair<double,double> vm = dVmdn(n, lam1, c1);
    Upot[i] -= uMD(n, lam1,c1);
    Vpot[i] -= vm.first;
    dVdn[i] -= vm.second;
    dVdnV[i] -= (vm.second - vm.first/n);
    VpotR[i] -= vm.first/n;

    if(outputRMF)
    ofs << n/rho0 << "  " << Vpot[i] << " " << dVdn[i]
      << " "<< vm.first << " " << vm.second << "  " << Upot[i] <<endl;

  }

  if(outputRMF) ofs.close();

}

// make momentum-dependent potential for the VDF potential.
void VectorPotential2::makeMDPotVDF(double lam1, double c1)
{
  useTable=2;
  double rmax=30.0;
  double dn=rmax/600;
  rhoMin=1e-8;
  for(int i=0; i<600;i++) {
    double n=(rhoMin+dn*i)*rho0;
    double ncut = min(n,cutOffPot);

    //auto [vm,dvm] =  dVmdn(n, lam1, c1);
    pair<double,double> a = dVmdn(n*rho0, lam1, c1);
    double vm = a.first;
    double dvm = a.second;

    double v=vVDF(ncut)-vm;
    double dv=dvVDF(ncut)-dvm;
    //Upot[i] -= uMD(n*rho0, lam1,c1);
    Upot.push_back(uVDF(n*rho0)-uMD(n*rho0, lam1,c1));
    Vpot.push_back(v);
    dVdn.push_back(dv);
    dVdnV.push_back(dv - v/n*rho0);
    VpotR.push_back(v/n*rho0);
    //cout << n/rho0 << " V= "<< v*1e+3 << " dV= " << dv*1e+3*rho0 << " vm= "<< vm*1e+3 <<endl;
  }
  rhoMax=rmax*rho0;
  dRho=dn*rho0;
  maxN=Vpot.size();
}

// MD single-particle potential at p_F
double VectorPotential2::uMD(double rho, double lam, double c)
{
  if(rho==0) return 0.0;
  double pf=pow(1.5*M_PI*M_PI*rho,1.0/3.0)*HBARC;
  double x=2*pf/lam;
  return c/rho0*rho*6/(x*x*x)*(x + log(x*x+1.0)/x - 2*atan(x));
  //return d*c/rho0*singleParticlePotMD(pf,pf,lam);
}

// MD single-particle potential divided by 4pi
double VectorPotential2::singleParticlePotMD(double p, double pf, double lambda)
{
  if (p==0.0) return lambda*lambda*(pf-lambda*atan(pf/lambda));
  double x=p/lambda;
  double y=pf/lambda;
  return 1.0/4.0*pow3(lambda)*( 2*y + 0.5/x*(y*y - x*x +1.0)*log((pow2(x+y)+1)/(pow2(x-y)+1))
        - 2*(atan(x+y) - atan(x-y)) );
}

// divided by (4pi)^2
double VectorPotential2::potMomdepT0(double pf, double lam)
{
  double x=pf/lam;
  return 2.0/3.0*pow4(pf)*lam*lam*(3.0/8.0 - 0.5/x*atan(2*x)
      -1.0/(16.0*x*x) + (3.0/(16.0*x*x)+1.0/(64.0*pow4(x)))*log(1.0+4*x*x));
}

double VectorPotential2::dVmdpf(double pf, double lam)
{
  double x=pf/lam;
  double x2=x*x;
  double x3=x2*x;
  double x4=x3*x;

  /*
  double A=2.0/3.0*pow4(pf)*lam*lam;
  double dA=8.0/3.0*pow3(pf)*lam*lam;
  double a= -0.5/x*atan(2*x);
  double b= -1.0/(16.0*x2);
  double c= 3.0/(16.0*x2)+1.0/(64.0*x4);
  double d = log(1.0+4*x2);
  double da = -a/x - 1.0/(x + 4*x3);
  double db = -b*2/x;
  double dc = -3.0/(8.0*x3) - 1.0/(16*x4*x);
  double dd = 8*x/(1.0+4*x2);
  double B=(3.0/8.0 + a + b + c*d);
  double dB= da + db + dc*d + c*dd;
  return A*dB/lam + dA*B;
  */

  double b=3.0/(16.0*x2)+1.0/(64.0*x4);
  double c=log(1.0+4*x2);
  double db = -3.0/(8.0*x3) - 1.0/(16*x4*x);
  double dc=8.0*x/(1.0+4*x2);
  double A = 3.0/8.0 - 0.5/x*atan(2*x)-1.0/(16.0*x2) + b*c;
  double dA = atan(2*x)/(2*x2)-1.0/(x*(1.0+4*x2)) + 1.0/(8*x3) + db*c + b*dc;
  return 8.0/3.0*pow3(pf)*lam*lam*A + 2.0/3.0*pow4(pf)*lam*lam * dA/lam;
}

// Derivative of the momentum-dependent potential per baryon density.
pair<double,double> VectorPotential2::dVmdn(double rho, double lam, double c)
{
  if(c==0.0) return {0.0,0.0};
  double d=4/(2*M_PI*M_PI*HBARC*HBARC*HBARC);
  double pf=pow(3/d*rho,1.0/3.0);

  // the momentum-dependent potential per baryon density
  double deg=d*d*c/(rho0*2);
  double MDPot=deg*potMomdepT0(pf,lam)/rho;

  // derivative of the momentum-dependent potential per baryon density
  //double dMDPot = deg*pf/(3*rho)*dVmdpf(pf,lam)/rho - MDPot/rho;
  double dMDPot = d*c/rho0*singleParticlePotMD(pf,pf,lam)/rho - MDPot/rho;

  return {MDPot,dMDPot};
}

void VectorPotential2::readPotentialParam(string fname,double lam1,double c1)
{
  ifstream in(fname.c_str(), ios::in);
  if(!in) {
    cout << "VectorPotential2::readPotentialParam fail to open file "<< fname<<endl;
    exit(1);
  }

  // read the values of the potential and its derivative from the file.
  useTable=3;
  double nsave=0.0, dn=0.0;
  string templine;
  rhoMin=-1.0;
  while(getline(in,templine)) {
    if(templine.find('#')<0) continue;
    istringstream is(templine);

    //  n [n_0] V [MeV]  dV/dn*n_0 [MeV]
    double n,v,dvdn;
    is >> n >> v >> dvdn;
    if(n==0.0) continue;
    if(rhoMin<0.0) rhoMin=n*rho0;
    Upot.push_back(n*dvdn*1e-3 + v*1e-3);
    Vpot.push_back(v*1e-3);
    dVdn.push_back(dvdn*1e-3/rho0);
    dVdnV.push_back((dvdn/rho0 - v/n*rho0)*1e-3);
    VpotR.push_back(v/n*rho0*1e-3);
    dn = n-nsave;
    nsave=n;
  }
  in.close();
  rhoMax=nsave*rho0;
  dRho=dn*rho0;
  maxN=Vpot.size();

  // Add phase transition.
  if(settings->flag("MeanField:PhaseTransition")) makePT();

  ofstream ofs;
  bool outputRMF = settings->flag("MeanField:outputPotential");
  if(outputRMF) ofs.open("rmf.out");
  if(!outputRMF && c1==0) return;

  // momentum-dependent potential.
  for(int i=0;i<maxN;i++) {
    double n = rhoMin + i*dRho;
    //auto [vm,dvm] =  dVmdn(n*rho0, lam1, c1);
    pair<double,double> a = dVmdn(n, lam1, c1);
    double vm = a.first;
    double dvm = a.second;
    //double V=v*1e-3-vm;
    //double dV=dvdn*1e-3/rho0-dvm;
    Upot[i] -= uMD(n, lam1,c1);
    Vpot[i] -= vm;
    dVdn[i] -= dvm;
    dVdnV[i] -= dvm - vm/n;;
    VpotR[i] -= - vm/n;

    if(outputRMF)
    ofs << n/rho0 << "  " << Vpot[i]
        << " " << dVdn[i]
        << " "<< vm
       	<< " " << dvm
        << " "<< Upot[i]
       	<<endl;

  }

  if(outputRMF) ofs.close();

}

// make a phase transition between the density n0 and n0+dn.
void VectorPotential2::makePT()
{
  double n0 = settings->parm("MeanField:rhoCutPT") * rho0;
  double dn  = settings->parm("MeanField:rhoShiftPT") * rho0;
  double dv1 = settings->parm("MeanField:VShiftPT") * 1e-3; // MeV -> GeV

  // copy original potential.
  std::vector<double> vpot=Vpot, dvdn=dVdn, dvdnv=dVdnV;

  double n1 = n0 + dn;
  double v0=getV(n0);
  double dv0=getdVdn(n0);
  //cout << " n0= "<< n0 << " v0= "<< v0 << " dv0= "<< dv0 <<endl;
  for(int i=0;i<maxN;i++) {
    double n=rhoMin + i*dRho;
    if(n<=n0){
    } else if (n>=n1) {
      vpot[i]=getV(n-dn) + dv1;
      dvdn[i]=getdVdn(n-dn);
      dvdnv[i]= dvdn[i] - vpot[i]/n;
    } else {
      //vpot[i] = v0 + dv0*(n-n0) - 3*dv0/dn*(n-n0)*(n-n0) + 2*dv0/(dn*dn)*pow3(n-n0);
      //dvdn[i] = dv0 - 6*dv0/dn*(n-n0) + 6*dv0/(dn*dn)*(n-n0)*(n-n0);
      double vv=dv1 - dv0*dn;
      vpot[i] = v0 + dv0*(n-n0) + 3/(dn*dn)*vv*pow2(n-n0) - 2/pow3(dn)*vv*pow3(n-n0);
      dvdn[i] = dv0 + 6/(dn*dn)*vv*(n-n0) - 6/pow3(dn)*vv*pow2(n-n0);
      dvdnv[i] = dvdn[i] - vpot[i]/n;

    }
  }
  Vpot=vpot;
  dVdn=dvdn;
  dVdnV=dvdnv;
}

void VectorPotential2::setPotentialParam(EventParticle* p)
{
  return;

  // Lambda potential U_Lambda(rho)= a*rho/rho0 + b*(rho/rho0)^4/3 + c*(rho/rho0)^5/3
 
  //int islam = isHyperon(p);

  //            wid,alpha,beta,gam,  c1,  gam2,  cs, cv   mus muv
  double fp[12]={1.0, 1.0, 1.0, 1.0, 0.0, 5./3., 1.0,1.0, 1.0,1.0,0.0,0.0};
  int idp=1;

  // Hyperon potential is the same as nucleon potential multiplied by some factor.
    //for(int i=0;i<10;i++) fp[i]=facPotL[i];
    p->setPotentialParam(fp);
    p->setPotentialId(idp);
    return;

}

void VectorPotential2::setMDParam(int opt) 
{
  if(opt==1) {
  // MD1 with rho0=0.168 B=-16MeV
    C1 = -1.697659e-01;
    mu1=3.172916e+00*HBARC;

  } else if(opt==2) {
  // MD2 with rho0=0.168 B=-16MeV
    C1 = -1.5400071e-1;
    mu1 = 2.100138e+00*HBARC;

  } else if(opt==3) {
    // MD1 with rho0=0.16 BE=-16.3MeV
    C1= -0.16665950120627837;
    mu1= 3.248020503473469*HBARC;

  } else if(opt==4) {
    // MD2 with rho0=0.16 BE=-16.3MeV
    C1= -0.14845072399566206;
    mu1= 2.1665240217804356 *HBARC;
  

  // MD1 rho0=0.16 BE=-16.MeV
  } else if(opt==5) {
    C1= -0.1661270741543753;
    mu1= 3.2621575461030505*HBARC;

  // Giessen
  } else if(opt==7) {
    C1    = -2*63.6e-3;
    mu1   = 2.13*HBARC; 
  } else {
    cout << "VectorPotential2::setMDParam wrong opt= "<< opt<<endl;
    exit(1);
  }

  setMDParam();

}

void VectorPotential2::setMDParam() 
{
  pmu1= mu1*mu1;
  pmu2= mu2*mu2;
  // QMD mode
  if(transportModel==1) {
    vex1=C1/(2*rho0);
    vex2=C2/(2*rho0);
    uex1=vex1;
    uex2=vex2;
    if(optPotentialEval<2) {
      uex1=2*vex1;
      uex2=2*vex2;
    }

  // BUU mode 
  // (currently BUU mode is not implemented for RMF (single-particle potential U and dU/dn are needed).
  } else {
    vex1=C1/rho0;
    vex2=C2/rho0;
    uex1=vex1;
    uex2=vex2;
  }

}

} // namespace jam2
