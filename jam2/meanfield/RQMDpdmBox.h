#ifndef jam2_meanfield_RQMDpdmBox_h
#define jam2_meanfield_RQMDpdmBox_h

#include <jam2/meanfield/MBoxPDM.h>
#include <jam2/meanfield/MCell.h>
#include <jam2/meanfield/ParityDoublet.h>

namespace jam2 {

class RQMDpdmBox : public MCell, public MeanField
{
private:
  std::vector<MBoxPDM*> mBox;
  int optBoxBoundary;
  int maxIt,optIntMC;
  bool firstEng;
  Vec4 eFree,eFree0;
  Vec4 pTot;
  ParityDoublet *potential;

public:
  RQMDpdmBox(Pythia8::Settings* s, JamParticleData* jpd, Pythia8::Rndm* r,InitialCondition* ini);
  ~RQMDpdmBox();
  void evolution(std::list<EventParticle*>& plist,double t,double dt,int step);
  void init(std::list<EventParticle*>& plist);
  void singleParticlePotential();

  void initBox(Pythia8::Settings* s,JamParticleData* jpd,Pythia8::Rndm* r);
  void addParticle(std::list<EventParticle*>& plist,double dt, int step);

  Pythia8::Vec4 computeEnergy(std::list<EventParticle*>& plist, int step);

}; 
}  // namespace jam2
#endif
