#ifndef jam2_meanfield_MakePotential_h
#define jam2_meanfield_MakePotential_h

#include <Pythia8/Settings.h>
#include <jam2/collision/EventParticle.h>

namespace jam2 {

class MakePotential
{
private:
  Pythia8::Settings* settings;
  double rho0,cutOffPot;
  double vex1,vex2,pmu1,pmu2,C1,C2,mu1,mu2;
  bool withMomDep;
  int optVectorPotential,eosType,transportModel,optPotentialType;
  std::vector<double> Vpot={}, dVdn={};
  //std::array<double,601> Vpot={}, dVdn={};
  double rhoMax=0.0,dRho=0.0,rhoMin=0.0;
  int maxN=0;
  static bool firstCall;
  bool useTable=false;
public:
  MakePotential(Pythia8::Settings* s);
  ~MakePotential() { };
  double getMu1() const {return mu1;}
  double getMu2() const {return mu2;}
  double getC1() const {return C1;}
  double getC2() const {return C2;}
  bool isMomDep() const {return withMomDep;}
  double potcut() const {return cutOffPot;}
  void setPotentialParam(EventParticle* p);
  void setLambdaPotential(double* fp);
  void setSigmaPotential(double* fp);
  void readPotentialParam(std::string fn,double mu1,double c1);
  void makeMDPotVDF(double mu1, double c1);
  void makePT();
  bool isPotTable() const {return maxN;}
  double getRhoMin() const {return rhoMin;}

  double Vrho(std::vector<double>& rhog, double rho) {
    return getV(rho)/rho;
  }
  double delV(std::vector<double>& rhog, double rho) {
    return (rho*getdVdn(rho)-getV(rho))/(rho*rho*rho);
  }

  // n in the unit of 1/fm^3
  double getV(double n) {
    //if(n<0.014) return 0.0;
    int i=min(max(0,int((n-rhoMin)/dRho)),maxN-2);
    double x=(n-(rhoMin+i*dRho))/dRho;
    return Vpot[i]*(1.0-x) + Vpot[i+1]*x;
  }
  // n in the unit of 1/fm^3
  double getdVdn(double n) {
    //if(n<0.014) return 0.0;
    int i=min(max(0,int((n-rhoMin)/dRho)),maxN-2);
    double x=(n-(rhoMin+i*dRho))/dRho;
    return dVdn[i]*(1.0-x) + dVdn[i+1]*x;
  }

  double singleParticlePotMD(double p, double pf, double lambda);
  double potMomdepT0(double pf, double lam);
  double dVmdpf(double pf, double lam);
  std::pair<double,double> dVmdn(double rho, double lam, double c);

};

} // end namespace jam2
#endif
