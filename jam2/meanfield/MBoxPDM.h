#ifndef jam2_meanfield_MBoxPDM_h
#define jam2_meanfield_MBoxPDM_h

#include <vector>
#include <list>
#include <array>
#include <algorithm>
#include <jam2/meanfield/MBox.h>
#include <jam2/meanfield/RQMDpdm.h>

namespace jam2 {

class MBoxPDM;

class NeighborPDM
{
public:
  MBoxPDM* box;    // pointer to the neighbor box
  NeighborPDM*  neighbor; // pointer to the neighbor of the neighbor box
  std::vector<std::vector<double> > rhom,rhomx;
private:
  bool action;
public:
  NeighborPDM(MBoxPDM* b) : box(b) {neighbor=nullptr;action=false;}
  void setAction() {action=true;}
  bool getAction() {return action;}
  void resize(int n);
  void setNeighbor(NeighborPDM* n) {neighbor=n;}
  void setRhom(int i, int j, double a) {rhom[i][j]=a;rhomx[i][j]=a;}
  //std::vector<EventParticle*>& getParticle() {return box->getParticle();}
 
};

class MBoxPDM : public MBox, public RQMDpdm
{
private:
  std::vector<NeighborPDM*> neighbors;

public:
  MBoxPDM(std::array<double,3>& xmin, std::array<double,3>& xmax, std::array<int,3>& pos, bool ed,Pythia8::Settings* s, JamParticleData* jpd, Pythia8::Rndm* r)  :
    MBox(xmin,xmax,pos,ed),RQMDpdm(s,jpd,r,2) { }

  ~MBoxPDM();
  void initBox();
  void qmdMatrix();
  void computeForce();
  void updateRP(double dt);
  void clearMatrix();

  void qmdMatrixNeighbor(NeighborPDM& b);
  void computeForceNeighbor(NeighborPDM& b);
  Pythia8::Vec4 omegaPotentialNeighbor(NeighborPDM& neighbor,int j, Pythia8::Vec4& u2);

  void qmdMatrixNeighborL(int i, Pythia8::Vec4& r1, NeighborPDM& neighbor);
  void computeForceNeighborL(int i, Pythia8::Vec4& r, Pythia8::Vec4& r1, Pythia8::Vec4& pk1, 
        Pythia8::Vec4& forcei,Pythia8::Vec4& foceri,
        Pythia8::Vec4& forcej,Pythia8::Vec4& focerj,
	NeighborPDM& neighbor,bool opt=false);
  void computeForceL();

  void clear() {part.clear();}
  const std::vector<NeighborPDM*>& getNeighbors() const {return neighbors;}
  void addNeighbor(MBoxPDM* b) {neighbors.push_back(new NeighborPDM(b));}
  void setAction() {neighbors.back()->setAction();}
  bool haveThisSite(MBoxPDM* box) const;
  //{if(find(neighbors.begin(),neighbors.end(),box) != neighbors.end()) return true;
  //  else return false;}

  //std::vector<NeighborV>::iterator findNeighbor(MBoxPDM* box);
  NeighborPDM* findNeighbor(MBoxPDM* box);

  void saveMatrix() {
      rhomx=rhom;
      rhos2x=rhos2;
      omegax=omega;
      rho_mesonx=rho_meson;
  }

};


} // end namespace jam2
#endif
