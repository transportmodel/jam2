#include <jam2/meanfield/RQMDpdm.h>
#include <jam2/hadrons/JamStdlib.h>

// RQMD for parity doublet model.

namespace jam2 {

using namespace std;

bool RQMDpdm::firstCall=true;

RQMDpdm::RQMDpdm(Pythia8::Settings* s,JamParticleData* jpd,Pythia8::Rndm* rnd, int itm) : MeanField(s)
{
  rndm=rnd;
  maxIt=itm;
  optBaryonCurrent = settings->mode("MeanField:optBaryonCurrent"); 
  widG = settings->parm("MeanField:gaussWidth");
  wmG = 1.0/(4*widG);
  facG = 1.0/pow(4.0*M_PI*widG, 1.5);
  wG2 = 1.0/(2*widG);
  facG2 = 1.0/pow(2.0*M_PI*widG, 1.5);

  overSample = settings->mode("Cascade:overSample");
  transportModel = settings->mode("MeanField:transportModel"); 
  optOmega4 = settings->mode("MeanField:optOmegaQuartic"); 
  optIntMC = settings->mode("MeanField:optMCIntegral");

  potential = new ParityDoublet(settings,jpd);

  // (g_omega/m_omega)^2 in GeV/fm^3
  Comega2 = potential->getCv2();

  g_omega = potential->gOmega();
  f_pion = potential->getFPion();
  withMomDep = potential->isMomDep();
  optRMF=potential->isRMF();
  mOmega2=potential->mOmega2();
  mOmega3=potential->mOmega3();
  mOmega2fm=mOmega2/HBARC3;
  mPhi2=potential->mPhi2();
  mPhi2fm=mPhi2/HBARC3;
  mRho2=potential->mRho2();
  mRho3=potential->mRho3();
  mRho2fm=mRho2/HBARC3;
  C4 = potential->getC4();
  optRhoMeson = potential->rhoMeson();

  facCoupling=potential->getFacCoupling();
  facCouplingv=potential->getFacCouplingV();
  facCoupling2=facCouplingv2=1.0;

  //if(transportModel==2) facCoupling2=facCouplingv2=0.5;
  //else if(transportModel==3) facCoupling2=facCouplingv2=1.0/sqrt(2);

  // omega-rho coupling term
  cOmegaRho = optRhoMeson==3 ? C4*3.0 : optRhoMeson==2? C4*1.0:0.0;

  if(firstCall) {
    cout << "RQMDpdm mode g_omega= " << g_omega << " cv2= "<< Comega2 << endl;
    firstCall=false;
  }

}

RQMDpdm::~RQMDpdm()
{
  part.clear();
  rho.clear();
  rho_meson.clear();
  rhos.clear();
  rhos2.clear();
  vmoms.clear();
  vmom4.clear();
  force.clear();
  forcer.clear();
  omega.clear();
  //for(int i=0;i<NV;i++) rhom[i].clear();
  rhom.clear();
  JB.clear();
  IB.clear();
  Vdot.clear();
  lambda.clear();
  delete potential;
}

void RQMDpdm::evolution(list<EventParticle*>& plist, double t, double dt,int step)
{
  part.clear();
  globalTime = t;

  // import particles.
  for(auto& i : plist) {
    if(i->tevol() > t ) continue;
    double tp = optCollisionOrdering < 100 ? t : tau2t(t,i);
    if(i->isMeanField(tp,optPotential)) {
      if(potential->setPotentialParam(i)) part.push_back(i);
    }
  }

  NV = part.size();

  rhom.resize(NV);
  for(int i=0;i<NV;i++) {
    rhom[i].resize(NV);
  }
  rho.assign(NV,0.0);
  omega.assign(NV,0.0);
  rho_meson.assign(NV,0.0);
  Vdot.assign(NV,0.0);
  force.assign(NV,0.0);
  forcer.assign(NV,0.0);
  lambda.assign(NV,1.0);

  // additional vector for the full Monte-Carlo calculation.
  //if((optIntMC>=3 && optIntMC<11) || optIntMC>11) {
  if(optIntMC>=2) {
    rhomx.resize(NV);
    for(int i=0;i<NV;i++) rhomx[i].resize(NV);
    rhos2x.assign(NV,0.0);
    omegax.assign(NV,0.0);
    rho_mesonx.assign(NV,0.0);
    dGij.resize(NV);
    dGji.resize(NV);
    forceD.resize(NV);
    dUx.resize(NV);
    dUy.resize(NV);
    dUz.resize(NV);
    for(int i=0;i<NV;i++) {
      dGij[i].resize(NV);
      dGji[i].resize(NV);
      forceD[i].resize(NV);
      dUx[i].assign(NV,0.0);
      dUy[i].assign(NV,0.0);
      dUz[i].assign(NV,0.0);
    }
  }

  // change from kinetic to canonical momenta.
  if(optVdot==1) {
    for(auto& i : part) i->setFree();
  }
  if(optPotentialArg >= 1) {
    for(auto& i : part) i->setPotS(0.0);
  }

  bool optSet= optPotentialArg == 0 ? true :  false;
  setZero();

  if(optPotentialEval<5) {

    // compute sigma field.
    for(int itry=0;itry<maxIt;itry++) {
      setZero();
      qmdMatrix();
      //singleParticlePotential();
      double diff=sigmaField(optSet);
      if(diff <1e-5) break;
    }

    omegaField();
    singleParticlePotential();
    computeForce();
  //if(transportModel==1) computeForceMatrix2();
  
  } else if(optPotentialEval==6) {
    computeForceL();
  } else {
    computeForceL2();
  }

  // Note that optVdot=0,1: all momenta are canonical.
  // Canonical momenta update.
  if(optVdot == 0) {
    for(int i=0; i< NV; i++) {

      Vec4 fr= optVelocity==0? 0.0 :forcer[i];

      part[i]->updateByForce(force[i], fr,dt);
      //part[i]->updateByForce(force[i], forcer[i],dt);
      
      part[i]->updateR(part[i]->getT(), optPropagate);

      if(optCollisionOrdering > 100) {
        part[i]->addT(forcer[i][0]*dt);
	if(optCollisionOrdering==101) part[i]->tevol(pHat*part[i]->getR());
      }

      // update Lagrangue multiplier lambda/2: v=lambada*p
      part[i]->lambda(MeanField::lambda(part[i]));

    }

  // canonical momenta are used for the evaluation of forces,
  // after update canonical momenta, set kinetic momenta.
  } else if(optVdot == 1) {

    for(int i=0; i< NV; i++) {

      // canonical momenta update.
      part[i]->updateByForce(force[i], forcer[i],dt);
      // change to kinetic momenta.
      part[i]->setKinetic();
    }

  // compute time derivatives of vector potential V_i^\mu numerically.
  } else if(optVdot == 2) {

    // Vdot[] is already computed in singleParticlePotential().
    for(int i=0; i< NV; i++) {
      part[i]->updateByForce(force[i] - Vdot[i]/dt, forcer[i],dt);
    }

  // compute time derivatives of vector potential analytically.
  } else if(optVdot == 3) {

    cout <<"RQMDpdm error not implemented now optVdot= "<< optVdot <<endl;
    exit(1);
    //computeVdot();
    for(int i=0; i< NV; i++) {
      part[i]->updateByForce(force[i] - Vdot[i], forcer[i],dt);
    }

  // compute time derivatives of V^\mu numerically; (V(t+dt) - V(t))/dt
  // (under construction)
  } else if(optVdot == 4 ) {

    for(int i=0;i<NV;i++) {
      part[i]->updateByForce(force[i] + part[i]->potv()/dt, forcer[i],dt);
      vmom4[i]=0.0;
      JB[i]=0.0;
    }
    setZero();
    qmdMatrix();
    sigmaField(optSet);
    singleParticlePotential();
    for(int i=0;i<NV;i++) {
      part[i]->addP(-part[i]->potv());
      part[i]->setOnShell();
    }

  } else {
    cout << "RQMDpdm wrong option optVdot = "<< optVdot<<endl;
    exit(1);
  }

  if(!optSet) setScalarPotential();

}

void RQMDpdm::setScalarPotential()
{
  for(int i=0;i<NV;i++) {
    part[i]->setPotS(part[i]->pots());
  }
}

// Compute single particle potential.
void RQMDpdm::singleParticlePotential()
{
  //double fact= transportModel==1 ? 0.5: 1.0;
  double fact= optPotentialEval==0 ? 0.5: 1.0;

  Vec4 vTotal=0.0;
  Vec4 vc = 0.0;
  bool optV=false;
  if(optVdot==1) optV=true;
  if(optV && optVectorPotential==1) {
    for(int i=0; i< NV; i++) {
      double xv = part[i]->facPotential(5);
      double gp = part[i]->facPotential(9);
      double gr = part[i]->facPotential(10);
      double x=C4*omega[i].m2Calc()/mOmega2;
      double y=C4*rho_meson[i].m2Calc()/mRho2;
      double xc = optOmega4==1? (1.0+1.5*x)/(1.0+x): 1.0;
      double yc = optOmega4==1? (1.0+1.5*y)/(1.0+y): 1.0;
      //double vsky1 = facCouplingv2*g_omega*xv*xc*part[i]->baryon()/3;
      double vsky1 = fact*facCouplingv2*g_omega*xv*xc*part[i]->baryon()/3;
      double vsky2 = fact*gp/mPhi2fm*part[i]->baryon()/3;
      double vsky3 = fact*gr/mRho2fm*yc*part[i]->baryon()/3;
      vTotal += vsky1 * omega[i] + vsky2*JB[i] + vsky3*IB[i] + vmom4[i];
    }
    vc = vTotal / (NV*overSample);
    vc[0]=0.0;
  }

  Vec4 vdott = 0.0;
  for(int i=0; i< NV; i++) {

    Vec4 vpot0 = part[i]->potv();

    double* gfac= part[i]->facPotential();
    double sigma=gfac[0];
    double xv=gfac[5];
    double dmi=gfac[6];     // dm*/dsigma
    //double dmi2 = gfac[7]; // d^2m/dsigma^2
    double C_phi = gfac[9]/mPhi2fm;
    double C_rho = gfac[10]/mRho2fm;

    // new RQMD
    if(optPotentialEval > 0 && optPotentialEval <= 4) {
      /*
      double Vsigma = optRMF==1 ? potential->SigmaPotentialRMF(sigma):
                      optRMF==2 ? potential->SigmaPotentialPDM1(sigma):
                                  potential->SigmaPotentialPDM2(sigma);
      double dVsigma = optRMF==1 ? potential->dPotDSigmaRMF(sigma):
                       optRMF==2 ? potential->dPotDSigmaPDM1(sigma):
		                   potential->dPotDSigmaPDM2(sigma);
      //double nsd=rhos[i]/dmi;
      //double nsd=-dVsigma/dmi;
      //double nsnsd=1.0/(1.0+dmi2*dsigma*nsd);
      //rhos2[i] = dsigma * nsnsd *(dmi - dVsigma/nsd) - Vsigma/(nsd*nsd);
      rhos2[i] =  - dmi*Vsigma/(dVsigma*dVsigma);
      */

      double dsigma = optRMF==1 ? potential->dSigmaRMF2(sigma):
                      optRMF==2 ? potential->dSigmaPDM1(sigma):
    		                  potential->dSigmaPDM2(sigma);
      rhos2[i] =  - dmi*dsigma;

    } else {

      // dsigma/drho_s = (del^2 V_sigma/del sigma^2)^{-1}
      double dsigma = optRMF==1 ? potential->dSigmaRMF(sigma):
                      optRMF==2 ? potential->dSigma(sigma):
    		                  potential->dSigma2(sigma);

      // dm^*/dsigma * dsigma/drho_s
      double fac5 = optPotentialEval==0? 0.5: 1.0;
      rhos2[i] = fac5*dmi*dsigma/( 1.0 - dsigma*rhos2[i]);
      //rhos2[i] = fac5*dmi*dsigma;

    }

    if(part[i]->rhob() < 1e-15) {
      part[i]->setPotV( Vec4(0.0));
      part[i]->setPotVm(Vec4(0.0));
      continue;
    }

    double x=C4*max(0.0,omega[i].m2Calc())/mOmega2;
    double y=C4*max(0.0,rho_meson[i].m2Calc())/mRho2;
    double xc = optOmega4==1? (1.0+1.5*x)/(1.0+x): 1.0;
    double yc = optOmega4==1? (1.0+1.5*y)/(1.0+y): 1.0;
    double vsky1 = fact*facCouplingv2*g_omega*xv*xc*part[i]->baryon()/3;
    double vsky2 = fact*C_phi*part[i]->baryon()/3;
    double vsky3 = fact*C_rho*yc*part[i]->baryon()/3;

    // four-components of vector potential are fully included.
    if(optVectorPotential==1) {
      part[i]->setPotV( vsky1 * omega[i] + vsky2*JB[i] + vsky3*IB[i] + vmom4[i] -vc);
      part[i]->setPotVm(vmom4[i] -vc);

    // only time component of vector potential is included.
    } else if(optVectorPotential==2) {
      part[i]->setPotV(0,vsky1*omega[i][0] +vsky2*JB[i][0] +vsky3*IB[i][0] + vmom4[i][0] );
      part[i]->setPotVm(0,vmom4[i][0] );

    // only time component of vector potential is included in the form
    // of V(rho_B) where rho_B is an invariant baryon density.
    } else if(optVectorPotential==3) {
      part[i]->setPotV(0,vsky1*rho[i] +vsky2*rho[i] + vsky3*IB[i].mCalc() + vmom4[i][0] );
      part[i]->setPotVm(0,vmom4[i][0] );

    // fully non-relativistic
    } else {
      part[i]->setPotV(0, vsky1*omega[i][0] + vsky2*JB[i][0] +vsky3*JB[i][0] + vmom4[i][0] );
      part[i]->setPotVm(0,vmom4[i][0] );
    }

    if(optVdot == 2) {
      Vdot[i] = part[i]->potv() - vpot0;
      vdott += Vdot[i];
    }
  }

  if(optVdot == 2) {
    vdott /= NV;
    for(int i=0; i< NV; i++) Vdot[i] -= vdott;
  } 

}

// compute single particle potential energy.
Vec4 RQMDpdm::computeEnergy(list<EventParticle*>& plist, int step)
{
  pTot=0.0;   
  for(auto& i :plist) {
    double m = i->getEffectiveMass();
    if(optVectorPotential==1 && optVdot==0) {
      Vec4 pk= i->getP() - i->potv();
      pTot[0] += sqrt( m*m + pk.pAbs2());
    } else {
      pTot[0] += sqrt( m*m + i->pAbs2());
    }
    pTot[1] += i->getP(1);
    pTot[2] += i->getP(2);
    pTot[3] += i->getP(3);
    pTot[0] += i->potv(0);
  }

  if(step==1) pTot0 = pTot/overSample;

  return pTot/overSample;
}

// Compute sigma-field.
double RQMDpdm::sigmaField(bool optSet)
{
  if(NV==0) return 0.0;
  const double  eps = 1e-5;

  double diff=0.0;
    for(int i=0; i< NV; i++) {
      double meff0 = part[i]->getEffectiveMass();
      double* gfac1= part[i]->facPotential();
      double m0=gfac1[1]; // M0
      double a1=gfac1[2]; // g1
      double a2=gfac1[3]; // g2
      double as=gfac1[4]; // parity
      double m1=gfac1[8]; // su3 symmetry breaking term
      //double mmin=gfac1[11];

      double sigma,meff,spot;
      double dmi,dmi2;
      if(optRMF !=1) { // parity doublet or singlet model
        // Find sigma-field from bisection method.
	//sigma = potential->sigma_bisec(rhos[i]*HBARC3 );
	sigma = potential->sigma_bisec(rhos[i]*HBARC3 );

        // Effective mass including momentum-dependent potential.
        meff =  potential->mEff(sigma, m0, a1, a2, as) + vmoms[i] + m1;

        // Scalar potential defined as m* = m + S.
        spot = meff - part[i]->getMass();
        dmi  = potential->dMdSigma(sigma, m0, a1, a2, as);
        dmi2 = potential->d2MdSigma2(sigma, m0, a1, as);

	//if(meff< mmin) {
	if(spot>1e-10) {
	//if(meff< 0.0) {
        double rhb = sqrt(max(0.0,JB[i].m2Calc()));
        double smin=2*m0*abs(a2)/(2*a1*sqrt(a1*a1-a2*a2));
	//if(rhb<1e-5) {
	//if(rhos[i]*HBARC3<1e-7) {
	cout << "spot>0 id= "<< part[i]->getID()
	  << " m= "<< part[i]->getMass()
	  << " meff= "<< meff
	  << scientific <<" spot "<< spot
	  <<" sigma="<< sigma
	  << scientific
	  << " rhob= "<< rhb
	  << " rhos= "<< rhos[i]*HBARC3
	  << fixed
	  << " vmoms= "<< vmoms[i]
	  << " m0 "<< m0
	  << " m1 "<< m1
	  << " a1 "<<  a1
	  << " a2 "<<  a2
	  << " as "<<  as
          << " meff_vac= "<<potential->mEff(f_pion, m0, a1, a2, as) + m1
          << " meff0= "<<potential->mEff(0.0, m0, a1, a2, as) + m1
          << " sigma_min= "<< smin
          << " m_min= "<< potential->mEff(smin, m0, a1, a2, as) + m1
	  <<endl;
	}

      } else { //Walecka model
	if(a2==0.0) {
	  cout << "RQMDpdm::sigmaField a2= "<< a2 << " m0= "<< m0
	    << " id= "<< part[i]->getID()
	    <<  "meff= "<< meff0
	    << " a1= "<< a1
	    << " as= "<< as
	    << " m1= "<< m1
	    <<endl;
	  exit(1);
	}
	double g2 = optPotentialEval==0? 0.5*a2 : a2;
	//g2=a2;
        double m = part[i]->getMass();
	//static double m_min = 0.1;
	static double m_min = 0.0;
	double sigmax= abs((m - m_min + vmoms[i])/g2);
        sigma = potential->sigma_bisecRMF(rhos[i]*HBARC3, g2,sigmax);
	spot = g2*sigma;
	meff = m + spot + vmoms[i];
	if(meff< m_min) {
	  cout << "RQMDpdm::sigmax= "<< sigmax
	    << " meff= "<< meff
	    << " m= "<< m
	    << " sigma= "<< sigma
	    << " a2= "<< a2
	    << " spot= "<< spot << " vm= "<< vmoms[i]
	    <<endl;
	  spot = m_min - m - vmoms[i];
	}
	dmi = a2;
	dmi2 = 0.0;
      }

      // Save relevant quantities.
      part[i]->setPotS(spot,optSet);
      part[i]->setPotSm(vmoms[i]);
      part[i]->setRhoS(rhos[i]);
      part[i]->setPotentialParam(sigma,0);
      part[i]->setPotentialParam(dmi,6);
      part[i]->setPotentialParam(dmi2,7);

      diff += abs(meff-meff0);
    }

    //diff=abs(pot0-pot);
    if(diff/NV < eps) return diff/NV;
    //cout << " diff = "<< diff/NV << " itry= "<< itry << " mxit= "<< maxIt << endl;

  return diff/NV;
}

// Compute omega-field.
void RQMDpdm::omegaField()
{
  if(NV==0) return;

  // Compute invariant baryon density.
  for(int i=0; i< NV; i++) {
    //rho[i] = sqrt(max(0.0,JB[i].m2Calc()));
    rho[i] = sqrt(abs(JB[i].m2Calc()));
    part[i]->setRhoB(rho[i]);
  }

  // Compute omega-field in the local density approximation.
  if(optRhoMeson==0) {
    for(int i=0; i< NV; i++) {
      if(rho[i]<1e-15) {
	omega[i] = Vec4(0.0,0.0,0.0,0.0);
	continue;
      }
      Vec4 jb = g_omega*JB[i]*HBARC3;
      //Vec4 jb = JB[i]*HBARC3;
      Vec4 beta = jb/jb[0];
      //jb.bst(-beta[1],-beta[2],-beta[3]);
      omega[i] = Vec4(0.0,0.0,0.0,potential->Omega(1.0,max(0.0,jb.mCalc())));
      omega[i].bst(beta[1],beta[2],beta[3]);
    }

    return;
  }


  for(int i=0; i< NV; i++) {

    if(rho[i]<1e-15) {
      omega[i] = Vec4(0.0,0.0,0.0,0.0);
      rho_meson[i] = Vec4(0.0,0.0,0.0,0.0);
      continue;
    }
    // boost baryon current
    Vec4 jb = g_omega*JB[i]*HBARC3;
    //Vec4 jb = JB[i]*HBARC3;
    Vec4 beta = jb/jb[0];
    //jb.bst(-beta[1],-beta[2],-beta[3]);
    double jb0=jb.mCalc();

    // boost isospin current
    Vec4 ib = IB[i]*HBARC3;
    Vec4 beta2 = ib/ib[0];
    //ib.bst(-beta2[1],-beta2[2],-beta2[3]);
    double ib0=ib.mCalc();

    // initial omega and rho fields at rest.
    double om0 = jb0/mOmega2;
    double rh0 = ib0/mRho2;

    double mo2 = mOmega2 + cOmegaRho*rh0*rh0;
    double rh2 = mRho2   + cOmegaRho*om0*om0;
    double eps=10;
    int ntry=0;
    do {
      om0 = potential->Omega(mo2,C4,jb0);
      rh0 = potential->Omega(rh2,C4,ib0);
      mo2 = mOmega2 + cOmegaRho*rh0*rh0;
      rh2 = mRho2   + cOmegaRho*om0*om0;
      eps = abs(mo2*om0 + C4*om0*om0*om0 - jb0 + rh2*rh0 + C4*rh0*rh0*rh0 - ib0);
      if(++ntry > 10) break;
    } while(eps > 1e-15);

    // boost back omega field to the computational frame.
    Vec4 om=Vec4(0.0, 0.0, 0.0, om0);
    om.bst(beta[1],beta[2],beta[3]);
    omega[i]=om;

    // boost back rho field to the computational frame.
    Vec4 rh=Vec4(0.0, 0.0, 0.0, rh0);
    rh.bst(beta2[1],beta2[2],beta2[3]);
    rho_meson[i]=rh;

    /*
    cout << " C4 "<< C4 <<endl;
    cout << " ntry "<< ntry <<endl;
    cout << " eps= "<< eps <<endl;
    cout << " comega= "<< cOmegaRho <<endl;
    cout << " J "<< JB[i]<<endl;
    cout << " ome= "<< omega[i]*mOmega2/HBARC3;
    cout << " I "<< IB[i]<<endl;
    cout << "rho_m="<<rho_meson[i]*mRho2/HBARC3;
    cout << " om0= "<< mOmega2*om0/HBARC3<< " jb0= "<< jb0/HBARC3 << endl;
    cout << " om1= "<< (mOmega2 + C4*(om*om))/HBARC3*om;
    cout << " rho0= "<< mRho2*rh0/HBARC3<< " ib0= "<< ib0/HBARC3 << endl;
    cout << " beta2= "<< beta2[1]*beta2[1] + beta2[2]*beta2[2] + beta2[3]*beta2[3] << endl;
    cout << " beta2= "<< beta2;
    cout << " ib0= "<< IB[i][0]<<endl;
    */
  }
}

// The transverse distance is defined by the rest frame of a particle.
void RQMDpdm::qmdMatrix()
{
  int selfi = optPotentialEval<2? 1:0;
  // loop over all baryons
  for(int i=0; i< NV; i++) {

    Vec4 r1  = part[i]->getR();
    Vec4 p1 = optPotentialArg>=1? part[i]->getPcan(optVdot) : part[i]->getPkin(optVdot);
    double m1 = optPotentialArg>=1? part[i]->getMass(): part[i]->getEffectiveMass();
    //double m1=p1.mCalc();

    Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : p1;
    if(optPotentialArg <= 1) p1 = pk1;

    Vec4 u1 = p1/m1;

    // set factor for leading baryons within its formation time.
    double qfac1=1.0;
    if(optPotential<4) qfac1 = part[i]->getTf() > globalTime ? part[i]->qFactor() : 1.0;

    // option for baryon current
    Vec4 fri = optBaryonCurrent ? part[i]->forceR()*u1[0] : 0.0;

    // baryon number
    int bi   = part[i]->baryon()/3;
    if(bi==0) {
      qfac1 = facMesonPot;
      bi=1;
    }

    // get potential factors
    double* gfac1= part[i]->facPotential();
    double xi=gfac1[5];     // factor for omega coupling
    double yi=gfac1[10];    // rho coupling x charge
    double dmi  = gfac1[6]; // dm/dsigma
    double dmi2 = gfac1[7]; // d^2m/dsigma^2

    for(int j=i+selfi; j< NV; j++) {

      Vec4 r2  = part[j]->getR();
      Vec4 p2 = optPotentialArg>=1 ? part[j]->getPcan(optVdot) : part[j]->getPkin(optVdot);
      double m2 = optPotentialArg>=1? part[j]->getMass():part[j]->getEffectiveMass();
      //double m2=p2.mCalc();

       Vec4 pk2 = optVdot <= 1 ? part[j]->getPkin(optVdot) : p1;
       if(optPotentialArg <= 1) p2 = pk2;

      Vec4 u2 = p2/m2;

      Vec4 frj = optBaryonCurrent ? part[j]->forceR()*u2[0] : 0.0;
      int bj = part[j]->baryon()/3;
      double qfac2=1.0;
      if(optPotential<4) qfac2 = part[j]->getTf() > globalTime ? part[j]->qFactor() : 1.0;
      if(bj==0) {
	qfac2 = facMesonPot;
	bj=1;
      }
      double* gfac2= part[j]->facPotential();
      double xj=gfac2[5];
      double yj=gfac2[10];
      double dmj  = gfac2[6];
      double dmj2 = gfac2[7];

      Vec4 dr = r1 - r2;
      double den1,den2,den3,den4;
      double pfac=qfac1*qfac2/overSample;

      if(optTwoBodyDistance==3) {

      double drsq1 = dr.m2Calc() - pow2(dr*u2);
      double drsq2 = dr.m2Calc() - pow2(dr*u1);

      if(optPotentialEval==4) {
        Vec4 dq = rGaussDistance(dr,u1,u2);
        den1 = facG/sqrt(Det) * exp(dq*dr*wmG)*pfac;
	den2 = den1;
        den3 = facG2 * exp(drsq1*wG2)*pfac;
        den4 = facG2 * exp(drsq2*wG2)*pfac;
      } else {
        den1 = facG * exp(drsq1*wmG)*pfac;
        den2 = facG * exp(drsq2*wmG)*pfac;
        den3 = optPotentialEval == 0? den1 : facG2 * exp(drsq1*wG2)*pfac;
        den4 = optPotentialEval == 0? den2 : facG2 * exp(drsq2*wG2)*pfac;
      }

      } else {

      Vec4 pCM=p1 + p2;
      double sInv = pCM.m2Calc();
      double drcmsq = dr.m2Calc() - pow2(dr * pCM)/sInv;
      double den = pCM[0]/sqrt(sInv)*facG * exp(drcmsq*wmG)*pfac;
      double denw = optPotentialEval == 0? den: pCM[0]/sqrt(sInv)*facG2 * exp(drcmsq*wG2)*pfac;
      den1 = m2/p2[0]*den;
      den2 = m1/p1[0]*den;
      den3 = m2/p2[0]*denw;
      den4 = m1/p1[0]*denw;

      }
      if(i==j) {
	den1 /=2.0;
	den2 /=2.0;
	den3 /=2.0;
	den4 /=2.0;
      }

      //rhom[i][j] = den1 * u2[0];
      //rhom[j][i] = den2 * u1[0];

      rhom[i][j] = den1;
      rhom[j][i] = den2;

      // scalar density times dm*/dsigma (coupling constant).
      rhos[i] += den3*dmj;
      rhos[j] += den4*dmi;

      // scalar density times d^2m*/dsigma^2
      rhos2[i] += den3*dmj2;
      rhos2[j] += den4*dmi2;

      // vector current
      JB[i] += den3*(u2+frj)*bj*xj;
      JB[j] += den4*(u1+fri)*bi*xi;

      // isospin vector current
      IB[i] += den3*(u2+frj)*bj*yj;
      IB[j] += den4*(u1+fri)*bi*yi;

      if(!withMomDep) continue;

      Vec4 pc1 = part[i]->getPcan(optVdot);
      Vec4 pc2 = part[j]->getPcan(optVdot);

      Vec4 dp = pc1 - pc2;
      //double dpsq = dp.m2Calc();
      //double psq1 = dpsq - pow2(dp * u2);
      //double psq2 = dpsq - pow2(dp * u1);

      Vec4 pCM=pc1 + pc2;
      double sInv = pCM.m2Calc();
      double pma = pow2(dp * pCM)/sInv;
      double psq1 = dp.m2Calc() - optPV * pma;
      double psq2=psq1;

      // momentum-dependent scalar potential
      vmoms[i] += potential->Vmds(psq1)*den3;
      vmoms[j] += potential->Vmds(psq2)*den4;

      // momentum-dependent vector potential
      vmom4[i] += potential->Vmdv(psq1)*den3*u2;
      vmom4[j] += potential->Vmdv(psq2)*den4*u1;

    }
  }

}

Vec4 RQMDpdm::omegaPotential(int i, Vec4& ui)
{
  //if(rho[i]<1e-15) return Vec4(0.0,0.0,0.0,0.0);

  double omi = max(0.0,omega[i].m2Calc());
  double rhi = max(0.0,rho_meson[i].m2Calc());

  // GeVfm^3
  double dui=HBARC3/(mOmega2 + C4*omi + cOmegaRho*rhi);

  // domega/dj*u
  double du = 2*C4*omega[i]*ui/(mOmega2 + 3*C4*omi + cOmegaRho*rhi);
  Vec4 domegadj= dui*(ui - du*omega[i]);

  //if(rho[i]<1e-15) cout<< "opmegaPot= "<< domegadj/2*mOmega2/HBARC3<<endl;

  if(optPotentialEval==0) return domegadj/2*mOmega2/HBARC3;
  if(optPotentialEval>=5) return domegadj*mOmega2/HBARC3;
  if(rho[i]<1e-15 || C4==0.0) return domegadj/2*mOmega2/HBARC3;

  double jomega=JB[i]*domegadj;
  double Vomega = 0.5*mOmega2*omi + 0.25*(C4*omi*omi + cOmegaRho*omi*rhi);
  double Vn=Vomega/(rho[i]*rho[i])/HBARC3;

  // dimension in GeVfm^3 This is not correct for anti-baryons.
  Vec4 dv=domegadj - JB[i]/(rho[i]*rho[i])*(jomega - 2*Vn*JB[i]*ui) - Vn*ui;

  /*
  cout << " c4= "<< C4 << " come= "<< cOmegaRho<<endl;
  cout << "Vomega= "<< Vomega << endl;
  cout << " J= "<< JB[i];
  cout << " rho_meson= "<< rhi/HBARC3<<endl;
  cout << " m^2*omega= "<< mOmega2*omega[i]/HBARC3;
  cout << " rho= "<< JB[i].mCalc()<<endl;
  cout << " rho= "<<  rho[i] << " m^2omega "<< mOmega2*sqrt(omi)/HBARC3<<endl;

  cout << "seond= "<< jomega << " " << 2*Vn*JB[i]*ui <<endl;
  cout << "dom= "<< domegadj;
  cout << " thrid "<< Vn*ui;
  */

  // dimensionless
  return dv*mOmega2/HBARC3;
}

// The transverse distance is defined by the rest frame of a particle.
void RQMDpdm::computeForce()
{
  double fact= transportModel==2 ? 1.0: 0.5;

  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    Vec4 pc1 = part[i]->getPcan(optVdot);
    Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : p1;
    Vec4 vk1 = optRQMDevolution > 0 ? pk1/(pk1*pHat) : pk1/pk1[0];
    if(optPotentialArg>=1) p1 = part[i]->getPcan(optVdot);
    double meff1 = part[i]->getEffectiveMass();
    double m1= optPotentialArg>=1 ? part[i]->getMass():meff1;
    Vec4 u1  = p1/m1;
    double fi=1.0;
    //u1 = p1/p1[0];
    //double fi= m1/p1[0];

    //Vec4 u1  = p1/p1.mCalc();
    double fengi = optCollisionOrdering == 101 ? meff1/(pk1*pHat) :
           optCollisionOrdering >  101 ? meff1*part[i]->getMass()/(pk1*p1) : meff1/pk1[0];
    if(optPotentialEval==4) {
      u1=pk1/meff1;
      vk1=u1;
      fengi=1.0;
    }

    int bar1   = part[i]->baryon()/3;       // baryon number
    double xvi = part[i]->facPotential(5); // omega coupling
    double dmi = part[i]->facPotential(6); // sigma coupling
    double gpi = part[i]->facPotential(9);  // phi coupling
    double gri = part[i]->facPotential(10); // rho coupling

    // omega, rho and phi mesons
    double omi = max(0.0,omega[i].m2Calc());
    double rhi = max(0.0,rho_meson[i].m2Calc());
    double ri=C4/mRho2*rhi;
    double si=cOmegaRho/mRho2*omi;
    double dri=1.0/(1.0 + 3*ri + si);
    Vec4 dOmegadJ1 = omegaPotential(i, vk1);

    for(int j=i+1; j< NV; j++) {

      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      Vec4 pk2 = optVdot <= 1 ? part[j]->getPkin(optVdot) : p2;
      Vec4 vk2 = optRQMDevolution > 0 ? pk2/(pk2*pHat) : pk2/pk2[0];
      if(optPotentialArg>=1) p2 = part[j]->getPcan(optVdot);
      double meff2 = part[j]->getEffectiveMass();
      double m2=optPotentialArg>=1? part[j]->getMass():meff2;
      Vec4 u2  = p2/m2;
      double fj=1.0;

      //u2 = p2/p2[0];
      //double fj= m2/p2[0];

      //Vec4 u2  = p2/p2.mCalc();
      double fengj = optCollisionOrdering == 101 ? meff2/(pk2*pHat) :
             optCollisionOrdering  > 101 ? meff2*part[j]->getMass()/(pk2*p2) : meff2/pk2[0];
      if(optPotentialEval==4) {
	u2 = pk2/meff2;
        vk2=u2;
        fengj=1.0;
      }

      int bar2  = part[j]->baryon()/3;
      double xvj= part[j]->facPotential(5);
      double dmj= part[j]->facPotential(6);
      double gpj= part[j]->facPotential(9);  // phi coupling
      double grj= part[j]->facPotential(10); // rho coupling


      // Scalar part
      //double fsky1 = -wmG*fengi*rhom[i][j]*dmj*rhos2[i];
      //double fsky2 = -wmG*fengj*rhom[j][i]*dmi*rhos2[j];
      double s1 = fengi*dmj*rhos2[i] * fj;
      double s2 = fengj*dmi*rhos2[j] * fi;

      // omega
      double omj = max(0.0,omega[j].m2Calc());
      double rhj = max(0.0,rho_meson[j].m2Calc());
      double rj=C4/mRho2*rhj;
      double sj=cOmegaRho/mRho2*omj;
      double drj=1.0/(1.0 + 3*rj + sj);
      Vec4 dOmegadJ2 = omegaPotential(j, vk2);

      // Vector part
      double facP1=Comega2*bar1*bar2*xvi*xvj;      // omega
      double facP2=fact*bar1*bar2*gri*grj/mRho2fm;  // rho
      double facP3=fact*bar1*bar2*gpi*gpj/mPhi2fm;  // phi
      double facpi = facP1*(dOmegadJ1*u2) + (u2 * vk1)*(facP2*dri + facP3);
      double facpj = facP1*(dOmegadJ2*u1) + (u1 * vk2)*(facP2*drj + facP3);
      //facpi = 0.5*facP1*(vk1*u2);
      //facpj = 0.5*facP1*(vk2*u1);

      //fsky1 += -wmG*rhom[i][j]*facpi;
      //fsky2 += -wmG*rhom[j][i]*facpj;

      double fsky1 = -wmG*rhom[i][j]*( s1 + facpi);
      double fsky2 = -wmG*rhom[j][i]*( s2 + facpj);

      Vec4 dr = r1 - r2;
      double rbi = dr*u1;
      double rbj = dr*u2;
      Vec4 dr2ijri =  2*(dr - rbj*u2);  // R^2_{ij}/dr_i
      Vec4 dr2jiri =  2*(dr - rbi*u1);  // R^2_{ji}/dr_i
      Vec4 dr2ijrj =  -dr2ijri;
      Vec4 dr2jirj =  -dr2jiri;

      Vec4 dr2jipi = 0.0;    // R^2_{ji}/dp_i
      Vec4 dr2ijpj = 0.0;    // R^2_{ij}/dp_j
      Vec4 dr2ijpi = 0.0;
      Vec4 dr2jipj = 0.0;
      if(optMomDerKernel) {
        dr2jipi =  2*dr*rbi/m1;     // R^2_{ji}/dp_i
        dr2ijpj =  2*dr*rbj/m2;     // R^2_{ij}/dp_j
      }
       
      // Overlap of relativistic Gaussian
      if(optPotentialEval==4) {
        Vec4 dq = rGaussDistance(dr,u1,u2);
	dr2ijri =  2*dq;  // R^2_{ij}/dr_i
        dr2jiri =  2*dq;  // R^2_{ji}/dr_i
        dr2ijrj =  -dr2ijri;
        dr2jirj =  -dr2jiri;
        dr2jipi =  0.0;
        dr2ijpj =  0.0;

      // two-body distance is defined by the two-body c.m. frame.
      } else if(optTwoBodyDistance==2) {
        Vec4 pCM = p1 + p2;
        Vec4 bbi = pCM/pCM[0] - optP0dev*p1/p1[0];
        Vec4 bbj = pCM/pCM[0] - optP0dev*p2/p2[0];
        double sInv = pCM.m2Calc();
        double rbij = -dr * pCM/sInv;
        dr2ijri =  2*(dr + rbij*pCM);
        dr2jiri =  dr2ijri;
        dr2jirj = -dr2ijri;
        dr2ijrj = -dr2jiri;
        if(optMomDerKernel) {
          dr2ijpi = 2*(dr + pCM[0]*rbij*bbi)*rbij;
          dr2jipi = dr2ijpi;
          dr2jipj = 2*(dr + pCM[0]*rbij*bbj)*rbij;
          dr2ijpj = dr2jipj;
        }
      }


      // correct the sign of the time-component.
      dr2ijri[0] *= -1.0;
      dr2jiri[0] *= -1.0;
      dr2ijrj[0] *= -1.0;
      dr2jirj[0] *= -1.0;

      dr2jipi[0] *= -1.0;
      dr2ijpj[0] *= -1.0;
      dr2ijpi[0] *= -1.0;
      dr2jipj[0] *= -1.0;

      force[i]  += -fsky1*dr2ijri - fsky2*dr2jiri;
      force[j]  += -fsky1*dr2ijrj - fsky2*dr2jirj;
      forcer[i] +=  fsky1*dr2ijpi + fsky2*dr2jipi;
      forcer[j] +=  fsky1*dr2ijpj + fsky2*dr2jipj;

      // Derivative of p0/m term.
      if(optDerivative) {
        Vec4 devV1 = (optP0dev*p1/p1[0]*vk2[0] - vk2)/m1;
        Vec4 devV2 = (optP0dev*p2/p2[0]*vk1[0] - vk1)/m2;
        forcer[i] += devV1*rhom[j][i]*facpj;
        forcer[j] += devV2*rhom[i][j]*facpi;
      }

      if(!withMomDep) continue;

      Vec4 pc2 = part[j]->getPcan(optVdot);
      Vec4 dp = pc1 - pc2;

      //double psq = dp.m2Calc();
      // distance squared in the rest frame of particle 2
      //double dot4j = dp * p2 / m2;
      //double psq2 = psq - optPV*dot4j*dot4j;

      // distance squared in the rest frame of particle 1
      //double dot4i = (dp * p1)/m1;
      //double psq1 = psq - optPV*dot4i*dot4i;

      Vec4 pCM = pc1 + pc2;
      double sInv = pCM.m2Calc();
      double pma = pow2(dp * pCM)/sInv;
      double psq1 = dp.m2Calc() - optPV * pma;
      double psq2 = psq1;


      // derivatives
      //Vec4 bi = p1/p1.e();
      //Vec4 bb = optP0dev * p2.e()*bi - p2;
      //Vec4 dp2ijpi = 2*(dp - optP0dev*dp[0]*bi + bb*dot4j/m2);
      //Vec4 dp2jipi = 2*(dp - optP0dev*dp[0]*bi - bb*dot4i/m1);

      //Vec4 bj = p2/p2.e();
      //Vec4 bb2 = optP0dev * p1.e()*bj - p1;
      //Vec4 dp2ijpj = 2*(-dp + optP0dev*dp[0]*bj + bb2*dot4j/m2);
      //Vec4 dp2jipj = 2*(-dp + optP0dev*dp[0]*bj - bb2*dot4i/m1);

      Vec4 v1 = pc1/pc1[0];
      Vec4 v2 = pc2/pc2[0];
      Vec4 bbi = pCM/pCM[0] - optP0dev*v1;
      Vec4 bbj = pCM/pCM[0] - optP0dev*v2;
      Vec4 dp2ijpi = 2*( dp - optP0dev*dp[0]*v1 + optPV * pCM[0]/sInv*pma*bbi);
      Vec4 dp2ijpj = 2*(-dp + optP0dev*dp[0]*v2 + optPV * pCM[0]/sInv*pma*bbj);
      Vec4 dp2jipi = dp2ijpi;
      Vec4 dp2jipj = dp2ijpj;


      // add scalar part
      double fmomdi = -wmG*fengi*potential->devVmds(psq2)*rhom[i][j];
      double fmomdj = -wmG*fengj*potential->devVmds(psq1)*rhom[j][i];
      double fmomei =     fengi*potential->devVmes(psq2)*rhom[i][j];
      double fmomej =     fengj*potential->devVmes(psq1)*rhom[j][i];

      force[i]  += -fmomdi*dr2ijri - fmomdj*dr2jiri;
      force[j]  += -fmomdi*dr2ijrj - fmomdj*dr2jirj;
      forcer[i] +=  fmomei*dp2ijpi + fmomej*dp2jipi
	          + fmomdi*dr2ijpi + fmomdj*dr2jipi;
      forcer[j] +=  fmomei*dp2ijpj + fmomej*dp2jipj
	          + fmomdi*dr2ijpj + fmomdj*dr2jipj;


      // vector part
      double vf1=1.0;
      double vf2=1.0;
      if(optVectorPotential==1) {
        vf1 = vk1 * u2;
        vf2 = vk2 * u1;
      }
      fmomdi = -wmG*potential->devVmdv(psq2)*rhom[i][j]*vf1;
      fmomdj = -wmG*potential->devVmdv(psq1)*rhom[j][i]*vf2;
      fmomei =     potential->devVmev(psq2)*rhom[i][j]*vf1;
      fmomej =     potential->devVmev(psq1)*rhom[j][i]*vf2;

      force[i]  += -fmomdi*dr2ijri   - fmomdj*dr2jiri;
      force[j]  += -fmomdi*dr2ijrj   - fmomdj*dr2jirj;

      forcer[i] +=  fmomei*dp2ijpi + fmomej*dp2jipi
	          + fmomdi*dr2ijpi + fmomdj*dr2jipi;
      forcer[j] +=  fmomei*dp2ijpj + fmomej*dp2jipj
	          + fmomdi*dr2ijpj + fmomdj*dr2jipj;

      if(optDerivative) {
        double facm1 = -fmomdj/(wmG*vf2);
        double facm2 = -fmomdi/(wmG*vf1);
        Vec4 devV1 = (optP0dev*p1/p1[0]*vk2[0] - vk2)/m1;
        Vec4 devV2 = (optP0dev*p2/p2[0]*vk1[0] - vk1)/m2;
        forcer[i] += facm1*devV1;
        forcer[j] += facm2*devV2;
      }

    } // end loop over j
  } // end loop over i

}

// The transverse distance is defined by the rest frame of a particle.
void RQMDpdm::qmdMatrix(int i, Vec4& r1)
{
  rhos[i]=0.0;
  rhos2[i]=0.0;
  JB[i]=0.0;
  IB[i]=0.0;
  vmoms[i]=0.0;
  vmom4[i]=0.0;

  // set factor for leading baryons within its formation time.
  double qfac1=1.0;
  if(optPotential<4) qfac1 = part[i]->getTf() > globalTime ? part[i]->qFactor() : 1.0;

  // baryon number
  int bi   = part[i]->baryon()/3;
  if(bi==0) {
    qfac1 = facMesonPot;
    bi=1;
  }
  Vec4 pc1 = part[i]->getPcan(optVdot);

  for(int j=0; j< NV; j++) {

      Vec4 r2 = part[j]->getR();
      Vec4 pk2 = optVdot <= 1 ? part[j]->getPkin(optVdot) : part[j]->getP();
      double m2 = part[j]->getEffectiveMass();
      Vec4 u2 = pk2/m2;
      Vec4 frj = optBaryonCurrent ? part[j]->forceR() : 0.0;
      int bj = part[j]->baryon()/3;
      double qfac2=1.0;
      if(optPotential<4) qfac2 = part[j]->getTf() > globalTime ? part[j]->qFactor() : 1.0;
      if(bj==0) {
	qfac2 = facMesonPot;
	bj=1;
      }
      double* gfac2= part[j]->facPotential();
      double xj=gfac2[5];
      double yj=gfac2[10];
      double dmj  = gfac2[6];
      double dmj2 = gfac2[7];

      Vec4 dr = r1 - r2;
      double drsq1 = dr.m2Calc() - pow2(dr*u2);
      double den1 = facG2 * exp(drsq1*wG2)*qfac1*qfac2/overSample;

      rhom[i][j] = den1;

      // scalar density times dm*/dsigma (coupling constant).
      rhos[i] += den1*dmj;

      // scalar density times d^2m*/dsigma^2
      rhos2[i] += den1*dmj2;

      // vector current
      JB[i] += den1*(u2+u2[0]*frj)*bj*xj;

      // isospin vector current
      IB[i] += den1*(u2+u2[0]*frj)*bj*yj;

      if(!withMomDep) continue;

      Vec4 pc2 = part[j]->getPcan(optVdot);
      Vec4 dp = pc1 - pc2;
      Vec4 pCM=pc1 + pc2;
      double sInv = pCM.m2Calc();
      double pma = pow2(dp * pCM)/sInv;
      double psq1 = dp.m2Calc() - optPV * pma;

      // momentum-dependent scalar potential
      vmoms[i] += potential->Vmds(psq1)*den1;

      // momentum-dependent vector potential
      vmom4[i] += potential->Vmdv(psq1)*den1*u2;

  }

}

// Compute sigma-field.
double RQMDpdm::sigmaField(int i, bool optSet)
{
  if(NV==0) return 0.0;

  double diff=0.0;
      double meff0 = part[i]->getEffectiveMass();
      double* gfac1= part[i]->facPotential();
      double m0=gfac1[1]; // M0
      double a1=gfac1[2]; // g1
      double a2=gfac1[3]; // g2
      double as=gfac1[4]; // parity
      double m1=gfac1[8]; // su3 symmetry breaking term

      double sigma,meff,spot;
      double dmi,dmi2;
      if(optRMF !=1) { // parity doublet or singlet model
        // Find sigma-field from bisection method.
	sigma = potential->sigma_bisec(rhos[i]*HBARC3 );

        // Effective mass including momentum-dependent potential.
        meff =  potential->mEff(sigma, m0, a1, a2, as) + vmoms[i] + m1;

        // Scalar potential defined as m* = m + S.
        spot = meff - part[i]->getMass();
        dmi  = potential->dMdSigma(sigma, m0, a1, a2, as);
        dmi2 = potential->d2MdSigma2(sigma, m0, a1, as);

	if(spot>1e-10) {
        double rhb = sqrt(max(0.0,JB[i].m2Calc()));
        double smin=2*m0*abs(a2)/(2*a1*sqrt(a1*a1-a2*a2));
	cout << "spot>0 id= "<< part[i]->getID()
	  << " m= "<< part[i]->getMass()
	  << " meff= "<< meff
	  << scientific <<" spot "<< spot
	  <<" sigma="<< sigma
	  << scientific
	  << " rhob= "<< rhb
	  << " rhos= "<< rhos[i]*HBARC3
	  << fixed
	  << " vmoms= "<< vmoms[i]
	  << " m0 "<< m0
	  << " m1 "<< m1
	  << " a1 "<<  a1
	  << " a2 "<<  a2
	  << " as "<<  as
          << " meff_vac= "<<potential->mEff(f_pion, m0, a1, a2, as) + m1
          << " meff0= "<<potential->mEff(0.0, m0, a1, a2, as) + m1
          << " sigma_min= "<< smin
          << " m_min= "<< potential->mEff(smin, m0, a1, a2, as) + m1
	  <<endl;
	}

      } else { //Walecka model
	if(a2==0.0) {
	  cout << "RQMDpdm::sigmaField a2= "<< a2 << " m0= "<< m0
	    << " id= "<< part[i]->getID()
	    <<  "meff= "<< meff0
	    << " a1= "<< a1
	    << " as= "<< as
	    << " m1= "<< m1
	    <<endl;
	  exit(1);
	}
	double g2 = optPotentialEval==0 ? 0.5*a2: a2;
        double m = part[i]->getMass();
	double sigmax= abs((m + vmoms[i])/g2);
	if(isinf(sigmax)) {
  	  cout << "RQMpdm::sigmaField sigmax= "<< sigmax << " a2= "<< a2 << " id= "<< part[i]->getID()<<endl;
	  exit(1);
	}

        sigma = potential->sigma_bisecRMF(rhos[i]*HBARC3, g2,sigmax);
	spot = g2*sigma;
	meff = m + spot + vmoms[i];
	if(meff<0.0) {
	  cout << "RQMDpdm::sigmax= "<< sigmax << " m= "<< m
	    << " sigma= "<< sigma
	    << " a2= "<< a2
	    << " spot= "<< spot << " vm= "<< vmoms[i]
	    <<endl;
	}
	dmi = a2;
	dmi2 = 0.0;
      }

      // Save relevant quantities.
      part[i]->setPotS(spot,optSet);
      part[i]->setPotSm(vmoms[i]);
      part[i]->setRhoS(rhos[i]);
      part[i]->setPotentialParam(sigma,0);
      part[i]->setPotentialParam(dmi,6);
      part[i]->setPotentialParam(dmi2,7);

      diff += abs(meff-meff0);

  return diff;
}

// Compute omega-field.
void RQMDpdm::omegaField(int i)
{
  // Compute invariant baryon density.
  rho[i] = sqrt(abs(JB[i].m2Calc()));
  part[i]->setRhoB(rho[i]);

  if(rho[i]<1e-15) {
      omega[i] = Vec4(0.0,0.0,0.0,0.0);
      rho_meson[i] = Vec4(0.0,0.0,0.0,0.0);
      return;
  }

  // Compute omega-field in the local density approximation.
  if(optRhoMeson==0) {
      Vec4 jb = g_omega*JB[i]*HBARC3;
      Vec4 beta = jb/jb[0];
      omega[i] = Vec4(0.0,0.0,0.0,potential->Omega(1.0,max(0.0,jb.mCalc())));
      omega[i].bst(beta[1],beta[2],beta[3]);
    return;
  }

    // boost baryon current
    Vec4 jb = g_omega*JB[i]*HBARC3;
    Vec4 beta = jb/jb[0];
    double jb0=jb.mCalc();

    // boost isospin current
    Vec4 ib = IB[i]*HBARC3;
    Vec4 beta2 = ib/ib[0];
    double ib0=ib.mCalc();

    // initial omega and rho fields at rest.
    double om0 = jb0/mOmega2;
    double rh0 = ib0/mRho2;

    double mo2 = mOmega2 + cOmegaRho*rh0*rh0;
    double rh2 = mRho2   + cOmegaRho*om0*om0;
    double eps=10;
    int ntry=0;
    do {
      om0 = potential->Omega(mo2,C4,jb0);
      rh0 = potential->Omega(rh2,C4,ib0);
      mo2 = mOmega2 + cOmegaRho*rh0*rh0;
      rh2 = mRho2   + cOmegaRho*om0*om0;
      eps = abs(mo2*om0 + C4*om0*om0*om0 - jb0 + rh2*rh0 + C4*rh0*rh0*rh0 - ib0);
      if(++ntry > 10) break;
    } while(eps > 1e-15);

    // boost back omega field to the computational frame.
    Vec4 om=Vec4(0.0, 0.0, 0.0, om0);
    om.bst(beta[1],beta[2],beta[3]);
    omega[i]=om;

    // boost back rho field to the computational frame.
    Vec4 rh=Vec4(0.0, 0.0, 0.0, rh0);
    rh.bst(beta2[1],beta2[2],beta2[3]);
    rho_meson[i]=rh;

}

Vec4 RQMDpdm::omegaPotentialx(int i, Vec4& ui)
{
  if(rho[i]<1e-15) return Vec4(0.0,0.0,0.0,0.0);

  double omi = max(0.0,omegax[i].m2Calc());
  double rhi = max(0.0,rho_mesonx[i].m2Calc());

  // GeVfm^3
  double dui=HBARC3/(mOmega2 + C4*omi + cOmegaRho*rhi);

  // domega/dj*u
  double du = 2*C4*omegax[i]*ui/(mOmega2 + 3*C4*omi + cOmegaRho*rhi);
  Vec4 domegadj= dui*(ui - du*omegax[i]);

  return domegadj*mOmega2/HBARC3;
}

// Compute single particle potential.
double RQMDpdm::singleParticlePotential(int i, Vec4& pkin)
{
  double mi = part[i]->getEffectiveMass();
  Vec4 pi = optPotentialArg>=1 ? part[i]->getPcan(optVdot) : part[i]->getP();

  if(part[i]->rhob() < 1e-15) {
    //rhos2[i]=0.0;
    pkin = pi;
    return sqrt(mi*mi + pi.pAbs2());
  }

  double* gfac= part[i]->facPotential();
  double sigma=gfac[0];
  double xv=gfac[5];
  double dmi=gfac[6];     // dm*/dsigma
  double C_phi = gfac[9]/mPhi2fm;
  double C_rho = gfac[10]/mRho2fm;

  // dsigma/drho_s = (del^2 V_sigma/del sigma^2)^{-1}
  double dsigma = optRMF==1 ? potential->dSigmaRMF(sigma):
                  optRMF==2 ? potential->dSigma(sigma):
		              potential->dSigma2(sigma);
  // dm^*/dsigma * dsigma/drho_s
  //rhos2[i] = dmi*dsigma/( 1.0 - dsigma*rhos2[i]);
  rhos2[i] = dmi*dsigma;

  double vsky1 = g_omega*xv*part[i]->baryon()/3;
  double vsky2 = C_phi*part[i]->baryon()/3;
  double vsky3 = C_rho*part[i]->baryon()/3;
  Vec4 V = vsky1 * omega[i] + vsky2*JB[i] + vsky3*IB[i] + vmom4[i];
  pkin = pi - V;
  //double m = part[i]->getEffectiveMass();

  // mass-shell correction.
  pkin[0]=sqrt(mi*mi+pkin.pAbs2());

  // save potentials
  part[i]->setPotV(V);
  part[i]->setPotVm(vmom4[i]);
  return pkin[0] + V[0];

}


void RQMDpdm::computeForce(int i, Vec4& rg, Vec4& r, Vec4& pk1, Vec4& forcei, Vec4& forceri,
      Vec4& forcej, Vec4& forcerj, bool optGamma)
{
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    Vec4 pc1 = part[i]->getPcan();
    if(optPotentialArg>=1) p1 = part[i]->getPcan(optVdot);
    Vec4 vk1 = optRQMDevolution > 0 ? pk1/(pk1*pHat) : pk1/pk1[0];
    double meff1 = part[i]->getEffectiveMass();
    double fengi = optCollisionOrdering == 101 ? meff1/(pk1*pHat) :
           optCollisionOrdering >  101 ? meff1*part[i]->getMass()/(pk1*p1) : meff1/pk1[0];

    int bar1   = part[i]->baryon()/3;       // baryon number
    double xvi = part[i]->facPotential(5);  // omega coupling
    double dmi = part[i]->facPotential(6);
    double gpi = part[i]->facPotential(9);  // phi coupling
    double gri = part[i]->facPotential(10); // rho coupling
    Vec4 u1 = pk1/meff1;
    Vec4 u1a=u1;
    u1a[0]=0.0;

    // This part should be modified when omega quartic term is included.
    double omi = max(0.0,omega[i].m2Calc());
    double rhi = max(0.0,rho_meson[i].m2Calc());
    double ri=C4/mRho2*rhi;
    double si=cOmegaRho/mRho2*omi;
    double dri=1.0/(1.0 + 3*ri + si);
    Vec4 dOmegadJ1 = omegaPotential(i, vk1);

    for(int j=0; j< NV; j++) {

      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getPkin(optVdot);
      Vec4 pk2 = optVdot <= 1 ? part[j]->getPkin(optVdot) : part[j]->getP();
      Vec4 vk2=pk2/pk2[0];
      vk2[0]=0.0;
      Vec4 pc2 = part[j]->getP();
      double meff2 = part[j]->getEffectiveMass();
      double fengj = optCollisionOrdering == 101 ? meff2/(pk2*pHat) :
           optCollisionOrdering >  101 ? meff2*part[j]->getMass()/(pk2*p2) : meff2/pk2[0];
      Vec4 u2  = pk2/meff2;
      Vec4 u2a= u2;
      u2a[0]=0.0;

      int bar2  = part[j]->baryon()/3;
      double xvj= part[j]->facPotential(5);
      double dmj= part[j]->facPotential(6);
      double gpj= part[j]->facPotential(9);  // phi coupling
      double grj= part[j]->facPotential(10); // rho coupling
      // omega
      double omj = max(0.0,omega[j].m2Calc());
      double rhj = max(0.0,rho_meson[j].m2Calc());
      double rj=C4/mRho2*rhj;
      double sj=cOmegaRho/mRho2*omj;
      double drj=1.0/(1.0 + 3*rj + sj);

      Vec4 dr = r - r2;
      double rbj = dr*u2;
      Vec4 dr2ijri =  2*(dr - rbj*u2);  // R^2_{ij}/dr_i
      Vec4 dr2jiri =  dr2ijri;
      dr2ijri[0] *= -1.0;
      dr2jiri[0] *= -1.0;

      // Vector part
      double facP1=Comega2*xvi*xvj;  // omega
      double facP2=gri*grj/mRho2fm;  // rho
      double facP3=gpi*gpj/mPhi2fm;  // phi

      Vec4 rg2=0.0;
      if(optGamma) {

	Vec4 dr12=r1-r2;
        dr2ijri =  2*(dr12 - (dr12*u2)*u2);  // R^2_{ij}/dr_i
        dr2ijri[0] *= -1.0;

        dr2jiri =  2*(dr12 - (dr12*u1)*u1);  // R^2_{ij}/dr_i
        dr2jiri[0] *= -1.0;


	double p0=0.0;
	// position around x_j
        double aa=u2[0]*(u2[0]+1);
        rg2= sqrt(widG)*(rg+(u2*rg)*u2/aa);
	Vec4 dr1=r-r1;

      if(optIntMC==3 || optIntMC==13) {

	// int maxit=maxIt;
      for(int itry=0;itry<1;itry++) {
        qmdMatrix(j,rg2);
        double diff=sigmaField(j,true);
        if(diff <1e-5) break;
      }
      omegaField(j);

      Vec4 pkin;
      p0 = singleParticlePotential(j,pkin);

      } else {
        rg2=r1-r2;
	//dr1=r2-r1;
        double m = part[j]->getEffectiveMass();
        if(optVectorPotential==1 && optVdot==0) {
          Vec4 pk= part[j]->getP() - part[j]->potv();
          p0 = sqrt( m*m + pk.pAbs2());
        } else {
         p0 = sqrt( m*m + part[j]->pAbs2());
        }
        p0 += part[j]->potv(0);
      }

	// du_i/dx_i
        Vec4 dOmegadJx = omegaPotentialx(i, dr1);
        double eij= (dr1*u1)*dmj*rhos2x[i];
        double facpi = facP1*(dOmegadJx*u2) + (u2 * dr1)*(facP2*dri + facP3);
        eij += facpi*bar1*bar2;
        //forceri += wG2*(dr1*u1)*eij*dr2ijri*rhomx[i][j]/meff1/(1-eij*rhomx[i][j]);
        forceri += 2*wG2*(dr1*u1)*eij*dGij[i][j]/meff1;


	// dgam_i/dx_i
	//fsky1= -dmj*rhos2x[i]*u1a.pAbs2();
        //facpi = facP1*(omegaPotentialx(i, u1a)*u2) + (u2 * u1a)*(facP2*dri + facP3);
        //fsky1 += facpi*bar1*bar2;
        //forcei += -fsky1*dr2ijri*wG2*rhomx[i][j]/meff1*fengi*fengi/(1-eij*rhomx[i][j]);
	forcei += forceD[i][j]*dGij[i][j];

	// du_j/dx_i
        dOmegadJx = omegaPotentialx(j, rg2);
        double eji = (rg2*u2)*dmi*rhos2x[j];
        double facpj = facP1*(dOmegadJx*u1) + (u1 * rg2)*(facP2*drj + facP3);
        eji += facpj*bar1*bar2;
        //forcerj += wG2*(rg2*u2)*eji*dr2jiri*rhomx[j][i]/meff2/(1-eji*rhomx[j][i]);
        //forcerj += p0*(rg2*u2)*2*wG2*eji/meff2*dGji[j][i];
        //forcerj += -p0*(rg2*u2)*2*wG2*(dr12*forceD[j][i])*dGji[j][i];
        forcerj += -p0*(rg2*u2)*2*wG2*(dr12*forceD[j][i])*rhomx[j][i]*dr2jiri*2*wG2;


	// dgam_j/dx_i
	//double fsky2= -dmi*rhos2x[j]*u2a.pAbs2();
        //facpj = facP1*(omegaPotentialx(j, u2a)*u1) + (u1 * u2a)*(facP2*drj + facP3);
        //fsky2 += facpj*bar1*bar2;
        //forcej += -fsky2*dr2jiri*wG2*rhomx[j][i]/meff2*fengj*fengj/(1-eji*rhomx[j][i]);
	//forcej += -p0*(forceD[j][i]*vk2)/u2[0]*dGji[j][i];
	forcej += -p0*(forceD[j][i]*vk2)/u2[0]*rhomx[j][i]*dr2jiri*2*wG2;


      } else {
        // Scalar part
        double s1 = fengi*dmj*rhos2[i];
        // Vector part
        double facpi = facP1*(dOmegadJ1*u2) + (u2 * vk1)*(facP2*dri + facP3);
        double fsky1 = -wG2*rhom[i][j]*(s1+facpi*bar1*bar2);
        forcei += -fsky1*dr2ijri;
      }

      // under construction for the option optGamma
      if(!withMomDep) continue;

      Vec4 dp = pc1 - pc2;
      Vec4 pCM = pc1 + pc2;
      double sInv = pCM.m2Calc();
      double pma = pow2(dp * pCM)/sInv;
      double psq1 = dp.m2Calc() - optPV * pma;
      //double psq2=psq1;

      Vec4 v1 = pc1/pc1[0];
      Vec4 bbi = pCM/pCM[0] - optP0dev*v1;
      Vec4 dp2ijpi = 2*( dp - optP0dev*dp[0]*v1 + optPV * pCM[0]/sInv*pma*bbi);
      Vec4 dp2jipi = dp2ijpi;

      // add scalar part
      double fmomdi = -wG2*fengi*potential->devVmds(psq1)*rhom[i][j];
      double fmomei =     fengi*potential->devVmes(psq1)*rhom[i][j];
      double fmomdj=0.0,fmomej=0.0;
      double ru1=0.0, ru2=0.0;
      if(optGamma) {
	ru1=(r-r1)*u2;
	double mvpot=potential->devVmds(psq1)*rhomx[i][j]*wG2;
        fmomdi = -mvpot*u1a.pAbs2()/meff1*fengi*fengi;
        fmomei = -mvpot*ru1*ru1*meff1;
	ru2=rg2*u1;
        fmomdj = -mvpot*u2a.pAbs2()/meff2*fengj*fengj;
        fmomej = -mvpot*ru2*ru2/meff2;
      }

      forcei  += -fmomdi*dr2ijri;
      forceri +=  fmomei*dp2ijpi;
      forcej  += -fmomdj*dr2jiri;
      forcerj +=  fmomej*dp2jipi;

      double mvpot=wG2*potential->devVmdv(psq1);
      // vector part
      if(optGamma) {
	double vf1 = u1a * u2;
	double vf2 = u2a * u1;
        fmomdi = -mvpot*rhomx[i][j]*vf1;
        fmomdj = -mvpot*rhomx[i][j]*vf2;
        fmomei = -mvpot*rhomx[i][j]*ru1*ru1;
        fmomej = -mvpot*rhomx[i][j]*ru2*ru2;
      } else {
        double  vf1 = vk1 * u2;
        fmomdi = -mvpot*rhom[i][j]*vf1;
        fmomei = potential->devVmev(psq1)*rhom[i][j]*vf1;
        fmomdj=0.0;
        fmomej=0.0;
      }
      forcei  += -fmomdi*dr2ijri;
      forceri +=  fmomei*dp2ijpi;
      forcej  += -fmomdj*dr2jiri;
      forcerj +=  fmomej*dp2jipi;

    } // end loop over j

}


// space integral is numerically evaluated.
void RQMDpdm::computeForceL()
{
  bool optSet= true;

    //optIntMC=1: dp^0/dx
    //optIntMC=2: dp^0/dx*gam(x)/gam(x_i)
    //optIntMC=3: dp^0/dx*gam(x)/gam(x_i)+ G(x-x_j)
    //optIntMC=11: p^0
    //optIntMC=12: p^0 + dgam/dx_i
    //optIntMC=13: p^0 + dgam/dx_i + G(x-x_j)

    //Vec4 forceg=0.0,forcerg=0.0,forcegj=0.0,forcergj=0.0;
    if((optIntMC>=3 && optIntMC<11) || optIntMC>11) {
      for(int itry=0;itry<maxIt;itry++) {
        setZero();
        qmdMatrix();
        double diff=sigmaField(optSet);
        if(diff <1e-5) break;
      }
      omegaField();
      singleParticlePotential();

      rhomx=rhom;
      rhos2x=rhos2;
      omegax=omega;
      rho_mesonx=rho_meson;
      computeForceDE();
      //computeForce(i,r1, r1,pk1, forceg, forcerg,forcegj,forcergj,true);
    }

  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : part[i]->getP();
    double meff1 = part[i]->getEffectiveMass();
    //double em1=pk1.mCalc();
    Vec4 u1  = pk1/meff1;

    Vec4 forcdi=0.0, forcdri=0.0,forcejh=0.0,forcerjh=0.0;
    if(optIntMC==5 || optIntMC==15) {
      computeForce(i, r1, r1,pk1,forcdi, forcdri,forcejh,forcerjh);
    }

    force[i]=0.0;
    forcer[i]=0.0;

    // Gauss integral
    if(optPotentialEval==5) {

    for(int j=0;j<nGaussPoint;j++)
    for(int k=0;k<nGaussPoint;k++)
    for(int l=0;l<nGaussPoint;l++) {
      Vec4 dr = Vec4(xG[j], xG[k], xG[l],0.0);
      Vec4 r = r1 + dr;
      double drsq1 = dr.m2Calc() - pow2(dr*u1);
      double rhoij= u1[0]*facG2*exp(drsq1*wG2);

      // compute sigma field.
      for(int itry=0;itry<maxIt;itry++) {
        qmdMatrix(i,r);
        double diff=sigmaField(i,optSet);
        if(diff <1e-5) break;
      }
      omegaField(i);
      Vec4 pkin;
      singleParticlePotential(i,pkin);
      Vec4 forcei=0.0, forceri=0.0,forcej=0.0,forcerj=0.0;
      computeForce(i, r, r,pkin,forcei, forceri,forcej,forcerj);
      forcer[i] += forceri*rhoij*wG[j]*wG[k]*wG[l];
      force[i]  += forcei*rhoij*wG[j]*wG[k]*wG[l];

      //Vec4 dr2ijri = 2*(dr - (dr*u1)*u1);
      //force[i]  += -p0*rhoij*dr2ijri*wG2*wG[j]*wG[k]*wG[l];

    }

    // Monte-Carlo sampling.
    } else {

    double aa=u1[0]*(u1[0]+1);
    Vec4 pkin;

    for(int iev=0;iev<nMCPoint;iev++) {

      Vec4 dr = Vec4(rndm->gauss(),rndm->gauss(),rndm->gauss(),0.0);
      Vec4 r= r1 + sqrt(widG)*(dr+(u1*dr)*u1/aa);
      r[0]=r1[0];
      // compute sigma field.
      for(int itry=0;itry<maxIt;itry++) {
        qmdMatrix(i,r);
        double diff=sigmaField(i,optSet);
        if(diff <1e-5) break;
      }
      omegaField(i);
      double p0 = singleParticlePotential(i,pkin);

      Vec4 forcei=0.0, forceri=0.0,forcej=0.0,forcerj=0.0;
      if(optIntMC<=10) {
        computeForce(i,dr,r,pkin, forcei, forceri,forcej,forcerj);
	double gg=1.0;
	if(optIntMC>=2) {
          meff1 = part[i]->getEffectiveMass();
          double gam  = pkin[0]/meff1;
	  //gg=gam/u1[0];
	  Vec4 dr1=r-r1;
          double rden = exp(-wG2*(pow2(dr1*pkin/meff1)-pow2(dr1*u1)));
	  gg=gam/u1[0]*rden;
          //double drsq2 = dr1.m2Calc() - pow2(dr1*pkin/meff1);
          //double gg = facG2 * exp(drsq2*wG2);
	}
        forcer[i] += forceri*gg;
        force[i]  += forcei*gg;

        if(optIntMC==3||optIntMC==4) {
	  forcei=0.0;forceri=0.0; forcej=0.0;forcerj=0.0;
	  computeForce(i,dr,r,pk1, forcei, forceri,forcej,forcerj,true);
	  force[i] += -(forcej+forcerj);
	} else if(optIntMC==5) {
	  force[i] += -(forcejh+forcerjh);
	}
      } else {
        Vec4 dr2ijri = 2*((r-r1) - ((r-r1)*u1)*u1);
        if(optIntMC>11 && optIntMC<=14) {
	  computeForce(i,dr,r,pk1, forcei, forceri,forcej,forcerj,true);
          //force[i]  += -p0*(dr2ijri*wG2*facG2 + forcei + forceri);
          force[i]  += -p0*(dr2ijri*wG2 + forcei + forceri);
	} else {
          //force[i]  += -p0*(dr2ijri*wG2*facG2 + forcdi + forcdri);
          force[i]  += -p0*(dr2ijri*wG2 + forcdi + forcdri);
	}

	// include the contribution from j
	if(optIntMC==13 || optIntMC==14) force[i] += -(forcej+forcerj);
	else if(optIntMC>=15) {
	  force[i] += -(forcejh+forcerjh);
	}
      }

    } // end loop over MC points
      force[i]  /= nMCPoint;
      forcer[i] /= nMCPoint;
    }

  } // end loop over particle i

}

void RQMDpdm::qmdMatrix(vector<Vec4>& rg)
{
  // loop over all baryons
  for(int i=0; i< NV; i++) {

    Vec4 r1  = part[i]->getR();
    Vec4 p1 = optPotentialArg>=1? part[i]->getPcan(optVdot) : part[i]->getPkin(optVdot);
    double m1 = optPotentialArg>=1? part[i]->getMass(): part[i]->getEffectiveMass();
    Vec4 u1 = p1/m1;

    //double aa=u1[0]*(u1[0]+1);
    //Vec4 rg = Vec4(rndm->gauss(),rndm->gauss(),rndm->gauss(),0.0);
    //Vec4 rg1= r1 + sqrt(widG)*(rg+(u1*rg)*u1/aa);
    //rg1[0]=r1[0];

    // set factor for leading baryons within its formation time.
    double qfac1=1.0;
    if(optPotential<4) qfac1 = part[i]->getTf() > globalTime ? part[i]->qFactor() : 1.0;

    // option for baryon current
    Vec4 fri = optBaryonCurrent ? part[i]->forceR() : 0.0;

    // baryon number
    int bi   = part[i]->baryon()/3;
    if(bi==0) {
      qfac1 = facMesonPot;
      bi=1;
    }

    // get potential factors
    double* gfac1= part[i]->facPotential();
    double xi=gfac1[5];     // factor for omega coupling
    double yi=gfac1[10];    // rho coupling x charge
    double dmi  = gfac1[6]; // dm/dsigma
    double dmi2 = gfac1[7]; // d^2m/dsigma^2

    for(int j=i; j< NV; j++) {

      Vec4 r2  = part[j]->getR();
      Vec4 p2 = optPotentialArg>=1 ? part[j]->getPcan(optVdot) : part[j]->getPkin(optVdot);
      double m2 = optPotentialArg>=1? part[j]->getMass():part[j]->getEffectiveMass();
      Vec4 u2 = p2/m2;
      Vec4 frj = optBaryonCurrent ? part[j]->forceR() : 0.0;
      int bj = part[j]->baryon()/3;
      double qfac2=1.0;
      if(optPotential<4) qfac2 = part[j]->getTf() > globalTime ? part[j]->qFactor() : 1.0;
      if(bj==0) {
	qfac2 = facMesonPot;
	bj=1;
      }
      double* gfac2= part[j]->facPotential();
      double xj=gfac2[5];
      double yj=gfac2[10];
      double dmj  = gfac2[6];
      double dmj2 = gfac2[7];

      //double bb=u2[0]*(u2[0]+1);
      //Vec4 rg2= r2 + sqrt(widG)*(rg+(u2*rg)*u2/bb);
      //rg2[0]=r2[0];

      Vec4 dr1 = rg[i] - r2;
      Vec4 dr2 = rg[j] - r1;
      double pfac=qfac1*qfac2/overSample;

      double drsq1 = dr1.m2Calc() - pow2(dr1*u2);
      double drsq2 = dr2.m2Calc() - pow2(dr2*u1);

      double den1 = facG2 * exp(drsq1*wG2)*pfac;
      double den2 = facG2 * exp(drsq2*wG2)*pfac;

      //if(i==j) { den1 /=2.0; den2 /=2.0; }
      if(i==j) den2=0.0;

      rhom[i][j] = den1;
      rhom[j][i] = den2;

      // scalar density times dm*/dsigma (coupling constant).
      rhos[i] += den1*dmj;
      rhos[j] += den2*dmi;

      // scalar density times d^2m*/dsigma^2
      rhos2[i] += den1*dmj2;
      rhos2[j] += den2*dmi2;

      // vector current
      JB[i] += den1*(u2+u2[0]*frj)*bj*xj;
      JB[j] += den2*(u1+u2[0]*fri)*bi*xi;

      // isospin vector current
      IB[i] += den1*(u2+u2[0]*frj)*bj*yj;
      IB[j] += den2*(u1+u2[0]*fri)*bi*yi;

      if(!withMomDep) continue;

      Vec4 pc1 = part[i]->getPcan(optVdot);
      Vec4 pc2 = part[j]->getPcan(optVdot);

      Vec4 dp = pc1 - pc2;
      Vec4 pCM=pc1 + pc2;
      double sInv = pCM.m2Calc();
      double pma = pow2(dp * pCM)/sInv;
      double psq1 = dp.m2Calc() - optPV * pma;
      double psq2=psq1;

      // momentum-dependent scalar potential
      vmoms[i] += potential->Vmds(psq1)*den1;
      vmoms[j] += potential->Vmds(psq2)*den2;

      // momentum-dependent vector potential
      vmom4[i] += potential->Vmdv(psq1)*den1*u2;
      vmom4[j] += potential->Vmdv(psq2)*den2*u1;

    }
  }

}

void RQMDpdm::computeForceDE()
{
  vector<vector<vector<double> > > E(NV,vector<vector<double>>(NV,vector<double>(NV,0.0)));;
  //forceD.assign(NV,0.0);
  std::vector<std::vector<Pythia8::Vec4> > B(NV,vector<Vec4>(NV));

  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    Vec4 pc1 = part[i]->getPcan();
    Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : p1;
    if(optPotentialArg>=1) p1 = part[i]->getPcan(optVdot);
    Vec4 vk1 = optRQMDevolution > 0 ? pk1/(pk1*pHat) : pk1/pk1[0];
    double meff1 = part[i]->getEffectiveMass();
    double m1= optPotentialArg>=1 ? part[i]->getMass():meff1;
    Vec4 u1  = p1/m1;
    Vec4 u1a  = u1;
    u1a[0]=0.0;
    double fengi = optCollisionOrdering == 101 ? meff1/(pk1*pHat) :
           optCollisionOrdering >  101 ? meff1*part[i]->getMass()/(pk1*p1) : meff1/pk1[0];
    if(optPotentialEval==4) {
      u1=pk1/meff1;
      vk1=u1;
      fengi=1.0;
    }

    int bar1   = part[i]->baryon()/3;       // baryon number
    double xvi = part[i]->facPotential(5); // omega coupling
    double dmi = part[i]->facPotential(6); // sigma coupling
    double gpi = part[i]->facPotential(9);  // phi coupling
    double gri = part[i]->facPotential(10); // rho coupling

    // omega, rho and phi mesons
    double omi = max(0.0,omega[i].m2Calc());
    double rhi = max(0.0,rho_meson[i].m2Calc());
    double ri=C4/mRho2*rhi;
    double si=cOmegaRho/mRho2*omi;
    double dri=1.0/(1.0 + 3*ri + si);
    //Vec4 dOmegadJ1 = omegaPotential(i, vk1);


    for(int j=i+1; j< NV; j++) {

      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      Vec4 pc2 = part[j]->getPcan();
      Vec4 pk2 = optVdot <= 1 ? part[j]->getPkin(optVdot) : p2;
      if(optPotentialArg>=1) p2 = part[j]->getPcan(optVdot);
      Vec4 vk2 = optRQMDevolution > 0 ? pk2/(pk2*pHat) : pk2/pk2[0];
      double meff2 = part[j]->getEffectiveMass();
      double m2=optPotentialArg>=1? part[j]->getMass():meff2;
      Vec4 u2  = p2/m2;
      Vec4 u2a  = u2;
      u2a[0]=0.0;
      double fengj = optCollisionOrdering == 101 ? meff2/(pk2*pHat) :
             optCollisionOrdering  > 101 ? meff2*part[j]->getMass()/(pk2*p2) : meff2/pk2[0];
      if(optPotentialEval==4) {
	u2 = pk2/meff2;
        vk2=u2;
        fengj=1.0;
      }

      int bar2  = part[j]->baryon()/3;
      double xvj= part[j]->facPotential(5);
      double dmj= part[j]->facPotential(6);
      double gpj= part[j]->facPotential(9);  // phi coupling
      double grj= part[j]->facPotential(10); // rho coupling

      // omega
      double omj = max(0.0,omega[j].m2Calc());
      double rhj = max(0.0,rho_meson[j].m2Calc());
      double rj=C4/mRho2*rhj;
      double sj=cOmegaRho/mRho2*omj;
      double drj=1.0/(1.0 + 3*rj + sj);
      //Vec4 dOmegadJ2 = omegaPotential(j, vk2);


      // Scalar part
      //double fsky1 = -wmG*fengi*rhom[i][j]*dmj*rhos2[i];
      //double fsky2 = -wmG*fengj*rhom[j][i]*dmi*rhos2[j];

      // Vector part
      double facP1=Comega2*bar1*bar2*xvi*xvj;      // omega
      double facP2=0.5*bar1*bar2*gri*grj/mRho2fm;  // rho
      double facP3=0.5*bar1*bar2*gpi*gpj/mPhi2fm;  // phi

      //fsky1 += -wmG*rhom[i][j]*facpi;
      //fsky2 += -wmG*rhom[j][i]*facpj;

	Vec4 dr12=r1-r2;
        Vec4 dr2ijri =  2*(dr12 - (dr12*u2)*u2);  // R^2_{ij}/dr_i
        Vec4 dr2jiri =  2*(dr12 - (dr12*u1)*u1);  // R^2_{ij}/dr_i
        Vec4 dr2ijrj =  -dr2ijri;
        Vec4 dr2jirj =  -dr2jiri;

        dr2ijri[0] *= -1.0;
        dr2jiri[0] *= -1.0;
        dr2ijrj[0] *= -1.0;
        dr2jirj[0] *= -1.0;

	// du_i/dx_i
        Vec4 dOmegadJx = omegaPotentialx(i, dr12);
        double eijs= (dr12*u1)*dmj*rhos2x[i];
        double facpi = facP1*(dOmegadJx*u2) + (u2 * dr12)*(facP2*dri + facP3);
        double eij = 2*wG2*(eijs + facpi*bar1*bar2*(dr12*u1))/meff1;
        dGij[j][i] = wG2*(dr2jirj*rhom[j][i] + eij*dr2ijrj*rhom[i][j]*rhom[j][i]);
        dGji[j][i] = wG2*dr2ijrj*rhom[i][j];

	// du_j/dx_i
        Vec4 dOmegadJy = omegaPotentialx(j, dr12);
        double ejis = (dr12*u2)*dmi*rhos2x[j];
        double facpj = facP1*(dOmegadJy*u1) + (u1 * dr12)*(facP2*drj + facP3);
        double eji = 2*wG2*(ejis + facpj*bar1*bar2*(dr12*u2))/meff2;
        dGij[i][j] = wG2*(dr2ijri*rhom[i][j] + eji*dr2jiri*rhom[i][j]*rhom[j][i]);
        dGji[i][j] = wG2*dr2jiri*rhom[j][i];


	// dgam_i/dx_i
	//double fsky1= dmj*rhos2x[i]*u1a.pAbs2();
	double fsky1= dmj*rhos2x[i];
        //facpi = facP1*(omegaPotentialx(i, u1a)*u2) + (u2 * u1a)*(facP2*dri + facP3);
        Vec4 vpot1= facP1*omegaPotentialx(i, u2) + u2*(facP2*dri + facP3);
        //fsky1 += facpi*bar1*bar2;
        //forceD[i][j] = -fsky1*dr2ijri*wG2*rhom[i][j]/meff1*fengi*fengi/(1-eij*rhom[i][j]);
        //forceD[i][j] = -fsky1/meff1*fengi*fengi;
        forceD[i][j] = (fsky1*u1 + vpot1)/meff1;
	B[i][j]=-2*wG2*dr12*(dr12*u2);
	B[j][i]=-2*wG2*dr12*(dr12*u1);

	// dgam_j/dx_i
	//double fsky2= dmi*rhos2x[j]*u2a.pAbs2();
	double fsky2= dmi*rhos2x[j];
        Vec4 vpot2 = facP1*omegaPotentialx(j, u1) + u1*(facP2*drj + facP3);
        //facpj = facP1*(omegaPotentialx(j, u2a)*u1) + (u1 * u2a)*(facP2*drj + facP3);
        //fsky2 += facpj*bar1*bar2;
        //forceD[j][i] = -fsky2*dr2jiri*wG2*rhom[j][i]/meff2*fengj*fengj/(1-eji*rhom[j][i]);
        forceD[j][i] = (fsky2*u2+vpot2)/meff2;
	//Hx[j][i] = forceD[j][i][1] + Vec4(1,0,1.0,1.0)*facP1/meff2;

	dUx[i][i] += wG2*dr2ijri*forceD[i][j][1]*rhom[i][j];
	dUy[i][i] += wG2*dr2ijri*forceD[i][j][2]*rhom[i][j];
	dUz[i][i] += wG2*dr2ijri*forceD[i][j][3]*rhom[i][j];

	dUx[j][j] += wG2*dr2jirj*forceD[j][i][1]*rhom[j][i];
	dUy[j][j] += wG2*dr2jirj*forceD[j][i][2]*rhom[j][i];
	dUz[j][j] += wG2*dr2jirj*forceD[j][i][3]*rhom[j][i];

	dUx[j][i]=-wG2*dr2jiri*forceD[j][i][1]*rhom[j][i];
	dUy[j][i]=-wG2*dr2jiri*forceD[j][i][2]*rhom[j][i];
	dUz[j][i]=-wG2*dr2jiri*forceD[j][i][3]*rhom[j][i];

	dUx[j][i]=-wG2*dr2jiri*forceD[j][i][1]*rhom[j][i];
	dUy[i][j]=-wG2*dr2ijrj*forceD[i][j][2]*rhom[i][j];
	dUz[i][j]=-wG2*dr2ijrj*forceD[i][j][3]*rhom[i][j];

	/*
	for(int k=0;k<NV;k++) {
          Vec4 pk3 = optVdot <= 1 ? part[k]->getPkin(optVdot) : part[k]->getP();
          double meff3 = part[k]->getEffectiveMass();
          Vec4 u3  = pk3/meff3;
          double facpij = facP1*(dOmegadJx*u3) + (u3 * dr12)*(facP2*dri + facP3);
          double facpji = facP1*(dOmegadJy*u3) + (u3 * dr12)*(facP2*drj + facP3);
          E[i][k][j] = 2*wG2*(dr12*u1)*(eijs + facpij*bar1*bar2)/meff1;
          E[j][k][i] = 2*wG2*(dr12*u2)*(ejis + facpji*bar1*bar2)/meff2;
	}
	*/


      // under construction for the option optGamma
      if(!withMomDep) continue;
      cout << " not implemented RQMD.PRM"<<endl;
      exit(1);

      Vec4 dp = pc1 - pc2;
      Vec4 pCM = pc1 + pc2;
      double sInv = pCM.m2Calc();
      double pma = pow2(dp * pCM)/sInv;
      double psq1 = dp.m2Calc() - optPV * pma;
      double psq2 = psq1;

      Vec4 v1 = pc1/pc1[0];
      Vec4 v2 = pc2/pc2[0];
      Vec4 bbi = pCM/pCM[0] - optP0dev*v1;
      Vec4 bbj = pCM/pCM[0] - optP0dev*v2;
      Vec4 dp2ijpi = 2*( dp - optP0dev*dp[0]*v1 + optPV * pCM[0]/sInv*pma*bbi);
      Vec4 dp2ijpj = 2*(-dp + optP0dev*dp[0]*v2 + optPV * pCM[0]/sInv*pma*bbj);
      Vec4 dp2jipi = dp2ijpi;
      Vec4 dp2jipj = dp2ijpj;

      // add scalar part
      double fmomdi = -wmG*fengi*potential->devVmds(psq2)*rhom[i][j];
      double fmomdj = -wmG*fengj*potential->devVmds(psq1)*rhom[j][i];
      double fmomei =     fengi*potential->devVmes(psq2)*rhom[i][j];
      double fmomej =     fengj*potential->devVmes(psq1)*rhom[j][i];

      force[i]  += -fmomdi*dr2ijri - fmomdj*dr2jiri;
      force[j]  += -fmomdi*dr2ijrj - fmomdj*dr2jirj;
      forcer[i] +=  fmomei*dp2ijpi + fmomej*dp2jipi;
	          //+ fmomdi*dr2ijpi + fmomdj*dr2jipi;
      forcer[j] +=  fmomei*dp2ijpj + fmomej*dp2jipj;
	          //+ fmomdi*dr2ijpj + fmomdj*dr2jipj;


      // vector part
      double vf1=1.0;
      double vf2=1.0;
      if(optVectorPotential==1) {
        vf1 = vk1 * u2;
        vf2 = vk2 * u1;
      }
      fmomdi = -wmG*potential->devVmdv(psq2)*rhom[i][j]*vf1;
      fmomdj = -wmG*potential->devVmdv(psq1)*rhom[j][i]*vf2;
      fmomei =     potential->devVmev(psq2)*rhom[i][j]*vf1;
      fmomej =     potential->devVmev(psq1)*rhom[j][i]*vf2;

      force[i]  += -fmomdi*dr2ijri   - fmomdj*dr2jiri;
      force[j]  += -fmomdi*dr2ijrj   - fmomdj*dr2jirj;

      forcer[i] +=  fmomei*dp2ijpi + fmomej*dp2jipi;
	          //+ fmomdi*dr2ijpi + fmomdj*dr2jipi;
      forcer[j] +=  fmomei*dp2ijpj + fmomej*dp2jipj;
	         // + fmomdi*dr2ijpj + fmomdj*dr2jipj;

      if(optDerivative) {
        double facm1 = -fmomdj/(wmG*vf2);
        double facm2 = -fmomdi/(wmG*vf1);
        Vec4 devV1 = (optP0dev*p1/p1[0]*vk2[0] - vk2)/m1;
        Vec4 devV2 = (optP0dev*p2/p2[0]*vk1[0] - vk1)/m2;
        forcer[i] += facm1*devV1;
        forcer[j] += facm2*devV2;
      }

    } // end loop over j
  } // end loop over i
 
  return;

  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : part[i]->getP();
    Vec4 vk1 = pk1/pk1[0];
    double meff1 = part[i]->getEffectiveMass();
    Vec4 u1=pk1/meff1;
    int bar1   = part[i]->baryon()/3;       // baryon number
    double xvi = part[i]->facPotential(5); // omega coupling
  for(int j=i+1; j< NV; j++) {
    Vec4 r2 = part[j]->getR();
    Vec4 pk2 = optVdot <= 1 ? part[j]->getPkin(optVdot) : part[j]->getP();
    //Vec4 vk2 = pk2/pk2[0];
    double meff2 = part[j]->getEffectiveMass();
    int bar2   = part[j]->baryon()/3;       // baryon number
    double xvj = part[j]->facPotential(5); // omega coupling
    double facP1=Comega2*bar1*bar2*xvi*xvj;      // omega

    Vec4 u2=pk2/meff2;
    Vec4 dr12=r1-r2;
    Vec4 dr2ijri =  2*(dr12 - (dr12*u2)*u2);  // R^2_{ij}/dr_i
    Vec4 dr2jiri =  2*(dr12 - (dr12*u1)*u1);  // R^2_{ij}/dr_i
    Vec4 dr2ijrj =  -dr2ijri;
    Vec4 dr2jirj =  -dr2jiri;
    dr2ijri[0] *= -1.0; dr2jiri[0] *= -1.0;
    dr2ijrj[0] *= -1.0; dr2jirj[0] *= -1.0;
    dGji[i][j] = wG2*dr2jiri*rhom[j][i];
    dGji[j][i] = wG2*dr2ijrj*rhom[i][j];

    Vec4 Bu0 = B[i][j][0]*(vk1[1]*dUx[i][i]+vk1[2]*dUy[i][i]+vk1[3]*dUz[i][i]);
    Vec4 Bux = B[i][j][1]*dUx[i][j]+B[i][j][2]*dUy[i][j]+B[i][j][3]*dUz[i][j];

    dUx[i][i] -= (forceD[i][j][1]*(Bu0-Bux) + facP1*dUx[i][j]/meff1)*rhom[i][j];
    dUy[i][i] -= (forceD[i][j][2]*(Bu0-Bux) + facP1*dUy[i][j]/meff1)*rhom[i][j];
    dUz[i][i] -= (forceD[i][j][3]*(Bu0-Bux) + facP1*dUz[i][j]/meff1)*rhom[i][j];

    for(int k=0; k< NV; k++) {
      dGji[i][j] += E[i][k][j]*dGij[i][k]*rhom[j][i];
      dGji[j][i] += E[j][k][i]*dGij[j][k]*rhom[i][j];
    }
    dGij[i][j] = (wG2*dr2ijri + E[j][i][i]*dGji[i][j])*rhom[i][j];
    dGij[j][i] = (wG2*dr2jirj + E[i][j][j]*dGji[j][i])*rhom[j][i];
  }
  }

}

void RQMDpdm::computeForceMC(vector<Vec4>& rg,vector<Vec4>& pkin,vector<Vec4>& u,vector<double>& p0)
{
  Vec4 force1=0.0, force2=0.0;

  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    Vec4 pc1 = part[i]->getPcan();
    Vec4 pk1 = pkin[i];
    if(optPotentialArg>=1) p1 = part[i]->getPcan(optVdot);
    Vec4 vk1 = optRQMDevolution > 0 ? pk1/(pk1*pHat) : pk1/pk1[0];
    double meff1 = part[i]->getEffectiveMass();
    double m1= optPotentialArg>=1 ? part[i]->getMass():meff1;
    Vec4 u1  = p1/m1;
    double fengi = optCollisionOrdering == 101 ? meff1/(pk1*pHat) :
           optCollisionOrdering >  101 ? meff1*part[i]->getMass()/(pk1*p1) : meff1/pk1[0];

    u1=pk1/meff1;

    /*
    if(optPotentialEval==4) {
      u1=pk1/meff1;
      vk1=u1;
      fengi=1.0;
    }
    */

    int bar1   = part[i]->baryon()/3;       // baryon number
    double xvi = part[i]->facPotential(5); // omega coupling
    double dmi = part[i]->facPotential(6); // sigma coupling
    double gpi = part[i]->facPotential(9);  // phi coupling
    double gri = part[i]->facPotential(10); // rho coupling

    // omega, rho and phi mesons
    double omi = max(0.0,omega[i].m2Calc());
    double rhi = max(0.0,rho_meson[i].m2Calc());
    double ri=C4/mRho2*rhi;
    double si=cOmegaRho/mRho2*omi;
    double dri=1.0/(1.0 + 3*ri + si);
    Vec4 dOmegadJ1 = omegaPotential(i, vk1);
    //double rden1 = optIntMC>=2? exp(-wG2*(pow2((rg[i]-r1)*u1)-pow2((rg[i]-r1)*u[i]))):1.0;

    if(optIntMC==2 || optIntMC==4) {
      Vec4 dr1 = rg[i] - r1;
      force[i] += -p0[i]/widG*(dr1 - (dr1*u1)*u1);
      //force[i] += -part[i]->potv(0)/widG*(dr1 - (dr1*u1)*u1);
      force1 += -p0[i]/widG*(dr1 - (dr1*u1)*u1);
    }

    //for(int j=i+1; j< NV; j++) {
    //for(int j=i; j< NV; j++) {
    for(int j=0; j< NV; j++) {

      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      Vec4 pc2 = part[j]->getPcan();
      //Vec4 pk2 = optVdot <= 1 ? part[j]->getPkin(optVdot) : p2;
      Vec4 pk2 = pkin[j];
      if(optPotentialArg>=1) p2 = part[j]->getPcan(optVdot);
      Vec4 vk2 = optRQMDevolution > 0 ? pk2/(pk2*pHat) : pk2/pk2[0];
      double meff2 = part[j]->getEffectiveMass();
      double m2=optPotentialArg>=1? part[j]->getMass():meff2;
      Vec4 u2  = p2/m2;
      double fengj = optCollisionOrdering == 101 ? meff2/(pk2*pHat) :
             optCollisionOrdering  > 101 ? meff2*part[j]->getMass()/(pk2*p2) : meff2/pk2[0];

	u2 = pk2/meff2;
      /*
      if(optPotentialEval==4) {
	u2 = pk2/meff2;
        vk2=u2;
        fengj=1.0;
      }
      */

      int bar2  = part[j]->baryon()/3;
      double xvj= part[j]->facPotential(5);
      double dmj= part[j]->facPotential(6);
      double gpj= part[j]->facPotential(9);  // phi coupling
      double grj= part[j]->facPotential(10); // rho coupling

      // omega
      double omj = max(0.0,omega[j].m2Calc());
      double rhj = max(0.0,rho_meson[j].m2Calc());
      double rj=C4/mRho2*rhj;
      double sj=cOmegaRho/mRho2*omj;
      double drj=1.0/(1.0 + 3*rj + sj);
      Vec4 dOmegadJ2 = omegaPotential(j, vk2);

      Vec4 dr1 = rg[i] - r2;
      Vec4 dr2 = rg[j] - r1;
      Vec4 dr2ijri =  2*(dr1 - (dr1*u2)*u2);  // R^2_{ij}/dr_i
      Vec4 dr2jirj =  2*(dr2 - (dr2*u1)*u1);  // R^2_{ji}/dr_j
      Vec4 dr2ijrj =  -dr2ijri;
      Vec4 dr2jiri =  -dr2jirj;

      // correct the sign of the time-component.
      dr2ijri[0] *= -1.0;
      dr2jiri[0] *= -1.0;
      dr2ijrj[0] *= -1.0;
      dr2jirj[0] *= -1.0;

      // Scalar part
      double fsky1 = -wG2*fengi*rhom[i][j]*dmj*rhos2[i];
      double fsky2 = -wG2*fengj*rhom[j][i]*dmi*rhos2[j];

      // Vector part
      double facP1=Comega2*bar1*bar2*xvi*xvj;      // omega
      double facP2=0.5*bar1*bar2*gri*grj/mRho2fm;  // rho
      double facP3=0.5*bar1*bar2*gpi*gpj/mPhi2fm;  // phi
      double facpi = facP1*(dOmegadJ1*u2) + (u2 * vk1)*(facP2*dri + facP3);
      double facpj = facP1*(dOmegadJ2*u1) + (u1 * vk2)*(facP2*drj + facP3);

      fsky1 += -wG2*rhom[i][j]*facpi;
      fsky2 += -wG2*rhom[j][i]*facpj;

      if(i==j) fsky2=0.0;

      //force[i]  += -0.5*fsky1*dr2ijri - 0.5*fsky2*dr2jiri;

      if(optIntMC==1 || optIntMC==3) {
      force[i]  += -fsky1*dr2ijri; //- fsky2*dr2jiri;
      //force[j]  += -fsky2*dr2jirj; //- fsky2*dr2jirj;

      force1  += -fsky1*dr2ijri; //- fsky2*dr2jiri;
      //force1  += -fsky2*dr2jirj; //- fsky2*dr2jirj;
      }

      //forcer[i] +=  fsky1*dr2ijpi //+ fsky2*dr2jipi;
      //forcer[j] +=  fsky1*dr2ijpj //+ fsky2*dr2jipj;


      if(optIntMC>=3) {
        Vec4 vk1a=vk1; vk1a[0]=0.0;
        Vec4 vk2a=vk2; vk2a[0]=0.0;
	Vec4 u1a=u1; u1a[0]=0.0;
	Vec4 u2a=u2; u2a[0]=0.0;

        //Vec4 vpot1= facP1*omegaPotentialx(i, u2) + u2*(facP2*dri + facP3);
        //Vec4 vpot2= facP1*omegaPotentialx(j, u1) + u1*(facP2*dri + facP3);
        //Vec4 forceDij = (dmj*rhos2x[i]*u1 + vpot1)/meff1;
        //Vec4 forceDji = (dmi*rhos2x[j]*u2 + vpot2)/meff2;

        Vec4 vpot1= facP1*omegaPotentialx(i, u[j]) + u[j]*(facP2*dri + facP3);
        Vec4 vpot2= facP1*omegaPotentialx(j, u[i]) + u[i]*(facP2*dri + facP3);
        Vec4 forceDij = (dmj*rhos2x[i]*u[i] + vpot1)/meff1;
        Vec4 forceDji = (dmi*rhos2x[j]*u[j] + vpot2)/meff2;

        Vec4 qr1 = rg[i] - r1;
        Vec4 qr2 = rg[j] - r2;

        double cdij=(-vk1a/u1[0] + 2*wG2*(qr1*u1a)*qr1)*forceDij;
        double cdji=(-vk2a/u2[0] + 2*wG2*(qr2*u2a)*qr2)*forceDji;
        //double cdij=-2*wG2*(qr1*u1a)*qr1*forceDij;
        //double cdji=-2*wG2*(qr2*u2a)*qr2*forceDji;

	// gamma only
        //double cdij=-vk1a/u1[0]*forceDij;
        //double cdji=-vk2a/u2[0]*forceDji;

        //double cdij=-(vk1a/u1[0] + 2*wG2*(qr1*u1a)*qr1)*forceD[i][j];
        //double cdji=-(vk2a/u2[0] + 2*wG2*(qr2*u2a)*qr2)*forceD[j][i];

        //double cdij=-2*wG2*(qr1*u1a)*qr1*forceD[i][j];
        //double cdji=-2*wG2*(qr2*u2a)*qr2*forceD[j][i];

	Vec4 dr=r1-r2;
        //double rbi = dr*u1;
        //double rbj = dr*u2;
        Vec4 dq2ijri =  2*wG2*(dr - (dr*u2)*u2);  // R^2_{ij}/dr_i
        Vec4 dq2jirj = -2*wG2*(dr + (dr*u1)*u1);  // R^2_{ji}/dr_j

        //Vec4 dr2ijpi = 0.0;
        //Vec4 dr2jipj = 0.0;
        //Vec4 dr2jipi =  2*dr*rbi/m1;     // R^2_{ji}/dp_i
        //Vec4 dr2ijpj =  2*dr*(dr*u2)/m2;     // R^2_{ij}/dp_j

	// E_{ij} part
          force[i] += -p0[i]*dq2ijri*cdij*rhomx[i][j];
          //force[j] += -p0[j]*dq2jirj*cdji*rhomx[j][i];

          force2 += -p0[i]*dq2ijri*cdij*rhomx[i][j];
          //force2 += -p0[j]*dq2jirj*cdji*rhomx[j][i];

	// E{ji} part
          force[i] += p0[j]*dq2jirj*cdji*rhomx[j][i];
          //force[j] += p0[i]*dq2ijri*cdij*rhomx[i][j];

          force2 += p0[j]*dq2jirj*cdji*rhomx[j][i];
          //force2 += p0[i]*dq2ijri*cdij*rhomx[i][j];


	  if(force2.pAbs()>100){
	cout <<" r1-r2= "<< r1-r2;
	cout <<" p1= "<< pk1;
	cout <<" u1= "<< u1;
	cout <<" u2= "<< u2;
	cout <<" dq2ijri= "<< dq2ijri;
	cout <<" dq2jiri= "<< dq2jirj;
	  cout << "p0i= "<< p0[j]*dq2jirj*cdji*rhomx[j][i]<<endl;
	  cout << "p0j= "<< -p0[i]*dq2ijri*cdij*rhomx[i][j]<<endl;
	  cout << "p0= "<< p0[j] <<endl;
	  cout << "dr= "<< dq2jirj <<endl;
	  cout << " cdij= "<< cdij<<endl;
	  cout << " cdji= "<< cdji<<endl;
	  cout << " Dij= "<< forceDij<<endl;
          cout << " gam= "<< -vk1a/u1[0]*forceDij<<endl;
	  cout << " Bij*Dji= "<< 2*wG2*(qr1*u1a)*qr1*forceDij<<endl;;
	  cout << " dr1= "<< dr1;
	  cout << " qr1= "<< qr1;
	  cout << " Bij= "<< 2*wG2*(qr1*u1a)*qr1<<endl;;
	  cout << " rhom= "<< scientific << rhomx[j][i]<<endl;
	  cout << "force2= "<< force2;
	  cout << "force2.abs= "<< scientific << force2.pAbs()<<endl;
	  cout << "force1.abs= "<< scientific << force1.pAbs()<<endl;
	  }
      }


      if(!withMomDep) continue;

      Vec4 dp = pc1 - pc2;
      Vec4 pCM = pc1 + pc2;
      double sInv = pCM.m2Calc();
      double pma = pow2(dp * pCM)/sInv;
      double psq1 = dp.m2Calc() - optPV * pma;
      double psq2 = psq1;

      Vec4 v1 = pc1/pc1[0];
      Vec4 v2 = pc2/pc2[0];
      Vec4 bbi = pCM/pCM[0] - optP0dev*v1;
      Vec4 bbj = pCM/pCM[0] - optP0dev*v2;
      Vec4 dp2ijpi = 2*( dp - optP0dev*dp[0]*v1 + optPV * pCM[0]/sInv*pma*bbi);
      Vec4 dp2ijpj = 2*(-dp + optP0dev*dp[0]*v2 + optPV * pCM[0]/sInv*pma*bbj);
      //Vec4 dp2jipi = dp2ijpi;
      Vec4 dp2jipj = dp2ijpj;


      // add scalar part
      double fmomdi = -wG2*fengi*potential->devVmds(psq2)*rhom[i][j];
      //double fmomdj = -wG2*fengj*potential->devVmds(psq1)*rhom[j][i];
      double fmomei =     fengi*potential->devVmes(psq2)*rhom[i][j];
      //double fmomej =     fengj*potential->devVmes(psq1)*rhom[j][i];

      force[i]  += -fmomdi*dr2ijri;// - fmomdj*dr2jiri;
      force[j]  += -fmomdi*dr2jirj;// - fmomdj*dr2jirj;
      forcer[i] +=  fmomei*dp2ijpi; //+ fmomej*dp2jipi
	          //+ fmomdi*dr2ijpi; //+ fmomdj*dr2jipi;
      forcer[j] +=  fmomei*dp2jipj; //+ fmomej*dp2jipj
	          //+ fmomdi*dr2ijpj;// + fmomdj*dr2jipj;

      // vector part
      double vf1=1.0;
      //double vf2=1.0;
      if(optVectorPotential==1) {
        vf1 = vk1 * u2;
        //vf2 = vk2 * u1;
      }
      fmomdi = -wG2*potential->devVmdv(psq2)*rhom[i][j]*vf1;
      //fmomdj = -wG2*potential->devVmdv(psq1)*rhom[j][i]*vf2;
      fmomei =     potential->devVmev(psq2)*rhom[i][j]*vf1;
      //fmomej =     potential->devVmev(psq1)*rhom[j][i]*vf2;

      force[i]  += -fmomdi*dr2ijri;//   - fmomdj*dr2jiri;
      force[j]  += -fmomdi*dr2jirj;//   - fmomdj*dr2jirj;

      forcer[i] +=  fmomei*dp2ijpi;// + fmomej*dp2jipi
	          //+ fmomdi*dr2ijpi;// + fmomdj*dr2jipi;
      forcer[j] +=  fmomei*dp2jipj;// + fmomej*dp2jipj
	          //+ fmomdi*dr2ijpj;// + fmomdj*dr2jipj;

    } // end loop over j
  } // end loop over i

  /*
    if(force1.pAbs()>3|| force2.pAbs()>1e-10) {
    cout << "force1= "<< force1;
    cout << " force2= "<< force2;
    cout << "force1= "<< scientific << force1.pAbs();
    cout << " force2= "<< force2.pAbs()<<endl;
    }
    */

}

// space integral is numerically evaluated.
void RQMDpdm::computeForceL2()
{
  bool optSet= true;
  force.assign(NV,0.0);
  forcer.assign(NV,0.0);
  vector<Vec4> pkin(NV),rg(NV),u(NV);
  vector<double> p0(NV);
  
    //optIntMC=1:  dp^0/dx
    //optIntMC=2:  dp^0/dx*gam(x)/gam(x_i)
    //optIntMC=3:  p^0
    //optIntMC=4:  p^0 + dgam/dx_i
    //optIntMC=11: dp^0/dx*gam(x)/gam(x_i)+ G(x-x_j)
    //optIntMC=12: p^0 + dgam/dx_i + G(x-x_j)

    if(optIntMC>=2) {
      for(int itry=0;itry<maxIt;itry++) {
        setZero();
        qmdMatrix();
        double diff=sigmaField(optSet);
        if(diff <1e-5) break;
      }
      omegaField();
      singleParticlePotential();
      rhomx=rhom;
      rhos2x=rhos2;
      omegax=omega;
      rho_mesonx=rho_meson;
      computeForceDE();
    }

    // save initial u.
    for(int i=0;i<NV;i++) {
      Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : part[i]->getP();
      double meff1 = part[i]->getEffectiveMass();
      u[i] = pk1/meff1;
    }

  // Monte-Carlo loop.
  for(int iev=0;iev<nMCPoint;iev++) {

    // Generate Monte-Carlo point.
    for(int i=0;i<NV;i++) {
      double aa=u[i][0]*(u[i][0]+1);
      Vec4 rgauss = Vec4(rndm->gauss(),rndm->gauss(),rndm->gauss(),0.0);
      rg[i]= part[i]->getR() + sqrt(widG)*(rgauss+(u[i]*rgauss)*u[i]/aa);
      rg[i][0]=part[i]->getT();
    }

    // compute sigma and omega field at the Monte-Carlo point for each particle.
    for(int itry=0;itry<maxIt;itry++) {
      setZero();
      qmdMatrix(rg);
      double diff=sigmaField(optSet);
      if(diff <1e-5) break;
    }
    omegaField();
    for(int i=0; i< NV; i++) {
      p0[i] = singleParticlePotential(i,pkin[i]);
    }

    computeForceMC(rg,pkin,u,p0);

  } // end loop over MC points

  for(int i=0; i< NV; i++) {
    force[i]  /= nMCPoint;
    forcer[i] /= nMCPoint;
  }

}

// The transverse distance is defined by the rest frame of a particle.
void RQMDpdm::computeForce1()
{
  double fact= transportModel==2 ? 1.0: 0.5;

  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : p1;
    if(optPotentialArg>=1) p1 = part[i]->getPcan(optVdot);
    Vec4 vk1 = optRQMDevolution > 0 ? pk1/(pk1*pHat) : pk1/pk1[0];
    double m1=part[i]->getMass();
    Vec4 u1  = p1/m1;
    double meff1 = part[i]->getEffectiveMass();
    double fengi = optCollisionOrdering == 101 ? meff1/(pk1*pHat) :
           optCollisionOrdering >  101 ? meff1*part[i]->getMass()/(pk1*p1) : meff1/pk1[0];

    int bar1   = part[i]->baryon()/3;
    double xvi = part[i]->facPotential(5);
    double dmi = part[i]->facPotential(6);
    double gpi = part[i]->facPotential(9);  // phi coupling
    double gri = part[i]->facPotential(10); // rho coupling x charge

    double omi = max(0.0,omega[i].m2Calc());
    double rhi = max(0.0,rho_meson[i].m2Calc());
    double xi=C4/mOmega2*omi;
    double yi=cOmegaRho/mOmega2*rhi;
    double ri=C4/mRho2*rhi;
    double si=cOmegaRho/mRho2*omi;
    double dui=1.0/(1.0 + 3*xi + yi);
    double dri=1.0/(1.0 + 3*ri + si);
    double dui1 = dui;
    double dri1 = dri;
    double dui2 = 0.0;
    double dri2 = 0.0;

    if(optOmega4==1) {
      double gn=g_omega*sqrt(max(0.0,JB[i].m2Calc()))*HBARC3;
      double gi=sqrt(max(0.0,IB[i].m2Calc()))*HBARC3;
      dui1 = dui*devVec0(xi, yi, gn, mOmega3); // omega
      dri1 = dri*devVec0(ri, si, gi, mRho3);   // rho
    } else if(optOmega4==2) {
      double gn=g_omega*sqrt(max(0.0,JB[i].m2Calc()))*HBARC3;
      double gi=sqrt(max(0.0,IB[i].m2Calc()))*HBARC3;
      dui1 = dui*devVec1(xi, yi, gn, mOmega3); // omega
      dri1 = dri*devVec1(ri, si, gi, mRho3);   // rho
      if(cOmegaRho>0.0) {
        double omr=omega[i]*rho_meson[i];
        dui2 = dui*devVec2(ri, si, gi,C4*omr/mRho2,mRho3);     // omega-rho coupling term.
        dri2 = dri*devVec2(xi, yi, gn,C4*omr/mOmega2,mOmega3); // omega-rho coupling term.
      }
    } else if(optOmega4==3) {
      dui *=(1.5*xi*xi+7./2.*xi+1.0)/(1.0+2*xi+xi*xi);
      dri *=(1.5*ri*ri+7./2.*ri+1.0)/(1.0+2*ri+ri*ri);
    } else if(optOmega4==4) {
      dui1 =dui*(1.0+7./2.*xi+1.5*yi)/(1+yi);
      dui2 =dui*si*(1-3*ri)/((1+si)*(1+si));
      dri1 =dri*(1.0+7./2.*ri+1.5*si)/(1+yi);
      dri2 =dui*yi*(1-3*xi)/((1+yi)*(1+yi));
    }

    for(int j=i+1; j< NV; j++) {

      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      Vec4 pk2 = optVdot <= 1 ? part[j]->getPkin(optVdot) : p2;
      if(optPotentialArg>=1) p2 = part[j]->getPcan(optVdot);
      Vec4 vk2 = optRQMDevolution > 0 ? pk2/(pk2*pHat) : pk2/pk2[0];
      double m2=part[j]->getMass();
      Vec4 u2  = p2/m2;
      double meff2 = part[j]->getEffectiveMass();
      double fengj = optCollisionOrdering == 101 ? meff2/(pk2*pHat) :
             optCollisionOrdering  > 101 ? meff2*part[j]->getMass()/(pk2*p2) : meff2/pk2[0];

      int bar2  = part[j]->baryon()/3;
      double xvj= part[j]->facPotential(5);
      double dmj= part[j]->facPotential(6);
      double gpj= part[j]->facPotential(9);  // phi coupling
      double grj= part[j]->facPotential(10); // rho coupling

      double omj = max(0.0,omega[j].m2Calc());
      double rhj = max(0.0,rho_meson[j].m2Calc());
      double xj=C4/mOmega2*omj;
      double yj=cOmegaRho/mOmega2*rhj;
      double rj=C4/mRho2*rhj;
      double sj=cOmegaRho/mRho2*omj;
      double duj=1.0/(1.0 + 3*xj + yj);
      double drj=1.0/(1.0 + 3*rj + sj);
      double duj1 = duj;
      double drj1 = drj;
      double duj2 = 0.0;
      double drj2 = 0.0;

      if(optOmega4==1) {
        double gn=g_omega*sqrt(max(0.0,JB[j].m2Calc()))*HBARC3;
        double gi=sqrt(max(0.0,IB[j].m2Calc()))*HBARC3;
        duj1 = duj*devVec0(xj, yj, gn,mOmega3); // omega
        drj1 = drj*devVec0(rj, sj, gi,mOmega3); // rho
      } else if(optOmega4==2) {
        double gn=g_omega*sqrt(max(0.0,JB[j].m2Calc()))*HBARC3;
        double gi=sqrt(max(0.0,IB[j].m2Calc()))*HBARC3;
        duj1 = duj*devVec1(xj, yj, gn,mOmega3); // omega
        drj1 = drj*devVec1(rj, sj, gi,mOmega3); // rho
        if(cOmegaRho>0.0) {
          double omr=omega[j]*rho_meson[j];
          duj2 = duj*devVec2(rj, sj, gi,C4*omr/mRho2,mRho3);   // omega-rho coupling term.
          drj2 = drj*devVec2(xj, yj, gn,C4*omr/mOmega2,mRho3); // omega-rho coupling term.
	}
      } else if(optOmega4==3) {
	duj1 =duj * ((1.5*xj*xj+7./2.*xj+1.0)/(1.0+2*xj+xj*xj));
	drj1 =duj * ((1.5*rj*rj+7./2.*rj+1.0)/(1.0+2*rj+rj*rj));
      } else if(optOmega4==4) {
        duj1 =duj*(1.0+7./2.*xj+1.5*yj)/(1+yj);
        drj1 =drj*(1.0+7./2.*rj+1.5*sj)/(1+sj);
        duj2 =duj*sj*(1-3*rj)/((1+sj)*(1+sj));
        drj2 =duj*yi*(1-3*xj)/((1+yj)*(1+yj));
      }


      // Note that the following is only valid for the spacial part of the 4-vector:
      Vec4 dr = r1 - r2;

      double rbi = dr*u1;
      double rbj = dr*u2;
      Vec4 dr2ijri =  2*(dr - rbj*u2);  // R^2_{ij}/dr_i
      Vec4 dr2jiri =  2*(dr - rbi*u1);  // R^2_{ji}/dr_i
      Vec4 dr2ijrj =  -dr2ijri;
      Vec4 dr2jirj =  -dr2jiri;

      Vec4 dr2jipi =  2*dr*rbi/m1;     // R^2_{ji}/dp_i
      Vec4 dr2ijpj =  2*dr*rbj/m2;     // R^2_{ij}/dp_j
      Vec4 dr2ijpi = 0.0;
      Vec4 dr2jipj = 0.0;
      

      if(optTwoBodyDistance==2) {
  Vec4 pCM = p1 + p2;
  Vec4 bbi = pCM/pCM[0] - optP0dev*p1/p1[0];
  Vec4 bbj = pCM/pCM[0] - optP0dev*p2/p2[0];
  double sInv = pCM.m2Calc();
  double rbij = -dr * pCM/sInv;
  dr2ijri =  2*(dr + rbij*pCM);
  dr2jiri =  dr2ijri;
  dr2jirj = -dr2ijri;
  dr2ijrj = -dr2jiri;

  dr2ijpi = 2*(dr + pCM[0]*rbij*bbi)*rbij;
  dr2jipi = dr2ijpi;
  dr2jipj = 2*(dr + pCM[0]*rbij*bbj)*rbij;
  dr2ijpj = dr2jipj;
      }

      

      // correct the sign of the time-component.
      dr2ijri[0] *= -1.0;
      dr2jiri[0] *= -1.0;
      dr2ijrj[0] *= -1.0;
      dr2jirj[0] *= -1.0;

      dr2jipi[0] *= -1.0;
      dr2ijpj[0] *= -1.0;
      dr2ijpi[0] *= -1.0;
      dr2jipj[0] *= -1.0;

      // Scalar part
      double dvi   =  fengi*dmj*rhos2[i];
      double dvj   =  fengj*dmi*rhos2[j];
      double fsky1 = -wmG*dvi*rhom[i][j];
      double fsky2 = -wmG*dvj*rhom[j][i];

      // Vector part
      double facP1=Comega2*bar1*bar2*xvi*xvj;      // omega
      double facP2=fact*bar1*bar2*gri*grj/mRho2fm;  // rho
      double facP3=fact*bar1*bar2*gpi*gpj/mPhi2fm;  // phi
      double facOR1=fact*g_omega*bar1*bar2*xvi*grj/mOmega2fm;  // omega-rho coupling
      double facOR2=fact*g_omega*bar1*bar2*gri*xvj/mRho2fm;    // omega-rho coupling
      double facOR3=fact*g_omega*bar1*bar2*xvj*gri/mOmega2fm;  // omega-rho coupling
      double facOR4=fact*g_omega*bar1*bar2*grj*xvi/mRho2fm;    // omega-rho coupling
      double facpi = facP1*dui1+facOR1*dui2 + facP2*dri1+facOR2*dri2 + facP3;
      double facpj = facP1*duj1+facOR3*duj2 + facP2*drj1+facOR4*drj2 + facP3;
      fsky1 += -wmG*(u2 * vk1)*rhom[i][j]*facpi;
      fsky2 += -wmG*(u1 * vk2)*rhom[j][i]*facpj;

      if(transportModel!=2) {
      force[i]  += -fsky1*dr2ijri - fsky2*dr2jiri;
      force[j]  += -fsky1*dr2ijrj - fsky2*dr2jirj;
      forcer[i] +=  fsky1*dr2ijpi + fsky2*dr2jipi;
      forcer[j] +=  fsky1*dr2ijpj + fsky2*dr2jipj;
      } else {
      force[i]  += -fsky1*dr2ijri;
      force[j]  += -fsky2*dr2jirj;
      forcer[i] +=  fsky1*dr2ijpi;
      forcer[j] +=  fsky2*dr2jipj;
      }

      // Derivative of p0/m term.
      if(optDerivative) {
        Vec4 devV1 = (optP0dev*p1/p1[0]*vk2[0] - vk2)/m1;
        Vec4 devV2 = (optP0dev*p2/p2[0]*vk1[0] - vk1)/m2;
        forcer[i] += devV1*rhom[j][i]*facpj;
        forcer[j] += devV2*rhom[i][j]*facpi;
      }

      if(!withMomDep) continue;

      Vec4 dp = p1 - p2;
      double psq = dp.m2Calc();

      // distance squared in the rest frame of particle 2
      double dot4j = dp * p2 / m2;
      double psq2 = psq - optPV*dot4j*dot4j;

      // distance squared in the rest frame of particle 1
      double dot4i = (dp * p1)/m1;
      double psq1 = psq - optPV*dot4i*dot4i;

      // derivatives
      Vec4 bi = p1/p1.e();
      Vec4 bb = optP0dev * p2.e()*bi - p2;
      Vec4 dp2ijpi = 2*(dp - optP0dev*dp[0]*bi + bb*dot4j/m2);
      Vec4 dp2jipi = 2*(dp - optP0dev*dp[0]*bi - bb*dot4i/m1);

      Vec4 bj = p2/p2.e();
      Vec4 bb2 = optP0dev * p1.e()*bj - p1;
      Vec4 dp2ijpj = 2*(-dp + optP0dev*dp[0]*bj + bb2*dot4j/m2);
      Vec4 dp2jipj = 2*(-dp + optP0dev*dp[0]*bj - bb2*dot4i/m1);
      //dp2ijpj = 2*(-dp + dot4j*u2);
      //dp2jipj = 2*(-dp + dot4i*u1);

      // add scalar part
      double fmomdi = -wmG*fengi*potential->devVmds(psq2)*rhom[i][j];
      double fmomei =     fengi*potential->devVmes(psq2)*rhom[i][j];
      double fmomdj = -wmG*fengj*potential->devVmds(psq1)*rhom[j][i];
      double fmomej =     fengj*potential->devVmes(psq1)*rhom[j][i];

      force[i]  += -fmomdi*dr2ijri - fmomdj*dr2jiri;
      force[j]  += -fmomdi*dr2ijrj - fmomdj*dr2jirj;
      forcer[i] +=  fmomei*dp2ijpi + fmomej*dp2jipi
	          + fmomdi*dr2ijpi + fmomdj*dr2jipi;
      forcer[j] +=  fmomei*dp2ijpj + fmomej*dp2jipj
	          + fmomdi*dr2ijpj + fmomdj*dr2jipj;


      // vector part
      double vf1=1.0;
      double vf2=1.0;
      if(optVectorPotential==1) {
        vf1 = vk1 * u2;
        vf2 = vk2 * u1;
      }
      fmomdi = -wmG*potential->devVmdv(psq2)*rhom[i][j]*vf1;
      fmomdj = -wmG*potential->devVmdv(psq1)*rhom[j][i]*vf2;
      fmomei =     potential->devVmev(psq2)*rhom[i][j]*vf1;
      fmomej =     potential->devVmev(psq1)*rhom[j][i]*vf2;

      force[i]  += -fmomdi*dr2ijri   - fmomdj*dr2jiri;
      force[j]  += -fmomdi*dr2ijrj   - fmomdj*dr2jirj;

      forcer[i] +=  fmomei*dp2ijpi + fmomej*dp2jipi
	          + fmomdi*dr2ijpi + fmomdj*dr2jipi;
      forcer[j] +=  fmomei*dp2ijpj + fmomej*dp2jipj
	          + fmomdi*dr2ijpj + fmomdj*dr2jipj;

      if(optDerivative) {
        double facm1 = -fmomdj/(wmG*vf2);
        double facm2 = -fmomdi/(wmG*vf1);
        Vec4 devV1 = (optP0dev*p1/p1[0]*vk2[0] - vk2)/m1;
        Vec4 devV2 = (optP0dev*p2/p2[0]*vk1[0] - vk1)/m2;
        forcer[i] += facm1*devV1;
        forcer[j] += facm2*devV2;
      }

    } // end loop over j
  } // end loop over i

}


void RQMDpdm::setFreeLagrangeMultiplier()
{
  for(int i=0; i< NV; i++) {
    Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : part[i]->getP();
    if(optCollisionOrdering <= 101) { 
      if(optRQMDevolution == 0)lambda[i]=1.0/pk1[0];
      else lambda[i]=1.0/(pk1*pHat);
    } else if(optCollisionOrdering >= 102) {
      double m=part[i]->getEffectiveMass();
      if(m==0.0) m=1.0;
      if(optCollisionOrdering==110) m=1.0/lambdaLag;
      lambda[i]=1.0/m;
    }
  }

}

void RQMDpdm::computeForceMatrix()
{
  vector<std::vector<Vec4> > matp(NV,vector<Vec4>(NV,0.0));
  vector<std::vector<Vec4> > matr(NV,vector<Vec4>(NV,0.0));

  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : p1;
    if(optPotentialArg>=1) p1 = part[i]->getPcan(optVdot);
    int bar1= part[i]->baryon()/3;
    double m1=part[i]->getMass();
    Vec4 u1  = p1/m1;
    Vec4 vk1 = optRQMDevolution > 0 ? pk1/(pk1*pHat) : pk1/pk1[0];
    double dmi= part[i]->facPotential(6);
    double xvi= part[i]->facPotential(5);
    double meff1 = part[i]->getEffectiveMass();
    double fengi = optCollisionOrdering == 101 ? meff1/(pk1*pHat) :
           optCollisionOrdering >  101 ? meff1*part[i]->getMass()/(pk1*p1) : meff1/pk1[0];

    matp[i][i] += pk1;

    for(int j=i+1; j< NV; j++) {

      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      Vec4 pk2 = optVdot <= 1 ? part[j]->getPkin(optVdot) : p2;
      if(optPotentialArg>=1) p2 = part[j]->getPcan(optVdot);
      int bar2 = part[j]->baryon()/3;
      double m2=part[j]->getMass();
      Vec4 u2  = p2/m2;
      Vec4 vk2 = optRQMDevolution > 0 ? pk2/(pk2*pHat) : pk2/pk2[0];
      double meff2 = part[j]->getEffectiveMass();
      double fengj = optCollisionOrdering == 101 ? meff2/(pk2*pHat) :
             optCollisionOrdering  > 101 ? meff2*part[j]->getMass()/(pk2*p2) : meff2/pk2[0];

      double dmj= part[j]->facPotential(6);
      double dvi = fengi*dmj*rhos2[i];
      double dvj = fengj*dmi*rhos2[j];
      double xvj= part[j]->facPotential(5);

      // Scalar part
      double fsky1 = -wmG*dvi*rhom[i][j];
      double fsky2 = -wmG*dvj*rhom[j][i];

      // Vector part
      double facP=Comega2*bar1*bar2;
      fsky1 += -wmG*(u2 * vk1)*facP*xvi*rhom[i][j];
      fsky2 += -wmG*(u1 * vk2)*facP*xvj*rhom[j][i];

      Vec4 dr = r1 - r2;
      double rbi = dr*u1;
      double rbj = dr*u2;
      Vec4 dr2ijri =  2*(dr - rbj*u2);  // R^2_{ij}/dr_i
      Vec4 dr2jiri =  2*(dr - rbi*u1);  // R^2_{ji}/dr_i
      Vec4 dr2ijrj =  -dr2ijri;
      Vec4 dr2jirj =  -dr2jiri;

      Vec4 dr2jipi =  2*dr*rbi/m1;     // R^2_{ji}/dp_i
      Vec4 dr2ijpj =  2*dr*rbj/m2;     // R^2_{ij}/dp_j
      Vec4 dr2ijpi = 0.0;
      Vec4 dr2jipj = 0.0;

      matp[i][j] =  fsky1*dr2ijpj;
      matp[j][i] =  fsky2*dr2jipi;
      matp[i][i] += fsky1*dr2ijpi;
      matp[j][j] += fsky2*dr2jipj;

      matr[i][j] =  -fsky1*dr2ijrj;
      matr[j][i] =  -fsky2*dr2jiri;
      matr[i][i] += -fsky1*dr2ijri;
      matr[j][j] += -fsky2*dr2jirj;

      // Derivative of p0/m and p/p0 term.
      if(optDerivative) {
        Vec4 devV1 = (optP0dev*p1/p1[0]*vk2[0] - vk2)/m1;
        Vec4 devV2 = (optP0dev*p2/p2[0]*vk1[0] - vk1)/m2;
        matp[j][i] += devV1*rhom[j][i]*bar1*bar2*facP*xvi; 
        matp[i][j] += devV2*rhom[i][j]*bar1*bar2*facP*xvj;
      }

      if(!withMomDep) continue;

      //distanceP->distanceP();
      Vec4 dp = p1 - p2;
      double psq = dp.m2Calc();

      // distance squared in the rest frame of particle 2
      double dot4j = dp * p2 / m2;
      double psq2 = psq - optPV*dot4j*dot4j;

      // distance squared in the rest frame of particle 1
      double dot4i = (dp * p1)/m1;
      double psq1 = psq - optPV*dot4i*dot4i;

      // derivatives
      Vec4 bi = p1/p1.e();
      Vec4 bb = optP0dev * p2.e()*bi - p2;
      Vec4 dp2ijpi = 2*(dp - optP0dev*dp[0]*bi + bb*dot4j/m2);
      Vec4 dp2jipi = 2*(dp - optP0dev*dp[0]*bi - bb*dot4i/m1);

      Vec4 bj = p2/p2.e();
      Vec4 bb2 = optP0dev * p1.e()*bj - p1;
      Vec4 dp2ijpj = 2*(-dp + optP0dev*dp[0]*bj + bb2*dot4j/m2);
      Vec4 dp2jipj = 2*(-dp + optP0dev*dp[0]*bj - bb2*dot4i/m1);

      // add scalar part
      double fmomdi = -wmG*fengi*potential->devVmds(psq2)*rhom[i][j];
      double fmomei =     fengi*potential->devVmes(psq2)*rhom[i][j];
      double fmomdj = -wmG*fengj*potential->devVmds(psq1)*rhom[j][i];
      double fmomej =     fengj*potential->devVmes(psq1)*rhom[j][i];

      /*
      force[i]  += -fmomdi*dr2ijri - fmomdj*dr2jiri;
      force[j]  += -fmomdi*dr2ijrj - fmomdj*dr2jirj;
      forcer[i] +=  fmomei*dp2ijpi + fmomej*dp2jipi
	          + fmomdi*dr2ijpi + fmomdj*dr2jipi;
      forcer[j] +=  fmomei*dp2ijpj + fmomej*dp2jipj
	          + fmomdi*dr2ijpj + fmomdj*dr2jipj;
		  */

      double vf1=1.0;
      double vf2=1.0;
      if(optVectorPotential==1) {
        vf1 = pk1 * u2;
        vf2 = pk2 * u1;
      }
      fmomdi += -wmG*potential->devVmdv(psq2)*rhom[i][j]*vf1;
      fmomdj += -wmG*potential->devVmdv(psq1)*rhom[j][i]*vf2;
      fmomei +=     potential->devVmev(psq2)*rhom[i][j]*vf1;
      fmomej +=     potential->devVmev(psq1)*rhom[j][i]*vf2;

      matp[i][j] +=  fmomdi*dr2ijpj + fmomei*dp2ijpj;
      matp[j][i] +=  fmomdj*dr2jipi + fmomej*dp2jipi;
      matp[i][i] +=  fmomdi*dr2ijpi + fmomei*dp2ijpi;
      matp[j][j] +=  fmomdj*dr2jipj + fmomej*dp2jipj;

      matr[i][j] += -fmomdi*dr2ijrj;
      matr[j][i] += -fmomdj*dr2jiri;
      matr[i][i] += -fmomdi*dr2ijri;
      matr[j][j] += -fmomdj*dr2jirj;

      if(optDerivative) {
        double facm1 = -fmomdj/(wmG*vf2);
        double facm2 = -fmomdi/(wmG*vf1);
        Vec4 devV1 = (optP0dev*p1/p1[0]*vk2[0] - pk2)/m1;
        Vec4 devV2 = (optP0dev*p2/p2[0]*vk1[0] - pk1)/m2;
        matp[j][i] += facm1*devV1;
        matp[i][j] += facm2*devV2;
      }

    } // end loop over j
  } // end loop over i

  setFreeLagrangeMultiplier();
  vector<double> lambdaf = lambda;
  vector<double> lambdaw(NV-1);

  // take only the diagonal part.
  if(optRQMDevolution==2) {
    for(int i=0; i< NV; i++) lambda[i]=1.0/(pHat*matp[i][i]);

  // full solution.
  } else if(optRQMDevolution==3 || optRQMDevolution==4) {
    vector<std::vector<double> > mat(NV,vector<double>(NV+1,1.0));
    for(int i=0; i< NV; i++) 
    for(int j=0; j< NV; j++) {
      mat[i][j]=matp[j][i]*pHat;
    }
    findLagrangeMultiplier(mat,NV);
    for(int i=0;i<NV;i++) lambda[i]=mat[i][NV];

    // do not assume H_i's are first class [H_i,H_j]=0.
    if(optRQMDevolution==4) {
      vector<std::vector<double> > hh(NV,vector<double>(NV,0.0));
      double hhmax=-1.0;
      for(int i=0; i< NV; i++)
      for(int j=i+1; j< NV; j++) {
      for(int k=0;k<NV;k++) {
        hh[i][j] +=matp[i][k]*matr[j][k] - matr[i][k]*matp[j][k];
      }
      hh[j][i]=-hh[i][j];
      hhmax=max(hhmax,abs(hh[i][j]));
      }

      // Joseph Samuel, PRD26 (1982)3475.
      for(int i=0;i<NV-1;i++) {
	lambdaw[i]=0.0;
        for(int j=0;j<NV-1;j++)
  	  lambdaw[i] += lambda[i]*lambda[j]*hh[i][j];
      }

      /*
      for(int i=0; i< NV; i++) {
	mat[i][NV]=0.0;
        for(int j=0; j< NV; j++)
  	  mat[i][NV] += hh[j][i]*lambda[j];
      }
      for(int i=0; i< NV; i++)
      for(int j=0; j< NV; j++) {
	mat[i][j] = matp[i][j]*pHat;
	if(j<NV-1) mat[i][j] -= matp[i][NV-1]*pHat;
      }
      findLagrangeMultiplier(mat,NV);
      for(int i=0;i<NV-1;i++) lambdaw[i]=mat[i][NV];
      */
    }

  }

    double pm=pHat.mCalc();
  for(int i=0; i< NV; i++) {
    //Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : part[i]->getP();
    //forcer[i] =  - lambdaf[i]*pk1;
    Vec4 q1 = part[i]->getR();
    if(optRQMDevolution==4) {
      force[i] -= (lambdaw[i]-lambdaw[NV-2])*pHat;
    }

    for(int j=0; j< NV; j++) {
      force[i]  += lambda[j]*matr[j][i];
      forcer[i] += lambda[j]*matp[j][i];
    }

    //Vec4 qdot= lambdaf[i]*pk1 + forcer[i];
    Vec4 qdot= forcer[i];
    if(qdot.m2Calc()<0.0) {
      double q2=qdot.pAbs();
      double eps=1e-12;
      qdot[1] *= qdot[0]*(1.0-eps)/q2;
      qdot[2] *= qdot[0]*(1.0-eps)/q2;
      qdot[3] *= qdot[0]*(1.0-eps)/q2;
      forcer[i][1] = qdot[1];
      forcer[i][2] = qdot[2];
      forcer[i][3] = qdot[3];
      //cout << " force^2= "<< forcer[i].m2Calc()<<endl;
    }

    double lam0=qdot[0];

    //cout << i << setprecision(16) << " qdot^2~= "<< qdot.mCalc() << " v= "<< qdot.pAbs()/qdot[0] <<endl;

    //if(forcer[i].pAbs()/lam0 >1.0) {
    if(qdot.m2Calc() <= 0.0) {
      double fmax=0.0;
      int imax=0;
      for(int j=0;j<NV;j++) {
	double f1=lambda[j]*matp[j][i].pAbs()/lam0;
	if(f1> fmax) {
	   fmax = f1; imax=j;
	}
	//cout << j << " matp= "<< matp[j][i].mCalc() << " pabs= "<< matp[j][i].pAbs() <<endl;
      }
      cout << i << " force= "<< qdot.pAbs()/lam0
	<< " qdot^2= "<< qdot.mCalc()
	<< " force^2= "<< forcer[i].mCalc()
	<< " force0= "<< forcer[i][0]
	<< " force= "<< forcer[i]
       	<< " fmax= "<< fmax << " imax= "<< imax <<endl;
    }
    //double vx=forcer[i][1]+pk[1]*lam;
    //double vy=forcer[i][2]+pk[2]*lam;
    //double vz=forcer[i][3]+pk[3]*lam;
    //double v0=forcer[i][0]+pk[0]*lam;
    //double vv=sqrt(vx*vx+vy*vy+vz*vz)/v0;

    if(optRQMDevolution==4) {
      for(int j=0; j< NV-1; j++) {
        Vec4 q12 = q1 - part[j]->getR();
        forcer[i]  += lambdaw[j]*(q12 - (pHat*q12)*pHat)/pm;
      }
    }

    //if(forcer[i][0]<0.0) {forcer[i]=0.0;} //force[i]=0.0;}
  }

}

void RQMDpdm::findLagrangeMultiplier(vector<vector<double> >& mat,int nv)
{
  //vector<std::vector<double> > mat(NV,vector<double>(NV+1,1.0));
  //mat.resize(NV);
  //for(int i=0;i<NV;i++) mat[i].resize(NV+1);

  for (int k=0; k<nv; k++) {

    // Initialize maximum value and index for pivot
    //int i_max = k;
    //int v_max = mat[i_max][k];

    // find greater amplitude for pivot if any
    //for (int i = k+1; i < N; i++)
    //  if (abs(mat[i][k]) > v_max) v_max = mat[i][k], i_max = i;

    //if (!mat[k][i_max]) {
    //  cout << " singlular matirx"<<endl;
    //  exit(1);
    //}
    if (!mat[k][k]) {
      cout << " singlular matirx "<< mat[k][k] << endl;
      exit(1);
    }

   // Swap the greatest value row with current row.
   //if (i_max != k) swap_row(mat, k, i_max);

    for (int i=k+1; i<nv; i++) {
      // factor f to set current row kth element to 0,and subsequently remaining kth column to 0.
      double f = mat[i][k]/mat[k][k];

      // subtract fth multiple of corresponding kth row element
      for (int j=k+1; j<=nv; j++) mat[i][j] -= mat[k][j]*f;

     // filling lower triangular matrix with zeros
     mat[i][k] = 0;
    }
  }

  // Start calculating from last equation up to the first.
  for (int i = nv-1; i >= 0; i--) {
    // start with the RHS of the equation
    //lambda[i] = mat[i][nv];

    // Initialize j to i+1 since matrix is upper. triangular.
    for (int j=i+1; j<nv; j++) {
    // subtract all the lhs values except the coefficient of the variable whose value is being calculated
      //lambda[i] -= mat[i][j]*lambda[j];
      mat[i][nv] -= mat[i][j]*mat[j][nv];
    }

    // divide the RHS by the coefficient of the unknown being calculated
    //lambda[i] = lambda[i]/mat[i][i];
    mat[i][nv] /= mat[i][i];
  }

}


} // namespace jam2


