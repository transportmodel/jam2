#include <jam2/meanfield/ParityDoublet.h>

namespace jam2 {

bool ParityDoublet::firstCall=true;

using Pythia8::Vec4;
using namespace std;

ParityDoublet::ParityDoublet(Pythia8::Settings* s,JamParticleData* jpt) //: PotentialType(s)
{
   //ofs.open("sigma.dat");

  settings = s;
  jamtable = jpt;
  pTable = jamtable->getParticleData();
  int optLambdaPot = settings->mode("MeanField:optLambdaPotential");
  if(optLambdaPot>0) {
    cout << "MeanField:optLambdaPotential > 0 cannot be used in PDM mode" <<endl;
    exit(1);
  }
  eosType=settings->mode("MeanField:EoS");
  transportModel = settings->mode("MeanField:transportModel"); 
  // =1:original RQMD
  // =2:BUU type mode
  //if(transportModel==1) {facG=0.5; facGv=0.5;}
  //else if(transportModel==2) {facG=facGv=1.0;}
  //else if(transportModel==3) {facG=facGv=1.0/sqrt(2);}
  //else if(transportModel==4) {facG=facGv=1.0;}
  //else if(transportModel==5) {facG=facGv=1.0;}

  optDeltaCoupling=settings->mode("MeanField:optDeltaCoupling");
  optHyperonCoupling=settings->mode("MeanField:optHyperonCoupling");
  optDeltaSigmaCoupling=settings->mode("MeanField:optDeltaSigmaCoupling");
  optHyperonSigmaCoupling=settings->mode("MeanField:optHyperonSigmaCoupling");
  optVectorCoupling=settings->mode("MeanField:optVectorCoupling");
  xDSigma=settings->parm("MeanField:DeltaSigmaCoupling");
  xYSigma=settings->parm("MeanField:HyperonSigmaCoupling");
  Uns=settings->parm("MeanField:U_NStar");
  Ud=settings->parm("MeanField:U_Delta");
  Ul=settings->parm("MeanField:U_Lambda");
  Us=settings->parm("MeanField:U_Sigma");
  Uls=settings->parm("MeanField:U_LambdaStar");
  Uss=settings->parm("MeanField:U_SigmaStar");
  Ux=settings->parm("MeanField:U_Xi");
  Uxs=settings->parm("MeanField:U_XiStar");
  Uo=settings->parm("MeanField:U_Omega");
  zG81=settings->parm("MeanField:zG81");
  //zG101=settings->parm("MeanField:zG101");
  zG101=zG81;
  tanV=settings->parm("MeanField:tanV");
  alphaV=settings->parm("MeanField:alphaV");

  optNStarCoupling=settings->mode("MeanField:optNStarCoupling");
  optDeltaStarCoupling=settings->mode("MeanField:optDeltaStarCoupling");
  optLambdaStarCoupling=settings->mode("MeanField:optLambdaStarCoupling");
  optSigmaStarCoupling=settings->mode("MeanField:optSigmaStarCoupling");
  optXiStarCoupling=settings->mode("MeanField:optXiStarCoupling");
  optOmegaCoupling=settings->mode("MeanField:optOmegaCoupling");

  optPhiMeson = settings->mode("MeanField:optPhiMeson"); 
  optRhoMeson = settings->mode("MeanField:optRhoMeson"); 
  optMassPDM = settings->mode("MeanField:optMassPDM"); 


  // parity doublet model 1
  if(eosType>=0 && eosType <10) {
    optRMF=2;
    setPDM1Model();

  // RMF: sigma-omega model.
  } else if(eosType>=10 && eosType<20) {
    optRMF=1;
    setSigmaOmegaModel();

  // Singlet model. 
  } else if(eosType>=20 && eosType<40) {
    optRMF=3;
    setSingletModel();
    sigmaIni = f_pion+1e-2;

  // parity doublet model 2
  } else if(eosType>=40 && eosType<150) {
    optRMF=4;
    setPDM2Model();

  } else {
    cout << "RQMDpdm mode: MeanField:EoStype not implemented " << eosType << endl;
    exit(1);
  }

//-------------------------------------------------------------------------------------

  // Set sigma meson mass.
  if(optRMF==3 || optRMF==4) M_sigma=sqrt(A+B)/f_pion;

  // Compute omega-nucleon coupling.
  pF0=pow(3.*M_PI*M_PI*rho0gev/2.,1.0/3.0);
  double mu=M_nucl+BE;
  double mus=sqrt(Meff0*Meff0+pF0*pF0) + pow2(g_phi/M_phi)*rho0gev;
  muS0 = mu - mus;

  if(g_omega == 0.0) {
  // omega coupling square.
  double go2 =M_omega2/(2*rho0gev)*(mu-mus)*(1.+sqrt(1.+(4*C4*rho0gev*(mu-mus))/pow4(M_omega)));

  // save omega coupling
  g_omega = sqrt(go2);
  }

  // introduce nucleon-phi coupling.
  if(optPhiMeson==2) {
    double tmp=(4*alphaV-1)*zG81/sqrt(3.0);
    double r = (-tanV+tmp)/(1+tanV*tmp);
    if(C4==0) {
      g_omega = g_omega /sqrt(1.0+ pow2(r*M_omega/M_phi));
      // other formula.
      //double g_omega2=(mu-mus)/(rho0gev*(1.0/M_omega2+r*r/(M_phi*M_phi)));

    // with omega quartic term
    } else {
      g_omega = sqrt(bisecGomega(r*r));
    }
    g_phi=r*g_omega;
  }

  double fact= transportModel==1 ? 0.5: 1.0;
  //Cv2 =  fact*pow2(g_omega/M_omega)*CONV;  // GeVfm^3
  Cv2 =  pow2(g_omega/M_omega)*CONV;  // GeVfm^3


  // The value of the omega and phi field at the saturation density.
  Omega0 = Omega(g_omega,rho0gev);
  Phi0 = g_phi/(M_phi2)*rho0gev;

  // Compute nucleon single particle potential U_N at p=0
  if(optRMF==1) {
    Un = -g_sigma*Sigma0 + g_omega*Omega0 + g_phi*Phi0;
  } else {
    pair<double,double> g = paramMass(M_nucl,1.535,M0);
    double a=g.first,b=g.second;
    Un = mEff(Sigma0, M0, a, b, 1.0) - M_nucl + g_omega*Omega0 + g_phi*Phi0;

    // Check sigma meson at the saturation density.
    double g1=a+b, g2=a-b;
    double det=sqrt(pow2(Meff0*b)-g1*g2*(M0*M0-Meff0*Meff0));
    double sigma0=(-Meff0*b + det)/(g1*g2);
    if(abs(sigma0-Sigma0) > 1e-9) {
      cout << "ParityDoublet Sigma0= "<< Sigma0 << " sigma0= "<< sigma0<<endl;
      exit(1);
    }

    // compute maximum scalar density.
    sigmaMax=bisec2(0.0,f_pion);
    rhosMax= optRMF==2? funcSigma(sigmaMax,EpsPi):funcSigma2(sigmaMax,EpsPi);
    rhosMax = abs(rhosMax)+EpsPi - 1e-9;

  }

  // rho meson coupling
  double eff=sqrt(Meff0*Meff0 + pF0*pF0);
  g_rho0=sqrt(M_rho*M_rho+C4*Omega0*Omega0)*sqrt(2/(rho0*CONV)*(Esym - pF0*pF0/(6*eff)));
  Cr2 =  fact*pow2(g_rho/M_rho)*CONV;  // Gevfm^3
  if(optRhoMeson) g_rho=g_rho0;

  setHyperonVectorCoupling();

  // Momentum-dependent potential (not implemented)
  C1 = -pow2(gsp/M_sigma)*CONV;
  C2 =  pow2(gvp/M_omega)*CONV;
  vex1 = fact*C1;
  vex2 = fact*C2;
  pmu1 = mu1*mu1;
  pmu2 = mu2*mu2;

  if(firstCall) printInfo();
  // sigma-omega or singlet model
  if(optRMF==1 || optRMF==3) return;

  Md0=Ml0=Ms0=Mx0=Mo0=M0;
  double ml0=settings->parm("MeanField:M0_Lambda");
  double ms0=settings->parm("MeanField:M0_Sigma");
  double mx0=settings->parm("MeanField:M0_Xi");
  double mo0=settings->parm("MeanField:M0_Omega");
  if(ml0 > 0.0) Ml0=ml0;
  if(ms0 > 0.0) Ms0=ms0;
  if(mx0 > 0.0) Ml0=mx0;
  if(mo0 > 0.0) Mo0=mo0;

  // N*
  vector<pair<int,int>> nucl={
     {2112,102112},   //n(939)0  1/2+ n(1535)0 1/2-
     {2212,102212},   //p(938)+  1/2+ p(1535)+ 1/2-
     {202112,122112}, //N(1440)0 1/2+ N(1650)0 1/2- 
     {202212,122212}, //N(1440)+ 1/2+ N(1650)+ 1/2- 
     {212114,102114}, //N(1720)0 3/2+ N(1520)0 3/2- 
     {212214,102214}, //N(1720)+ 3/2+ N(1520)+ 3/2- 
     {202116,102116}, //N(1680)0 5/2+ N(1670)0 3/2- 
     {202216,102216}, //N(1680)+ 5/2+ N(1670)+ 3/2- 
     {102118,112118}, //N(1990)0 7/2+ N(2190)0 7/2- 
     {102218,112218}, //N(1990)+ 7/2+ N(2190)+ 7/2- 
     {212112,302112}, //N(1710)0 1/2+ N(1895)0 1/2- 
     {212212,302212}, //N(1710)+ 1/2+ N(1895)+ 1/2- 
     {302114,112114}, //N(1900)0 3/2+ N(1700)0 3/2- 
     {302214,112214}  //N(1900)+ 3/2+ N(1700)+ 3/2- 
  };

  for(int i=0;i<(int)nucl.size();i++) {
    int charge=pTable->charge(nucl[i].first);
    double gr = charge == 0? -g_rho: g_rho;
    double u=i>0? Uns: Un;
    setParamPDM(1,pBaryon,nucl[i].first,nucl[i].second,M0,1.0,u,g_phi,gr,1);
  }
  // N(1895),N(1900) are newly implemented in JAM2.

  // Delta*
  vector<pair<int,int>> Delta={
    {1114,121114},   //D(1232)-  3/2+ D(1700)-  3/2-
    {2114,122114},   //D(1232)0  3/2+ D(1700)0  3/2-
    {2214,122214},   //D(1232)+  3/2+ D(1700)+  3/2-
    {2224,122224},   //D(1232)++ 3/2+ D(1700)++ 3/2-
    {221112,111112}, //D(1910)-  1/2+ D(1620)-  1/2-
    {222112,112112}, //D(1910)0  1/2+ D(1620)0  1/2-
    {222212,112212}, //D(1910)+  1/2+ D(1620)+  1/2-
    {222222,112222}, //D(1910)++ 1/2+ D(1620)++ 1/2-
    {211116,221116}, //D(1910)-  5/2+ D(1930)-  5/2-
    {212116,222116}, //D(1910)0  5/2+ D(1930)0  5/2-
    {212216,222216}, //D(1910)+  5/2+ D(1930)+  5/2-
    {212226,222226}, //D(1910)++ 5/2+ D(1930)++ 5/2-
    {321112,231112}, //D(1750)-  1/2+ D(1900)-  1/2-
    {322112,232112}, //D(1750)0  1/2+ D(1900)0  1/2-
    {322212,232212}, //D(1750)+  1/2+ D(1900)+  1/2-
    {322222,232222}, //D(1750)++ 1/2+ D(1900)++ 1/2-
    {201114,231114}, //D(1600)-  3/2+ D(1940)-  3/2-
    {202114,232114}, //D(1600)0  3/2+ D(1940)0  3/2-
    {202214,232214}, //D(1600)+  3/2+ D(1940)+  3/2-
    {202224,232224}, //D(1600)++ 3/2+ D(1940)++ 3/2-
    {221114,331114}, //D(1920)-  3/2+ D(2350)-  3/2-
    {222114,332114}, //D(1920)0  3/2+ D(2350)0  3/2-
    {222214,332214}, //D(1920)+  3/2+ D(2350)+  3/2-
    {222224,332224}, //D(1920)++ 3/2+ D(2350)++ 3/2-
    {201118,221118}, //D(1950)-  7/2+ D(2200)-  7/2-
    {202118,222118}, //D(1950)0  7/2+ D(2200)0  7/2-
    {202218,222218}, //D(1950)+  7/2+ D(2200)+  7/2-
    {202228,222228}  //D(1950)++ 7/2+ D(2200)++ 7/2-
  };

  for(int i=0;i<(int)Delta.size();i++) {
    int charge=pTable->charge(Delta[i].first);
    // rho coupling
    double gr = gDr;
    if(charge==-1) gr *= -3;
    else if(charge==0) gr *= -1;
    else if(charge==2) gr *= 3;
    int opt = i > 3? optDeltaStarCoupling:optDeltaCoupling;
    setParamPDM(2,pBaryon,Delta[i].first,Delta[i].second,Md0,1.0,Ud,gDp,gr,opt);
  }
  // D(1750), D(1940), D(2350)5/2- (assume 3/2-),D(2200) are newly implemented in JAM2.


  // Lambda
  vector<pair<int,int>> Lambda={
   {3122,103122},   //L(1116)0 1/2+ L(1670)0  1/2-
   {213122,102132}, //L(1810)0 1/2+ L(1405)0  1/2-
   {203122,123122}, //L(1600)0 1/2+ L(1800)0  1/2-
   {213124,102134}, //L(1890)0 3/2+ L(1520)0  3/2-
   {123124,103124}, //L(2070)0 3/2+ L(1690)0  3/2-
   {203126,123126}, //L(1820)0 5/2+ L(2080)0  5/2-
   {213126,103126}, //L(2110)0 5/2+ L(1830)0  5/2-
   {102138,302138}  //L(2085)0 7/2+ L(2100)0  7/2-
  };

  setParamPDM(3,pBaryon,Lambda[0].first,Lambda[0].second,Ml0,gLo,Ul,gLp,gLr,optHyperonCoupling);
  for(int i=1;i<(int)Lambda.size();i++) {
    setParamPDM(3,pBaryon,Lambda[i].first,Lambda[i].second,Ml0,gLso,Uls,gLsp,gLsr,optLambdaStarCoupling);
  }

  // Sigma
  vector<pair<int,int>> Sigma={
    {3112,113112},    //S(1197)- 1/2+ S(1750)- 1/2-
    {3212,113212},    //S(1193)0 1/2+ S(1750)0 1/2-
    {3222,113222},    //S(1189)+ 1/2+ S(1750)+ 1/2-

    {203112,103112},  //S(1660)- 1/2+ S(2110)- 1/2-
    {203212,103212},  //S(1660)0 1/2+ S(2110)0 1/2-
    {203222,103222},  //S(1660)+ 1/2+ S(2110)+ 1/2-

    {3114,103114},   //S(1385)- 3/2+ S(1670)-  3/2-
    {3214,103214},   //S(1385)0 3/2+ S(1670)0  3/2-
    {3224,103224},   //S(1385)+ 3/2+ S(1670)+  3/2-
    {113114,203114}, //S(1940)- 3/2+ S(1910)-  3/2-
    {113214,203214}, //S(1940)0 3/2+ S(1910)0  3/2-
    {113224,203224}, //S(1940)+ 3/2+ S(1910)+  3/2-

    //{3114,203114},   //S(1385)- 3/2+ S(1910)-  3/2-
    //{3214,203214},   //S(1385)0 3/2+ S(1910)0  3/2-
    //{3224,203224},   //S(1385)+ 3/2+ S(1910)+  3/2-
    //{113114,103114}, //S(1940)- 3/2+ S(1670)-  3/2-
    //{113214,103214}, //S(1940)0 3/2+ S(1670)0  3/2-
    //{113224,103224}, //S(1940)+ 3/2+ S(1670)+  3/2-

    {203116,103116}, //S(1915)- 5/2+ S(1775)-  5/2-
    {203216,103216}, //S(1915)0 5/2+ S(1775)0  5/2-
    {203226,103226}, //S(1915)+ 5/2+ S(1775)+  5/2-
    {203118,103118}, //S(2030)- 7/2+ S(2100)-  7/2-
    {203218,103218}, //S(2030)0 7/2+ S(2100)0  7/2-
    {203228,103228}  //S(2030)+ 7/2+ S(2100)+  7/2-
  };
  int dec[18]={0,0,0, 0,0,0, 1,1,1, 0,0,0, 1,1,1, 0,0,0};


  for(int i=0;i<3;i++) {
    int charge=pTable->charge(Sigma[i].first);
    double gr = charge * gSr;
    setParamPDM(4,pBaryon,Sigma[i].first,Sigma[i].second,Ms0,gSo,Us,gSp,gr,optHyperonCoupling);
  }
  for(int i=3;i<(int)Sigma.size();i++) {
    int charge=pTable->charge(Sigma[i].first);
    double go = gSso;
    double gp = gSsp;
    double gr = charge * gSsr;
    if(dec[i]==1) {
      go = gSso;
      gp = gSsp;
      gr = charge * gSsr;
    }
    setParamPDM(4,pBaryon,Sigma[i].first,Sigma[i].second,Ms0,go,Uss,gp,gr,optSigmaStarCoupling);
  }

  // Xi
  vector<pair<int,int>> Xi={
    {3312,213312}, //Xi(1321)- 1/2+ Xi(1950)- 1/2-
    {3322,213322}, //Xi(1312)0 1/2+ Xi(1950)0 1/2-
    {3314,103314}, //Xi(1530)- 3/2+ Xi(1820)- 3/2-
    {3324,103324}, //Xi(1530)0 3/2+ Xi(1820)0 3/2-
    {113316,203316},//Xi(1690)- 5/2+ Xi(2030)- 5/2-
    {113326,203326} //Xi(1690)0 5/2+ Xi(2030)0 5/2-
  };

  for(int i=0;i<2;i++) {
    int charge=pTable->charge(Xi[i].first);
    double gr = charge==0? gXr: -gXr;
    setParamPDM(5,pBaryon,Xi[i].first,Xi[i].second,Mx0,gXo,Ux,gXp,gr,optHyperonCoupling);
  }
  for(int i=2;i<(int)Xi.size();i++) {
    int charge=pTable->charge(Xi[i].first);
    double go = gXo;
    double gp = gXp;
    double gr = charge==0? gXr: -gXr;
    int id = Xi[i].first;
    if(id == 3314 || id==3324) {
      go = gXso;
      gp = gXsp;
      gr = charge==0? gXsr : -gXsr;
    }
    setParamPDM(5,pBaryon,Xi[i].first,Xi[i].second,Ms0,go,Uxs,gp,gr,optXiStarCoupling);
  }

  // Omega baron
  setParamPDM(6,pBaryon,3334,203334,Mo0,gOo,Uo,gOp,-gOr,optOmegaCoupling); //Omega(1672)- 3/2+ Omega(2250)- 3/2-

}

double ParityDoublet::bisecGomega(double r2)
{
  // bisection method start.
  double x0=1e-5, x1=200.0;
  double f1 =  funcGomega(x1,r2);
  double x,f;
  int itry=0;
  do {
    x = 0.5*(x0+x1);
    f =  funcGomega(x,r2);
    if(f*f1 > 0.0) x1=x;
    else x0=x;
    if(++itry > 20) {
      cout << "does not converge bisecGomega " << x << endl;
      exit(1);
    }
  }while (abs(f) > 1e-5);

  return x;
}

void ParityDoublet::printInfo()
{
  cout << " (RQMD.PDM mode eosType= "<< eosType << ")" << endl
      << " rho0= " << rho0 << endl
      << " Un= " << Un << endl
      << " g_omega= "<< g_omega
      << " g_rho= "<< g_rho
      << " g_phi= "<< g_phi
      << " g_Lphi= "<< gLp <<endl
      << " Cv^2= "<< Cv2
      << " M0= "<< M0 << " Md0= "<< Md0 << " Ml0= "<< Ml0 <<endl
      << " Ms0= "<< Ms0 << " Mx0= "<< Mx0 << " Mo0= "<< Mo0 <<endl
      << " M_sigam= "<< M_sigma
      << " A= "<< A << " B= "<< B << " C= "<< C
      << " C4= "<< C4 << endl
      << " optPhiMeson= "<< optPhiMeson
      << " optRhoMeson= "<< optRhoMeson
      << " vex1=  "<< vex1
      << " vex2=  "<< vex2
      << " mu1=  "<< mu1
      << " mu2=  "<< mu2 << endl
      << " optDeltaCoupling= "<< optDeltaCoupling
      << " optHyperonCoupling= "<< optHyperonCoupling
      <<endl;
    firstCall=false;
}

// slope of symmetry energy
double ParityDoublet::getL()
{
  double mu0s=sqrt(Meff0*Meff0+pF0*pF0);
  double cr4=3*C4;
  double mrw=M_rho*M_rho + cr4*Omega0*Omega0;
  double mow=M_omega2 + 3*C4*Omega0*Omega0;
  double l1=(3*rho0gev*g_rho*g_rho)/(2*mrw)*(1-(2*cr4*rho0gev*g_omega*Omega0)/(mrw*mow));
  double l2=pF0*pF0/(3*mu0s)*(1-K/(6*mu0s));
  double l3=(rho0gev*pF0*pF0)/(2*mu0s*mu0s)*(g_omega*g_omega/mow + pow2(g_phi/M_phi));
  return l1+l2+l3;
}


void ParityDoublet::setHyperonVectorCoupling()
{
  double sq2=sqrt(2.0);
  double sq3=sqrt(3.0);

  if(optVectorCoupling==1) {
    double zsq3=zG81/sq3;
    double tanv3=tanV*zsq3;
    double go=1+tanv3*(4*alphaV-1);

    //g_phi = (tanv-(4*alphaV-1)*zsq3)/go

    // hyperon omega coupling
    gLo=(1+tanv3*2*(alphaV-1))/go;
    gSo=(1-tanv3*2*(alphaV-1))/go;
    gXo =(1-tanv3*(2*alphaV+1))/go;
    gLso=gLo;
    double z23=zG101*sq3;
    double gdo=1+tanV*z23;
    gSso=1.0/gdo;
    gXso =(1.0-tanV*z23)/gdo;
    gOo =(1.0-tanV*2*z23)/gdo;

    if(optRhoMeson) {
      gLr=0.0;
      gSr=2*alphaV*g_rho;
      gXr=(2*alphaV-1)*g_rho;
      double g10=g_rho;
      gDr=g10;
      gLsr=gLr;
      gSsr=2*g10;
      gXsr=g10;
      gOsr=0.0;
    }
    if(optPhiMeson) {
      gLp=(2*(alphaV-1)*zsq3-tanV)/go*g_omega;
      gSp=(-2*(alphaV-1)*zsq3-tanV)/go*g_omega;
      gXp =(-(2*alphaV+1)*zsq3-tanV)/go*g_omega;
      gDp=(-tanV+z23)/gdo;
      gLsp=gLp;
      gSsp=-tanV/gdo*g_omega;
      gXsp =(-tanV-z23)/gdo*g_omega;
      gOp =(-tanV - 2*z23)/gdo*g_omega;
    }
    return;
  }


  // omega coupling
  gLo=(5*g_omega - sq2*g_phi - 3*g_rho0)/6.0/g_omega;
  gSo=(g_omega   - sq2*g_phi + g_rho0)/2.0/g_omega;
  gXo=(g_omega   - sq2*g_phi - g_rho0)/2.0/g_omega;

  gLso= gLo;
  gSso= (g_omega - g_rho0)/g_omega;
  gXso=g_omega/g_omega;
  gOo=(g_omega-3*g_rho0)/g_omega;
  gOso=gOo;

  if(optRhoMeson) {
    gSr=(g_omega + sq2*g_phi + g_rho0)/2.0;
    gXr=(g_omega + sq2*g_phi - g_rho0)/2.0;

    gDr=g_rho0;
    gSsr=2*g_rho0;
    gXsr=g_rho0;
  }

  if(optPhiMeson) {
    gLp=-(sq2*g_omega -4*g_phi + 3*sq2*g_rho0)/6.0;
    gSp=-(g_omega - g_rho0)/sq2;
    gXp=-(g_omega + g_rho0)/sq2;

    gDp=(-g_omega + 3*g_rho0)/sq2;
    gLsp=gLp;
    gSsp= (-g_omega + g_rho0)/sq2;
    gXsp=-(g_omega + g_rho0)/sq2;
    gOp=-(g_omega + 3*g_rho0)/sq2;
    gOsp=gOp;
  }

}

void ParityDoublet::setParamPDM(int potid,unordered_map<int,ParamPDM>& p, int id1, int id2, double m0,double xv,double uv,double gphi,double grho,int opt,double m1,double m2)
{
  // opt=0: this baryon does not feel potentials.
  // opt=1: baryon-omega coupling is the same as nucleon
  // opt=2: baryon-omega coupling is fixed by SU(3)
  // opt=3: baryon-omega coupling is fixed by the single particle potential U
  // opt=4: baryon-sigma coupling is fixed by the single particle potential U

  ParamPDM pdm;
  array<double,12> fp;

  fp[0]=Sigma0; // sigma field at saturation density n0
  fp[1]=m0;     // chiral invariant mass

  //pair<double, double> a=paramMass(id1,id2,m0);
  if(m2==0) {
    ParticleDataEntryPtr pd1=jamtable->find(id1); // positive parity
    ParticleDataEntryPtr pd2=jamtable->find(id2); // negative parity
    m1 = pd1->m0(), m2=pd2->m0();
    if(optMassPDM==2) {
      m1=pd1->mMin(); m2= pd2->mMin();
    }
  }
  pair<double, double> a=paramMass(m1,m2,m0);
  fp[2]=a.first;  // sigma coupling g1
  fp[3]=a.second; // sigma coupling g2
  fp[4]=1.0;  // parity
  fp[5]=1.0; // xv = gv/g_{Nomega}
  fp[8]=0.0; // explicit symmetry breaking mass
  fp[9]=gphi; // phi coupling
  fp[10]=grho; // rho coupling
  if(opt==2) {
    fp[5]=xv;
  } else if(opt==3) {
    fp[5] = (uv- mEff(Sigma0, m0, fp[2],fp[3], 1.0) + m1 - gphi*Phi0)/(g_omega*Omega0);
  } else if(opt==4) {
    fp[5]=xv;
    fp[2]=getGSigma(fp[3],fp[1],Sigma0,fp[5]*g_omega,uv,Omega0,gphi,Phi0);
    // explicit symmetry breaking term
    fp[8]= m1 - mEff(f_pion, m0, fp[2], fp[3], 1.0); 

  } else if(opt==5) {
    fp[5]=xv;
    double g=fp[3];
    double m=m1;
    double parity=1.0;
    if(m1>m2) {
      g=-fp[3];
      m=m2;
      parity=-1.0;
    }
    fp[2]=getGSigma(g,fp[1],Sigma0,fp[5]*g_omega,uv,Omega0,gphi,Phi0);
    fp[8]= m - mEff(f_pion, m0, fp[2], fp[3], parity); 
  }

  fp[6] = dMdSigma(Sigma0, fp[1],fp[2],fp[3],fp[4]);
  fp[7] = d2MdSigma2(Sigma0, fp[1],fp[2],fp[3]);

  pdm.fp = fp;
  pdm.opt = opt;
  pdm.potid = potid;
  pdm.uv = uv;
  pdm.id2 = id2;
  pdm.m2 = m2;
  // minimum mass
  pdm.fp[11]=min(fp[1],mEffMin(fp[1],fp[2],fp[3],1.0) + fp[8]);
  p[id1]=pdm;

  pdm.fp[4]=-1.0;
  pdm.fp[11]=min(fp[1],mEffMin(fp[1],fp[2],fp[3],-1.0) + fp[8]);
  pdm.id2 = id1;
  pdm.m2 = m1;
  p[id2]=pdm;

  /*
  //if(id1==3334) {
  if(id1==2112) {
    cout <<" opt= "<< opt <<endl;
    cout <<" a1= "<< fp[2] << " a2= "<< fp[3]<<endl;
    cout <<" a1= "<< pdm.fp[2] << " a2= "<< pdm.fp[3]<<endl;
    cout << "m1= "<< m1 <<  " m2= " << m2 << " m0= "<< m0 << " m1= "<< pdm.fp[8] <<endl;
    cout <<"g="<< g_omega*fp[5] << " fp5= "<< fp[5] << " g_o= " << g_omega <<endl;
    cin.get();
  }
  */
  
}

void ParityDoublet::setParamPDM2(int id1, double m1, ParamPDM& pdm)
{
  array<double,12> fp=pdm.fp;
  double m2=pdm.m2;
  if(m1< fp[1]) {
    cout <<"setParamPDM2 m1= "<< m1 << " m2= "<< m2 << " fp1= "<< fp[1] << " id= "<< id1<<endl;
    exit(1);
  }
  if(fp[4] < 0.0) swap(m1,m2);

  //fp[0]=Sigma0; // sigma field at saturation density n0
  //fp[1]=m0;     // chiral invariant mass

  pair<double, double> a=paramMass(m1,m2,fp[1]);
  fp[2]=a.first;  // sigma coupling g1
  fp[3]=a.second; // sigma coupling g2

  fp[6] = dMdSigma(Sigma0, fp[1],fp[2],fp[3],fp[4]);
  fp[7] = d2MdSigma2(Sigma0, fp[1],fp[2],fp[3]);
  pdm.fp[2]=fp[2];
  pdm.fp[3]=fp[3];
  pdm.fp[6]=fp[6];
  pdm.fp[7]=fp[7];

  //fp[4]=1.0;  // parity
  //fp[5]=1.0; // xv = gv/g_{Nomega}
  //fp[8]=0.0; // explicit symmetry breaking mass
  //fp[9]=gphi; // phi coupling
  //fp[10]=grho; // rho coupling

  if(pdm.opt==3) {
    pdm.fp[5] = (pdm.uv- mEff(Sigma0, fp[1], fp[2],fp[3], 1.0) + m1 -fp[9]*Phi0)/(g_omega*Omega0);
  } else if(pdm.opt==4) {
    pdm.fp[2]=getGSigma(fp[3],fp[1],Sigma0,fp[5]*g_omega,pdm.uv,Omega0,fp[9],Phi0);
    pdm.fp[8]= m1 - mEff(f_pion, fp[1], pdm.fp[2], fp[3], 1.0); 
  } else if(pdm.opt==5) {
    double g=fp[3];
    double m=m1;
    double parity=1.0;
    if(m1>m2) {
      g=-fp[3];
      m=m2;
      parity=-1.0;
    }
    pdm.fp[2]=getGSigma(g,fp[1],Sigma0,fp[5]*g_omega,pdm.uv,Omega0,fp[9],Phi0);
    pdm.fp[8]= m - mEff(f_pion, fp[1], fp[2], fp[3], parity); 
  }

  pdm.fp[11]=min(pdm.fp[1],mEffMin(fp[1],fp[2],fp[3],fp[4])+pdm.fp[8]);

  //if(fp[4]>0.0) pdm.fp[11]=mEffMin(fp[1],fp[2],fp[3],1.0)+pdm.fp[8];
  //else pdm.fp[11]=pdm.fp[1];
}

// set particle for sigma-omega (Walecka) model or singlet model.
bool  ParityDoublet::setParticle1(EventParticle* p,double* fp)
{
  int pid = p->getPID();
  bool isopt=true;
  int optS=optHyperonSigmaCoupling;
  int opt=1;
  double U=Un;
  double vfac=1.0;
  double sfac=xYSigma;
  double gphi= g_phi;
  double grho= g_rho;
  int charge=p->charge()/3;
  switch(pid) {
    case id_nucl:   isopt=false;sfac=1.0;optS=0;
		    if(charge==0) {grho=-g_rho;}break;
    case id_nucls:  opt=optNStarCoupling;U=Un;sfac=1.0;optS=0;
		    if(charge==0) {grho=-g_rho;} break;
    case id_delt:   opt=optDeltaCoupling;U=Ud;optS=optDeltaSigmaCoupling;sfac=xDSigma;
                    grho = g_rho;
                    if(charge==-1) grho *= -3;
                    else if(charge==0) grho *= -1;
                    else if(charge==2) grho *= 3;
		    break;
    case id_delts:  opt=optDeltaStarCoupling;U=Ud;optS=optDeltaSigmaCoupling;sfac=xDSigma;
                    grho = g_rho;
                    if(charge==-1) grho *= -3;
                    else if(charge==0) grho *= -1;
                    else if(charge==2) grho *= 3;
		    break;
    case id_lambda: opt=optHyperonCoupling;U=Ul;vfac=gLo;gphi=gLp; grho=gLr;break;
    case id_lambdas:opt=optLambdaStarCoupling;U=Uls;vfac=gLo;gphi=gLp; grho=gLr;break;
    case id_sigma:  opt=optHyperonCoupling;U=Us;vfac=gSo;gphi=gSp;
		    grho=charge*gSr;
		    break;
    case id_sigmas: opt=optSigmaStarCoupling;U=Uss;vfac=gSo;gphi=gSp;
		    grho=charge*gSr;
		    break;
    case id_xi:     opt=optHyperonCoupling;U=Ux;vfac=gXo;gphi=gXp;
		    grho= charge==0? gXr : -gXr;
		    break;
    case id_xis:    opt=optXiStarCoupling;U=Uxs;vfac=gXo;gphi=gXp;
		    grho= charge==0? gXr: -gXr;
		    break;
    case id_omega:  opt=optHyperonCoupling;U=Uo;vfac=gOo;gphi=gOp;grho=-gOr;break;
    case id_omegas: opt=optHyperonCoupling;U=Uo;vfac=gOo;gphi=gOp;grho=-gOr;break;
    default: cout << "ParityDoublet::setPotentialParam wrong pid= "<< pid <<endl;return false;
  };

  // This baryon does not feel potential.
  if(opt==0) return false;

  double m0=p->getMass();
  if(optMassPDM==1) {
    int ida  = abs(p->getID());
    m0=jamtable->find(ida)->m0();
  } else if(optMassPDM==2) {
    int ida  = abs(p->getID());
    m0=jamtable->find(ida)->mMin();
  }

  fp[0]=Sigma0;
  if(optRMF==1) {
    fp[1]= m0;
    fp[2]=0.0;
    fp[3]=-g_sigma;
  } else {
    fp[1]= 0.0;
    fp[2]=m0/f_pion;
    fp[3]=0.0;
  }
  fp[4]= 1.0; // parity
  fp[5]= 1.0; // ratio of the omega coupling
  fp[6] = dMdSigma(Sigma0, fp[1],fp[2],fp[3],fp[4]); // dM/ds = sigma coupling
  //fp[7] = d2MdSigma2(Sigma0, fp[1],fp[2],fp[3]);
  fp[7]=0.0;

  fp[9]=gphi;
  fp[10]=grho;
  fp[11]=0.0;

  if(isopt) {
    // test sigma coupling in RMF mode.
    if(optRMF==1 && optS) fp[3]=-sfac*g_sigma;

    // simple quark counting for omega coupling
    if(opt==2) {
       fp[5]=vfac;

    // fit depth of the optical potential by changing g_omega
    // normalized by the nucleon omega coupling
    } else if(opt==3) {
       double meff=mEff(Sigma0, fp[1], fp[2], fp[3], 1.0);
       fp[5] = (U- meff + m0 - gphi*Phi0)/(g_omega*Omega0);

    // fit depth of the optical potential by changing g_sigma
    } else if(opt==4 || opt==5) {
      fp[5]=vfac;
      if(optRMF==1) {
        fp[3]=(U-g_omega*vfac*Omega0 - gphi*Phi0)/Sigma0;
      } else if(optRMF==3) {
        fp[2]=(U-g_omega*vfac*Omega0 - gphi*Phi0)/(Sigma0-f_pion);
        fp[8]=m0-fp[2]*f_pion;
      }
    }
  }
  if(optRMF==1) fp[1]= 0.0;
  p->setPotentialId(1);
  p->setPotentialParam(fp);
  return true;
}

bool ParityDoublet::setPotentialParam(EventParticle* p)
{
  // 0. sigma field
  // 1. m0: chiral invariant mass in the parity doublet model
  // 2. g1: sigma coupling  (=0 for the Walecka model)
  // 3. g2: sigma coupling  (=0 for the singlet model)
  // 4. a:  parity
  // 5. Ratio of the omega coupling xv= g_{omega,i}/g_{omega,N}
  // 6. Derivative of mass with respect to sigma field: dm/ds,
  // 7. dm^2/ds^2,
  // 8. m_1: explicit SU(3) symmetry breaking term
  // 9. phi meson coupling
  //10. rho meson coupling
  //11. minumum mass
  //              0    1    2    3    4    5   6      7          8    9    10
  //            sigma, m0,  g1,  g2,  a,   xv, dm/ds  dm^2/ds^2  m_1
  double fp[12]={0.0,  0.0, 0.0, 0.0, 0.0, 0.0, 1.0,  1.0,       0.0, 0.0, 0.0,0.0};

  // exclude mesons.
  if(p->baryon()==0) return false;

  // =1:sigma-omega model, =3:singlet model.
  if(optRMF==1 || optRMF==3) return setParticle1(p,fp);

  int id  = p->getID();
  int ida  = abs(id);

  //if(pBaryon.contains(ida)) { // -std=c++20
  if(pBaryon.count(ida)) {
    ParamPDM& baryon=pBaryon[ida];

    // This baryon does not feel potential
    if(baryon.opt==0) return false;

    // find a new sigma coupling for resonance to ensure correct vacuum mass,
    // since resonance can have different vacuum mass.
    if(optMassPDM==3 && p->getParticleDataEntry()->mWidth() > 1e-5) {

      if(p->getEffectiveMass() < baryon.fp[11]) {
	p->setPotS(baryon.fp[11] - p->getMass() + 1e-4);
	//cout <<"Parity Doublet meff= "<< p->getEffectiveMass()
	// << " meff_min= "<< baryon.fp[11]  <<endl;
      }
      setParamPDM2(id, p->getMass(),baryon);
    }
      
    for(int i=0;i<12;i++) fp[i] = baryon.fp[i];
    //array<double,12> fp=baryon.fp;

    p->setPotentialId(baryon.potid);
    p->setPotentialParam(fp);
    return true;
  }
  //if(p->getPID() != id_delts && p->getPID() != id_nucls)
  cout << "ida= "<< ida <<endl;
  return false;

}

double ParityDoublet::bisec1(double sigma0, double sigma1,double rhos)
{
  // bisection method start.
  //double sigma0=0.0, sigma1=f_pion;
  double f1 =  optRMF==2? funcSigma(sigma1,rhos):funcSigma2(sigma1,rhos);
  double f=1.0;
  double sigma=0.0;
  int itry=0;
  do {
    sigma=0.5*(sigma0+sigma1);
    f =  optRMF==2? funcSigma(sigma,rhos):funcSigma2(sigma,rhos);
    if(f*f1 > 0.0) sigma1=sigma;
    else sigma0=sigma;
    if(++itry > 100) {
      cout << "does not converge bisec1 " << sigma << " rhos= "<< rhos << endl;
      exit(1);
    }
      cout << "bisec1 " << sigma << " f= "<< f << " rhos= "<< rhos
           << " rhomax= "<< rhosMax
           << " sigmax= "<< sigmaMax
	<< endl;
  }while (abs(f) > 1e-8);

  return sigma;
}

double ParityDoublet::bisec2(double sigma0, double sigma1)
{
  // bisection method start.
  //double sigma0=0.0, sigma1=f_pion;
  double f1 =  optRMF==2? dfuncSigma(sigma1):dfuncSigma2(sigma1);
  double f=1.0;
  double sigma=0.0;
  int itry=0;
  do {
    sigma=0.5*(sigma0+sigma1);
    f =  optRMF==2? dfuncSigma(sigma):dfuncSigma2(sigma);
    if(f*f1 > 0.0) sigma1=sigma;
    else sigma0=sigma;
    if(++itry > 100) {
      cout << "does not converge bisec2 " << sigma << endl;
      exit(1);
    }
  }while (abs(f) > 1e-8);

  return sigma;
}

double ParityDoublet::newton(double s0, double rhos,int opt)
{
  double s=s0;
  int ntry=0;
  do {
    s0=s;
    double f=optRMF   > 2? funcSigma2(s0,rhos):funcSigma(s0,rhos);
    double df= optRMF > 2? dfuncSigma2(s0): dfuncSigma(s0);
    s = s0 - f/df;

    // improved Newton method.
    double df2 = optRMF > 2? f - funcSigma2(s,rhos): f - funcSigma(s,rhos);
    if(abs(df2) < 1e-9) break;
    s = s0 - f*f/(df*df2);

    if(ntry++>20) {
      cout <<"ParityDoublet::sigma_bisec too many try eps= "<< (s-s0)/s0
         << " rhos= "<< rhos << " sigma= "<< s
        << " smax= "<< sigmaMax
        << " rhosmax= "<< rhosMax
	<<endl;
      if(opt==1) return -1.0;
      return Sigma0;
    }
      //cout <<ntry << " sigma_bisec= "<< s << " df= "<< f/df
      //   << " rhos= "<< rhos << " sigma= "<< s << " df2="<< df2
      //   << " s'= "<< f_pion+1e-4 - funcSigma2(f_pion+1e-4,rhos)/dfuncSigma2(f_pion+1e-4)
      //   << " opt= "<< opt
      //  <<endl;
  } while(abs((s-s0)/s0)>1e-8);

  if(opt==1 && (s<0.0 || s>f_pion)) {
    cout << "s0= "<< s0 << scientific << " rhos= "<< rhos << " s= "<< s
      << " f_pion= "<< f_pion
      << " s-f_pion= "<< s-f_pion
      << " sigma0= "<< Sigma0
      << " f= "<< funcSigma2(s,rhos)
      << " s= "<< (s-s0)/s0
      << " n= "<< ntry
      << " optRMF= "<< optRMF
      << " smax= "<< sigmaMax
      << " rhosmax= "<< rhosMax
      << " f/df= "<< funcSigma2(f_pion,rhos)/dfuncSigma2(f_pion)
      <<endl;
    if(opt==1) return -1.0;
    return Sigma0;
  }
  return s;
}


// Find sigma field in GeV
double ParityDoublet::sigma_bisec(double scalarDens)
{
  if(scalarDens>=rhosMax) {
    return sigmaMax;
  }
  if(scalarDens < 1e-15) return f_pion;
  //return bisec1(0.0,s,scalarDens);
  double sgm = newton(sigmaIni,scalarDens,1);
  if(sgm>0.0) return sgm;
  sigmaIni=f_pion+1e-2;
  return newton(sigmaIni,scalarDens,2);
//----------------------------------------------------------------------------------------


// obsolete
  double x0=(scalarDens-EpsPi)/A;
  //if(optRMF==4) x0 *= f_pion2;
  //double x0=(scalarDens-EpsPi)/A3;
  double sigma0= x0 > 0.0 ?  x0+f_pion*1e-3: 0.0;

  double sigma1=f_pion;
  double f0 = optRMF==2? funcSigma(sigma0,scalarDens):funcSigma2(sigma0,scalarDens);
  double f1 = optRMF==2? funcSigma(sigma1,scalarDens):funcSigma2(sigma1,scalarDens);

  double sig0=sigma1;
  // seeking a new initial condition.
  if(f0*f1 > 0.0) {
    do {
      sigma0 += f_pion*0.01;
      f0=optRMF==2? funcSigma(sigma0,scalarDens):funcSigma2(sigma0,scalarDens);
      if(f0*f1 < 0.0) break;
    }while(sigma1 > sigma0);

    if(sigma1 < sigma0) {
      return Sigma0;
      return 0.0;
      cout << "sigma_bisec no solution  sigma1 = "<< sigma1
      << " rhos in 1/fm^3= "<< scalarDens/HBARC3
      << " rhos in rho0= "<< scalarDens/HBARC3/rho0
      << " rhos in GeV^3= "<< scalarDens
           << " sigma0= "<< sig0
           <<endl;
    }
  }

  // check if sigma field is positive.
  if(sigma1 <0.0) {
      cout << " sigma < 0 ? "<< sigma1 << " sig0= "<< sig0 <<endl;
      exit(1);
  }

  // check if initial condition is ok.
  if(f0*f1 > 0.0) {
    cout << "bisec no solution sigma= " << sigma1
	 << " dens= "<< scalarDens <<endl;
    if(optRMF==2) {
    cout << "func1= " << funcSigma(sigma0,scalarDens)
         << " f2= "<< funcSigma(sigma1,scalarDens)
  	 <<endl;
    } else {
    cout << "func1= " << funcSigma2(sigma0,scalarDens)
         << " f2= "<< funcSigma2(sigma1,scalarDens)
  	 <<endl;
    }
    exit(1);
    return 0.0;
  }

  // bisection method start.
  double f=1.0;
  double sigma=0.0;
  int itry=0;
  do {
    sigma=0.5*(sigma0+sigma1);
    f =  optRMF==2? funcSigma(sigma,scalarDens):funcSigma2(sigma,scalarDens);
    if(f*f1 > 0.0) sigma1=sigma;
    else sigma0=sigma;
    if(++itry > 50) {
      cout << "does not converge sigma_bisec " << sigma << endl;
    }
  }while (abs(f) > 1e-5);

  //ofs << scalarDens << "  "<< sigma<<endl;
  return sigma;
}

double ParityDoublet::bisec3(double sigma1)
{
  // bisection method start.
  double sigma0=0.0;
  double f1 =  dfuncSigmaRMF(sigma1);
  double f=1.0;
  double sigma=0.0;
  int itry=0;
  do {
    sigma=0.5*(sigma0+sigma1);
    f =  dfuncSigmaRMF(sigma);
    if(f*f1 > 0.0) sigma1=sigma;
    else sigma0=sigma;
    if(++itry > 20) {
      cout << "does not converge bisec3 " << sigma << endl;
      exit(1);
    }
  }while (abs(f) > 1e-5);

  return sigma;
}

// Find sigma field in GeV
double ParityDoublet::sigma_bisecRMF(double scalarDens, double gs,double sigmax)
{
  //double sol=cubicEquation(scalarDens,A,B,C);
  //if(sol<0 || sol>sigmax) {
  //  cout << "ParityDoublet::sigma_bisecRMF sigma= "<< sol << " rhos= "<< scalarDens <<endl;
  //  if(sol<0) sol=0.0;
  //  else if(sol>sigmax) sol=sigmax-1e-4;
  //}
  //return sol;

  double sigma0= 0.0;
  double sigma1= sigmax;
  double f0 = funcSigmaRMF(sigma0,scalarDens);
  double f1 = funcSigmaRMF(sigma1,scalarDens);
  
  double sig0=sigma1;
  // seeking a new initial condition.
  if(f0*f1 > 0.0) {
    do {
      //sigma0 += f_pion*0.01;
      //f0=funcSigmaRMF(sigma0,scalarDens);
      sigma1 -= 0.03;
      f1=funcSigmaRMF(sigma1,scalarDens);
      if(f0*f1 < 0.0) break;
    }while(sigma1 > sigma0);

    if(sigma1 < sigma0) {
      cout << "sigma_bisecRMF no solution  sigma1 = "<< sigma1
      << " scalarDens= "<< scalarDens
      << " rhos in 1/fm^3= "<< scalarDens/HBARC3/gs
      << " rhos in rho0= "<< scalarDens/HBARC3/rho0/gs
      << " rhos in GeV^3= "<< scalarDens/gs
      << " sigma0= "<< sig0
      << " gs= "<< gs
           <<endl;
      return 0.0;
    }
  }

  // check if sigma field is positive.
  if(sigma1 <0.0) {
      cout << "sigma_bisecRMF sigma < 0 ? "<< sigma1 << " sig0= "<< sig0 <<endl;
      exit(1);
  }

  // check if initial condition is ok.
  if(f0*f1 > 0.0) {
    cout << "bisec no solution sigma= " << sigma1
	 << " dens= "<< scalarDens <<endl;
    cout << "func1= " << funcSigmaRMF(sigma0,scalarDens) << " f2= "<< funcSigmaRMF(sigma1,scalarDens)
	<<endl;
    exit(1);
    return 0.0;
  }

  double s0=sigma0, s1=sigma1;
  // bisection method start.
  double f=1.0;
  double sigma=0.0;
  int itry=0;
  do {
    sigma=0.5*(sigma0+sigma1);
    f =  funcSigmaRMF(sigma,scalarDens);
    if(f*f1 > 0.0) sigma1=sigma;
    else sigma0=sigma;
    if(++itry > 50) {
      cout << "2 does not converge sigma_bisecRMF sigma= " << sigma << " rhos= "<< scalarDens
	<< " sigma0= "<< s0 << " sigma1= "<< s1
	<< endl;
      exit(1);
    }
  }while (abs(f) > 1e-5);

  return sigma;
}

// [22] I. J. Shin, W.-G. Paeng, M. Harada, and Y. Kim, arXiv:1805.03402. Table IV
// Myungkuk Kim, Sangyong Jeon, Young-Min Kim, Youngman Kim, and Chang-Hwan Lee,
// Phys Rev C 101, 064614 (2020)
void ParityDoublet::setPDM1Model()
{
// meff=  0.7801195112980114  M*/M= 0.8307982015953264 rho0= 0.16
  // BE=  -0.015999999998961956
  // P=  -1.2754554357119474e-15
  // K=  0.21499999999333802
  // e(0)=  -0.34056665820750553 -0.00041867932023280504
  // sigma(rho0)= 0.06265711151614894 muB= 0.9291705594699763
  if(eosType==0) {
    Sigma0 = 0.06265711151614894;
    M0= 0.7; Md0= 0.7;
    rho0=0.16;
    Meff0=  0.7801195112980114;

    // N(939) N(1535)
    //g1= 14.170825359352433  g2= 7.7622232088148
    //GN1[0]= 21.933048568167234 * 0.5; // (g1+g2)/2
    //GN2[0]= -6.408602150537633 * 0.5; // (g2-g1)/2

    // Delta(1232) Delta(1700)
    // g3= 16.36647629578016  g4= 11.334218231264028;
    //GD1[0]=27.700694527044188 * 0.5;
    //GD2[0]= -5.032258064516132 * 0.5;

    M_sigma= 0.38450347939154145;
    lam6fPion= 15.735060094211498;
    g_omega= 7.052292393272834;
    g_rho= 4.092575261035796;
    A= 0.18103249758693601;  B= 38.89999217733509;  C= 1819.2924146388596;

    //rho0= 0.168;
    // m0= 0.5 m_pion= 0.139 f_pion= 0.093
    // g1= 15.370382103087197  g2= 8.961779952549563
    // m_sigma= 0.4033436261248961  lambda6*f_pion^2= 17.7649735684408  g_omega= 10.9721459348452
    // g_rho= 3.288737505505816 m_rho= 0.776
    // A= 0.2060107967612345  B= 43.8179041686529  C= 2053.991625441184
    // sigma(rho0)= 0.047810782975293745 muB= 0.8130548270355539
    // Cv= 196.36298549727695 196.36298549727695 239.51044962319452
    // meff=  0.6138329887519894  M*/M= 0.6537092531970069 rho0= 0.168
    // single e =  -0.016000000000000014
    // e(0)=  -0.3563869998418047 -0.00046003464050140415
    // BE=  -0.015999999996137104
    // P=  -4.985432639631471e-15
    // K=  0.21499999988207982
  } else if(eosType==1) {
    M0= 0.5;
    M_sigma= 0.4033436261248961;  lam6fPion= 17.7649735684408;  g_omega= 10.9721459348452;
    g_rho= 3.288737505505816;
    Sigma0= 0.047810782975293745;
    Meff0=  0.6138329887519894;

  } else if(eosType==2) {
  // rho0= 0.168
  // m0= 0.6 m_pion= 0.139 f_pion= 0.093
  // g1= 14.835951613935757  g2= 8.427349463398123
  // m_sigma= 0.42268509858653897  lambda6*f_pion^2= 17.712171408266677  g_omega= 8.650120340020125
  // g_rho= 3.7950798823066565 m_rho= 0.776
  // A= 0.21354241679365457  B= 44.63591019814465  C= 2047.886623686747
  // sigma(rho0)= 0.058046449578468856 muB= 0.9229999999999999
  // Cv= 122.04522001998059 122.04522001998059 122.04522001998059
  // meff=  0.7172528565569739  M*/M= 0.7638475575686624 rho0= 0.168
  // single e =  -0.016000000000000014
  // e(0)=  -0.3692581105758294 -0.00047664904226692623
  // BE=  -0.016000000002287518
  // P=  2.953908818936757e-15
  // K=  0.21500000002504813
    M0=0.6;
    M_sigma= 0.42268509858653897; lam6fPion= 17.712171408266677; g_omega= 8.650120340020125;
    Sigma0= 0.058046449578468856;
    g_rho= 3.7950798823066565;
    Meff0=  0.7172528565569739;


  } else if(eosType==3) {
  // sigma(rho0)= 0.06257260765213898 muB= 0.9309970400680866
  // Cv= 76.46053396503561
  // meff=  0.7797413321109258  M*/M= 0.830395454857216 rho0= 0.168
  // single e =  -0.016000000000000014
  // e(0)=  -0.3376784003753442 -0.00043588503955171103
  // BE=  -0.015999999990084612
  // P=  -1.2799277696734501e-14
  // K=  0.21499999999371922
  //A= 0.19145461916855394; B= 40.95994368272281; C= 1918.1411429308987;
  // M_sigma= 0.384428 M_omega= 0.783
  //  rho0= 0.168;
    M0= 0.7;
    M_sigma= 0.39229882851020176; lam6fPion= 16.590002745209343; g_omega= 6.84668622824865;
    g_rho= 3.9457227516313598;
    Sigma0= 0.06257260765213898;
    Meff0=  0.7797413321109258;

    //A3= 0.00016471155600000003;  B3= 0.001165833499308284;  C3 = -0.00993042896029791; D3 = 0.0;

  } else if(eosType==4) {
  // rho0= 0.168
  // m0= 0.8 m_pion= 0.139 f_pion= 0.093
  // g1= 13.349329879299919  g2= 6.940727728762285
  // m_sigma= 0.33027539550169255  lambda6*f_pion^2= 11.141693259032216  g_omega= 5.294062762404347
  // g_rho= 4.0073137805551475 m_rho= 0.776
  // A= 0.12192392343426936  B= 27.472474093148225  C= 1288.205949708893
  // sigma(rho0)= 0.06283817916453191 muB= 0.9229999999999999
  // Cv= 45.71457085721052 45.71457085721052 45.71457085721052
  // meff=  0.8215845278153883  M*/M= 0.8749568986319365 rho0= 0.168
  // single e =  -0.016000000000000014
  // e(0)=  -0.2475198550034851 -0.0003195057832188948
  // BE=  -0.015999999996833436
  // P=  -4.0876048205962245e-15
  // K=  0.2149999999938342
    M0= 0.8;
    M_sigma= 0.33027539550169255; lam6fPion= 11.141693259032216;  g_omega= 5.294062762404347;
    g_rho= 4.0073137805551475;
    Sigma0= 0.06283817916453191;
    Meff0=  0.8215845278153883;


  // M0=0.9
  } else if(eosType==5) {
  // rho0= 0.168
  // m0= 0.9 m_pion= 0.139 f_pion= 0.093
  // g1= 12.329330277267774  g2= 5.9207281267301415
  // m_sigma= 0.2627578192269408  lambda6*f_pion^2= 2.077846367163067  g_omega= 3.232191362169835
  // g_rho= 4.072720554609878 m_rho= 0.776
  // A= 0.02351062901204221  B= 7.030052288314901  C= 240.24122640340698
  // sigma(rho0)= 0.05949597466697888 muB= 0.9229999999999999
  // Cv= 17.040039866455427 17.040039866455423 17.040039866455423
  // meff=  0.8604245993896107  M*/M= 0.9163201271454854 rho0= 0.168
  // single e =  -0.016000000000000014
  // e(0)=  -0.12644088166628545 -0.00016321354473605888
  // BE=  -0.016000000000081394
  // P=  1.0524892589403034e-16
  // K=  0.21500000000012764
    M0= 0.9;
    M_sigma= 0.2627578192269408; lam6fPion= 2.077846367163067; g_omega= 3.232191362169835;
    g_rho= 4.072720554609878;
    Sigma0= 0.05949597466697888;
    Meff0=  0.8604245993896107;

  } else {
    cout << "RQMDpdm PDM1 mode: MeanField:EoStype not implemented " << eosType << endl;
    exit(1);
  }

  std::tuple<double,double,double> t =coefScalarPot(M_sigma,lam6fPion);
  A = std::get<0>(t);
  B = std::get<1>(t);
  C = std::get<2>(t);
  D = 0.0;
}



 
void ParityDoublet::setPDM2Model()
{
  // Parity doublet model =================================================================================
  // Jurgen Eser and Jean-Paul Blaizot nucl-th2309.06566
  // Eduardo S. Fraga,Rodrigo da Mata,and J. Schaffner-Bielich2,hep-ph2309.02368

  A=M_pion*M_pion*f_pion2;

// m0=0.7 -----------------------------------------------------------------------------
    if(eosType==41) { // K=0.24
      M_nucl=0.938;
      Sigma0= 0.05087455594876428;
      M0= 0.7 ;Meff0= 0.78*M_nucl; C4= 0.0 ;
      B= 0.0004592119418789858; C = -0.01697394419650093; D = -0.08016790890661943;

    // m_sigma= 0.3917069434700516  g_omega= 6.858858568503943
    } else if(eosType==42) { 
      //K=0.215;
      //M0=0.7;Meff0=0.83*M_nucl;
      //B= 0.0011623419603873613;  C = -0.00995975609736991; D = -0.0006209487914824753;
      //Sigma0= 0.06248953645686446;

      M_nucl=0.938;
      Sigma0= 0.05579390168268775; K=0.24;
      M0= 0.7 ;Meff0= 0.8*M_nucl; C4= 0.0 ;
      B= 0.0008532248710544381; C = -0.012062637273254004; D = -0.04185309532130668;

    // m_sigma= 0.4085163025350842  g_omega= 6.858858568503943
    } else if(eosType==43) { //K=0.24;
      //M0=0.7;Meff0=0.83*M_nucl;
      //B= 0.001278681734060063;  C = -0.007414535770392405; D = 0.017940048043771317;
      //Sigma0= 0.06248953645686446;

      M_nucl=0.938;
      Sigma0= 0.06244261144005894;
      M0= 0.7 ;Meff0= 0.83*M_nucl; C4= 0.0 ;
      B= 0.0012712101179685811;  C = -0.007429021433024273; D = 0.017321951637020112;

     } else if(eosType==44) { //K=0.24
       M_nucl=0.938;
       Sigma0= 0.058090914255549894;
       M0= 0.7 ;Meff0= 0.81*M_nucl; C4= 0.0 ;
       B= 0.001002798781828682; C = -0.010374718946356722; D = -0.024238108099217393;

     } else if(eosType==45) { //K=0.24
       M_nucl=0.938;
       Sigma0= 0.060303003472438284;
       M0= 0.7;Meff0= 0.82*M_nucl; C4= 0.0 ;
       B= 0.0011387659647906806; C = -0.008899268507550582; D = -0.005265734008654749;

// m0=0.6 -----------------------------------------------------------------------------
    // m_sigma= 0.496480414712725  g_omega= 6.858858568503943
    } else if(eosType==46) { //K=0.215;
      M0=0.6;Meff0=0.83*M_nucl;
      B= 0.00196720469017002;  C = -0.009746676370679351; D = 0.10406380572586442;
      Sigma0= 0.06873768730124252;

    } else if(eosType==47) { //K=0.24;
      // m_sigma= 0.5169265363368714  g_omega= 6.858858568503943
      M0=0.6;Meff0=0.83*M_nucl;
      B= 0.0021464140612899116;  C = -0.005006828155399918; D = 0.14585135467581617;
      Sigma0= 0.06873768730124252;


    // m_sigma= 0.571910382580437  g_omega= 6.858858568503943
    } else if(eosType==48) { //K=0.215;
      M0=0.5;Meff0=0.83*M_nucl;
      B= 0.0026642162138478575; C = -0.011988627017492188; D = 0.22067712028080655;
      Sigma0= 0.07219900428445725;

    // m_sigma= 0.5953962304716174  g_omega= 6.858858568503943
    } else if(eosType==49) { //K=0.24;
      M0=0.5;Meff0=0.83*M_nucl; 
      B= 0.0029013301537261084; C = -0.004826983820248504; D = 0.29277907164781836;
      Sigma0 = 0.07219900428445725;

    // m_sigma= 0.6416411110982385  g_omega= 6.858858568503943
    } else if(eosType==50) { // K=0.24
      //M0=0.46; Meff0=0.83*M_nucl;//K=0.260
      //B= 0.003396110419339003;  C = 0.0018309671992826114; D = 0.42725348637225596;
      //Sigma0= 0.07317230614217692;

       M_nucl=0.938;
       Sigma0= 0.05355918617426453;
       M0= 0.77 ;Meff0= 0.83*M_nucl; C4= 0.0 ;
       B= 0.0003588964637663389;  C = -0.014937393675247119; D = -0.07928473527400205;


    //} else if(eosType==49) { //K=0.240
    //  M0= 0.46;Meff0= 0.83*M_nucl;
    //  Sigma0= 0.07317230614217692;
    //  B= 0.003178809802061433;  C = -0.0050140926764261195; D = 0.3553794081343844;

    //} else if(eosType==50) { //K=0.240
    //  Sigma0= 0.056919157112081536;
    //  M0= 0.75 ;Meff0= 0.83*M_nucl;
    //  B= 0.000704315773067826; C = -0.011430736042153907; D = -0.04621956657199954;

      // does not work!
    //} else if(eosType==50) { //K=0.240
    //  Sigma0= 0.049275008941849135;
    //  M0= 0.79 ;Meff0= 0.83*M_nucl;
    //  B= -0.0001429620032637133;  C = -0.02108811448823034; D = -0.12305492280006365;


    // m_sigma= 0.46237725615977654  g_omega= 7.724939376757135
    } else if(eosType==51) { //K=0.215;
      M0=0.6;Meff0=0.8*M_nucl;
      B= 0.0016843817399427336;  C = -0.010123428116800202; D = 0.038453839699841376;
      Sigma0= 0.06402616571301815;

    // m_sigma= 0.5947692982440794  g_omega= 7.724939376757135
    } else if(eosType==52) { //K=0.260
      M0=0.46; Meff0=0.8*M_nucl;
      B= 0.0028948766753388445;  C = -0.003821904500051865; D = 0.22513108081393982;
      Sigma0= 0.06953285566012056;

    // m_sigma= 0.44130953198481004  g_omega= 8.250210087757706
    } else if(eosType==53) { //K=0.215;
      Sigma0= 0.060765618526258836;
      M0=0.6;Meff0=0.78*M_nucl;
      B= 0.0015197166810256195;  C = -0.010197056642543594; D = 0.01534936952190744;


    // m_sigma= 0.40425909209610805  g_omega= 8.978025472191026
    } else if(eosType==54) { //K=0.215;
      M0=0.6;Meff0=0.75*M_nucl;
      B= 0.0012487548457279546;  C = -0.011312792569275872; D = -0.012459759708850823;
      Sigma0= 0.055642514427722174;

    // m_sigma= 0.4713995729558624  g_omega= 11.041012424506084
    } else if(eosType==55) { //K=0.260
      //M0=0.46;Meff0=0.65*M_nucl;
      //B= 0.0017572480978053024;  C = -0.004333117380471646; D = 0.05076604177098075;
      //Sigma0= 0.050100484076397236;

      M_nucl=0.938;
      Sigma0= 0.0709490049261978;
      M0= 0.4;Meff0= 0.8*M_nucl; C4= 0.0 ;
      B= 0.0030913357667526216;  C = -0.008401300579286996; D = 0.2460113976270332;

    } else if(eosType==56) { //K=0.24
      //M0=0.41 ;Meff0=0.83*M_nucl; C4= 0.0;
      //Sigma0= 0.07416485506501452;
      //B= 0.003503409800725019;  C = -0.005360296284816003; D = 0.43447316701585986;

      M_nucl=0.938;
      Sigma0= 0.07433322167463588;
      M0= 0.4 ;Meff0= 0.83*M_nucl; C4= 0.0;
      B= 0.0035590208001657487;  C = -0.005359417194227459; D = 0.45043320488717764;

    } else if(eosType==57) { //K=0.24
      //Sigma0= 0.0684413322058643;
      //M0= 0.41;Meff0= 0.78*M_nucl; C4= 0.0 ;
      //B= 0.0028332766227366075; C = -0.007846300306888957;D = 0.17968485529362135;

      M_nucl=0.938;
      Sigma0= 0.06867213426067453;
      M0= 0.4 ;Meff0= 0.78*M_nucl; C4= 0.0 ;
      B= 0.0028797126122699023; C = -0.007927662392063196; D = 0.18693843557436962;

    //} else if(eosType==33) { //K=0.215;
    //  M0=0.8;Meff0=0.84*M_nucl;
    //  B= -8.756553967234534e-05;  C = -0.019813519608288702; D = -0.11657974791629933;
    //  Sigma0= 0.05075910107215111;


// omega quartic term ====================================================================

    } else if(eosType==60) { //K=0.240
      Sigma0= 0.06248953645686446;
      M0=0.7;Meff0=0.83*M_nucl;C4=3.0;
      B= 0.001282169018587416;  C = -0.007300371227638684; D = 0.018910681003727094;

    } else if(eosType==61) { //K=0.240
      //Sigma0= 0.06248953645686446;
      //M0=0.7;Meff0=0.83*M_nucl;C4=3.8;
      //B= 0.0012830964338223219;  C = -0.007270008650899023; D = 0.019168828730686416;

      M_nucl=0.938;
      Sigma0= 0.05560486882049909;
      M0= 0.6 ;Meff0= 0.75*M_nucl;
      B= 0.0013358373445499384; C = -0.009133396053647339; D = 0.0022098516227373674;

    // m0=0.7
    } else if(eosType==62) { //K=0.240
      //Sigma0= 0.06248953645686446; 
      // M0= 0.7;Meff0= 0.83*M_nucl; C4= 5.0;
      // B= 0.001284485570143165;  C = -0.007224528693660897; D = 0.019555509716635257;

      M_nucl=0.938;
      Sigma0= 0.06073579467249012;
      M0= 0.6 ;Meff0= 0.78*M_nucl; C4= 20.0;
      B= 0.0016353851655410796; C = -0.007193705870378163;D = 0.03751029114719743;


    } else if(eosType==63) { //K=0.240
      //Sigma0= 0.06248953645686446;
      //M0= 0.7; Meff0= 0.83*M_nucl; C4= 10.0;
      //B= 0.0012902481347763246;  C = -0.007035849380251755; D = 0.02115973958603577;

      M_nucl=0.938;
      Sigma0= 0.05579390168268775;
      M0= 0.7 ;Meff0= 0.8*M_nucl; C4= 20.0 ;
      B= 0.0008695969997720279; C = -0.0115048318697987; D = -0.03758338108792406;


    } else if(eosType==64) { //K=0.240
      // m_sigma= 0.39205172789323317  g_omega= 7.285932628597089  C4= 21.0
      //Sigma0= 0.06248953645686446;
      //M0=0.7;Meff0=0.83*M_nucl;C4=21.0;
      //B= 0.0011646791604688592;  C = -0.006248543660805786; D = 0.020150978296160037;

      M_nucl=0.938;
      Sigma0= 0.06244261144005894;
      M0= 0.7 ;Meff0= 0.83*M_nucl; C4= 20.0 ;
      B= 0.0012940602041003242; C = -0.00668212299953896; D = 0.02366372386294083;


    } else if(eosType==65) { //K=0.240
      Sigma0= 0.06248953645686446;
      // m_sigma= 0.40271794602238986  g_omega= 7.3732199666213205  C4= 100.0
      M0=0.7;Meff0=0.83*M_nucl;C4=100.0;
      B= 0.0012379983482754115;  C = -0.003834033341457004; D = 0.040713952507899234;


    // m0=0.41 ===============================================
    } else if(eosType==70) { //K=0.240
      //Sigma0= 0.07416485506501452;
      //M0= 0.41;Meff0= 0.83*M_nucl; C4= 3.0;
      //B= 0.003512789030845804; C = -0.004921578900620465; D = 0.44000542963795825;
      //Sigma0= 0.07085852036778122;
      //M0= 0.4 ;Meff0= 0.8*M_nucl; C4= 20.0 ;
      //B= 0.0030378348544334906;  C = -0.007280505793715097; D = 0.266148939359448;

      M_nucl=0.938;
      Sigma0= 0.0709490049261978;
      M0= 0.4 ;Meff0= 0.8*M_nucl; C4= 20.0 ;
      B= 0.003146572529757716;  C = -0.005914737597234995; D = 0.27411664123263774;


    } else if(eosType==71) { //K=0.240
      //Sigma0= 0.07416485506501452;
      //M0= 0.41;Meff0= 0.83*M_nucl; C4= 3.8 ;
      //B= 0.0035152833947314026;  C = -0.0048048995399524504; D = 0.4414767833030518;

      M_nucl=0.938;
      Sigma0= 0.07433322167463588;
      M0= 0.4 ;Meff0=0.83*M_nucl; C4= 20.0 ;
      B= 0.0036217538851683684;  C = -0.002404244087022221; D = 0.48798600274039317;


    } else if(eosType==72) { //K=0.240
      //Sigma0= 0.07416485506501452;
      //M0= 0.41;Meff0= 0.83*M_nucl; C4= 5.0;
      //B= 0.003519019623690607;  C = -0.004630125705449868; D = 0.44368073524725804;

      M_nucl=0.938;
      Sigma0= 0.06867213426067453;
      M0= 0.4 ;Meff0= 0.78*M_nucl; C4= 20.0 ;
      B= 0.002931677843539697; C = -0.0056482446550973784; D = 0.2109823398405504;


    } else if(eosType==73) { //K=0.240
      M_nucl=0.938;
      Sigma0= 0.06522108482538586;
      M0= 0.4;Meff0= 0.75*M_nucl; C4= 20.0;
      B= 0.002691281441872297; C = -0.004172642690561305; D = 0.16162122349630342;

    } else if(eosType==74) { //K=0.240
       M0= 0.41;Meff0= 0.83*M_nucl; C4= 10.0;
       Sigma0= 0.07416485506501452;
       B= 0.003534518984481768;  C = -0.0039050496003902864; D = 0.4528243421972581;
      //M0= 0.41;Meff0= 0.83*M_nucl; C4= 21.0;
      //Sigma0= 0.07416485506501452;
      //B= 0.003568235747253801;  C = -0.002327494682115234; D = 0.47271914159501155;



    } else if(eosType==75) { //K=0.240
      //Sigma0= 0.07433824442590918;
      //M0= 0.4; Meff0= 0.83*M_nucl; C4= 21.0;
      //B= 0.0036310514208638574;  C = -0.0023300298720804117; D = 0.4897593650156445;

      M_nucl=0.938;
      Sigma0= 0.07433322167463588;
      M0= 0.4 ;Meff0= 0.83*M_nucl; C4= 20.0 ;
      B= 0.0036217538851683684; C = -0.002404244087022221; D = 0.48798600274039317;

    } else if(eosType==76) { //K=0.240
      Sigma0= 0.06522925173093945;
      M0=0.4; Meff0=0.75*M_nucl;C4=5.0;
      B= 0.0026602391380370315;  C = -0.005722721730687829; D = 0.147099523499012;

    } else if(eosType==77) { //K=0.240
      Sigma0= 0.06867903496116644;
      M0=0.4; Meff0=0.78*M_nucl;C4=5.0;
      B= 0.0028987663546388796;  C = -0.0073704420467120575; D = 0.19327317045361056;

    } else if(eosType==78) { //K=0.240
      Sigma0= 0.06867903496116644;
      M0=0.4; Meff0=0.78*M_nucl;C4=10.0;
      B= 0.00291184611911224;  C = -0.0067960808404690445; D = 0.19933443798487144;

    } else if(eosType==79) { //K=0.240
      Sigma0= 0.06867903496116644;
      M0=0.4; Meff0=0.78*M_nucl;C4=20.0;
      B= 0.002937600659952143;  C = -0.005664627912475035; D = 0.2112760874113314;


    } else if(eosType==80) { //K=0.240
      Sigma0= 0.07219900428445725;
      M0=0.5; Meff0=0.83*M_nucl;C4=21.0;
      B= 0.002667428707999239;  C = -0.002743750697522549; D = 0.2860322102352992;



// D=0  ====================================================================

    } else if(eosType==90) { //K=0.215
      M0= 0.7;Meff0= 0.8303076933036835*M_nucl; C4= 0.0;
      Sigma0= 0.06255418037095724;
      A= 0.00016471155600000003;  B= 0.001165833499308284;  C = -0.00993042896029791; D = 0.0;

      //double ms= 0.39229882851020176, l6p= 16.590002745209343;
      //std::tuple<double,double,double> t =coefScalarPot(ms,l6p);
      //A3 = std::get<0>(t);
      //B3 = std::get<1>(t);
      //C3 = std::get<2>(t);
      //D3 = 0.0;

    } else if(eosType==91) { //K=0.240
      M0= 0.7 ;Meff0= 0.8222373915515988*M_nucl; C4= 0.0;
      Sigma0= 0.06083856155455922;
      A= 0.00016471155600000003; B= 0.0011761106339692256; C = -0.008553923978394131;D = 0.0;

    } else if(eosType==92) { //K=0.240
      M0= 0.7 ;Meff0= 0.8194144450759822*M_nucl; C4= 21.0 ;
      Sigma0= 0.060228061756911595;
      A= 0.00016471155600000003;  B= 0.0011605831699387878;  C = -0.008242669229041317; D = 0.0;

    } else if(eosType==93) { //K=0.240
      M0= 0.8;Meff0= 0.8697101763342974*M_nucl; C4= 0.0;
      Sigma0= 0.06123245297751835;
      A= 0.00016710732900000004;  B= 0.0008549447152209708;  C = -0.005191945200254549; D = 0.0;

    } else if(eosType==94) { //K=0.240
      M0= 0.8; Meff0= 0.8695462321934424*M_nucl; C4= 21.0;
      Sigma0= 0.061181413658726036;
      A= 0.00016471155600000003; B= 0.0008154240789666746; C = -0.005381666333480953;D = 0.0;

    } else if(eosType==95) { //K=0.240
      M0= 0.75;Meff0= 0.83*M_nucl; C4= 334.80646443355744;
      Sigma0= 0.056919157112081536;
      A= 0.00016471155600000003; B= 0.0008974377298285008; C = -0.005377843794170912;D = 0.0;

    } else if(eosType==96) { //K=0.240
      Sigma0= 0.046439681522856414;
      M0= 0.5 ;Meff0= 0.645* M_nucl; C4= 10.890808500788626 ;
      A= 0.00016471155600000003;  B= 0.0011947458416936376;  C = -0.010398128831637243; D = 0.0;

 // phi + C4 L=82MeV
    } else if(eosType==100) { //K=0.240
      // g_rho= 3.9302522819958066 m_rho= 0.776
      // m_sigma= 0.6518242770469914  g_omega= 6.290199227693348  C4= 3.0
      g_phi= 3.5702056828755033;
      Sigma0= 0.07416485506501452;
      M0=0.41; Meff0=0.83*M_nucl;C4=3.0;
      B= 0.003510031351590607;  C = -0.005050516792958933; D = 0.4383797200242668;
      optPhiMeson=1;

    } else if(eosType==101) { //K=0.240 L=82MeV
      // g_rho= 3.9302841636427437 m_rho= 0.776
      // m_sigma= 0.6513236267846767  g_omega= 2.39953609016969  C4= 21.0
      g_phi= 8.370802572327191;
      Sigma0= 0.07416485506501452;
      M0=0.41; Meff0=0.83*M_nucl;C4=21.0;
      B= 0.0035043885594219157;  C = -0.005314468888999018; D = 0.4350512306393309;
      optPhiMeson=1;

    } else if(eosType==102) { //K=0.240 L=82MeV
      // g_rho= 3.9302522819958066 m_rho= 0.776
      // m_sigma= 0.4088645302496704  g_omega= 6.290199227693348  C4= 3.0
      M0=0.7;Meff0=0.83*M_nucl;C4=3.0;
      g_phi= 3.5702056828755033;
      Sigma0= 0.06248953645686446;
      B= 0.001281143539228757;  C = -0.0073339254166929395; D = 0.01862544370778662;
      optPhiMeson=1;

    } else if(eosType==103) { //K=0.240 L=82MeV
      // g_rho= 3.9302841636427437 m_rho= 0.776
      // m_sigma= 0.40856777946312367  g_omega= 2.39953609016969  C4= 21.0
      g_phi= 8.370802572327191;
      Sigma0= 0.06248953645686446;
      M0=0.7;Meff0=0.83*M_nucl;C4=21.0;
      B= 0.0012790455194630335;  C = -0.007402611704695574; D = 0.018041462864918097;
      optPhiMeson=1;

    } else if(eosType==104) { //K=0.240 L=0.0855
      //g_rho= 3.767542601023421 m_rho= 0.776
      //m_sigma= 0.5707380487324897  g_omega= 7.1202793495321135  C4= 5.0
      g_phi= 7.134961767062342;
      Sigma0= 0.06522925173093945;
      M0=0.4;Meff0=0.75*M_nucl;C4=5.0;
      B= 0.0026526303124236174;  C = -0.006042983514287938; D = 0.14402477011768544;
      optPhiMeson=1;

    } else if(eosType==105) { //K=0.240 L=0.0855
      g_rho= 3.767604606252072;
      g_phi= 9.647971467418268;
      Sigma0= 0.06522925173093945;
      M0=0.4;Meff0=0.75*M_nucl;C4=10.0;
      //m_sigma= 0.5943276069918602  g_omega= 6.406617487905334  C4= 5.0
      B= 0.0028903341020381714;  C = -0.00774033269238197; D = 0.18937071895245175;
      optPhiMeson=1;

    } else if(eosType==106) { //K=0.240 L=0.084
      g_rho= 3.8327192055881114;
      g_phi= 6.779186900796085;
      //m_sigma= 0.5943276069918602  g_omega= 6.406617487905334  C4= 5.0
      Sigma0= 0.06867903496116644;
      M0=0.4;Meff0=0.78*M_nucl;C4=5.0;
      B= 0.0028903341020381714;  C = -0.00774033269238197; D = 0.18937071895245175;
      optPhiMeson=1;

    } else if(eosType==107) { //K=0.240 L=0.83
      g_rho= 3.837744852908133;
      g_phi= 3.451020976116169;
      //m_sigma= 0.5956125150515384;  g_omega= 7.524649651080625;
      Sigma0= 0.06867903496116644;
      M0=0.4;Meff0=0.78*M_nucl;C4=10.0;
      B= 0.002903558108675979;  C = -0.0071587340355837034; D = 0.1955107526234421;
      optPhiMeson=1;



    } else {
      cout << "RQMDpdm PDM2 mode: MeanField:EoStype not implemented " << eosType << endl;
      exit(1);
    }
}

// Singlet model
// Eduardo S. Fraga, Rodrigo da Mata, Savvas Pitsinigkos, and Andreas Schmitt,
// nucl-th2206.09219
void ParityDoublet::setSingletModel()
{
    M0=0.0,Md0=0.0,Ml0=0.0,Ms0=0.0,Mx0=0.0,Mo0=0.0;
    A=M_pion*M_pion*f_pion2;

    // m_sigma= 3.237074252473986  g_omega= 2.1487302406557784
    //if(eosType==20) { //K=0.24
    //  M0=0.0;Meff0=0.93*M_nucl;
    //  B= 0.09046512983794361;  C = 7.636747580507403; D = 244.65445117241325;
    //  Sigma0= 0.08649000000000003;

    if(eosType==20) { //K=0.24
      M_nucl=0.938;
      //Sigma0= 0.06967571884984027;
      //M0= 0.0;Meff0= 0.75*M_nucl;C4= 0.0 ;
      //B= 0.0035628543330318233;  C = -0.009467850785069894; D = 0.2776047245618482;

      Sigma0= 0.06974999999999999;
      M0= 0.0 ;Meff0= 0.75*M_nucl; C4= 0.0 ;
      B= 0.003666986009386402;  C = -0.008199874595763686; D = 0.28439697703331723;


    } else if(eosType==21) { //K=0.24
      M_nucl=0.938;
      //Sigma0= 0.07246274760383388;
      //M0= 0.0;Meff0= 0.78*M_nucl; C4= 0.0;
      //B= 0.003837713863921861;  C = -0.013094913614413874; D = 0.35150163506724086;

      Sigma0= 0.07254;
      M0= 0.0 ;Meff0= 0.78*M_nucl; C4= 0.0;
      B= 0.003966503358730021;  C = -0.011368873439176282; D = 0.3622384969104394;


    } else if(eosType==22) { //K=0.24
      M_nucl=0.938;
      //Sigma0= 0.07432076677316296;
      //M0= 0.0 ;Meff0= 0.8*M_nucl; C4= 0.0;
      //B= 0.004092677426140448;  C = -0.01450168559885513; D = 0.45268291010467526;

      Sigma0= 0.0744;
      M0= 0.0 ;Meff0= 0.8*M_nucl; C4= 0.0 ;
      B= 0.00424262602388269;  C = -0.012350921755604892; D = 0.46764346967912496;


    // m_sigma= 0.7873563732932186  g_omega= 6.858858568503943
    //} else if(eosType==22) { //K=0.26
    //  M0=0.0;Meff0=0.83*M_nucl;
    //  B= 0.005197063520532578;  C = 0.00468150916167484; D = 0.9984624093175181;
    //  Sigma0= 0.07719000000000001;

    // m_sigma= 0.7328424798209737  g_omega= 6.858858568503943
    //} else if(eosType==22) { //K=0.215;
    //  M0=0.0;Meff0=0.83*M_nucl;
    //  B= 0.004480303952890605;  C = -0.022965918712653903; D = 0.6429827838038088;
    //  Sigma0= 0.07719000000000001;

    // m_sigma= 0.7629469738061259  g_omega= 6.858858568503943
    } else if(eosType==23) {
      //M0=0.0;Meff0=0.83*M_nucl; //K=0.24;
      //B= 0.004869768289780512; C = -0.007943186335025219; D = 0.8361390985274466;
      //Sigma0 = 0.07719000000000001;

      M_nucl=0.938;
      Sigma0= 0.07719;
      M0= 0.0;Meff0= 0.83*M_nucl; C4= 0.0 ;
      B= 0.004864028999895559; C = -0.007819632861236395; D = 0.8373929776761665;


    // m_sigma= 0.9424296587017298  g_omega= 6.858858568503943
    } else if(eosType==24) { //K=0.38;
      M0=0.0;Meff0=0.83*M_nucl;
      B= 0.0075171024431841;  C = 0.09417192083579341; D = 2.1490945105598023;
      Sigma0= 0.07719000000000001;


    // m_sigma= 0.6521733976742431  g_omega= 8.978025472191026
    } else if(eosType==25) { //K=0.24;
      Sigma0= 0.07254000000000002;
      M0= 0.0;Meff0= 0.78*M_nucl; C4= 0.0;
      B= 0.003972270728975707;  C = -0.011401355018065346; D = 0.3622040106585446;

      //M0=0.0; Meff0=0.75*M_nucl; // K=0.215
      //B= 0.0035139688303431763;  C = -0.012560548835378143; D = 0.24480061077039192;
      //Sigma0= 0.06975;

    // m_sigma= 0.6660742137319567  g_omega= 8.978025472191026
    } else if(eosType==26) { //K=0.24;
      M0=0.0;Meff0=0.75*M_nucl; 
      B= 0.003672459312560076;  C = -0.00821338132314314; D = 0.2845461423108266;
      Sigma0= 0.06975;

    // m_sigma= 0.7470588335271471  g_omega= 8.978025472191026
    } else if(eosType==27) { //K=0.38;
      M0=0.0; Meff0=0.75*M_nucl;
      B= 0.004662268538594893;  C = 0.018935671733811835; D = 0.5327660559744147;
      Sigma0= 0.06975;

// with omega quartic term omega^4 ====================================================================

    } else if(eosType==28) { //K=0.24
      Sigma0= 0.07719000000000001;
      M0= 0.0;Meff0= 0.83*M_nucl; C4= 1.0;
      B= 0.004874079594793407;  C = -0.007707603047405776; D = 0.8396135487379841;

    } else if(eosType==29) { //K=0.24
     Sigma0= 0.07719000000000001;
     M0= 0.0;Meff0= 0.83*M_nucl; C4= 0.5;
     B= 0.004871924711716211;  C = -0.00782535323568427; D = 0.8378769323601232;


    } else if(eosType==30) { //K=0.24
      M_nucl=0.938;
      //Sigma0= 0.06967571884984027;
      //M0= 0.0 ;Meff0= 0.7500000000000001*M_nucl; C4= 20.0 ;
      //B= 0.003632100282007897;  C = -0.006159832254966485; D = 0.31421274449746195;

      Sigma0= 0.06974999999999999;
      M0= 0.0 ;Meff0= 0.75* M_nucl; C4= 20.0 ;
      B= 0.0037344279404491467;  C = -0.0049245121336565865; D = 0.32085984132079604;


    } else if(eosType==31) { //K=0.24
      M_nucl=0.938;
      //Sigma0= 0.07432076677316296;
      //M0= 0.0 ;Meff0= 0.8*M_nucl; C4= 20.0;
      //B= 0.004169580181799154;  C = -0.010553765725532583; D = 0.5040994299403833;

      Sigma0= 0.0744;
      M0= 0.0 ;Meff0= 0.8*M_nucl; C4= 20.0 ;
      B= 0.004317710921831987; C = -0.008438043743856805;D = 0.5189534924769085;

      //Sigma0= 0.07719000000000001;
      //M0= 0.0;Meff0= 0.83*M_nucl; C4= 3.0;
      //B= 0.004882683778820818;  C = -0.007237429251411807; D = 0.8465478712402492;

    } else if(eosType==32) { //K=0.24
      M_nucl=0.938;
       Sigma0= 0.07719;
       M0= 0.0 ;Meff0= 0.83*M_nucl; C4= 20.0 ;
       B= 0.004948995088382799; C = -0.0031800703724013; D = 0.9058038230900126;


    } else if(eosType==33) { //K=0.24
      Sigma0= 0.07719000000000001;
      M0= 0.0;Meff0= 0.83*M_nucl; C4= 3.8;
      B= 0.004886118596182934;  C = -0.0070497291526271134; D = 0.8493161756569009;
    //M_sigma= 0.7673560623031397;  g_omega= 6.883139987804812;  

    } else if(eosType==34) { //K=0.24
      Sigma0= 0.07719000000000001;
      M0= 0.0;Meff0= 0.83*M_nucl; C4= 5.0;
      B= 0.004891263501823079;  C = -0.00676857344416456; D = 0.8534628402493727;

    } else if(eosType==35) { //K=0.24
      Sigma0= 0.07719000000000001;
      M0= 0.0;Meff0= 0.83*M_nucl; C4= 10.0;
      B= 0.00491260662665052;  C = -0.005602155536552193; D = 0.8706662443366171;

    } else if(eosType==36) { //K=0.24, L=  0.08453545694355168
      Sigma0= 0.07719;
      M0=0.0; Meff0=0.83*M_nucl; C4= 21.0;
      B= 0.004959035785578758; C = -0.0030643676839862575; D = 0.9080976850929412;

    } else if(eosType==37) { //K=0.24
      Sigma0= 0.07719000000000001;
      M0=0.0;Meff0= 0.83*M_nucl; C4= 50.0;
      B= 0.005078104748186062;  C = 0.00344637078298682; D = 1.0041400597969492;

    // m_sigma= 0.7929012810739848  g_omega= 6.970876919126405  C4= 100.0
    //} else if(eosType==38) { //K=0.24
    //  Sigma0= 0.0771900000000000;
    //  M0=0.0; Meff0= 0.83*M_nucl; C4= 100.0;
    //  B= 0.0052728493707823;  C = 0.014102890543092253; D = 1.1613738283597301;

    // phi-nucleon coupling ==============================================================
    //} else if(eosType==38) { //K=0.24
    //  g_phi= 3.5702056828755033; M_phi= 1.02;
    //  Sigma0= 0.07719000000000001;
    //  M0= 0.0; Meff0= 0.83*M_nucl; C4= 3.0;
    //  B= 0.004878886386223865;  C = -0.0074448491096474915; D = 0.8434891538753693;
    //  optPhiMeson=1;

    } else if(eosType==38) { //K=0.24 phi
      Sigma0= 0.07719000000000001;
      // m_sigma= 0.7630490923390769  g_omega= 2.39953609016969  C4= 21.0
      g_rho= 3.9302841636427437;
      g_phi= 8.370802572327191;
      M0= 0.0; Meff0= 0.83*M_nucl; C4= 21.0;
      B= 0.004871116084896262;  C = -0.007869464336761653; D = 0.8372267086530124;
      optPhiMeson=1;

    } else if(eosType==39) { //K=0.24 phi L=0.083
      g_rho= 3.837744852908133;
      g_phi= 4.451020976116169;
      // m_sigma= 0.6936550539235689  g_omega= 7.524649651080625  C4= 10.0
      Sigma0= 0.07254000000000002;
      M0= 0.0; Meff0= 0.78*M_nucl; C4= 10.0;
      B= 0.003996818224327752; C = -0.010153684704824029; D = 0.3774787268352753;
      optPhiMeson=1;


    } else {
      cout << "RQMDpdm mode: singlet model MeanField:EoStype not implemented " << eosType << endl;
      exit(1);
    }
}

// if(eosType>=10 && eosType<20)
void ParityDoublet::setSigmaOmegaModel()
{
    // RMF NS2 K=210, m*/m=0.83
    double gs=  7.901789322846869;
    double g2= 44.31272165001529;
    double g3= 21.98880833814041;
    g_omega = 6.858858568503943;
    M_nucl=0.938;
    M_sigma = 0.55;      // sigma mass (GeV)

    //if (eosType==10) { // NS4 K=380 MeV m*/m=0.65 rho0=0.168
    //  Meff0=0.65*M_nucl;
    //  gs= 9.499304490913083;
    //  g2= 4.51465816689875;
    //  g3= 2.444809464564049;
    //  g_omega= 11.032305124735593;

    if (eosType==10) { // NS1 K=380 MeV m*/m=0.83 rho0=0.168 PRC102(2020)
      Meff0=0.83*M_nucl;
      gs=  6.447696268747421;
      g2= -38.001619264755874;
      g3= 339.59550852599887;
      g_omega= 6.858858568503943;

    } else if (eosType==11) {// NS2 K=210 MeV m*/m=0.83 rho0=0.168 PRC102(2020)
      Meff0=0.83*M_nucl;
      gs=  7.901789322846869;
      g2= 44.31272165001529;
      g3= 21.98880833814041;
      g_omega = 6.858858568503943;

    } else if (eosType==12) {// NS3 K=380 MeV m*/m=0.7 rho0=0.168 PRC102(2020)
      Meff0=0.70*M_nucl;
      gs=  8.863811172925521;
      g2= 2.190629372074654; 
      g3= 27.070648189159165;
      g_omega= 10.067747992111274;


    //} else if (eosType==12) { // NS5 K=210 MeV m*/m=0.9 rho0=0.168
    //  Meff0=0.9*M_nucl;
    //  gs=  5.524246186973503;
    //  g2= -81.33367916298765;
    //  g3= 1311.0129665518198;
    //  g_omega= 4.1623094631430595;

    //} else if (eosType==13) { // NS2_C4 K=210 MeV m*/m=0.83 rho0=0.168
    //  Meff0=0.83*M_nucl;
    //  C4= 21.0;
    //  gs=  7.866501182738715;
    //  g2= 40.245831045382154;
    //  g3= 50.54746920169588;
    //  g_omega= 6.875614958864659;

    // K=240
    //} else if (eosType==11) { // K=240 MeV m*/m=0.65 rho0=0.168
    //  Meff0= 0.65*M_nucl;
    //  gs=  9.881049903178162;
    //  g2= 16.429764053372786;
    //  g3= -42.071516780610324;
    //  g_omega= 11.032305124735593;

    } else if (eosType==13) { // NS2' K=240 MeV m*/m=0.75 rho0=0.168
      gs=  8.758057738656774;
      g2= 23.685740355913186;
      g3= -31.272245120477393;
      g_omega= 8.970116460911852;
      C4= 0.0;
      Meff0=  0.7035;  // M*/M= 0.7500000000000001 rho0= 0.168

    //} else if (eosType==12) { // NS2' K=240 MeV m*/m=0.78 rho0=0.168
    //  gs=  8.38116765799462;
    //  g2= 26.391157455466484;
    //  g3= -12.821568250345786;
    //  g_omega= 8.242511367542606;
    //  C4= 0.0;
    //  Meff0= 0.73164; //  M*/M= 0.78 rho0= 0.168

    //} else if (eosType==13) { // NS2' K=240 MeV m*/m=0.8 rho0=0.168
    //} else if (eosType==14) { // NS2' K=240 MeV m*/m=0.8 rho0=0.168
    //  gs=  8.1058078601277;
    //  g2= 27.458242152551854;
    //  g3= 15.04821405086439;
    //  g_omega= 7.717362226320117;
    //  C4= 0.0;
    //  Meff0= 0.7504;  //M*/M= 0.8 rho0= 0.168

    //} else if (eosType==14) { // NS2' K=240 MeV m*/m=0.83 rho0=0.168
    //  Meff0=0.83*M_nucl;
    //  gs=  7.61859213019874;
    //  g2= 24.415162733405225;
    //  g3= 116.12808040939731;
    //  g_omega= 6.851412079560647;

    //} else if (eosType==15) { // K=240 MeV m*/m=0.9 rho0=0.168
    //  Meff0=0.9*M_nucl;
    //  gs=  4.905276706260943;
    //  g2= -107.81867500430985;
    //  g3= 1165.0182717154478;
    //  g_omega= 4.1623094631430595;

  } else if(eosType==14) {  //MD1  K=380 MeV m*/m=0.65 Hama1 U=0.065 at plab=3
    gs=  9.02956219322301;
    g2= 4.218109600022776;
    g3= 6.667054503467467;
    g_omega= 6.7402687603431675;
    gsp =  3.1855887031669394;
    gvp =  8.89548883191133;
    mu1 = 0.6411295603159707;
    mu2 = 1.841348887047491;
    withMomDep=true;

  } else if(eosType==15) {  // MD2   = JAM1 204 
    // K=380 MeV m*/m=0.65 U(1.7)=0.056  U(6)=0.025
    gs=  9.232564820463068;
    g2= 4.012495169512666;
    g3= 5.520251163159458;
    g_omega= 3.8884399009614437;
    gsp =  2.5019753284300768;
    gvp =  10.431917516759935;
    mu1 = 0.4896732480502826;
    mu2 = 2.4886149663285533;
    withMomDep=true;

    // K=240 C4
    //} else if (eosType==16) { // K=240 MeV m*/m=0.65 rho0=0.168
    //  Meff0=0.65*M_nucl;
    //  C4= 21.0;
    //  gs=  9.91930801149301;
    //  g2= 15.226549749509203;
    //  g3= -32.26844595795152;
    //  g_omega= 11.13197986761047;

    //} else if (eosType==14) { // NS2_C4 K=240 MeV m*/m=0.83 rho0=0.168
    //  Meff0=0.83*M_nucl;
    //  gs=  7.613196713353601;
    //  g2= 23.837944825779477;
    //  g3= 119.77285432788219;
    //  g_omega= 6.854895824715673;
    //  C4= 3.0;

    //} else if (eosType==15) { // NS2_C4 K=240 MeV m*/m=0.83 rho0=0.168
    //  Meff0=0.83*M_nucl;
    //  gs=  7.611763816891044;
    //  g2= 23.684839134873325;
    //  g3= 120.73866827992823;
    //  g_omega= 6.8558233310179;
    //  C4= 3.8;

    //} else if (eosType==16) { // NS2_C4 K=240 MeV m*/m=0.83 rho0=0.168
    //  Meff0=0.83*M_nucl;
    //  gs=  7.6096190877235985;
    //  g2= 23.45582261201977;
    //  g3= 122.18259162085039;
    //  g_omega= 6.857213416432635;
    //  C4= 5.0;

    //} else if (eosType==19) { // NS2_C4 K=240 MeV m*/m=0.83 rho0=0.168
      //gs=  7.532978051302832;
      //g2= 15.388682869508616;
      //g3= 172.46472133216272;
      //g_omega= 6.9083572118097685;
      //C4= 50.0;

    //} else if (eosType==19) { // NS2_C4 K=240 MeV m*/m=0.65 rho0=0.168
     //gs=  9.969442305685355;
     //g2= 13.679758170545712;
     //g3= -19.439770973692802;
     //g_omega= 11.262785279577887;
     //C4= 50.0;

    //} else if (eosType==19) { // NS2_C4 K=240 MeV m*/m=0.75 rho0=0.168
      //gs=  8.73934415998612;
      //g2= 18.068276544947995;
      //g3= 7.397184142568387;
      //g_omega= 9.096109481976697;
      //C4= 50.0;

    } else if (eosType==16) { // NS2_C4 K=240 MeV m*/m=0.8 rho0=0.168
      gs=  8.07944255922576;
      g2= 24.240312094532307;
      g3= 36.2868582181724;
      g_omega= 7.750244075088793;
      C4= 20.0;
      Meff0=  0.7504;  //M*/M= 0.8 rho0= 0.168

    } else if (eosType==17) { // NS2_C4 K=240 MeV m*/m=0.83 rho0=0.168
      Meff0=0.83*M_nucl;
      C4= 21.0;
      gs=  7.581540823253633;
      g2= 20.474030984740214;
      g3= 140.90017954309343;
      g_omega= 6.875614958864659;


    //} else if (eosType==17) { // NS2_C4 K=240 MeV m*/m=0.9 rho0=0.168
    //  Meff0=0.9*M_nucl;
    //  gs=  4.881239904646641;
    //  C4= 21.0;
    //  g2= -108.84514405280851;
    //  g3= 1161.0783230185123;
    //  g_omega= 4.167766245462079;

    //} else if (eosType==17) { // NS2_C4 K=240 MeV m*/m=0.75 rho0=0.168
    //  Meff0=0.75*M_nucl;
    //  C4= 21.0;
    //  gs=  8.749781683440151;
    //  g2= 21.23449917153425;
    //  g3= -14.395779344401987;
    //  g_omega= 9.024096630156135;


    } else if (eosType==18) { // GM1 K=300 MeV m*/m=0.7 rho0=0.153
      // S=  0.0325 L=  0.0939439306440231
      BE=-0.0163;
      rho0= 0.153;
      gs=  8.895;
      g2= 9.8589;
      g3= -6.6984;
      g_omega= 10.61;
      C4= 0.0;
      Meff0= 0.656553495253714; //  M*/M= 0.699950421379226;

      gs=  8.894131318342712;
      g2= 9.856047856289395;
      g3= -6.712488481748473;
      g_omega= 10.61;
      C4= 0.0;
      Meff0=  0.656553495253714;//  M*/M= 0.699950421379226 rho0= 0.153

    } else if (eosType==19) { // SOT K=260 MeV m*/m=0.76 rho0=0.153
      // S=  0.0316 L=  0.08783488296417012
      BE=-0.0163;
      rho0= 0.153;
      gs=  8.3;
      g2= 16.75;
      g3= -4.07;
      g_omega= 9.203;
      C4= 0.0;
      Meff0=0.7137078625439567; //  M*/M= 0.7608825826694635; 
    }

    g_sigma = gs;
    A = M_sigma*M_sigma;
    B = g2*HBARC;      // GeV
    C = g3;
    D = 0.0;
    EpsPi=0.0;
    M0=jamtable->find(2212)->m0();
    Ml0=jamtable->find(3122)->m0();
    Ms0=jamtable->find(3212)->m0();
    Mx0=jamtable->find(3312)->m0();
    Mo0=jamtable->find(3334)->m0();
    Sigma0=(M_nucl - Meff0)/gs;
    Omega0 = Omega(g_omega,rho0*CONV);
    Un = -gs * Sigma0 + g_omega*Omega0;

    /*
    // omega meson coupling
    Cv2 =  transportModel*0.5*pow2(g_omega/M_omega)*CONV;  // GeVfm^3

    // rho meson coupling
    double pf0=pow(6.*M_PI*M_PI*rho0/4.,1.0/3.0)*HBARC;
    double eff=sqrt(Meff0*Meff0 + pf0*pf0);
    if(optRhoMeson) {
      g_rho=sqrt(M_rho*M_rho+C4*Omega0*Omega0)*sqrt(2/(rho0*CONV)*(Esym - pf0*pf0/(6*eff)));
      Cr2 =  0.5*pow2(g_rho/M_rho)*CONV;  // Gevfm^3
    }

    // Momentum-dependent potential (not implemented)
    C1 = -pow2(gsp/M_sigma)*CONV;
    C2 =  pow2(gvp/M_omega)*CONV;
    vex1 = C1/2;
    vex2 = C2/2;

    if(transportModel==2) {
      Cv2  =  pow2(g_omega/M_omega)*CONV;  // Gevfm^3
      vex1 = C1;
      vex2 = C2;
    }
    pmu1 = mu1*mu1;
    pmu2 = mu2*mu2;
    */

    if(firstCall) {
      double gs1=-gs;
       if(optHyperonSigmaCoupling) gs1 *=xYSigma;
       double meff=mEff(Sigma0, Ml0, 0.0, gs1, 1.0);
       double gv = (Ul- meff + Ml0)/Omega0;
       cout << "# RQMD.RMF sigma-omega mode " <<endl;
       cout << "# optU=0: gs= "<< -gs << " g_omega "<< g_omega << " UL= "<< meff - Ml0 + g_omega*Omega0 <<endl;
       cout << "# optU=1: gs= "<< -gs << " g_lambda/g_omega "<< 2./3. << " UL= " << meff -Ml0 + g_omega*Omega0*2./3.<<endl;
       cout << "# optU=2: gs= "<<  gs1 << " gv= "<< gv << " gv/g_omega "<< gv/g_omega << " UL= "<< meff - Ml0 + gv*Omega0 <<endl;
       double sfac=2./3.;
       gs1=(Ul-g_omega*sfac*Omega0)/Sigma0;
      cout << "# optU=3: gs= "<< gs1 << " gv/g_omega "<<  2./3. << " UL= "<< gs1*Sigma0 + g_omega*2./3.*Omega0 <<endl;

//      } else if(optRMF==2) {
//        gs1=(Ul-g_omega*sfac*Omega0)/(Sigma0-f_pion);
//        m1=Ml0-Sigma0*f_pion;

      //printInfo();
    }

}

} // namespace jam2
