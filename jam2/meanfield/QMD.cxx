#include <jam2/meanfield/QMD.h>
#include <jam2/hadrons/JamStdlib.h>
#include <jam2/meanfield/NonRelPotential.h>

// Quantum molecular dynamics with Skyrme potential

namespace jam2 {

using namespace std;

QMD::QMD(Pythia8::Settings* s, Pythia8::Rndm* r) : MeanField(s)
{
  rndm=r;
  potential = new NonRelPotential(settings);
  t1  = potential->getT1();
  t3  = potential->getT3();
  t5  = potential->getT5();
  gam = potential->getGam();
  pmu1 = potential->getPmu1(); 
  pmu2 = potential->getPmu2();
  vex1 = potential->getVex1();
  vex2 = potential->getVex2();
  withMomDep = potential->isMomDep();
  potYukawa=settings->flag("MeanField:YukawaPotential");
  // urqmd
  //gamY=1.4; //vYuk= -0.498*1e-3;
  // irqmd
  //gamY=0.4; vYuk= -0.01;
  vYuk=settings->parm("MeanField:vYukawaPotential");
  gamY=settings->parm("MeanField:gYukawaPotential");

  typeMDPot=potential->typeMDPotential();
  rho0=settings->parm("MeanField:rho0");
  widG = settings->parm("MeanField:gaussWidth");
  facG = 1.0/pow(4.0*M_PI*widG, 1.5)/rho0;
  wmG = 1.0/(4*widG);
  transportModel = settings->mode("MeanField:transportModel");
  if(transportModel==2) {
    facG = 1.0/pow(2.0*M_PI*widG, 1.5)/rho0;
    wmG = 1.0/(2*widG);
  }
  overSample = settings->mode("Cascade:overSample");
  optIntMC = settings->mode("MeanField:optMCIntegral");

  outputForce=true;
}

void QMD::initMatrix(std::list<EventParticle*>& plist,double gtime)
{
  part.clear();
  // Select particles which feel the potential force.
  for(auto& p : plist) {
    if(p->isMeanField(gtime,optPotential)) {
      part.push_back(p);
      potential->setPotentialParam(p);
    }
  }

  NV = part.size();
  rhoB.assign(NV,0.0);
  rho1.assign(NV,0.0);
  rho2.assign(NV,0.0);
  rho3.assign(NV,0.0);
  vmoms.assign(NV,0.0);
  rhog2.resize(NV);
  rhog3.resize(NV);
  rhom.resize(NV);
  for(int i=0;i<NV;i++) rhom[i].resize(NV);

  force.assign(NV,0.0);
  forcer.assign(NV,0.0);

  // Yukawa force.
    rhoy.assign(NV,0.0);
    rhy.resize(NV);
    for(int i=0;i<NV;i++) rhy[i].assign(NV,0.0);

  if(firstCall && outputForce) {
    cout << "open file "<<endl;
    outputFOfs.open("force.dat");
  }
  //if(!firstCall && outputForce) outputFOfs.open("force.dat",std::ios::app);

  firstCall=false;
}

QMD::~QMD()
{
  part.clear();
  rho1.clear();
  rho2.clear();
  rho3.clear();
  rhog2.clear();
  rhog3.clear();
  vmoms.clear();
  for(int i=0;i<NV;i++) rhom[i].clear();
  rhom.clear();
  force.clear();
  forcer.clear();
  rhoy.clear();
  for(int i=0;i<NV;i++) rhy[i].clear();
  rhy.clear();

  delete potential;
  //if(outputForce) outputFOfs.close();
}

void QMD::evolution(list<EventParticle*>& plist, double gtime,double dt,int step)
{
  //std::vector<Vec4> force(NV,0.0);
  //std::vector<Vec4> forcer(NV,0.0);
  //std::vector<double> rho(NV,0.0);
  //std::vector<double> rhog(NV,0.0);
  //std::vector<double> vmoms(NV,0.0);
  //std::vector<std::vector<double> > rhom(NV, std::vector<double>(NV,0.0));

  globalTime = gtime;
  initMatrix(plist,gtime);

  // normal QMD
  if(optPotentialEval<5) {
    qmdMatrix();
    singleParticlePotential();
    computeForce();

  // spacial integration of the potential is done numerically i.e.QMD-L
  } else {
    computeForceL();
  }

  for(int i=0; i<NV;i++)  {
    part[i]->updateByForce(force[i], forcer[i],dt);
  }

  if(outputForce) {
    for(int i=0; i<NV;i++)  {
      outputFOfs << part[i]->rhos()
          	<< setw(16) << force[i][1]
          	<< setw(16) << force[i][2]
          	<< setw(16) << force[i][3]
		<<endl;
    }
  }
}

// compute single particle potential energy.
void QMD::singleParticlePotential()
{
  for(auto i=0; i< NV; i++) {
    double* pfac=part[i]->facPotential();

    int ibary = part[i]->baryon()/3;
    rhog2[i] = pfac[2]*pow(max(0.0,rho2[i]),pfac[3]-1);
    rhog3[i] = pfac[4]*pow(max(0.0,rho3[i]),pfac[5]-1);
    double vsky = ibary*(pfac[1]*t1*rho1[i] + t3*rhog2[i]*rho2[i] + t5*rhog3[i]*rho3[i] + rhoy[i]);

    part[i]->setPotV(0,vsky + vmoms[i]);
    part[i]->setPotVm(0,vmoms[i]);
    if(outputForce) {
      part[i]->setRhoS(rhoB[i]);
    } else {
      part[i]->setRhoS(rho1[i]);
    }
    part[i]->setRhoB(rho2[i]+rho3[i]);
  }

}


// compute total momentum and energy.
Vec4 QMD::computeEnergy(list<EventParticle*>& plist, int step)
{
  //initMatrix(plist,step);

  pTot=0.0;   
  for(auto i = plist.begin(); i != plist.end(); ++i) {
    pTot += (*i)->getP();
    pTot[0] += (*i)->potv(0);
  }
  if(step==1) pTot0 = pTot/overSample;
  return pTot/overSample;

}

void QMD::qmdMatrix()
{
  if(NV==0) return;
  double gf1[3]={1.0, 1.0, 1.0};
  double gf2[3]={1.0, 1.0, 1.0};
  int selfi = optPotentialEval>=2? 0: 1;


  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    int bi = part[i]->baryon()/3;
    double qfac1=1.0;
    if(optPotential<4)  qfac1 = part[i]->getTf() > globalTime ? part[i]->qFactor() : 1.0;
    double* gfac1= part[i]->facPotential();
    int potid1=part[i]->potentialID();

    if(optPotentialDensity==1) {
      gf1[0]=gfac1[1];
      gf1[1]=gfac1[2];
      gf1[2]=gfac1[4];
    }

    //for(int j=i+selfi; j< NV; j++) {
    for(int j=i; j< NV; j++) {
      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      int bj = part[j]->baryon()/3;
      int potid2=part[j]->potentialID();
      double qfac2=1.0;
      if(optPotential<4)  qfac2 = part[j]->getTf() > globalTime ? part[j]->qFactor() : 1.0;
      double* gfac2= part[j]->facPotential();

      if(optPotentialDensity==1) {
        gf2[0]=gfac2[1];
        gf2[1]=gfac2[2];
        gf2[2]=gfac2[4];
      }

      // to distinguish nucleon/lambda density.
      double pot1 = potential->matrix(potid1,potid2);
      double pot2 = potential->matrix(potid2,potid1);

      double wid=2*widG*(gfac1[0]+gfac2[0]);
      double wid2=wid/2;
      double fac = pow(M_PI*wid, 1.5)*overSample;
      double fac2 = pow(M_PI*wid2, 1.5)*overSample;
      double den = gamCM*exp( -((r1-r2).pT2() + pow2(gamCM*(r1[3]-r2[3])) )/wid )*qfac1*qfac2/fac;
      //double den2 = optPotentialEval==0? den: gamCM*exp( -((r1-r2).pT2() + pow2(gamCM*(r1[3]-r2[3])) )/wid2 )*qfac1*qfac2/fac2;
      double den1 = gamCM*exp( -((r1-r2).pT2() + pow2(gamCM*(r1[3]-r2[3])) )/wid2 )*qfac1*qfac2/fac2;
      double den2 = optPotentialEval==0? den: den1;
      if(i==j) {
	den /= 2;
	den2 /= 2;
      }
      rhoB[i] += den1*bj;
      rhoB[j] += den1*bj;
      if(i==j && selfi==1) continue;
 
      rhom[i][j] = den*pot1;
      rhom[j][i] = den*pot2;
      double rhomij = den2*pot1;
      double rhomji = den2*pot2;

      rho1[i] += rhomij*bj*gf2[0];
      rho2[i] += rhomij*bj*gf2[1];
      rho3[i] += rhomij*bj*gf2[2];

      rho1[j] += rhomji*bi*gf1[0];
      rho2[j] += rhomji*bi*gf1[1];
      rho3[j] += rhomji*bi*gf1[2];



      if(!withMomDep) continue;
      double ps = (p1 - p2).pAbs2();
      double vx1=gfac1[6]*gfac2[6]*vex1;
      double vx2=gfac1[7]*gfac2[7]*vex2;
      if(typeMDPot==1) {
      double pmom2i = den*( vx1/(1.0+ps/(gfac1[8]*pmu1)) + vx2/(1.0+ps/(gfac1[9]*pmu2)) );
      double pmom2j = den*( vx1/(1.0+ps/(gfac2[8]*pmu1)) + vx2/(1.0+ps/(gfac2[9]*pmu2)) );
      vmoms[i] += pmom2i*pot1;
      vmoms[j] += pmom2j*pot2;
      } else {
	double pmom=vx1*pow2(log(1.+pmu1*ps));
        vmoms[i] += pmom*den2*pot1;
        vmoms[j] += pmom*den2*pot2;
      }
    }
  }

  if(potYukawa) {

  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    //Vec4 p1 = part[i]->getP();
    //int bi = part[i]->baryon()/3;
    double qfac1=1.0;
    if(optPotential<4)  qfac1 = part[i]->getTf() > globalTime ? part[i]->qFactor() : 1.0;
    double* gfac1= part[i]->facPotential();

    for(int j=i+1; j< NV; j++) {
      Vec4 r2 = part[j]->getR();
      //Vec4 p2 = part[j]->getP();
      //int bj = part[j]->baryon()/3;
      double qfac2=1.0;
      if(optPotential<4) qfac2 = part[j]->getTf() > globalTime ? part[j]->qFactor() : 1.0;
      double* gfac2= part[j]->facPotential();

      double wid=2*widG*(gfac1[0]+gfac2[0]);
      double fac = pow(M_PI*wid, 1.5)*overSample;
      double den = gamCM*exp( -((r1-r2).pT2() + pow2(gamCM*(r1[3]-r2[3])) )/wid )*qfac1*qfac2/fac;
      double clw  = 2.0/sqrt(M_PI*wid);
      double yex  = exp(widG/gamY*gamY);
      double wc=sqrt(wid);
      double rr = (r1-r2).pT2() + pow2(gamCM*(r1[3]-r2[3]));
      double rs   = sqrt( rr );
      double ef1=exp(-rs/gamY)*(1.0-erf(0.5*wc/gamY-rs/wc));
      double ef2=exp( rs/gamY)*(1.0-erf(0.5*wc/gamY+rs/wc));
      double rhoyij = 0.5*vYuk*yex/rs*(ef1-ef2);
      rhoy[i] += rhoyij;
      rhoy[j] += rhoyij;
      // Derivative
      double dev1 = 0.25*vYuk/rr*(
                    yex*( (-1.0/gamY - 1.0/rs )*ef1
                         +(-1.0/gamY + 1.0/rs )*ef2 )
                      + 2.0*clw*den );
      double dev2 = 0.25*vYuk/rr*(
                    yex*( (-1.0/gamY - 1.0/rs )*ef1
                         +(-1.0/gamY + 1.0/rs )*ef2 )
                      + 2.0*clw*den );
      rhy[i][j] = dev1; // * emj;
      rhy[j][i] = dev2; // * emi;
    }
  }
  }


}

void QMD::computeForce()
{
  if(NV==0) return;

  for(auto i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    int bar1= part[i]->baryon()/3;
    double* gf1=part[i]->facPotential();

    for(auto j=i+1; j< NV; j++) {
      Vec4 r2 = part[j]->getR();
      int bar2= part[j]->baryon()/3;
      double* gf2=part[j]->facPotential();

      double vf1=gf1[1]*gf2[1]*t1;
      double vfi2=gf1[3]*t3*rhog2[i];
      double vfi3=gf1[5]*t5*rhog3[i];
      double vfj2=gf2[3]*t3*rhog2[j];
      double vfj3=gf2[5]*t5*rhog3[j];
      if(optPotentialDensity==1) {
        vfi2 *= gf2[2];
        vfi3 *= gf2[4];
        vfj2 *= gf1[2];
        vfj3 *= gf1[4];
      }

      double fsky1 = (vf1 + vfi2 + vfi3)*rhom[i][j]*bar1*bar2 + rhy[i][j]; 
      double fsky2 = (vf1 + vfj2 + vfj3)*rhom[j][i]*bar1*bar2 + rhy[j][i]; 

      //double fsky1 = (gf1[1]*t1*gf2[1] + gf1[3]*t3*rhog2[i] + t5*gf1[5]*rhog3[i])*rhom[i][j]*bar1*bar2; 
      //double fsky2 = (gf2[1]*t1*gf1[1] + gf2[3]*t3*rhog2[j] + t5*gf2[5]*rhog3[j])*rhom[j][i]*bar1*bar2; 


      double wid=1.0/(2*widG*(gf1[0]+gf2[0]));
      double fsky = -wid*(fsky1 + fsky2);
      Vec4 dr = r1 - r2;
      dr[3] *= gamCM2;
      force[i] += -2*fsky*dr;
      force[j] +=  2*fsky*dr;

      if(!withMomDep) continue;

      Vec4 p2 = part[j]->getP();
      Vec4 dp = p1 - p2;
      double psq = dp.pAbs2();

      double vx1=gf1[6]*gf2[6]*vex1;
      double vx2=gf1[7]*gf2[7]*vex2;

      double fmomei, fmomej, fmomdi, fmomdj;
      if(typeMDPot==1) {
        double mui1=pmu1*gf1[8];
        double mui2=pmu2*gf1[9];
        double faci1 = 1.0 + psq/mui1;
        double faci2 = 1.0 + psq/mui2;
        double muj1=pmu1*gf2[8];
        double muj2=pmu2*gf2[9];
        double facj1 = 1.0 + psq/muj1;
        double facj2 = 1.0 + psq/muj2;
        fmomei = -rhom[i][j]*(vx1/(mui1*faci1*faci1)+vx2/(mui2*faci2*faci2));
        fmomej = -rhom[j][i]*(vx1/(muj1*facj1*facj1)+vx2/(muj2*facj2*facj2));
        fmomdi = -wid*rhom[i][j]*(vx1/faci1 + vx2/faci2);
        fmomdj = -wid*rhom[j][i]*(vx1/facj1 + vx2/facj2);
      } else {
	double pd= 1.+pmu1*psq;
	double plog= log(pd);
	double pmom=2*vx1*pmu1*plog/pd;
	double dmom=vx1*plog*plog;

        fmomei = rhom[i][j]*pmom;
        fmomej = rhom[j][i]*pmom;
        fmomdi = -wid*rhom[i][j]*dmom;
        fmomdj = -wid*rhom[j][i]*dmom;
      }

      force[i]  += -2*(fmomdi+fmomdj)*dr;
      forcer[i] +=  2*(fmomei+fmomej)*dp;
      force[j]  +=  2*(fmomdi+fmomdj)*dr;
      forcer[j] += -2*(fmomei+fmomej)*dp;

      /*
      //double vfac=gf1[6]*gf2[6];
      double mu1=pmu1*gf1[8];
      double mu2=pmu2*gf1[9];
      double fac1 = 1.0 + psq/mu1;
      double fac2 = 1.0 + psq/mu2;
      double rhomij = gf1[6]*gf2[6]*(rhom[i][j] + rhom[j][i]);
      double fmome = -rhomij*(vex1/(mu1*fac1*fac1)+vex2/(mu2*fac2*fac2));
      double fmomd = -wid*rhomij*(vex1/fac1 + vex2/fac2);
      force[i]  += -2*fmomd*dr;
      forcer[i] +=  2*fmome*dp;
      force[j]  +=  2*fmomd*dr;
      forcer[j] += -2*fmome*dp;
      */

    }
  }

}

void QMD::computeForceL()
{
  if(NV==0) return;

  for(auto i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    force[i]=0.0;
    forcer[i]=0.0;

    // Gauss integral
    if(optPotentialEval==5) {

    for(int j=0;j<nGaussPoint;j++)
    for(int k=0;k<nGaussPoint;k++)
    for(int l=0;l<nGaussPoint;l++) {
      Vec4 dr = Vec4(xG[j], xG[k], xG[l],0.0);
      Vec4 r = r1 + dr;
      //double drsq1 = dr.pAbs2();
      //double wid=2*widG*(gfac1[0]+gfac2[0]);
      //double fac = pow(M_PI*wid, 1.5)*overSample;
      //double rhoij= fac*exp(-drsq1/wid);

      qmdMatrix(i,r,p1);
      singleParticlePotential(i);
      //double p0= part[i]->potv(0)+part[i]->potvm(0);

      Vec4 forcei=0.0, forceri=0.0;
      computeForce(i,r,p1,forcei, forceri);
      forcer[i] += forceri*rhom[i][i]*wG[j]*wG[k]*wG[l];
      force[i]  += forcei*rhom[i][i]*wG[j]*wG[k]*wG[l];

    }

    } else {

      for(int iev=0;iev<nMCPoint;iev++) {

	// generate coordinate according to the Gaussian distribution.
        Vec4 dr = Vec4(rndm->gauss(),rndm->gauss(),rndm->gauss(),0.0);
        Vec4 r= r1 + sqrt(widG)*dr;
	r[0]=r1[0];

        qmdMatrix(i,r,p1);
        singleParticlePotential(i);

        Vec4 forcei=0.0, forceri=0.0;
        computeForce(i,r,p1,forcei, forceri);
        forcer[i] += forceri;

	if(optIntMC==1) {
          force[i]  += forcei;
	} else {
          double p0= part[i]->potv(0)+part[i]->potvm(0);
          force[i]  += -p0*(r-r1)/widG;
	}
      } // end MC loop

      force[i]  /= nMCPoint;
      forcer[i] /= nMCPoint;
      qmdMatrix(i,r1,p1);
      singleParticlePotential(i);

    }


  } // end loop over particles

}

// compute single particle potential energy.
void QMD::qmdMatrix(int i, Vec4& r1, Vec4& p1)
//void QMD::singleParticlePotential(int i, Vec4& r1, Vec4& p1)
{
  double gf1[3]={1.0, 1.0, 1.0};
  double gf2[3]={1.0, 1.0, 1.0};
  rhoB[i]=0.0;
  rho1[i]=0.0;
  rho2[i]=0.0;
  rho3[i]=0.0;
  vmoms[i]=0.0;
  rhoy[i] = 0.0;

    //int bi = part[i]->baryon()/3;
    double qfac1=1.0;
    if(optPotential<4)  qfac1 = part[i]->getTf() > globalTime ? part[i]->qFactor() : 1.0;
    double* gfac1= part[i]->facPotential();
    int potid1=part[i]->potentialID();

    if(optPotentialDensity==1) {
      gf1[0]=gfac1[1];
      gf1[1]=gfac1[2];
      gf1[2]=gfac1[4];
    }

    for(int j=0; j< NV; j++) {
      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      int bj = part[j]->baryon()/3;
      int potid2=part[j]->potentialID();
      double qfac2=1.0;
      if(optPotential<4)  qfac2 = part[j]->getTf() > globalTime ? part[j]->qFactor() : 1.0;
      double* gfac2= part[j]->facPotential();

      if(optPotentialDensity==1) {
        gf2[0]=gfac2[1];
        gf2[1]=gfac2[2];
        gf2[2]=gfac2[4];
      }

      // to distinguish nucleon/lambda density.
      double pot1 = potential->matrix(potid1,potid2);

      double wid=widG*(gfac1[0]+gfac2[0]);
      double fac = pow(M_PI*wid, 1.5)*overSample;
      double den = gamCM*exp( -((r1-r2).pT2() + pow2(gamCM*(r1[3]-r2[3])) )/wid )*qfac1*qfac2/fac;
 
      rhom[i][j] = den*pot1;
      rhoB[i] += rhom[i][j]*bj;
      rho1[i] += rhom[i][j]*bj*gf2[0];
      rho2[i] += rhom[i][j]*bj*gf2[1];
      rho3[i] += rhom[i][j]*bj*gf2[2];

      if(!withMomDep) continue;
      double ps = (p1 - p2).pAbs2();
      double vx1=gfac1[6]*gfac2[6]*vex1;
      double vx2=gfac1[7]*gfac2[7]*vex2;
      if(typeMDPot==1) {

      // Lorentzian 
      double pmom2i = den*( vx1/(1.0+ps/(gfac1[8]*pmu1)) + vx2/(1.0+ps/(gfac1[9]*pmu2)) );
      vmoms[i] += pmom2i*pot1;

      // log^2
      } else {
	double pmom=vx1*pow2(log(1.+pmu1*ps));
        vmoms[i] += pmom*den*pot1;
      }
  }

  if(potYukawa) {

    for(int j=0; j< NV; j++) {
      Vec4 r2 = part[j]->getR();
      double qfac2=1.0;
      if(optPotential<4) qfac2 = part[j]->getTf() > globalTime ? part[j]->qFactor() : 1.0;
      double* gfac2= part[j]->facPotential();

      double wid=2*widG*(gfac1[0]+gfac2[0]);
      double fac = pow(M_PI*wid, 1.5)*overSample;
      double den = gamCM*exp( -((r1-r2).pT2() + pow2(gamCM*(r1[3]-r2[3])) )/wid )*qfac1*qfac2/fac;
      double clw  = 2.0/sqrt(M_PI*wid);
      double yex  = exp(widG/gamY*gamY);
      double wc=sqrt(wid);
      double rr = (r1-r2).pT2() + pow2(gamCM*(r1[3]-r2[3]));
      double rs   = sqrt( rr );
      double ef1=exp(-rs/gamY)*(1.0-erf(0.5*wc/gamY-rs/wc));
      double ef2=exp( rs/gamY)*(1.0-erf(0.5*wc/gamY+rs/wc));
      double rhoyij = 0.5*vYuk*yex/rs*(ef1-ef2);
      rhoy[i] += rhoyij;
      //rhoy[j] += rhoyij;
      // Derivative
      double dev1 = 0.25*vYuk/rr*(
                    yex*( (-1.0/gamY - 1.0/rs )*ef1
                         +(-1.0/gamY + 1.0/rs )*ef2 )
                      + 2.0*clw*den );
      //double dev2 = 0.25*vYuk/rr*(
      //              yex*( (-1.0/gamY - 1.0/rs )*ef1
      //                   +(-1.0/gamY + 1.0/rs )*ef2 )
      //                + 2.0*clw*den );
      rhy[i][j] = dev1; // * emj;
      //rhy[j][i] = dev2; // * emi;
    }
  }

}

void QMD::singleParticlePotential(int i)
{
    double* pfac=part[i]->facPotential();
    int ibary = part[i]->baryon()/3;
    rhog2[i] = pfac[2]*pow(max(0.0,rho2[i]),pfac[3]-1);
    rhog3[i] = pfac[4]*pow(max(0.0,rho3[i]),pfac[5]-1);
    double vsky = ibary*(pfac[1]*t1*rho1[i] + t3*rhog2[i]*rho2[i] + t5*rhog3[i]*rho3[i] + rhoy[i]);

    part[i]->setPotV(0,vsky + vmoms[i]);
    part[i]->setPotVm(0,vmoms[i]);
    if(outputForce) {
      part[i]->setRhoS(rhoB[i]);
    } else {
    part[i]->setRhoS(rho1[i]);
    }
    part[i]->setRhoB(rho2[i]+rho3[i]);
}

void QMD::computeForce(int i, Vec4& r1, Vec4& p1, Vec4& forcei, Vec4& forceri)
{
  if(NV==0) return;

    int bar1= part[i]->baryon()/3;
    double* gf1=part[i]->facPotential();

    for(auto j=1; j< NV; j++) {
      Vec4 r2 = part[j]->getR();
      int bar2= part[j]->baryon()/3;
      double* gf2=part[j]->facPotential();

      double vf1=gf1[1]*gf2[1]*t1;
      double vfi2=gf1[3]*t3*rhog2[i];
      double vfi3=gf1[5]*t5*rhog3[i];
      double vfj2=gf2[3]*t3*rhog2[j];
      double vfj3=gf2[5]*t5*rhog3[j];
      if(optPotentialDensity==1) {
        vfi2 *= gf2[2];
        vfi3 *= gf2[4];
        vfj2 *= gf1[2];
        vfj3 *= gf1[4];
      }

      double wid=1.0/(widG*(gf1[0]+gf2[0]));
      double fsky = -wid*(vf1 + vfi2 + vfi3)*rhom[i][j]*bar1*bar2 + rhy[i][j]; 
      Vec4 dr = r1 - r2;
      dr[3] *= gamCM2;
      forcei += -2*fsky*dr;

      if(!withMomDep) continue;

      Vec4 p2 = part[j]->getP();
      Vec4 dp = p1 - p2;
      double psq = dp.pAbs2();

      double vx1=gf1[6]*gf2[6]*vex1;
      double vx2=gf1[7]*gf2[7]*vex2;

      double fmomei, fmomdi;
      if(typeMDPot==1) {
        double mui1=pmu1*gf1[8];
        double mui2=pmu2*gf1[9];
        double faci1 = 1.0 + psq/mui1;
        double faci2 = 1.0 + psq/mui2;
        fmomei = -rhom[i][j]*(vx1/(mui1*faci1*faci1)+vx2/(mui2*faci2*faci2));
        fmomdi = -wid*rhom[i][j]*(vx1/faci1 + vx2/faci2);
      } else {
	double pd= 1.+pmu1*psq;
	double plog= log(pd);
	double pmom=2*vx1*pmu1*plog/pd;
	double dmom=vx1*plog*plog;

        fmomei = rhom[i][j]*pmom;
        fmomdi = -wid*rhom[i][j]*dmom;
      }
      forcei  += -2*fmomdi*dr;
      forceri +=  2*fmomei*dp;

  }

}

} // namespace jam2


