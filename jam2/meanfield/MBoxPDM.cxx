#include <jam2/meanfield/MBoxPDM.h>

namespace jam2 {

void NeighborPDM::resize(int n) 
{
  int m = box->particleSize();
  rhom.resize(n);
  rhomx.resize(n);
  for(int i=0;i<n;i++) {
    rhom[i].resize(m);
    rhomx[i].resize(m);
  }
}

MBoxPDM:: ~MBoxPDM()
{
  for(auto& i : neighbors) delete i;
  neighbors.clear();
  clearMatrix();
}

bool MBoxPDM::haveThisSite(MBoxPDM* box) const
{
  auto first = neighbors.begin();
  auto last = neighbors.end();

  while(first != last) {
    if((*first)->box == box) return true;
    ++first;
  }
  return false;
}

NeighborPDM* MBoxPDM::findNeighbor(MBoxPDM* box)
{
  auto&& first = neighbors.begin();
  auto&& last = neighbors.end();

  while(first != last) {
    if((*first)->box == box) return *first;
    ++first;
  }
  cout << "MBoxPDM error no such neighbor "<<endl;
  exit(1);
}

void MBoxPDM::clearMatrix()
{
  part.clear();
  rhos.clear();
  rhos2.clear();
  rho.clear();
  vmoms.clear();
  vmom4.clear();
  force.clear();
  forcer.clear();
  Vdot.clear();
  JB.clear();
  IB.clear();
  omega.clear();
  rho_meson.clear();
  for(int i=0;i<NV;i++) rhom[i].clear();
  rhom.clear();
}

void MBoxPDM::initBox()
{
  NV = part.size();
  if(NV==0) return;

  for(auto& i : part) 
    potential->setPotentialParam(i);

  rhom.resize(NV);
  for(int i=0;i<NV;i++) {
    rhom[i].resize(NV);
  }

  setZero();
  rho.assign(NV,0.0);
  omega.assign(NV,0.0);
  rho_meson.assign(NV,0.0);
  Vdot.assign(NV,0.0);
  force.assign(NV,0.0);
  forcer.assign(NV,0.0);
  lambda.assign(NV,1.0);

  // free mass is used in the calculation of potential and force.
  if(optVdot==1) {
    for(auto& i : part) i->setFree();
  }
  if(optPotentialArg >= 1) {
    for(auto& i : part) i->setPotS(0.0);
  }

  for(auto& neighbor : neighbors) {
    neighbor->resize(NV);
  }

}

void MBoxPDM::qmdMatrix()
{
  if(NV==0) return;

  RQMDpdm::qmdMatrix();
  for(auto& neighbor : neighbors) {
    if(neighbor->getAction()) qmdMatrixNeighbor(*neighbor);
  }

}

void MBoxPDM::computeForce()
{
  if(NV==0) return;

  if(optPotentialEval<5) {
    RQMDpdm::computeForce();
    for(auto& neighbor : neighbors) {
      if(neighbor->getAction()) computeForceNeighbor(*neighbor);
    }

  } else {
    computeForceL();
    //RQMDpdm::computeBUUForce();
    //for(auto& neighbor : neighbors) {
    //  if(neighbor->getAction()) computeBUUForceNeighbor(*neighbor);
    //}
  }

}

void MBoxPDM::updateRP(double dt)
{
  // Note that optVdot=0,1: all momenta are canonical.
  // Canonical momenta update.
  if(optVdot == 0) {
    for(int i=0; i< NV; i++) {
      part[i]->updateByForce(force[i], forcer[i],dt);
      if(optCollisionOrdering > 100) {
        part[i]->addT(forcer[i][0]*dt);
        //if(optCollisionOrdering==101) part[i]->tevol(pHat*part[i]->getR());
      }

      part[i]->lambda(MeanField::lambda(part[i]));
    }

  // canonical momenta are used for the evaluation of forces,
  // after update canonical momenta, set kinetic momenta.
  } else if(optVdot == 1) {

    for(int i=0; i< NV; i++) {
      // canonical momenta update.
      part[i]->updateByForce(force[i], forcer[i],dt);
      part[i]->setKinetic();
    }

  // compute time derivatives of vector potential V_i^\mu numerically.
  } else if(optVdot == 2) {

    // Vdot[] is already computed in singleParticlePotential().
    for(int i=0; i< NV; i++) {
      part[i]->updateByForce(force[i] - Vdot[i]/dt, forcer[i],dt);
    }

  // compute time derivatives of vector potential analytically.
  } else if(optVdot == 3) {

    cout << "MBoxPDM::computeForce not implemented optVdot= "<< optVdot<<endl;
    exit(1);
    /*
    RQMDpdm::computeVdot();
    for(int i=0; i< NV; i++) {
      part[i]->updateByForce(force[i] - Vdot[i], forcer[i],dt);
    }
    */
  }

  if(optPotentialArg >= 1) setScalarPotential();

}

void MBoxPDM::qmdMatrixNeighbor(NeighborPDM& neighbor)
{
  auto& part2 = neighbor.box->getParticle();
  int n2 = part2.size();
  if(n2==0) return;

  for(int i=0; i< NV; i++) {

    Vec4 r1  = part[i]->getR();
    Vec4 p1 = optPotentialArg>=1? part[i]->getPcan(optVdot) : part[i]->getPkin(optVdot);
    double m1 = optPotentialArg>=1? part[i]->getMass() : part[i]->getEffectiveMass();
    Vec4 u1 = p1/m1;
    Vec4 fri = optBaryonCurrent ? part[i]->forceR()*u1[0] : 0.0;

    double qfac1=1.0;
    if(optPotential<4) qfac1 = part[i]->getTf() > globalTime ? part[i]->qFactor() : 1.0;
    //double qfac1= optPotential>=4? 1.0: part[i]->getTf() > globalTime ? part[i]->qFactor() : 1.0;

    int bi   = part[i]->baryon()/3;
    if(bi==0) {
      qfac1 = facMesonPot;
      bi=1;
    }
    double* gfac1= part[i]->facPotential();
    double xi=gfac1[5];
    double yi=gfac1[10];
    double dmi  = gfac1[6];
    double dmi2 = gfac1[7];

    for(int j=0; j< n2; j++) {

      Vec4 r2  = part2[j]->getR();
      Vec4 p2 = optPotentialArg>=1? part2[j]->getPcan(optVdot) : part2[j]->getPkin(optVdot);
      double m2 = optPotentialArg>=1? part2[j]->getMass() : part2[j]->getEffectiveMass();
      Vec4 u2 = p2/m2;

      Vec4 frj = optBaryonCurrent ? part2[j]->forceR()*u2[0] : 0.0;
      int bj = part2[j]->baryon()/3;
      double qfac2=1.0;
      if(optPotential<4) qfac2 = part2[j]->getTf() > globalTime ? part2[j]->qFactor() : 1.0;
      //double qfac2 = optPotential>=4? 1.0: part2[j]->getTf() > globalTime ? part2[j]->qFactor() : 1.0;

      if(bj==0) {
	qfac2 = facMesonPot;
	bj=1;
      }
      double* gfac2= part2[j]->facPotential();
      double xj=gfac2[5];
      double yj=gfac2[10];
      double dmj  = gfac2[6];
      double dmj2 = gfac2[7];

      Vec4 dr = r1 - r2;
      double den1,den2,den3,den4;
      double pfac=qfac1*qfac2/overSample;

      if(optTwoBodyDistance==3) {

      double drsq1 = dr.m2Calc() - pow2(dr*u2);
      double drsq2 = dr.m2Calc() - pow2(dr*u1);

      if(optPotentialEval==4) {
        Vec4 dq = rGaussDistance(dr,u1,u2);
        den1 = facG/sqrt(Det) * exp(dq*dr*wmG)*pfac;
	den2 = den1;
        den3 = facG2 * exp(drsq1*wG2)*pfac;
        den4 = facG2 * exp(drsq2*wG2)*pfac;
      } else {
        den1 = facG * exp(drsq1*wmG) * pfac;
        den2 = facG * exp(drsq2*wmG) * pfac;
        den3 = optPotentialEval == 0? den1: facG2 * exp(drsq1*wG2)*pfac;
        den4 = optPotentialEval == 0? den2: facG2 * exp(drsq2*wG2)*pfac;
      }

      } else {
      Vec4 pCM=p1 + p2;
      double sInv = pCM.m2Calc();
      double drcmsq = dr.m2Calc() - pow2(dr * pCM)/sInv;
      double den = pCM[0]/sqrt(sInv)*facG * exp(drcmsq*wmG)*pfac;
      double denw = optPotentialEval == 0? den: pCM[0]/sqrt(sInv)*facG2 * exp(drcmsq*wG2)*pfac;
      den1 = m2/p2[0]*den;
      den2 = m1/p1[0]*den;
      den3 = m2/p2[0]*denw;
      den4 = m1/p1[0]*denw;
      }

      neighbor.setRhom(i,j,den1);
      neighbor.neighbor->setRhom(j,i, den2);

      // scalar density
      rhos[i] += den3*dmj;
      neighbor.box->addRhos(j,den4*dmi);

      // scalar density 2
      rhos2[i] += den3*dmj2;
      neighbor.box->addRhos2(j,den4*dmi2);

      JB[i] += den3*(u2+frj)*bj*xj;
      IB[i] += den3*(u2+frj)*bj*yj;
      neighbor.box->addJB(j,den4*(u1+fri)*bi*xi);
      neighbor.box->addIB(j,den4*(u1+fri)*bi*yi);

      if(!withMomDep) continue;

      Vec4 pc1 = part[i]->getPcan(optVdot);
      Vec4 pc2 = part2[j]->getPcan(optVdot);
      Vec4 dp = pc1 - pc2;

      //double dpsq = dp.m2Calc();
      //double psq1 = dpsq - pow2(dp * u2);
      //double psq2 = dpsq - pow2(dp * u1);

      Vec4 pCM = pc1 + pc2;
      double sInv = pCM.m2Calc();
      double pma = pow2(dp * pCM)/sInv;
      double psq1 = dp.m2Calc() - optPV * pma;
      double psq2=psq1;

      vmoms[i] += potential->Vmds(psq1)*den3;
      vmom4[i] += potential->Vmdv(psq1)*den3*u2;

      neighbor.box->addVmoms(j, potential->Vmds(psq2)*den4);
      neighbor.box->addVmom4(j, potential->Vmdv(psq2)*den4*u1);

    }
  }

}

Vec4 MBoxPDM::omegaPotentialNeighbor(NeighborPDM& neighbor,int j, Vec4& u2)
{
  //if(rhoj<1e-15) return Vec4(0.0,0.0,0.0,0.0);

  Vec4 omegaj=neighbor.box->getOmega(j);
  Vec4 jb=neighbor.box->getJB(j);
  double omj = max(0.0,omegaj.m2Calc());
  double rhj = max(0.0,neighbor.box->getRhoMeson(j).m2Calc());
  double dui=HBARC3/(mOmega2 + C4*omj + cOmegaRho*rhj);

  // domega/dj*u
  double du = 2*C4*omegaj*u2/(mOmega2 + 3*C4*omj + cOmegaRho*rhj);
  Vec4 domegadj= dui*(u2 - du*omegaj);

  if(optPotentialEval==0) return domegadj/2*mOmega2/HBARC3;
  if(optPotentialEval>=5) return domegadj*mOmega2/HBARC3;
  double rhoj=neighbor.box->getRho(j);
  if(rhoj<1e-15 || C4==0.0) return domegadj/2*mOmega2/HBARC3;

  double jomega=jb*domegadj;
  double Vomega = 0.5*mOmega2*omj + 0.25*(C4*omj*omj + cOmegaRho*omj*rhj);
  double Vn=Vomega/(rhoj*rhoj)/HBARC3;

  // dimension in GeVfm^3
  Vec4 dv=domegadj - jb/(rhoj*rhoj)*(jomega - 2*Vn*jb*u2) - Vn*u2;

  // dimensionless
  return dv*mOmega2/HBARC3;
}

void MBoxPDM::computeForceNeighbor(NeighborPDM& neighbor)
{
  double fact= transportModel==2 ? 1.0: 0.5;
  auto& part2 = neighbor.box->getParticle();
  auto& omega2 = neighbor.box->getOmega();
  auto& rho_meson2 = neighbor.box->getRhoMeson();
  //auto& JB2 = neighbor.box->getJB();
  //auto& IB2 = neighbor.box->getIB();
  int n2 = part2.size();
  if(n2==0) return;

  for(auto i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getPkin(optVdot);
    Vec4 pc1 = part[i]->getP();
    Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : p1;
    Vec4 vk1 = optRQMDevolution > 0 ? pk1/(pk1*pHat) : pk1/pk1[0];
    if(optPotentialArg>=1) p1 = part[i]->getPcan(optVdot);
    double meff1 = part[i]->getEffectiveMass();
    double m1=optPotentialArg>=1? part[i]->getMass():meff1;
    Vec4 u1  = p1/m1;
    double fengi = optCollisionOrdering == 101 ? meff1/(pk1*pHat) :
           optCollisionOrdering >  101 ? meff1*part[i]->getMass()/(pk1*p1) : meff1/pk1[0];
    if(optPotentialEval==4) {
      u1 = pk1/meff1;
      vk1=u1;
      fengi=1.0;
    }

    int  bar1 = part[i]->baryon()/3;
    double xvi= part[i]->facPotential(5);
    double dmi= part[i]->facPotential(6);
    double gpi= part[i]->facPotential(9);
    double gri = part[i]->facPotential(10); // rho coupling

    double omi = max(0.0,omega[i].m2Calc());
    double rhi = max(0.0,rho_meson[i].m2Calc());
    double ri=C4/mRho2*rhi;
    double si=cOmegaRho/mRho2*omi;
    double dri=1.0/(1.0 + 3*ri + si);
    Vec4 dOmegadJ1 = omegaPotential(i, vk1);


    for(auto j=0; j< n2; j++) {

      Vec4 r2 = part2[j]->getR();
      Vec4 p2 = part2[j]->getPkin(optVdot);
      Vec4 pc2 = part2[j]->getP();
      Vec4 pk2 = optVdot <= 1 ? part2[j]->getPkin(optVdot): p2;
      Vec4 vk2 = optRQMDevolution > 0 ? pk2/(pk2*pHat) : pk2/pk2[0];
      if(optPotentialArg>=1) p2 = part2[j]->getPcan(optVdot);
      double meff2 = part2[j]->getEffectiveMass();
      double m2=optPotentialArg>=1? part2[j]->getMass():meff2;
      Vec4 u2  = p2/m2;

      double fengj = optCollisionOrdering == 101 ? meff2/(pk2*pHat) :
             optCollisionOrdering  > 101 ? meff2*part[j]->getMass()/(pk2*p2) : meff2/pk2[0];
      if(optPotentialEval==4) {
	u2 = pk2/meff2;
	vk2 = u2;
	fengj=1.0;
      }

      int  bar2 = part2[j]->baryon()/3;
      double xvj= part2[j]->facPotential(5);
      double dmj= part2[j]->facPotential(6);
      double gpj= part2[j]->facPotential(9);
      double grj= part2[j]->facPotential(10);

      double omj = max(0.0,omega2[j].m2Calc());
      double rhj = max(0.0,rho_meson2[j].m2Calc());
      double rj=C4/mRho2*rhj;
      double sj=cOmegaRho/mRho2*omj;
      double drj=1.0/(1.0 + 3*rj + sj);
      Vec4 dOmegadJ2 = omegaPotentialNeighbor(neighbor,j, vk2);

      Vec4 dr = r1 - r2;
      double rbi = dr*u1;
      double rbj = dr*u2;
      Vec4 dr2ijri =  2*(dr - rbj*u2);  // R^2_{ij}/dr_i
      Vec4 dr2jiri =  2*(dr - rbi*u1);  // R^2_{ji}/dr_i
      Vec4 dr2ijrj =  -dr2ijri;
      Vec4 dr2jirj =  -dr2jiri;

      Vec4 dr2jipi = 0.0;
      Vec4 dr2ijpj = 0.0; 
      Vec4 dr2ijpi = 0.0;
      Vec4 dr2jipj = 0.0;

      if(optMomDerKernel) {
        dr2jipi =  2*dr*rbi/m1;     // R^2_{ji}/dp_i
        dr2ijpj =  2*dr*rbj/m2;     // R^2_{ij}/dp_j
      }

      if(optPotentialEval==4) {
        Vec4 dq = rGaussDistance(dr,u1,u2);
	dr2ijri =  2*dq;  // R^2_{ij}/dr_i
        dr2jiri =  2*dq;  // R^2_{ji}/dr_i
        dr2ijrj =  -dr2ijri;
        dr2jirj =  -dr2jiri;

      } else if(optTwoBodyDistance==2) {
  Vec4 pCM = p1 + p2;
  Vec4 bbi = pCM/pCM[0] - optP0dev*p1/p1[0];
  Vec4 bbj = pCM/pCM[0] - optP0dev*p2/p2[0];

  double sInv = pCM.m2Calc();
  double rbij = -dr * pCM/sInv;
  dr2ijri =  2*(dr + rbij*pCM);
  dr2jiri =  dr2ijri;
  dr2jirj = -dr2ijri;
  dr2ijrj = -dr2jiri;

      if(optMomDerKernel) {
  dr2ijpi = 2*(dr + pCM[0]*rbij*bbi)*rbij;
  dr2jipi = dr2ijpi;
  dr2jipj = 2*(dr + pCM[0]*rbij*bbj)*rbij;
  dr2ijpj = dr2jipj;
      }
      }

      // correct the sign of the time-component.
      dr2ijri[0] *= -1.0;
      dr2jiri[0] *= -1.0;
      dr2ijrj[0] *= -1.0;
      dr2jirj[0] *= -1.0;

      dr2jipi[0] *= -1.0;
      dr2ijpj[0] *= -1.0;
      dr2ijpi[0] *= -1.0;
      dr2jipj[0] *= -1.0;

      double rhomij=neighbor.rhom[i][j];
      double rhomji=neighbor.neighbor->rhom[j][i];

      // Scalar part
      //double s1 = -wmG*fengi*rhomij*dmj*rhos2[i];
      //double s2 = -wmG*fengj*rhomji*dmi*neighbor.box->getRhos2(j);
      double s1 = fengi*dmj*rhos2[i];
      double s2 = fengj*dmi*neighbor.box->getRhos2(j);

      // Vector part
      double facP1=Comega2*bar1*bar2*xvi*xvj;
      double facP2=fact*gri*grj/mRho2fm*bar1*bar2;
      double facP3=fact*gpi*gpj/mPhi2fm*bar1*bar2;
      double facpi = facP1*(dOmegadJ1*u2) + (u2 * vk1)*(facP2*dri + facP3);
      double facpj = facP1*(dOmegadJ2*u1) + (u1 * vk2)*(facP2*drj + facP3);
      double fsky1 = -wmG*rhomij*(s1 + facpi);
      double fsky2 = -wmG*rhomji*(s2 + facpj);

      force[i]  += -fsky1*dr2ijri - fsky2*dr2jiri;
      forcer[i] +=  fsky1*dr2ijpi + fsky2*dr2jipi;
      neighbor.box->addForce(j, -fsky1*dr2ijrj - fsky2*dr2jirj);
      neighbor.box->addForceR(j, fsky1*dr2ijpj + fsky2*dr2jipj);

      if(optDerivative) {
        Vec4 devV1 = (optP0dev*p1/p1[0]*vk2[0] - vk2)/m1;
        Vec4 devV2 = (optP0dev*p2/p2[0]*vk1[0] - vk1)/m2;
        forcer[i] += devV1*rhomji*facpj;
        neighbor.box->addForceR(j, devV2*rhomij*facpi);
      }

      if(!withMomDep) continue;

      Vec4 dp = pc1 - pc2;

      //double psq = dp.m2Calc();
      // distance squared in the rest frame of particle 2
      //double dot4j = dp * p2 / m2;
      //double psq2 = psq - optPV*dot4j*dot4j;

      // distance squared in the rest frame of particle 1
      //double dot4i = (dp * p1)/m1;
      //double psq1 = psq - optPV*dot4i*dot4i;

      Vec4 pCM = pc1 + pc2;
      double sInv = pCM.m2Calc();
      double pma = pow2(dp * pCM)/sInv;
      double psq1 = dp.m2Calc() - optPV * pma;
      double psq2 = psq1;

      // derivatives
      //Vec4 bi = p1/p1.e();
      //Vec4 bb = optP0dev * p2.e()*bi - p2;
      //Vec4 dp2ijpi = 2*(dp - optP0dev*dp[0]*bi + bb*dot4j/m2);
      //Vec4 dp2jipi = 2*(dp - optP0dev*dp[0]*bi - bb*dot4i/m1);

      //Vec4 bj = p2/p2.e();
      //Vec4 bb2 = optP0dev * p1.e()*bj - p1;
      //Vec4 dp2ijpj = 2*(-dp + optP0dev*dp[0]*bj + bb2*dot4j/m2);
      //Vec4 dp2jipj = 2*(-dp + optP0dev*dp[0]*bj - bb2*dot4i/m1);

      Vec4 v1 = pc1/pc1[0];
      Vec4 v2 = pc2/pc2[0];
      Vec4 bbi = pCM/pCM[0] - optP0dev*v1;
      Vec4 bbj = pCM/pCM[0] - optP0dev*v2;
      Vec4 dp2ijpi = 2*( dp - optP0dev*dp[0]*v1 + optPV * pCM[0]/sInv*pma*bbi);
      Vec4 dp2ijpj = 2*(-dp + optP0dev*dp[0]*v2 + optPV * pCM[0]/sInv*pma*bbj);
      Vec4 dp2jipi = dp2ijpi;
      Vec4 dp2jipj = dp2ijpj;

      // add scalar part
      double fmomdi = -wmG*fengi*potential->devVmds(psq2)*rhomij;
      double fmomei =     fengi*potential->devVmes(psq2)*rhomij;
      double fmomdj = -wmG*fengj*potential->devVmds(psq1)*rhomji;
      double fmomej =     fengj*potential->devVmes(psq1)*rhomji;

      // vector part
      double vf1=1.0;
      double vf2=1.0;
      if(optVectorPotential==1) {
        vf1 = vk1 * u2;
        vf2 = vk2 * u1;
      }
      fmomdi += -wmG*potential->devVmdv(psq2)*rhomij*vf1;
      fmomdj += -wmG*potential->devVmdv(psq1)*rhomji*vf2;
      fmomei +=     potential->devVmev(psq2)*rhomij*vf1;
      fmomej +=     potential->devVmev(psq1)*rhomji*vf2;

      force[i]  +=              -fmomdi*dr2ijri   - fmomdj*dr2jiri;
      neighbor.box->addForce(j, -fmomdi*dr2ijrj   - fmomdj*dr2jirj);

      forcer[i] +=  fmomei*dp2ijpi + fmomej*dp2jipi
          	  + fmomdi*dr2ijpj  + fmomdj*dr2jipi;
      neighbor.box->addForceR(j, fmomei*dp2ijpj + fmomej*dp2jipj
	                       + fmomdi*dr2ijpj  + fmomdj*dr2jipj);

      if(optDerivative) {
        double facm1 = -fmomdj/(wmG*vf2);
        double facm2 = -fmomdi/(wmG*vf1);
        Vec4 devV1 = (optP0dev*p1/p1[0]*vk2[0] - vk2)/m1;
        Vec4 devV2 = (optP0dev*p2/p2[0]*vk1[0] - vk1)/m2;
        forcer[i] += facm1*devV1;
        neighbor.box->addForceR(j, facm2*devV2);
      }

    }
  }

}


void MBoxPDM::qmdMatrixNeighborL(int i, Vec4& r1, NeighborPDM& neighbor)
{
  auto& part2 = neighbor.box->getParticle();
  int n2 = part2.size();
  if(n2==0) return;

  double qfac1=1.0;
  if(optPotential<4) qfac1 = part[i]->getTf() > globalTime ? part[i]->qFactor() : 1.0;

  for(int j=0; j< n2; j++) {

      Vec4 r2  = part2[j]->getR();
      Vec4 p2 = part2[j]->getPkin(optVdot);
      double m2 = part2[j]->getEffectiveMass();
      Vec4 u2 = p2/m2;

      Vec4 frj = optBaryonCurrent ? part2[j]->forceR()*u2[0] : 0.0;
      int bj = part2[j]->baryon()/3;
      double qfac2=1.0;
      if(optPotential<4) qfac2 = part2[j]->getTf() > globalTime ? part2[j]->qFactor() : 1.0;
      //double qfac2 = optPotential>=4? 1.0: part2[j]->getTf() > globalTime ? part2[j]->qFactor() : 1.0;

      if(bj==0) {
	qfac2 = facMesonPot;
	bj=1;
      }
      double* gfac2= part2[j]->facPotential();
      double xj=gfac2[5];
      double yj=gfac2[10];
      double dmj  = gfac2[6];
      double dmj2 = gfac2[7];

      Vec4 dr = r1 - r2;
      double drsq1 = dr.m2Calc() - pow2(dr*u2);
      double den3 = facG2 * exp(drsq1*wG2)*qfac1*qfac2/overSample;
      neighbor.setRhom(i,j,den3);

      // scalar density
      rhos[i] += den3*dmj;

      // scalar density2
      rhos2[i] += den3*dmj2;

      JB[i] += den3*(u2+frj)*bj*xj;
      IB[i] += den3*(u2+frj)*bj*yj;

      if(!withMomDep) continue;

      Vec4 pc1 = part[i]->getPcan(optVdot);
      Vec4 pc2 = part2[j]->getPcan(optVdot);

      Vec4 dp = pc1 - pc2;
      Vec4 pCM = pc1 + pc2;
      double sInv = pCM.m2Calc();
      double pma = pow2(dp * pCM)/sInv;
      double psq1 = dp.m2Calc() - optPV * pma;

      vmoms[i] += potential->Vmds(psq1)*den3;
      vmom4[i] += potential->Vmdv(psq1)*den3*u2;

  }

}

void MBoxPDM::computeForceNeighborL(int i, Vec4& rg, Vec4& r, Vec4& pk1,Vec4& forcei, Vec4& forceri,
    Vec4& forcej, Vec4& forcerj, NeighborPDM& neighbor,bool optGamma)
{
  auto& part2 = neighbor.box->getParticle();
  int n2 = part2.size();
  if(n2==0) return;

  Vec4 r1=part[i]->getR();
  Vec4 vk1 = optRQMDevolution > 0 ? pk1/(pk1*pHat) : pk1/pk1[0];
  double meff1 = part[i]->getEffectiveMass();
  double fengi = optCollisionOrdering == 101 ? meff1/(pk1*pHat) :
           optCollisionOrdering >  101 ? meff1*part[i]->getMass()/(pk1*part[i]->getP()) : meff1/pk1[0];

  Vec4 u1 = pk1/meff1;
  Vec4 u1a=u1;
  u1a[0]=0.0;

    int  bar1 = part[i]->baryon()/3;
    double xvi= part[i]->facPotential(5);
    double gpi= part[i]->facPotential(9);
    double gri = part[i]->facPotential(10); // rho coupling
    double dmi = part[i]->facPotential(6);
    double omi = max(0.0,omega[i].m2Calc());
    double rhi = max(0.0,rho_meson[i].m2Calc());
    double ri=C4/mRho2*rhi;
    double si=cOmegaRho/mRho2*omi;
    double dri=1.0/(1.0 + 3*ri + si);
    Vec4 dOmegadJ1 = omegaPotential(i, vk1);

    for(auto j=0; j< n2; j++) {

      Vec4 r2 = part2[j]->getR();
      Vec4 pk2 = part2[j]->getPkin(optVdot);
      Vec4 p2 = part2[j]->getP();
      double meff2 = part2[j]->getEffectiveMass();
      double fengj = optCollisionOrdering == 101 ? meff2/(pk2*pHat) :
             optCollisionOrdering  > 101 ? meff2*part[j]->getMass()/(pk2*p2) : meff2/pk2[0];
      Vec4 u2  = pk2/meff2;
      Vec4 u2a=u2;
      u2a[0]=0.0;

      int  bar2 = part2[j]->baryon()/3;
      double xvj= part2[j]->facPotential(5);
      double dmj= part2[j]->facPotential(6);
      double gpj= part2[j]->facPotential(9);
      double grj= part2[j]->facPotential(10);

      Vec4 dr = r - r2;
      double rbj = dr*u2;
      Vec4 dr2ijri =  2*(dr - rbj*u2);  // R^2_{ij}/dr_i
      Vec4 dr2jiri = dr2ijri;
      dr2ijri[0] *= -1.0;
      dr2jiri[0] *= -1.0;

      double rhomij=neighbor.rhom[i][j];
      //double rhomji=neighbor.neighbor->rhom[j][i];
      // Scalar part
      double fsky1 = -wG2*fengi*dmj*rhos2[i];
      //double fsky2 = -wG2*fengj*rhomji*dmi*neighbor.box->getRhos2(j);

      // Vector part
      double facP1=Comega2*bar1*bar2*xvi*xvj;
      double facP2=gri*grj/mRho2fm*bar1*bar2;
      double facP3=gpi*gpj/mPhi2fm*bar1*bar2;
      double facpi = facP1*(dOmegadJ1*u2) + (u2 * vk1)*(facP2*dri + facP3);
      //double facpj = facP1*(dOmegadJ2*vk1) + (u1 * vk2)*(facP2*drj + facP3);

      Vec4 rg2=0.0;
      if(optGamma) {
        double rhomxij=neighbor.rhomx[i][j];
        double rhomxji=neighbor.neighbor->rhomx[j][i];

	Vec4 dr12 = r1 - r2;
        dr2ijri =  2*(dr12 - (dr12*u2)*u2);  // R^2_{ij}/dr_i

        dr2jiri =  2*(dr12 - (dr12*u1)*u1);  // R^2_{ij}/dr_i
        dr2ijri[0] *= -1.0;
        dr2jiri[0] *= -1.0;

        // position around x_j
        double aa=u2[0]*(u2[0]+1);
        rg2= sqrt(widG)*(rg+(u2*rg)*u2/aa);

        // dgam_i/dx_i
        fsky1= -dmj*rhos2x[i]*u1a.pAbs2();
        facpi = facP1*(omegaPotentialx(i, u1a)*u2) + (u2 * u1a)*(facP2*dri + facP3);
        fsky1 += facpi*bar1*bar2;
        forcei += -fsky1*dr2ijri*wG2*rhomxij/meff1*fengi*fengi;

        Vec4 dOmegadJ2 = omegaPotentialNeighbor(neighbor,j, u2a);
        double fsky2= -dmi*neighbor.box->getRhos2x(j)*u2a.pAbs2();
        double facpj = facP1*(dOmegadJ2*u1) + (u1 * u2a)*(facP2*dri + facP3);
        fsky2 += facpj*bar1*bar2;
        forcej += -fsky2*dr2jiri*wG2*rhomxji/meff2*fengj*fengj;

        // du_i/dx_i
        Vec4 dr1= r - r1;
        Vec4 dOmegadJx = omegaPotentialx(i, dr1);
        fsky1 = -(dr1*u1)*dmj*rhos2x[i];
        facpi = facP1*(dOmegadJx*u2) + (u2 * dr1)*(facP2*dri + facP3);
        fsky1 += -2*wG2*facpi*bar1*bar2;
        forceri += -wG2*(dr1*u1)*fsky1*dr2ijri*rhomxij/meff1;

        dOmegadJx = omegaPotentialNeighbor(neighbor,j, rg2);
        fsky2 = -(rg2*u2)*dmi*neighbor.box->getRhos2x(j);
        facpj = facP1*(dOmegadJx*u1) + (u1 * rg2)*(facP2*dri + facP3);
        fsky2 += -2*wG2*facpj*bar1*bar2;
        forcerj += -wG2*(rg2*u2)*fsky2*dr2jiri*rhomxji/meff2;

      } else {
        fsky1 += -wG2*facpi;
        forcei  += -fsky1*dr2ijri*rhomij;
      }

      // under construction
      if(!withMomDep) continue;

      Vec4 pc1 = part[i]->getPcan();
      Vec4 pc2 = part2[j]->getPcan();
      Vec4 dp = pc1 - pc2;
      Vec4 pCM = pc1 + pc2;
      double sInv = pCM.m2Calc();
      double pma = pow2(dp * pCM)/sInv;
      double psq1 = dp.m2Calc() - optPV * pma;

      Vec4 v1 = pc1/pc1[0];
      Vec4 bbi = pCM/pCM[0] - optP0dev*v1;
      Vec4 dp2ijpi = 2*( dp - optP0dev*dp[0]*v1 + optPV * pCM[0]/sInv*pma*bbi);

      // add scalar part
      double fmomdi = -wG2*fengi*potential->devVmds(psq1)*rhomij;
      double fmomei =      fengi*potential->devVmes(psq1)*rhomij;

      // vector part
      double vf1=1.0;
      if(optVectorPotential==1) {
        vf1 = vk1 * u2;
      }
      fmomdi += -wG2*potential->devVmdv(psq1)*rhomij*vf1;
      fmomei +=  potential->devVmev(psq1)*rhomij*vf1;
      forcei +=  -fmomdi*dr2ijri;
      forceri +=  fmomei*dp2ijpi;

  }

}

// space integral is numerically evaluated.
void MBoxPDM::computeForceL()
{
  bool optSet= true; 

  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getPkin(optVdot);
    Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : part[i]->getP();
    if(optPotentialArg>=1) p1 = part[i]->getPcan(optVdot);
    double meff1= part[i]->getEffectiveMass();
    Vec4 u1  = pk1/meff1;

    force[i]=0.0;
    forcer[i]=0.0;

    // Gauss integral
    if(optPotentialEval==5) {

    for(int j=0;j<nGaussPoint;j++)
    for(int k=0;k<nGaussPoint;k++)
    for(int l=0;l<nGaussPoint;l++) {
      Vec4 dr = Vec4(xG[j], xG[k], xG[l],0.0);
      Vec4 r = r1 + dr;
      double drsq1 = dr.m2Calc() - pow2(dr*u1);
      double rhoij= u1[0]*facG*exp(drsq1*wmG);

      Vec4 pkin;
      RQMDpdm::qmdMatrix(i,r);
      for(auto& neighbor : neighbors)
        qmdMatrixNeighborL(i, r, *neighbor);
      RQMDpdm::singleParticlePotential(i,pkin);
      // compute sigma field.
      for(int itry=0;itry<maxIt;itry++) {
        double diff=sigmaField(i,optSet);
        if(diff <1e-5) break;
	RQMDpdm::qmdMatrix(i,r);
        for(auto& neighbor : neighbors)
          qmdMatrixNeighborL(i, r, *neighbor);
        RQMDpdm::singleParticlePotential(i,pkin);
      }
      RQMDpdm::omegaField(i);
      Vec4 forcei=0.0, forceri=0.0,forcej=0.0,forcerj=0.0;
      RQMDpdm::computeForce(i, r,r,pkin,forcei, forceri,forcej,forcerj);
      for(auto& neighbor : neighbors) {
        computeForceNeighborL(i,dr,r,pkin,forcei,forceri,forcej,forcerj,*neighbor);
      }

      forcer[i] += forceri*rhoij*wG[j]*wG[k]*wG[l];
      force[i]  += forcei*rhoij*wG[j]*wG[k]*wG[l];

      //Vec4 dr2ijri = 2*(dr - (dr*u1)*u1);
      //force[i]  += -p0*rhoij*dr2ijri*wmG*wG[j]*wG[k]*wG[l];

    }

    // Monte-Carlo sampling.
    } else {

    double aa=u1[0]*(u1[0]+1);
    Vec4 pkin;
    Vec4 forceg=0.0,forcerg=0.0,forcegj=0.0,forcergj=0.0;
    if(optIntMC==3 || optIntMC>11) {
      RQMDpdm::computeForce(i,r1, r1,pk1, forceg, forcerg,forcegj,forcergj,true);
      for(auto& neighbor : neighbors)
        computeForceNeighborL(i,r1,r1,pk1,forceg,forcerg,forcegj, forcergj,*neighbor);
    }

    for(int iev=0;iev<nMCPoint;iev++) {

      Vec4 dr = Vec4(rndm->gauss(),rndm->gauss(),rndm->gauss(),0.0);
      Vec4 r= r1 + sqrt(widG)*(dr+(u1*dr)*u1/aa);
      r[0]=r1[0];

      RQMDpdm::qmdMatrix(i,r);
      for(auto& neighbor : neighbors)
        qmdMatrixNeighborL(i, r, *neighbor);

      // compute sigma field.
      for(int itry=0;itry<maxIt;itry++) {
        double diff=sigmaField(i,optSet);
        if(diff <1e-5) break;
	RQMDpdm::qmdMatrix(i,r);
        for(auto& neighbor : neighbors)
          qmdMatrixNeighborL(i, r, *neighbor);
      }
      RQMDpdm::omegaField(i);
      double p0 = RQMDpdm::singleParticlePotential(i,pkin);

      Vec4 forcei=0.0, forceri=0.0,forcej=0.0,forcerj=0.0;
      if(optIntMC<=10) {

        RQMDpdm::computeForce(i,dr,r,pkin, forcei, forceri,forcej,forcerj);
        for(auto& neighbor : neighbors) {
          computeForceNeighborL(i,dr,r,pkin,forcei,forceri,forcej,forcerj,*neighbor);
        }

        double gg=1.0;
        if(optIntMC>=2) {
          meff1 = part[i]->getEffectiveMass();
          double gam  = pkin[0]/meff1;
          gg=gam/u1[0];
        }
        forcer[i] += forceri*gg;
        force[i]  += forcei*gg;

        if(optIntMC==3) {
          forcej=0.0;forcerj=0.0;
	  RQMDpdm::computeForce(i,dr,r,pk1, forcei, forceri,forcej,forcerj,true);
          for(auto& neighbor : neighbors)
            computeForceNeighborL(i,dr,r,pk1,forcei,forceri,forcej,forcerj,*neighbor,true);
          force[i] += -p0*(forcej+forcerj);
        }

      } else {
        Vec4 dr2ijri = 2*((r-r1) - ((r-r1)*u1)*u1);
	if(optIntMC>11) {
	  RQMDpdm::computeForce(i,dr,r,pk1, forcei, forceri,forcej,forcerj,true);
          for(auto& neighbor : neighbors)
            computeForceNeighborL(i,dr,r,pkin,forcei,forceri,forcej,forcerj,*neighbor,true);
	}
        force[i]  += -p0*(forceg + dr2ijri*wG2*facG2 + forceri);

        if(optIntMC==13) force[i] += -p0*(forcej+forcerj);

	if(isnan(force[i])) {
	cout <<"p0= "<< p0 << endl;
	cout <<"forceg= "<< forceg;
	cout <<"forceri= "<< forceri;
	cout <<"dr= "<< dr2ijri*wG2*facG2;
	cin.get();
	}


    } // end loop over MC points
      force[i]  /= nMCPoint;
      forcer[i] /= nMCPoint;

    }

  }

  } // end loop over particle i

}


/*
// Pre-factor from the baryon current.
Vec4 MBoxPDM::facV(int i, Vec4& pkin, NeighborPDM& neighbor)
{
  double rh = neighbor.box->getRho(i);
  if(rh<1e-15) return Vec4(0.0);
  Vec4 bi=0.0;

  // 4-components of the vector potential is fully included.
  if(optVectorPotential==1) {
    bi = pkin/pkin[0];

  // Only time-component V^0 term is included.
  } else if(optVectorPotential==2) {
    bi[0]=1.0;

  // Only time-component V^0 term is included with the form of V(rho_B).
  } else if (optVectorPotential==3) {
    return (t1 + t3f*neighbor.box->getRhog(i))/rh * neighbor.box->getJB(i);
  }

  double vj = neighbor.box->getJB(i) * bi;
  double vv = t1 + t3*neighbor.box->getRhog(i);  // V/rho_B
  double dv = (gam-1.0)*t3*pow(rh,gam-3);  // del(V/rho)/rho

  return vj*dv*neighbor.box->getJB(i) + vv*bi;

}

//...Compute time-derivatives of the vector potential.
void MBoxPDM::computeVdot(NeighborPDM& neighbor)
{
  auto& part2 = neighbor.box->getParticle();
  int n2 = part2.size();
  if(n2==0) return;

  bool opt=true;
  //opt=false;

  Vdot.assign(NV,0.0);

  for(int i=0;i<NV; i++) {
    double vvi = t1 + t3*rhog[i];  // V_i/rho_i
    double dvi=0.0;
    // del(V_i/rho_i)/rho_i
    if(abs(rho[i]) > 1e-7) dvi = (gam-1.0)*t3*pow(rho[i],gam-3.0);

    Vec4 Bi = dvi*JB[i];
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    Vec4 vk1 = p1/p1[0];
    if(optPotentialArg==1) p1 = part[i]->getPcan(optVdot);
    double emi=p1[0];
    if(optTwoBodyDistance==3)  emi = p1.mCalc();
    Vec4 vi = p1 / emi;
    Vec4 b1 = p1 / p1[0];
    double* gf1=part[i]->facPotential();

    for(int j=0;j<n2; j++) {

      double vvj = t1 + t3*neighbor.box->getRhog(j);
      double dvj=0.0;
      if(abs(neighbor.box->getRho(j)) > 1e-7) dvj = (gam-1.0)*t3*pow(neighbor.box->getRho(j),gam-3.0);
      Vec4 jb = neighbor.box->getJB(j);
      Vec4 Bj = dvj*jb;
      Vec4 r2 = part2[j]->getR();
      Vec4 p2 = part2[j]->getP();
      Vec4 vk2 = p2/p2[0];
      if(optPotentialArg==1) p2 = part2[j]->getPcan(optVdot);
      double emj=p2[0];
      if(optTwoBodyDistance==3)  emj = p2.mCalc();
      Vec4 vj = p2 / emj;
      Vec4 b2 = p2 / p2[0];
      double* gf2=part[j]->facPotential();
   
      Vec4 Aj = (vj * JB[i]) * Bi;
      Vec4 Ai = (vi * jb   ) * Bj;

      // Compute derivatives

      // Non-relativistic.
      Vec4 rk = r1 - r2;
      Vec4 dgami=0.0;
      Vec4 dgamj=0.0;
      Vec4 dr2ri   = 2 * rk;
      Vec4 dr2rj   = -dr2ri;
      Vec4 drji2ri = dr2ri;
      Vec4 drji2rj = dr2rj;
      Vec4 dr2pi = 0.0;
      Vec4 dr2pj = 0.0;
      Vec4 drji2pi=0.0;
      Vec4 drji2pj=0.0;

      // two-body c.m. frame.
      if(optTwoBodyDistance==2) {

      Vec4 pcm = p1 + p2;
      Vec4 bet = pcm/pcm[0];
      Vec4 bbi = bet - optP0dev*b1;
      Vec4 bbj = bet - optP0dev*b2;
      double s = pcm.m2Calc();
      double rbij = dot3(rk,pcm)/s;
      dr2ri =  2*(rk + rbij*pcm);
      dr2rj = -dr2ri;
      dr2pi = 2*(rk + pcm[0]*rbij*bbi)*rbij;
      dr2pj = 2*(rk + pcm[0]*rbij*bbj)*rbij;
      drji2ri =  dr2ri;
      drji2rj = -drji2ri;
      drji2pi = dr2pi;
      drji2pj = dr2pj;

      // derivatives from gamma_{ij}.
      dgami = optP0dev*(1.0/pcm[0]-pcm[0]/s)*b1+pcm/s;
      dgamj = optP0dev*(1.0/pcm[0]-pcm[0]/s)*b2+pcm/s;

      // rest frame of particle i or j.
      } else if(optTwoBodyDistance == 3) {

        double rbi=dot3(rk,vi);
        double rbj=dot3(rk,vj);
        dr2ri =  2*(rk+rbj*vj);    // dR~^2_ij/dR_i
        dr2rj =  -dr2ri;           // dR~^2_ij/dR_j
        dr2pi =  0.0;              // dR~^2_ij/dP_i
        dr2pj =  2*rk*rbj/emj;     // dR~^2_ij/dP_j

        drji2rj = 2*(rk+rbi*vi);
        drji2rj =  -drji2ri;
        drji2pi =  2*rk*rbi/emi;
        drji2pj =  0.0;
      }

      Vec4 forcei=force[i];
      Vec4 forceri=forcer[i];
      Vec4 forcej=neighbor.box->getForce(j);
      Vec4 forcerj=neighbor.box->getForceR(j);
      double rhomij=neighbor.rhom[i][j];
      double rhomji=neighbor.neighbor->rhom[j][i];

      double xdoti=dot3(vk1+forceri,dr2ri)   + dot3(vk2+forcerj,dr2rj);
      double xdotj=dot3(vk2+forcerj,drji2rj) + dot3(vk1+forceri,drji2ri);

      double pdoti=dot3(forcei,dr2pi-dgami/wmG)
	         + dot3(forcej,dr2pj-dgamj/wmG);
      double pdotj=dot3(forcej,drji2pj-dgamj/wmG)
	         + dot3(forcei,drji2pi-dgami/wmG);

      double doti=-wmG*(xdoti+pdoti)*rhomij;
      double dotj=-wmG*(xdotj+pdotj)*rhomji;
      Vdot[i] += doti*(Aj + vvi*vj);
      neighbor.box->addVdot(j,dotj*(Ai + vvj*vi));

      // Momentum dependent potential.
      double fmomdi=0.0, fmomdj=0.0;
      if(withMomDep) {

      potential->dVdmd(distanceP->psq1,distanceP->psq2,gf1[6]*gf2[6],gf1[7]*gf2[7],gf1[8],gf2[8],gf1[9],gf2[9]);
      fmomdi=potential->fmomdi;
      fmomdj=potential->fmomdj;

      Vdot[i] += doti*fmomdi*vj;
      neighbor.box->addVdot(j, dotj*fmomdj*vi);

      distance->distanceP();

      pdoti=dot3(forcei,distance->dp2ijpi) + dot3(forcej,distance->dp2ijpj);
      pdotj=dot3(forcej,distance->dp2jipj) + dot3(forcei,distance->dp2jipi);

      //Vdot[i] += pdoti*devVme(psq2)*rhomij*vj;
      //neighbor.box->addVdot(j, pdotj*devVme(psq1)*rhomji*vi);

      Vdot[i] += pdoti*potential->fmomdv1*vj;
      neighbor.box->addVdot(j, pdotj*potential->fmomdv2*vi);

      }


      if(opt) continue;

      //Compute derivatives of pre-factors v_j^\mu in the vector potential.
      double fai = - dot3(forcej,Bi/emj) * rhomij;
      double faj = - dot3(forcei,Bj/emi) * rhomji;
      double fai2 = dot3(forcej,vj);
      double faj2 = dot3(forcei,vi); 
      double fai3 = dot3(vj,Bi/p2[0]);
      double faj3 = dot3(vi,Bj/p1[0]);

      double  fvi2=0.0;
      double  fvj2=0.0;
      if(optTwoBodyDistance==3) {
        fai3=Bi[0]/p2[0];
        faj3=Bj[0]/p1[0];
      } else {
        fvi2=-optP0dev*fai2*rhomij*vvi/p2[0];
        fvj2=-optP0dev*faj2*rhomji*vvj/p1[0];
      }

      fai += optP0dev*fai2*fai3*rhomij;
      faj += optP0dev*faj2*faj3*rhomji;

      double fvi = vvi*rhomij/emj;
      double fvj = vvj*rhomji/emi;

      Vdot[i] += fai*JB[i] + fvi*forcej + fvi2*vj;
      neighbor.box->addVdot(j, faj*neighbor.box->getJB(j) + fvj*forcei + fvj2*vi);

      // Momentum dependent potential.
      if(withMomDep) {
        Vdot[i] += fmomdi*rhomij*forcej/emj;
        neighbor.box->addVdot(j, fmomdj*rhomji*forcei/emi);
        if(optTwoBodyDistance != 3) {
          Vdot[i] += -optP0dev*fai2*rhomij*fmomdi/p2[0]*vj;
          neighbor.box->addVdot(i, -optP0dev*faj2*rhomji*fmomdj/p1[0]*vi);
	}
      }


    } // end j loop
  }   // end i loop

  //return;

  Vec4 vdott=0.0;
  for(int i=0;i<NV;i++) {
    vdott += Vdot[i];
  }

  Vec4 dv = vdott/NV;
  for(int i=0;i<NV;i++) {
    Vdot[i] -= dv;
  }

}
*/


} // end of namespace jam2
