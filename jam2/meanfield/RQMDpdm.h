#ifndef jam2_meanfield_RQMDpdm_h
#define jam2_meanfield_RQMDpdm_h

#include <jam2/hadrons/JamParticleData.h>
#include <jam2/meanfield/MeanField.h>
#include <jam2/meanfield/ParityDoublet.h>

namespace jam2 {

using Pythia8::Vec4;

class RQMDpdm : public MeanField
{
protected:
  Pythia8::Rndm* rndm;
  std::vector<EventParticle*> part; // particles for potential interaction
  std::vector<std::vector<double> > rhom,rhomx; // rho_{ij}
  std::vector<Vec4> omega,omegax;        // omega-meson field
  std::vector<Vec4> rho_meson,rho_mesonx;    // rho-meson field
  std::vector<double> rho;        // baryon density
  std::vector<double> rhos,rhos2,rhos2x;  // scalar baryon density
  std::vector<double> lambda;   // Lagrange multipliers
  std::vector<double> vmoms;   // momentum-dependent scalar potential
  std::vector<Vec4> vmom4; // momentum-dependent vector potential
  std::vector<Vec4> JB;       // baryon current
  std::vector<Vec4> IB;       // isospin current
  std::vector<Vec4> Vdot;    // time derivative of vector potential
  std::vector<Vec4> force;  // force for p
  std::vector<Vec4> forcer; // force for r
  std::vector<std::vector<Pythia8::Vec4> > dGij,dGji,forceD;
  std::vector<std::vector<Pythia8::Vec4> > dUx,dUy,dUz;
  //std::vector<vector<double> > forceD;
  static bool firstCall;
  int optRMF,optRhoMeson;
  int transportModel;
  int overSample;
  double g_omega,Comega2; // omega coupling
  double mOmega2,mOmega2fm,mOmega3,C4;
  double mPhi2,mRho2,mRho3,mPhi2fm,mRho2fm;
  double cOmegaRho; // omega-rho meson mixing parameter
  double f_pion;
  double facCoupling,facCouplingv,facCoupling2,facCouplingv2;
  ParityDoublet *potential;
  double widG,wmG,facG,wG2,facG2;
  int maxIt;
  int optOmega4=1;
  double Det=1.0;
  int optIntMC;

public:
  RQMDpdm(Pythia8::Settings* s, JamParticleData* jpd,Pythia8::Rndm* r,int itm=10);
  ~RQMDpdm();
  void evolution(std::list<EventParticle*>& plist,double t, double dt,int step);
  void init(std::list<EventParticle*>& plist) { };
  Pythia8::Vec4 computeEnergy(std::list<EventParticle*>& plist,int step);
  void singleParticlePotential();
  void setScalarPotential();
  void setFreeLagrangeMultiplier();
  void findLagrangeMultiplier(std::vector<std::vector<double> >& mat,int n);
  void omegaField();
  ParityDoublet *getPotential() const {return potential;}

  std::vector<EventParticle*>& getParticle() {return part;}
  std::vector<Vec4>& getOmega()              {return omega;}
  std::vector<Vec4>& getRhoMeson()           {return rho_meson;}
  std::vector<Vec4>& getJB()                 {return JB;}
  std::vector<Vec4>& getIB()                 {return IB;}
  int particleSize()                         {return part.size();}
  void add(EventParticle* p) { 
      potential->setPotentialParam(p);
      part.push_back(p); }

  void addRhos(int i, double a)                {rhos[i] +=a;}
  void addRhos2(int i, double a)               {rhos2[i] +=a;}
  void addRho(int i, double a)                 {rho[i] +=a;}
  void addVmoms(int i, double a)               {vmoms[i] +=a;}
  void addVmom4(int i, const Pythia8::Vec4& a) {vmom4[i] +=a;}
  void addJB(int i, const Pythia8::Vec4& a)    {JB[i] +=a;}
  void addIB(int i, const Pythia8::Vec4& a)    {IB[i] +=a;}
  void addForce(int i, const Pythia8::Vec4& f) {force[i] +=f;}
  void addForceR(int i, const Pythia8::Vec4& f){forcer[i] +=f;}
  void addVdot(int i, const Pythia8::Vec4& v)  {Vdot[i] +=v;}
  EventParticle* getParticle(int i)            {return part[i];}
  double getRho(int i)                         {return rho[i];}
  Vec4   getOmega(int i)                       {return omega[i];}
  Vec4   getRhoMeson(int i)                    {return rho_meson[i];}
  double getRhos(int i)                        {return rhos[i];}
  double getRhos2(int i)                       {return rhos2[i];}
  double getRhos2x(int i)                      {return rhos2x[i];}
  double getVmoms(int i)                       {return vmoms[i];}
  Pythia8::Vec4   getJB(int i)                 {return JB[i];}
  Pythia8::Vec4   getForce(int i)              {return force[i];}
  Pythia8::Vec4   getForceR(int i)             {return forcer[i];}
  void setZero() {
    rhos.assign(NV,0.0);
    rhos2.assign(NV,0.0);
    JB.assign(NV,0.0);
    IB.assign(NV,0.0);
    vmoms.assign(NV,0.0);
    vmom4.assign(NV,0.0);
  }

  void qmdMatrix();
  void computeForce();
  void computeForce1();
  void computeForceMatrix();
  double sigmaField(bool opt);
  Pythia8::Vec4 omegaPotential(int i, Pythia8::Vec4& ui);
  Pythia8::Vec4 omegaPotentialx(int i, Pythia8::Vec4& ui);

  // For Monte-Carlo integral
  void qmdMatrix(int i, Pythia8::Vec4& r1);
  double sigmaField(int i, bool optSet);
  void omegaField(int i);
  double singleParticlePotential(int i, Pythia8::Vec4& pkin);
  void computeForce(int i, Pythia8::Vec4& rg,Pythia8::Vec4& r1, Pythia8::Vec4& pk1, Pythia8::Vec4& forcei, Pythia8::Vec4& forceri,
       Pythia8::Vec4& forcej, Pythia8::Vec4& forcerj, bool optGamma=false);
  void qmdMatrix(std::vector<Pythia8::Vec4>& rg);
  //void computeForceMatrix(Pythia8::Vec4& rg);
  void computeForceMC(std::vector<Pythia8::Vec4>& rg,std::vector<Pythia8::Vec4>& pkin,std::vector<Pythia8::Vec4>& u,
       std::vector<double>& p0);
  void computeForceL();
  void computeForceL2();
  void computeForceDE();

  Vec4 getPcan(Vec4 p, Vec4 v) {
    double msq=p.m2Calc();
    p += v; p[0] = sqrt(msq + p.pAbs2());
    return p;
  }
  Vec4 getPkin(Vec4 p, Vec4 v, double m) {
    p -= v; p[0] = sqrt(m*m + p.pAbs2());
    return p;
  }

  double sol3(double x) {
    if (x < 1e-8) return 1.0 - 4.*x*x/27.;
    double y=cbrt(sqrt(1+x*x)-x);
    return 3./(2*x)*(1.-y*y)/y;
  }

  // derivative of sol3
  double dsol3(double x) {
    if( x < 1e-5) return -8.0*x/27. + 64.*x*x*x/243.;
    double y3=sqrt(1+x*x)-x;
    double y=cbrt(y3);
    return 1./(2.*x*x*y)*(-3*(1-y*y) + x*(1+y*y)/(y3+x) );
  }

  double devVec0(double xi, double yi, double gn,double m3) {
    double xo0=sqrt(27*C4)*gn/(2*m3*(1.+yi)*sqrt(1.+yi));
    return 1.0/(1+yi)*sol3(xo0)
            + xo0*(1+3*xi+yi)/((1+xi+yi)*(1+yi))*dsol3(xo0);
  }

  double devVec1(double xi, double yi, double gn,double m3) {
    double xo0=sqrt(27*C4)*gn/(2*m3*(1.+yi)*sqrt(1.+yi));
    return (1.0 + 7./2.*xi+1.5*yi)/(1+yi)*sol3(xo0)
            + xo0*(1+1.5*yi+1.5*xi)*(1+3*xi+yi)/((1+xi+yi)*(1+yi))*dsol3(xo0);
  }
  
  double devVec2(double ri, double si, double gi,double z,double m3) {
    double xr0=sqrt(27*C4)*gi/(2*m3*(1.+si)*sqrt(1.+si));
    return si*(1.-3*ri)/((1+si)*(1+si))*sol3(xr0)
           - xr0*3*z*(1+1.5*si+1.5*ri)/((1+si)*(1+si))*dsol3(xr0);
  }

  Pythia8::Vec4 rGaussDistance(Pythia8::Vec4& dr, Pythia8::Vec4& u1, Pythia8::Vec4& u2) {
  double a = -u1.m2Calc() + pow2(pHat*u1);
  double b = -u2.m2Calc() + pow2(pHat*u2);
  double c = (u1*pHat)*(u2*pHat) - u1*u2;
  double aa=1.0 + a/2.0;
  double bb=1.0 + b/2.0;
  Det=aa*bb - c*c/4;
  double uu1 = u1*dr;
  double uu2 = u2*dr;
  return dr - (bb*uu1*u1 + aa*uu2*u2 + c/2*(uu2*u1 + uu1*u2))/(2*Det);
  }


};
}
#endif
