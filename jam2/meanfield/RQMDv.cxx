#include <jam2/meanfield/RQMDv.h>

namespace jam2 {

using namespace std;

bool RQMDv::firstCall=true;

RQMDv::RQMDv(Pythia8::Settings* s) : MeanField(s)
{
  optBaryonCurrent = settings->mode("MeanField:optBaryonCurrent"); 
  double widG = settings->parm("MeanField:gaussWidth");
  wG = 1.0/(4*widG);
  facG = 1.0/pow(4.0*M_PI*widG, 1.5);
  if(optPotentialEval==0) {
    wG2=wG;
    facG2=facG;
  } else {
    wG2 = 1.0/(2*widG);
    facG2 = 1.0/pow(2.0*M_PI*widG, 1.5);
  }

  overSample = settings->mode("Cascade:overSample");
  transportModel = settings->mode("MeanField:transportModel"); 
  if(transportModel==2) {
    wG = 1.0/(2*widG);
    facG = 1.0/pow(2.0*M_PI*widG, 1.5);
    //selfInt = 0;
  }

  //int mode = settings->mode("MeanField:potentialType"); 
  potential = new VectorPotential2(settings);
  isPotTable=potential->isPotTable();
  nV = potential->nv();
  //cutOffPot=potential->potcut();
  cutOffPot=settings->parm("MeanField:cutOffPotential") * rho0;

  bFac = potential->bfac();
  //cFac = potential->cfac();
  tFac = potential->tfac();
  pFac = potential->pfac();
  sFac = potential->sfac();
  withMomDep = potential->isMomDep();
  rhoMin=potential->getRhoMin();
  if(firstCall) {
    cout << "RQMDv mode " << endl;
    firstCall=false;
    //for(int i=0;i<nV;i++) cout << "tFac = "<< tFac[i] << " bFac= "<< bFac[i] <<endl;
  }

  selfInt = optPotentialEval<2? 1:0;

}

RQMDv::~RQMDv()
{
  part.clear();
  rho.clear();
  //for(int i=0;i<nV;i++) rhog[i].clear();
  rhog.clear();
  vmom4.clear();
  force.clear();
  forcer.clear();
  //for(int i=0;i<NV;i++) rhom[i].clear();
  rhom.clear();
  JB.clear();
  Vdot.clear();
  lambda.clear();
  delete potential;
}

void RQMDv::evolution(list<EventParticle*>& plist, double t, double dt,int step)
{
  part.clear();
  globalTime = t;
  distance->phat(pHat);

  // import particles.
  for(auto& i : plist) {
    //if(i->tevol() > t ) continue;
    if(optCollisionOrdering>100 && i->tevol() > t ) continue;
    double tp = optCollisionOrdering < 100 ? t : tau2t(t,i);
    if(i->isMeanField(tp,optPotential)) {
    //if(i->isMeanField(i->getT(),optPotential)) {
    //if(i->isMeanField(t,optPotential)) {
      part.push_back(i);
      potential->setPotentialParam(i);
    }
  }

  NV = part.size();
  rhom.resize(NV);
  for(int i=0;i<NV;i++) {
    rhom[i].resize(NV);
  }
  rhog.resize(nV);
  for(int i=0;i<nV;i++) rhog[i].assign(NV,0.0);

  rho.assign(NV,0.0);

  vmom4.assign(NV,0.0);
  JB.assign(NV,0.0);
  Vdot.assign(NV,0.0);
  force.assign(NV,0.0);
  forcer.assign(NV,0.0);
  lambda.assign(NV,1.0);

  // change from kinetic to canonical momenta.
  if(optVdot==1) {
    for(auto& i : part) i->setFree();
  }

  qmdMatrix();
  singleParticlePotential();
  if(transportModel==1) computeForce();
  //if(transportModel==1) computeForceMatrix();
  //if(transportModel==1) computeForceMatrix2();
  else computeBUUForce();

  // Note that optVdot=0,1: all momenta are canonical.
  // Canonical momenta update.
  if(optVdot == 0) {
    for(int i=0; i< NV; i++) {

    double t0=part[i]->getT();

    Vec4 qdot = forcer[i];
    if(qdot.m2Calc() <=0.0) {
      cout << " forcer^2= "<< qdot.m2Calc()
           << " v= "<< forcer[i].pAbs()/forcer[i][0] << endl;
    }

      part[i]->updateByForce(force[i], forcer[i],dt);
      part[i]->addT(forcer[i][0]*dt);
      part[i]->updateR(t0, optPropagate);
      if(optCollisionOrdering > 100) {
	if(optCollisionOrdering==101) part[i]->tevol(pHat*part[i]->getR());
      }
      part[i]->lambda(MeanField::lambda(part[i]));

    }

  // canonical momenta are used for the evaluation of forces,
  // after update canonical momenta, set kinetic momenta.
  } else if(optVdot == 1) {

    for(int i=0; i< NV; i++) {

      // canonical momenta update.
      part[i]->updateByForce(force[i], forcer[i],dt);
      // change to kinetic momenta.
      part[i]->setKinetic();
    }

  // compute time derivatives of vector potential V_i^\mu numerically.
  } else if(optVdot == 2) {

    // Vdot[] is already computed in singleParticlePotential().
    for(int i=0; i< NV; i++) {
      part[i]->updateByForce(force[i] - Vdot[i]/dt, forcer[i],dt);
    }

  // compute time derivatives of vector potential analytically.
  } else if(optVdot == 3) {

    computeVdot();
    for(int i=0; i< NV; i++) {
      part[i]->updateByForce(force[i] - Vdot[i], forcer[i],dt);
    }

  // compute time derivatives of V^\mu numerically; (V(t+dt) - V(t))/dt
  // (under construction)
  } else if(optVdot == 4 ) {

    for(int i=0;i<NV;i++) {
      part[i]->updateByForce(force[i] + part[i]->potv()/dt, forcer[i],dt);
      vmom4[i]=0.0;
      JB[i]=0.0;
    }
    vmom4.assign(NV,0.0);
    JB.assign(NV,0.0);
    qmdMatrix();
    singleParticlePotential();
    for(int i=0;i<NV;i++) {
      part[i]->addP(-part[i]->potv());
      part[i]->setOnShell();
    }

  } else {
    cout << "RQMDv wrong option optVdot = "<< optVdot<<endl;
    exit(1);
  }

  //if(!optSet) setScalarPotential();

  //qmdMatrix();
  //singleParticlePotential(true);

}

/*
void RQMDv::setScalarPotential()
{
  for(int i=0;i<NV;i++) {
    part[i]->setPotS(part[i]->pots());
  }
}
*/

// Compute single particle potential.
void RQMDv::singleParticlePotential()
{
  // Compute invariant baryon density.
  for(int i=0; i< NV; i++) {
    //double* pfac=part[i]->facPotential();
    //rho[i] = sqrt(max(0.0,JB[i].m2Calc()));
    rho[i] = sqrt(min(cutOffPot,max(0.0,JB[i].m2Calc())));
    if(isPotTable<2) {
    if(rho[i] > 1e-8) {
      for(int j=0;j<nV;j++) 
      rhog[j][i]  = pow(rho[i],bFac[j]-1);
    }
    }
    part[i]->setRhoB(rho[i]);
  }

  Vec4 vTotal=0.0;
  Vec4 vc = 0.0;
  bool optV=false;
  if(optVdot==1) optV=true;
  if(optV && optVectorPotential==1) {
    for(int i=0; i< NV; i++) {
      //double* pfac=part[i]->facPotential();
      //double vsky = part[i]->baryon()/3*(pfac[1]*t1 + pfac[2]*t3*rhog[i] + pfac[4]*rhog2[i]);
      double vsky = 0.0;
      if(isPotTable) {
	vsky = potential->getU(rho[i])/max(rhoMin,rho[i]);
      } else {
        for(int j=0;j<nV;j++) vsky += sFac[j]*rhog[j][i];
      }
      vsky *= part[i]->baryon()/3;
      vTotal += vsky * JB[i] + vmom4[i];
    }
    vc = vTotal / (NV*overSample);
    vc[0]=0.0;
  }

  Vec4 vdott = 0.0;
  for(int i=0; i< NV; i++) {

    if(rho[i] < 1e-15) continue;
    Vec4 vpot0 = part[i]->potv();
    //double* pfac=part[i]->facPotential();
    //double vsky = part[i]->baryon()/3*(pfac[1]*t1 + pfac[2]*t3*rhog[i] + pfac[4]*rhog2[i]);
    double vsky = 0.0;
    if(isPotTable) {
      vsky = potential->getU(rho[i])/max(rhoMin,rho[i]);
    } else {
      for(int j=0;j<nV;j++) vsky += sFac[j]*rhog[j][i];
    }

    vsky *= part[i]->baryon()/3;

    // four-components of vector potential are fully included.
    if(optVectorPotential==1) {
      part[i]->setPotV( vsky * JB[i] + vmom4[i] -vc);
      part[i]->setPotVm(vmom4[i] -vc);

    // only time component of vector potential is included.
    } else if(optVectorPotential==2) {
      part[i]->setPotV(0,vsky*JB[i][0] + vmom4[i][0] );
      part[i]->setPotVm(0,vmom4[i][0] );

    // only time component of vector potential is included in the form
    // of V(rho_B) where rho_B is an invariant baryon density.
    } else if(optVectorPotential==3) {
      part[i]->setPotV(0,vsky*rho[i] + vmom4[i][0] );
      part[i]->setPotVm(0,vmom4[i][0] );

    // fully non-relativistic
    } else {
      double vs=0.0;
      if(!isPotTable) {
        for(int j=0;j<nV;j++)  vs += tFac[j]*pow(max(0.0,JB[i][0]),bFac[j]);
      } else {
	vs = potential->getV(JB[i][0]);
      }
      part[i]->setPotV(0, part[i]->baryon()/3*vs + vmom4[i][0] );
      part[i]->setPotVm(0,vmom4[i][0] );
    }

    if(optVdot == 2) {
      Vdot[i] = part[i]->potv() - vpot0;
      vdott += Vdot[i];
    }
  }

  if(optVdot == 2) {
    vdott /= NV;
    for(int i=0; i< NV; i++) Vdot[i] -= vdott;
  } 

}

// Compute single particle potential energy.
Vec4 RQMDv::computeEnergy(list<EventParticle*>& plist, int step)
{
  pTot=0.0;   
  for(auto& i :plist) {
    double m = i->getEffectiveMass();
    if(optVectorPotential==1 && optVdot==0) {
      Vec4 pk= i->getP() - i->potv();
      pTot[0] += sqrt( m*m + pk.pAbs2());
    } else {
      pTot[0] += sqrt( m*m + i->pAbs2());
    }
    pTot[1] += i->getP(1);
    pTot[2] += i->getP(2);
    pTot[3] += i->getP(3);
    pTot[0] += i->potv(0);
  }

  if(step==1) pTot0 = pTot/overSample;

  return pTot/overSample;
}

/*
// use distance:three options for two-body distance can be used:
// non-rel. distance, two-body cm frame, and rest-frame of particle.
void RQMDv::qmdMatrix()
{
  for(int i=0; i< NV; i++) {
    Vec4 r1  = part[i]->getR();
    Vec4 p1 = optPotentialArg>=1? part[i]->getPcan(optVdot) : part[i]->getP();
    distance->setRP1(r1,p1);
    distanceP->setRP1(r1,p1);
    Vec4 v1 = p1/p1[0];

    double qfac1=1.0;
    if(optPotential<4) qfac1 = part[i]->getTf() > globalTime ? part[i]->qFactor() : 1.0;
    Vec4 fri = optBaryonCurrent ? part[i]->forceR() : 0.0;
    int bi   = part[i]->baryon()/3;
    if(bi==0) {
      qfac1 = facMesonPot;
      bi=1;
    }
    double* gfac1= part[i]->facPotential();

    for(int j=i+selfInt; j< NV; j++) {

      double pot1=1.0, pot2=1.0;

      Vec4 r2  = part[j]->getR();
      Vec4 p2 = optPotentialArg>=1 ? part[j]->getPcan(optVdot) : part[j]->getP();
      distance->setRP2(r2,p2);
      distanceP->setRP2(r2,p2);
      Vec4 v2 = p2/p2[0];

      Vec4 frj = optBaryonCurrent ? part[j]->forceR() : 0.0;
      int bj = part[j]->baryon()/3;
      double qfac2=1.0;
      if(optPotential<4) qfac2 = part[j]->getTf() > globalTime ? part[j]->qFactor() : 1.0;
      if(bj==0) {
	qfac2 = facMesonPot;
	bj=1;
      }
      double* gfac2= part[j]->facPotential();
      
      distance->density();
      // exp(-(x_i-x_j)^2/(4L))
      rhom[i][j] = distance->density1*qfac1*qfac2/overSample*pot1;
      rhom[j][i] = distance->density2*qfac1*qfac2/overSample*pot2;

      if(optPotentialEval !=0) {
	double em1=p1.mCalc();
	double em2=p2.mCalc();
	rhom[i][j] *= em1/p1[0]*em2/p2[0];
	rhom[j][i] *= em1/p1[0]*em2/p2[0];
      }

      // exp(-(x_i-x_j)^2/(2L))
      double rhomij = distance->density1w*qfac1*qfac2/overSample*pot1;
      double rhomji = distance->density2w*qfac1*qfac2/overSample*pot2;
      if(i==j) {
	rhom[i][j] /=2;
	rhom[j][i] /=2;
	rhomij /=2;
	rhomji /=2;
      }


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      //double m1 = part[j]->getMass();
      double m1 = p1.mCalc();
      Vec4 u1 = p1/m1;
      //double m2 = part[j]->getMass();
      double m2 = p2.mCalc();
      Vec4 u2 = p2/m2;

      Vec4 dR = r1 - r2;
      double drsq1 = dR.m2Calc() - pow2(dR*u2);
      double drsq2 = dR.m2Calc() - pow2(dR*u1);

      double density1,density2;
      if(optPotentialEval==4) {
        Vec4 dq = rGaussDistance(dR,u1,u2);
        density1 = facG/sqrt(Det) * exp(dq*dR*wG);
        density2 = density1;
      } else {
        density1 = facG * exp(drsq1*wG);
        density2 = facG * exp(drsq2*wG);
      }
      double density3 = facG2 * exp(drsq1*wG2);
      double density4 = facG2 * exp(drsq2*wG2);
      double rhomija = density1*qfac1*qfac2/overSample;
      double rhomjia = density2*qfac1*qfac2/overSample;
      // exp(-(x_i-x_j)^2/(2L))
      double rhomijb = density3*qfac1*qfac2/overSample;
      double rhomjib = density4*qfac1*qfac2/overSample;
      cout << "drsq1= "<< drsq1 << " den1= "<< p2[0]/m2*density1 << " wG= "<< wG <<endl;
      cout  << "facG= "<< facG << "p0= "<< p2[0] << " em2= "<< m2<<endl;

      cout << "den1= "<< rhom[i][j] << " den1= "<< p2[0]/m2*rhomija<<endl;
      cout << "den3= "<< rhomij << " den3= "<< p2[0]/m2*rhomijb<<endl;

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      // vector current
      JB[i] += rhomij*(v2+frj)*bj;
      JB[j] += rhomji*(v1+fri)*bi;

      if(!withMomDep) continue;
      distanceP->psq();
      double vmom4i = potential->Umd(distanceP->psq1,gfac1[6]*gfac2[6],gfac1[7]*gfac2[7]
	  ,gfac1[8],gfac1[9],gfac2[8],gfac2[9]);
      double vmom4j = potential->Umd(distanceP->psq2,gfac1[6]*gfac2[6],gfac1[7]*gfac2[7]
	  ,gfac1[8],gfac1[9],gfac2[8],gfac2[9]);

      // momentum-dependent vector potential
      vmom4[i] += vmom4i*rhomij*v2;
      vmom4[j] += vmom4j*rhomji*v1;

    }
  }

}
*/

Vec4 RQMDv::rGaussDistance(Pythia8::Vec4& dr, Pythia8::Vec4& u1, Pythia8::Vec4& u2) {
  double a = -u1.m2Calc() + pow2(pHat*u1);
  double b = -u2.m2Calc() + pow2(pHat*u2);
  double c = (u1*pHat)*(u2*pHat) - u1*u2;
  double aa=1.0 + a/2.0;
  double bb=1.0 + b/2.0;
  Det=aa*bb - c*c/4;
  double uu1 = u1*dr;
  double uu2 = u2*dr;
  return dr - (bb*uu1*u1 + aa*uu2*u2 + c/2*(uu2*u1 + uu1*u2))/(2*Det);
  }


// This assumes that the transverse distance is defined by the rest frame of a particle.
void RQMDv::qmdMatrix()
{
  for(int i=0; i< NV; i++) {
    Vec4 r1  = part[i]->getR();
    Vec4 pk1 = part[i]->getPkin(optVdot);
    Vec4 p1  = optPotentialArg>=1   ? part[i]->getPcan(optVdot) : pk1;
    Vec4 pm1 = optPotentialArgMD>=1 ? part[i]->getPcan(optVdot) : pk1;
    Vec4 u1 = p1/p1.mCalc();

    double qfac1=1.0;
    if(optPotential<4) qfac1 = part[i]->getTf() > globalTime ? part[i]->qFactor() : 1.0;
    Vec4 fri = optBaryonCurrent ? part[i]->forceR()*u1[0] : 0.0;
    int bi   = part[i]->baryon()/3;
    if(bi==0) {
      qfac1 = facMesonPot;
      bi=1;
    }
    double* gfac1= part[i]->facPotential();

    for(int j=i+selfInt; j< NV; j++) {

      Vec4 r2  = part[j]->getR();
      Vec4 pk2 = part[j]->getPkin();
      Vec4 p2  = optPotentialArg>=1   ? part[j]->getPcan(optVdot) : pk2;
      Vec4 pm2 = optPotentialArgMD>=1 ? part[j]->getPcan(optVdot) : pk2;
      Vec4 u2 = p2/p2.mCalc();
      Vec4 frj = optBaryonCurrent ? part[j]->forceR()*u2[0] : 0.0;
      int bj = part[j]->baryon()/3;
      double qfac2=1.0;
      if(optPotential<4) qfac2 = part[j]->getTf() > globalTime ? part[j]->qFactor() : 1.0;
      if(bj==0) {
	qfac2 = facMesonPot;
	bj=1;
      }
      double* gfac2= part[j]->facPotential();

      Vec4 dR = r1 - r2;
      double drsq1 = dR.m2Calc() - pow2(dR*u2);
      double drsq2 = dR.m2Calc() - pow2(dR*u1);

      double density1,density2;
      if(optPotentialEval==4) {
        Vec4 dq = rGaussDistance(dR,u1,u2);
        density1 = facG/sqrt(Det) * exp(dq*dR*wG);
        density2 = density1;
      } else {
        density1 = facG * exp(drsq1*wG);
        density2 = facG * exp(drsq2*wG);
      }

      double density3 = facG2 * exp(drsq1*wG2);
      double density4 = facG2 * exp(drsq2*wG2);

      // exp(-(x_i-x_j)^2/(4L))
      rhom[i][j] = density1*qfac1*qfac2/overSample;
      rhom[j][i] = density2*qfac1*qfac2/overSample;
      // exp(-(x_i-x_j)^2/(2L))
      double rhomij = density3*qfac1*qfac2/overSample;
      double rhomji = density4*qfac1*qfac2/overSample;
      if(i==j) {
	rhom[i][j] /=2;
	rhom[j][i] /=2;
	rhomij /=2;
	rhomji /=2;
      }

      // vector current
      JB[i] += rhomij*(u2+frj)*bj;
      JB[j] += rhomji*(u1+fri)*bi;

      if(!withMomDep) continue;

      //Vec4 pc1 = part[i]->getPcan(optVdot);
      //Vec4 pc2 = part[j]->getPcan(optVdot);
      Vec4 dp = pm1 - pm2;
      //Vec4 dp = p1 - p2;
      double dpsq = dp.m2Calc();
      double psq1 = dpsq;
      double psq2 = dpsq;

      if(optMDarg3==2) {
        Vec4 pCM=pm1 + pm2;
        double sInv = pCM.m2Calc();
        double pma = pow2(dp * pCM)/sInv;
        psq1 = dpsq - optPV * pma;
        psq2=psq1;
      } else if(optMDarg3==3) {
        psq1 = dpsq - pow2(dp * u2);
        psq2 = dpsq - pow2(dp * u1);
      }

      double vmom4i = potential->Umd(psq1,gfac1[6]*gfac2[6],gfac1[7]*gfac2[7]
	  ,gfac1[8],gfac1[9],gfac2[8],gfac2[9]);
      double vmom4j = potential->Umd(psq2,gfac1[6]*gfac2[6],gfac1[7]*gfac2[7]
	  ,gfac1[8],gfac1[9],gfac2[8],gfac2[9]);

      // momentum-dependent vector potential
      vmom4[i] += vmom4i*rhomij*u2;
      vmom4[j] += vmom4j*rhomji*u1;

    }
  }

}

void RQMDv::computeForce()
{
  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 pk1 = part[i]->getPkin(optVdot);
    Vec4 p1  = optPotentialArg>=1   ? part[i]->getPcan(optVdot) : pk1;
    Vec4 pm1 = optPotentialArgMD>=1 ? part[i]->getPcan(optVdot) : pk1;
    int bar1= part[i]->baryon()/3;
    Vec4 vk1 = optRQMDevolution > 0 ? pk1/(pk1*pHat) : pk1/pk1[0];
    // RQMD2
    if(optPotentialEval==4) vk1 = pk1/pk1.mCalc();

    //Vec4 v1  = p1/p1[0];
    Vec4 v1  = p1/p1.mCalc();

    distance->setRP1(r1,p1);
    distanceP->setRP1(r1,pm1);

    for(int j=i+1; j< NV; j++) {

      Vec4 r2  = part[j]->getR();
      Vec4 pk2 = part[j]->getPkin(optVdot);
      Vec4 p2  = optPotentialArg>=1   ? part[j]->getPcan(optVdot) : pk2;
      Vec4 pm2 = optPotentialArgMD>=1 ? part[j]->getPcan(optVdot) : pk2;
      Vec4 vk2 = optRQMDevolution > 0 ? pk2/(pk2*pHat) : pk2/pk2[0];
      // RQMD2
      if(optPotentialEval==4) vk2 = pk2/pk2.mCalc(); 

      //Vec4 v2  = p2/p2[0];
      Vec4 v2  = p2/p2.mCalc();

      distance->setRP2(r2,p2);
      distanceP->setRP2(r2,pm2);
      distance->distanceR();

      int bar2 = part[j]->baryon()/3;
      Vec4 Ai = facV(i,v2);
      Vec4 Aj = facV(j,v1);
      double fsky1 = -wG*(Ai * vk1)*bar1*bar2*rhom[i][j];
      double fsky2 = -wG*(Aj * vk2)*bar1*bar2*rhom[j][i];

      force[i]  += -fsky1*distance->dr2ijri - fsky2*distance->dr2jiri;
      force[j]  += -fsky1*distance->dr2ijrj - fsky2*distance->dr2jirj;
      forcer[i] +=  fsky1*distance->dr2ijpi + fsky2*distance->dr2jipi;
      forcer[j] +=  fsky1*distance->dr2ijpj + fsky2*distance->dr2jipj;

      // Derivative of p0/m and p/p0 term.
      if(optDerivative) {

        Vec4 A1 = facV(i,vk1);
        Vec4 A2 = facV(j,vk2);
	devV(A1,A2,v1,v2,p1[0],p2[0]);
        forcer[i] += devV1*rhom[j][i]*bar1*bar2;
        forcer[j] += devV2*rhom[i][j]*bar1*bar2;

        // gamma derivative.
        double facsk= -(fsky1+fsky2)/wG;
        distance->devGamma();
        forcer[i] += distance->devgam1*facsk;
        forcer[j] += distance->devgam2*facsk;
      }

      if(!withMomDep) continue;

      distanceP->distanceP();
      double vf1=1.0;
      double vf2=1.0;
      if(optVectorPotential==1) {
        vf1 = vk1 * v2;
        vf2 = vk2 * v1;
      }
      double fmomdi = -wG*potential->devVmd2(distanceP->psq2,1.0,1.0,1.0,1.0)*rhom[i][j]*vf1;
      double fmomdj = -wG*potential->devVmd2(distanceP->psq1,1.0,1.0,1.0,1.0)*rhom[j][i]*vf2;
      double fmomei =     potential->devVme2(distanceP->psq2,1.0,1.0,1.0,1.0)*rhom[i][j]*vf1;
      double fmomej =     potential->devVme2(distanceP->psq1,1.0,1.0,1.0,1.0)*rhom[j][i]*vf2;

      force[i]  += -fmomdi*distance->dr2ijri   - fmomdj*distance->dr2jiri;
      force[j]  += -fmomdi*distance->dr2ijrj   - fmomdj*distance->dr2jirj;

      forcer[i] +=  fmomei*distanceP->dp2ijpi + fmomej*distanceP->dp2jipi
	          + fmomdi*distance->dr2ijpi  + fmomdj*distance->dr2jipi;
      forcer[j] +=  fmomei*distanceP->dp2ijpj + fmomej*distanceP->dp2jipj
	          + fmomdi*distance->dr2ijpj  + fmomdj*distance->dr2jipj;

      if(optDerivative) {
        double facm1 = -fmomdj/(wG*vf2);
        double facm2 = -fmomdi/(wG*vf1);
        devV(vk1,vk2,v1,v2,p1[0],p2[0]);
        forcer[i] += facm1*devV1;
        forcer[j] += facm2*devV2;
        // gamma derivative.
        double facmom = -(fmomdi + fmomdj)/wG;
        forcer[i] += distance->devgam1*facmom;
        forcer[j] += distance->devgam2*facmom;
      }

    } // end loop over j
  } // end loop over i

}

void RQMDv::computeBUUForce()
{
  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : p1;
    if(optPotentialArg>=1) p1 = part[i]->getPcan(optVdot);
    int bar1= part[i]->baryon()/3;
    Vec4 vk1=pk1/pk1[0];
    Vec4 v1  = p1/p1[0];

    //double* gf1=part[i]->facPotential();
    distance->setRP1(r1,p1);
    distanceP->setRP1(r1,p1);
    //potential->set1(p1,pk1,JB[i],fi,fengi,rho[i],rhog[i],rhog2[i],
    //               part[i]->pots()-vmoms[i],rhos[i],rhosg[i],bar1,gf1[1],gf1[2],gf1[3],gf1[4],gf1[5]);

    for(int j=i+selfInt; j< NV; j++) {

      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      Vec4 pk2 = optVdot <= 1 ? part[j]->getPkin(optVdot) : p2;
      if(optPotentialArg>=1) p2 = part[j]->getPcan(optVdot);
      int bar2 = part[j]->baryon()/3;
      Vec4 vk2=pk2/pk2[0];

      /*
      double fj=p2.mCalc()/p2[0];
      double fengj=fj;
      if(optPotentialArg==3) {
        double meff2 = part[j]->getEffectiveMass();
        fengj = meff2/sqrt(meff2*meff2 + pk2.pAbs2());
      }
      */

      //double* gf2=part[j]->facPotential();
      distance->setRP2(r2,p2);
      distanceP->setRP2(r2,p2);
      //potential->set2(p2,pk2,JB[j],fj,fengj,rho[j],rhog[j],rhog2[j],
      //	  part[j]->pots()-vmoms[j],rhos[j],rhosg[j],bar2,gf2[1],gf2[2],gf2[3],gf2[4],gf2[5]);

      distance->distanceR();

      //potential->dVdns(rhom[i][j],rhom[j][i]);
      //double fsky1=potential->fskyi;
      //double fsky2=potential->fskyj;

      Vec4 Ai = facV(i,vk2);
      Vec4 Aj = facV(j,vk1);
      double fsky1 = -wG*(Ai * vk1)*bar1*bar2*rhom[i][j];
      double fsky2 = -wG*(Aj * vk2)*bar1*bar2*rhom[j][i];

      force[i]  += -fsky1*distance->dr2ijri;
      force[j]  += -fsky2*distance->dr2jirj;
      forcer[i] +=  fsky1*distance->dr2ijpi;
      forcer[j] +=  fsky2*distance->dr2jipj;

      // Derivative of p0/m and p/p0 term.
      /*
      if(optDerivative) {
        distance->devGamma();
        forcer[i] += distance->devgam1*potential->facsk;
        forcer[j] += distance->devgam2*potential->facsk;
      }
      */

      if(!withMomDep) continue;

      distanceP->distanceP();
      //potential->dVdmd(distanceP->psq1,distanceP->psq2,gf1[6]*gf2[6],gf1[7]*gf2[7],gf1[8],gf2[8],gf1[9],gf2[9]);
      //double fmomdi=potential->fmomdi;
      //double fmomdj=potential->fmomdj;
      //double fmomei=potential->fmomei;
      //double fmomej=potential->fmomej;

      Vec4 v2  = p2/p2[0];
      double vf1=1.0;
      double vf2=1.0;
      if(optVectorPotential==1) {
        vf1 = vk1 * v2;
        vf2 = vk2 * v1;
      }
      double fmomdi = -wG*potential->devVmd2(distanceP->psq2,1.0,1.0,1.0,1.0)*rhom[i][j]*vf1;
      double fmomdj = -wG*potential->devVmd2(distanceP->psq1,1.0,1.0,1.0,1.0)*rhom[j][i]*vf2;
      double fmomei =     potential->devVme2(distanceP->psq2,1.0,1.0,1.0,1.0)*rhom[i][j]*vf1;
      double fmomej =     potential->devVme2(distanceP->psq1,1.0,1.0,1.0,1.0)*rhom[j][i]*vf2;

      force[i]  += -fmomdi*distance->dr2ijri;
      force[j]  += -fmomdj*distance->dr2jirj;

      forcer[i] +=  fmomei*distanceP->dp2ijpi + fmomdi*distance->dr2ijpi;
      forcer[j] +=  fmomej*distanceP->dp2jipj + fmomdj*distance->dr2jipj;

      /*
      if(optDerivative) {
        //distanceP->devGamma();
        forcer[i] += distance->devgam1*potential->facmom;
        forcer[j] += distance->devgam2*potential->facmom;
      }
      */

    } // end loop over j
  } // end loop over i

}

// This should be modified for isPotTable = true.
//*********************************************************************
//...Compute time-derivatives of the vector potential.
void RQMDv::computeVdot()
{
  bool opt=true;
  //opt=false;

  for(int i=0;i<NV; i++) Vdot[i]=0.0;

  for(int i=0;i<NV; i++) {

  double vvi=0.0, dvi=0.0;
  for(int k=0;k<nV;k++) {
    vvi += pFac[k]*tFac[k]*rhog[k][i];  // V/rho_B
    if(abs(rho[k]) > 1e-7)
    dvi += (bFac[k]-1.0)*pFac[k]*tFac[k]*rhog[k][i]/(rho[i]*rho[i]);  // del(V/rho)/rho
  }

    Vec4 Bi = dvi*JB[i];
    Vec4 r1 = part[i]->getR();
    Vec4 p1 = part[i]->getP();
    Vec4 vk1 = p1/p1[0];
    if(optPotentialArg==1) p1 = part[i]->getPcan(optVdot);
    double emi=p1[0];
    if(optTwoBodyDistance==3)  emi = p1.mCalc();
    Vec4 vi = p1 / emi;
    Vec4 b1 = p1 / p1[0];

    distance->setRP1(r1,p1);
    distanceP->setRP1(r1,p1);
    //double* gf1=part[i]->facPotential();

    for(int j=i+1;j<NV; j++) {

      //double vvj = t1 + t3*rhog[j];
      //double dvj=0.0;
      //if(abs(rho[j]) > 1e-7) dvj = (gam-1.0)*t3*pow(rho[j],gam-3.0);

      double vvj=0.0, dvj=0.0;
      for(int k=0;k<nV;k++) {
        vvi += pFac[k]*tFac[k]*rhog[k][j];  // V/rho_B
        if(abs(rho[j]) > 1e-7)
        dvi += (bFac[k]-1.0)*pFac[k]*tFac[k]*rhog[k][j]/(rho[j]*rho[j]);  // del(V/rho)/rho
      }

      Vec4 Bj = dvj*JB[j];
      Vec4 r2 = part[j]->getR();
      Vec4 p2 = part[j]->getP();
      Vec4 vk2 = p2/p2[0];
      if(optPotentialArg==1) p2 = part[j]->getPcan(optVdot);
      double emj=p2[0];
      if(optTwoBodyDistance==3)  emj = p2.mCalc();
      Vec4 vj = p2 / emj;
      Vec4 b2 = p2 / p2[0];

      distance->setRP2(r2,p2);
      distanceP->setRP2(r2,p2);
      //double* gf2=part[j]->facPotential();
   
      Vec4 Aj = (vj * JB[i]) * Bi;
      Vec4 Ai = (vi * JB[j]) * Bj;

      // Compute derivatives
      //distance->distanceR();

      // Non-relativistic.
      Vec4 rk = r1 - r2;
      Vec4 dgami=0.0;
      Vec4 dgamj=0.0;
      Vec4 dr2ri   = 2 * rk;
      Vec4 dr2rj   = -dr2ri;
      Vec4 drji2ri = dr2ri;
      Vec4 drji2rj = dr2rj;
      Vec4 dr2pi = 0.0;
      Vec4 dr2pj = 0.0;
      Vec4 drji2pi=0.0;
      Vec4 drji2pj=0.0;

      // two-body c.m. frame.
      if(optTwoBodyDistance==2) {

      Vec4 pcm = p1 + p2;
      Vec4 bet = pcm/pcm[0];
      Vec4 bbi = bet - optP0dev*b1;
      Vec4 bbj = bet - optP0dev*b2;
      double s = pcm.m2Calc();
      double rbij = dot3(rk,pcm)/s;
      dr2ri =  2*(rk + rbij*pcm);
      dr2rj = -dr2ri;
      dr2pi = 2*(rk + pcm[0]*rbij*bbi)*rbij;
      dr2pj = 2*(rk + pcm[0]*rbij*bbj)*rbij;
      drji2ri =  dr2ri;
      drji2rj = -drji2ri;
      drji2pi = dr2pi;
      drji2pj = dr2pj;

      // derivatives from gamma_{ij}.
      dgami = optP0dev*(1.0/pcm[0]-pcm[0]/s)*b1+pcm/s;
      dgamj = optP0dev*(1.0/pcm[0]-pcm[0]/s)*b2+pcm/s;

      // rest frame of particle i or j.
      } else if(optTwoBodyDistance == 3) {

        double rbi=dot3(rk,vi);
        double rbj=dot3(rk,vj);
        dr2ri =  2*(rk+rbj*vj);    // dR~^2_ij/dR_i
        dr2rj =  -dr2ri;           // dR~^2_ij/dR_j
        dr2pi =  0.0;              // dR~^2_ij/dP_i
        dr2pj =  2*rk*rbj/emj;     // dR~^2_ij/dP_j

        drji2rj = 2*(rk+rbi*vi);
        drji2rj =  -drji2ri;
        drji2pi =  2*rk*rbi/emi;
        drji2pj =  0.0;
      }
      //Vec4  dr2ri = distance->dr2ri; 
      //Vec4  dr2rj = distance->dr2rj;
      //Vec4  dr2pi = distance->dr2pj;
      //Vec4  dr2pj = distance->dr2pj;

      //Vec4  drji2rj = distance->drji2rj;
      //Vec4  drji2rj = distance->drji2rj;
      //Vec4  drji2pi = distance->drji2pi;
      //Vec4  drji2pj = distance->drji2pj;

      double xdoti=dot3(vk1+forcer[i],dr2ri)   + dot3(vk2+forcer[j],dr2rj);
      double xdotj=dot3(vk2+forcer[j],drji2rj) + dot3(vk1+forcer[i],drji2ri);

      double pdoti=dot3(force[i],dr2pi-dgami/wG)
	         + dot3(force[j],dr2pj-dgamj/wG);
      double pdotj=dot3(force[j],drji2pj-dgamj/wG)
	         + dot3(force[i],drji2pi-dgami/wG);

      double doti=-wG*(xdoti+pdoti)*rhom[i][j];
      double dotj=-wG*(xdotj+pdotj)*rhom[j][i];
      Vdot[i] += doti*(Aj + vvi*vj);
      Vdot[j] += dotj*(Ai + vvj*vi);

      // Momentum dependent potential.
      double fmomdi=0.0, fmomdj=0.0;
      if(withMomDep) {

      //distance->distanceP();
      distanceP->distanceP();

      //potential->dVdmd(distanceP->psq1,distanceP->psq2,gf1[6]*gf2[6],gf1[7]*gf2[7],gf1[8],gf2[8],gf1[9],gf2[9]);
      //fmomdi=potential->fmomdi;
      //fmomdj=potential->fmomdj;
      double vf1=1.0;
      double vf2=1.0;
      if(optVectorPotential==1) {
        vf1 = vk1 * vj;
        vf2 = vk2 * vi;
      }
      double fmomdv1 = potential->devVmd2(distanceP->psq2,1.0,1.0,1.0,1.0)*rhom[i][j];
      double fmomdv2 = potential->devVmd2(distanceP->psq1,1.0,1.0,1.0,1.0)*rhom[j][i];

      fmomdi = -wG*fmomdv1*vf1;
      fmomdj = -wG*fmomdv2*vf2;

      Vdot[i] += doti*fmomdi*vj;
      Vdot[j] += dotj*fmomdj*vi;

      pdoti=dot3(force[i],distanceP->dp2ijpi) + dot3(force[j],distanceP->dp2ijpj);
      pdotj=dot3(force[j],distanceP->dp2jipj) + dot3(force[i],distanceP->dp2jipi);

      //Vdot[i] += pdoti*devVme(psq2)*rhom[i][j]*vj;
      //Vdot[j] += pdotj*devVme(psq1)*rhom[j][i]*vi;


      Vdot[i] += pdoti*fmomdv1*vj;
      Vdot[j] += pdotj*fmomdv2*vi;

      }

      if(opt) continue;

      //Compute derivatives of pre-factors v_j^\mu in the vector potential.
      double fai = - dot3(force[j],Bi/emj) * rhom[i][j];
      double faj = - dot3(force[i],Bj/emi) * rhom[j][i];
      double fai2 = dot3(force[j],vj);
      double faj2 = dot3(force[i],vi); 
      double fai3 = dot3(vj,Bi/p2[0]);
      double faj3 = dot3(vi,Bj/p1[0]);

      double  fvi2=0.0;
      double  fvj2=0.0;
      if(optTwoBodyDistance==3) {
        fai3=Bi[0]/p2[0];
        faj3=Bj[0]/p1[0];
      } else {
        fvi2=-optP0dev*fai2*rhom[i][j]*vvi/p2[0];
        fvj2=-optP0dev*faj2*rhom[j][i]*vvj/p1[0];
      }

      fai += optP0dev*fai2*fai3*rhom[i][j];
      faj += optP0dev*faj2*faj3*rhom[j][i];

      double fvi = vvi*rhom[i][j]/emj;
      double fvj = vvj*rhom[j][i]/emi;

      Vdot[i] += fai*JB[i] + fvi*force[j] + fvi2*vj;
      Vdot[j] += faj*JB[j] + fvj*force[i] + fvj2*vi;

      // Momentum dependent potential.
      if(withMomDep) {
        Vdot[i] += fmomdi*rhom[i][j]*force[j]/emj;
        Vdot[j] += fmomdj*rhom[j][i]*force[i]/emi;
        if(optTwoBodyDistance != 3) {
          Vdot[i] += -optP0dev*fai2*rhom[i][j]*fmomdi/p2[0]*vj;
          Vdot[i] += -optP0dev*faj2*rhom[j][i]*fmomdj/p1[0]*vi;
	}
      }


    } // end j loop
  }   // end i loop

  //return;

  Vec4 vdott=0.0;
  for(int i=0;i<NV;i++) {
    vdott += Vdot[i];
  }

  Vec4 dv = vdott/NV;
  for(int i=0;i<NV;i++) {
    Vdot[i] -= dv;
  }

}

// Pre-factor from the baryon current.
Vec4 RQMDv::facV(int i, Vec4& vkin)
{
  //if(rho[i] < 1e-15) return Vec4(0.0);
  if(rho[i] < 1e-8) return Vec4(0.0);
  Vec4 bj=0.0;

  // 4-components of the vector potential is fully included.
  if(optVectorPotential==1) {
    bj = vkin;

  // Only time-component V^0 term is included.
  } else if(optVectorPotential==2) {
    bj[0]=1.0;

  // Only time-component V^0 term is included with the form of V(rho_B).
  } else if (optVectorPotential==3) {
    if(isPotTable==0) {
      for(int j=0;j<nV;j++) 
        bj[0] += pFac[j]*bFac[j]*tFac[j]*rhog[j][i]/rho[i] * JB[i]*vkin;
    } else {
      double rh=max(rhoMin,rho[i]);
      bj[0]= potential->getdVdn(rho[i])/rh*JB[i]*vkin;
    }
    return bj;

  // fully non-relativistic
  } else {
    if(isPotTable==0) {
    for(int j=0;j<nV;j++)
    if(JB[i][0]>1e-8) {
      bj[0] += pFac[j]*tFac[j]*bFac[j]*pow(max(0.0,JB[i][0]),bFac[j]-1);
    }
    } else {
      bj[0]=potential->getdVdn(JB[i][0]);
    }
    return bj;
  }

  Vec4 ub = JB[i] / rho[i];
  double vv=0.0, dv=0.0;

  if(isPotTable==0) {

  for(int j=0;j<nV;j++) {
    vv += pFac[j]*tFac[j]*rhog[j][i];  // V/rho_B
    dv += (bFac[j]-1.0)*pFac[j]*tFac[j]*rhog[j][i];  // rho*del(V/rho)
    //if(!isfinite(vv) || !isfinite(dv)) {
  }

  } else {

    double rh=max(rhoMin,rho[i]);

    // use original function for small density.
    if(isPotTable==3 || rh > 0.01) {
      //vv = potential->getV(rh)/rh;
      //dv = potential->getdVdn(rh) - vv;
      //dv = potential->getU(rh)/rh - 2*vv;
      vv = potential->getVpotR(rh);
      dv = potential->getdVdnV(rh);
     
      // use table
    } else {
      if(isPotTable==1) {
        vv = potential->vrSKM(rh,0);
        dv = potential->dvvSKM(rh,0);
      } else {
        vv = potential->vrVDF(rh);
        dv = potential->dvvVDF(rh);
      }
    }

  }

  return (ub*bj)*dv*ub + vv*bj;
}

void RQMDv::devV(const Vec4& Ai, const Vec4& Aj, const Vec4& v1, const Vec4& v2, const double p01, const double p02)
{
  // p_i/p^0_i * p^0_i / m_i
  if(optTwoBodyDistance == 3) {
    devV1 = (optP0dev*Aj.e()*v1 - Aj)/p01;
    devV2 = (optP0dev*Ai.e()*v2 - Ai)/p02;
  // p_i/p^0_i
  } else {
    devV1 = (optP0dev*dot3(Aj,v1)*v1 - Aj)/p01;
    devV2 = (optP0dev*dot3(Ai,v2)*v2 - Ai)/p02;
  }
}

void RQMDv::setFreeLagrangeMultiplier()
{
  for(int i=0; i< NV; i++) {
    Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin(optVdot) : part[i]->getP();
    if(optCollisionOrdering <= 101) { 
      if(optRQMDevolution == 0)lambda[i]=1.0/pk1[0];
      else lambda[i]=1.0/(pk1*pHat);
    } else if(optCollisionOrdering >= 102) {
      double m=part[i]->getEffectiveMass();
      if(m==0.0) m=1.0;
      if(optCollisionOrdering==110) m=1.0/lambdaLag;
      lambda[i]=1.0/m;
    }
  }

}

void RQMDv::computeForceMatrix()
{
  vector<std::vector<Vec4> > matp(NV,vector<Vec4>(NV,0.0));
  vector<std::vector<Vec4> > matr(NV,vector<Vec4>(NV,0.0));

  for(int i=0; i< NV; i++) {
    Vec4 r1  = part[i]->getR();
    Vec4 pk1 = part[i]->getPkin(optVdot);
    Vec4 p1  = optPotentialArg>=1   ? part[i]->getPcan(optVdot): pk1;
    Vec4 pm1 = optPotentialArgMD>=1 ? part[i]->getPcan(optVdot): pk1;
    Vec4 v1  = p1/p1[0];
    distance->setRP1(r1,p1);
    distanceP->setRP1(r1,pm1);
    int bar1= part[i]->baryon()/3;

    matp[i][i] += pk1;

    for(int j=i+1; j< NV; j++) {

      Vec4 r2 = part[j]->getR();
      Vec4 pk2 = part[j]->getPkin(optVdot);
      Vec4 p2  = optPotentialArg>=1   ? part[j]->getPcan(optVdot) : pk2;
      Vec4 pm2 = optPotentialArgMD>=1 ? part[j]->getPcan(optVdot) : pk2;
      Vec4 v2  = p2/p2[0];

  //Vec4 dR = r1 - r2;
  //double m1=part[i]->getMass();
  //double m2=part[j]->getMass();
  //double drsq1 = dR.m2Calc() - pow2(dR*p2/m2);
  //double drsq2 = dR.m2Calc() - pow2(dR*p1/m1);
  //if(drsq1>0.0 || drsq2>0.0){
  //  cout << "RQMDv::computeForceMatrix drsq <0? drdq1= "<< drsq1
  //    << " drsq2= "<< drsq2<<endl;
  //  exit(1);
  //}

      int bar2 = part[j]->baryon()/3;
      distance->setRP2(r2,p2);
      distanceP->setRP2(r2,pm2);
      distance->distanceR();

      Vec4 Ai = facV(i,v2);
      Vec4 Aj = facV(j,v1);
      double fsky1 = -wG*(Ai * pk1)*bar1*bar2*rhom[i][j];
      double fsky2 = -wG*(Aj * pk2)*bar1*bar2*rhom[j][i];

      //force[i]  += -fsky1*distance->dr2ijri - fsky2*distance->dr2jiri;
      //force[j]  += -fsky1*distance->dr2ijrj - fsky2*distance->dr2jirj;
      //forcer[i] +=  fsky1*distance->dr2ijpi + fsky2*distance->dr2jipi;
      //forcer[j] +=  fsky1*distance->dr2ijpj + fsky2*distance->dr2jipj;


      matp[i][j] =  fsky1*distance->dr2ijpj;
      matp[j][i] =  fsky2*distance->dr2jipi;
      matp[i][i] += fsky1*distance->dr2ijpi;
      matp[j][j] += fsky2*distance->dr2jipj;

      matr[i][j] =  -fsky1*distance->dr2ijrj;
      matr[j][i] =  -fsky2*distance->dr2jiri;
      matr[i][i] += -fsky1*distance->dr2ijri;
      matr[j][j] += -fsky2*distance->dr2jirj;


      // Derivative of p0/m and p/p0 term.
      if(optDerivative) {

        Vec4 A1 = facV(i,pk1);
        Vec4 A2 = facV(j,pk2);
	devV(A1,A2,v1,v2,p1[0],p2[0]);
        //forcer[i] += devV1*rhom[j][i]*bar1*bar2;
        //forcer[j] += devV2*rhom[i][j]*bar1*bar2;

        matp[j][i] += devV1*rhom[j][i]*bar1*bar2; 
        matp[i][j] += devV2*rhom[i][j]*bar1*bar2;

        // gamma derivative.
        double facsk= -(fsky1+fsky2)/wG;
        distance->devGamma();
        //forcer[i] += distance->devgam1*facsk;
        //forcer[j] += distance->devgam2*facsk;
        matp[j][i] += distance->devgam1*facsk;
        matp[i][j] += distance->devgam2*facsk;
      }

      if(!withMomDep) continue;

      distanceP->distanceP();

      double vf1=1.0;
      double vf2=1.0;
      if(optVectorPotential==1) {
        vf1 = pk1 * v2;
        vf2 = pk2 * v1;
      }
      double fmomdi = -wG*potential->devVmd2(distanceP->psq2,1.0,1.0,1.0,1.0)*rhom[i][j]*vf1;
      double fmomdj = -wG*potential->devVmd2(distanceP->psq1,1.0,1.0,1.0,1.0)*rhom[j][i]*vf2;
      double fmomei =     potential->devVme2(distanceP->psq2,1.0,1.0,1.0,1.0)*rhom[i][j]*vf1;
      double fmomej =     potential->devVme2(distanceP->psq1,1.0,1.0,1.0,1.0)*rhom[j][i]*vf2;

      //force[i]  += -fmomdi*distance->dr2ijri   - fmomdj*distance->dr2jiri;
      //force[j]  += -fmomdi*distance->dr2ijrj   - fmomdj*distance->dr2jirj;
      //forcer[i] +=  fmomei*distanceP->dp2ijpi + fmomej*distanceP->dp2jipi
      //          + fmomdi*distance->dr2ijpi  + fmomdj*distance->dr2jipi;
      //forcer[j] +=  fmomei*distanceP->dp2ijpj + fmomej*distanceP->dp2jipj
      //          + fmomdi*distance->dr2ijpj  + fmomdj*distance->dr2jipj;

      matp[i][j] +=  fmomdi*distance->dr2ijpj + fmomei*distanceP->dp2ijpj;
      matp[j][i] +=  fmomdj*distance->dr2jipi + fmomej*distanceP->dp2jipi;
      matp[i][i] +=  fmomdi*distance->dr2ijpi + fmomei*distanceP->dp2ijpi;
      matp[j][j] +=  fmomdj*distance->dr2jipj + fmomej*distanceP->dp2jipj;

      matr[i][j] += -fmomdi*distance->dr2ijrj;
      matr[j][i] += -fmomdj*distance->dr2jiri;
      matr[i][i] += -fmomdi*distance->dr2ijri;
      matr[j][j] += -fmomdj*distance->dr2jirj;

      if(optDerivative) {
        double facm1 = -fmomdj/(wG*vf2);
        double facm2 = -fmomdi/(wG*vf1);
        devV(pk1,pk2,v1,v2,p1[0],p2[0]);
        //forcer[i] += facm1*devV1;
        //forcer[j] += facm2*devV2;
        matp[j][i] += facm1*devV1;
        matp[i][j] += facm2*devV2;
        // gamma derivative.
        double facmom = -(fmomdi + fmomdj)/wG;
        //forcer[i] += distance->devgam1*facmom;
        //forcer[j] += distance->devgam2*facmom;
        matp[j][i] += distance->devgam1*facmom;
        matp[i][j] += distance->devgam2*facmom;
      }

    } // end loop over j
  } // end loop over i

  setFreeLagrangeMultiplier();
  vector<double> lambdaf = lambda;
  vector<double> lambdaw(NV-1);

  // take only the diagonal part.
  if(optRQMDevolution==2) {
    for(int i=0; i< NV; i++) lambda[i]=1.0/(pHat*matp[i][i]);

  // full solution.
  } else if(optRQMDevolution==3 || optRQMDevolution==4) {
    vector<std::vector<double> > mat(NV,vector<double>(NV+1,1.0));
    for(int i=0; i< NV; i++) 
    for(int j=0; j< NV; j++) {
      mat[i][j]=matp[j][i]*pHat;
    }
    findLagrangeMultiplier(mat,NV);
    for(int i=0;i<NV;i++) lambda[i]=mat[i][NV];

    // do not assume H_i's are first class [H_i,H_j]=0.
    if(optRQMDevolution==4) {
      vector<std::vector<double> > hh(NV,vector<double>(NV,0.0));
      double hhmax=-1.0;
      for(int i=0; i< NV; i++)
      for(int j=i+1; j< NV; j++) {
      for(int k=0;k<NV;k++) {
        hh[i][j] +=matp[i][k]*matr[j][k] - matr[i][k]*matp[j][k];
      }
      hh[j][i]=-hh[i][j];
      hhmax=max(hhmax,abs(hh[i][j]));
      }

      // Joseph Samuel, PRD26 (1982)3475.
      for(int i=0;i<NV;i++) {
	lambdaw[i]=0.0;
        for(int j=0;j<NV;j++)
  	  lambdaw[i] += 0.25*lambda[i]*lambda[j]*mat[i][j];
      }

      /*
      for(int i=0; i< NV; i++) {
	mat[i][NV]=0.0;
        for(int j=0; j< NV; j++)
  	  mat[i][NV] += hh[j][i]*lambda[j];
      }
      for(int i=0; i< NV; i++)
      for(int j=0; j< NV; j++) {
	mat[i][j] = matp[i][j]*pHat;
	if(j<NV-1) mat[i][j] -= matp[i][NV-1]*pHat;
      }
      findLagrangeMultiplier(mat,NV);
      for(int i=0;i<NV-1;i++) lambdaw[i]=mat[i][NV];
      */
    }

  }

    double pm=pHat.mCalc();
  for(int i=0; i< NV; i++) {
    Vec4 pk1 = part[i]->getPkin(optVdot);
    forcer[i] =  - lambdaf[i]*pk1;
    Vec4 q1 = part[i]->getR();
    //forcer[i] =  - lambda[i]*pk1;
    if(optRQMDevolution==4) {
      force[i] -= (lambdaw[i]-lambdaw[NV-1])*pHat;
    }

    //cout << " lam diff1= "<< lambda[i]- lambdaf[i]
    //   << " diff2= "<< lambda[i]-1.0/(pHat*matp[i][i])
    //   <<endl;

    for(int j=0; j< NV; j++) {
      force[i]  += lambda[j]*matr[j][i];
      forcer[i] += lambda[j]*matp[j][i];
    }
    if(optRQMDevolution==4) {
      for(int j=0; j< NV-1; j++) {
        Vec4 q12 = q1 - part[j]->getR();
        forcer[i]  += lambdaw[j]*(q12 - (pHat*q12)*pHat)/pm;
      }
    }

    //if(forcer[i][0]<0.0) {forcer[i]=0.0;} //force[i]=0.0;}
  }

}

void RQMDv::computeForceMatrix2()
{
  vector<std::vector<Vec4> > matp(NV,vector<Vec4>(NV,0.0));
  vector<std::vector<Vec4> > matr(NV,vector<Vec4>(NV,0.0));

  for(int i=0; i< NV; i++) {
    Vec4 r1 = part[i]->getR();
    Vec4 pk1 = part[i]->getPkin(optVdot);
    Vec4 p1  = optPotentialArg>=1   ? part[i]->getPcan(optVdot): pk1;
    Vec4 pm1 = optPotentialArgMD>=1 ? part[i]->getPcan(optVdot): pk1;
    int bar1= part[i]->baryon()/3;
    double m1=p1.mCalc();
    Vec4 u1  = p1/m1;

    matp[i][i] += pk1;

    for(int j=i+1; j< NV; j++) {

      Vec4 r2 = part[j]->getR();
      Vec4 pk2 = part[j]->getPkin(optVdot);
      Vec4 p2  = optPotentialArg>=1   ? part[j]->getPcan(optVdot) : pk2;
      Vec4 pm2 = optPotentialArgMD>=1 ? part[j]->getPcan(optVdot) : pk2;
      double m2=p2.mCalc();
      Vec4 u2  = p2/m2;

      int bar2 = part[j]->baryon()/3;
      Vec4 Ai = facV(i,u2);
      Vec4 Aj = facV(j,u1);
      double fsky1 = -wG*(Ai * pk1)*bar1*bar2*rhom[i][j];
      double fsky2 = -wG*(Aj * pk2)*bar1*bar2*rhom[j][i];

      Vec4 dr = r1 - r2;
      double rbi = dr*u1;
      double rbj = dr*u2;
      Vec4 dr2ijri =  2*(dr - rbj*u2);  // R^2_{ij}/dr_i
      Vec4 dr2jiri =  2*(dr - rbi*u1);  // R^2_{ji}/dr_i
      Vec4 dr2ijrj =  -dr2ijri;
      Vec4 dr2jirj =  -dr2jiri;

      Vec4 dr2jipi =  2*dr*rbi/m1;     // R^2_{ji}/dp_i
      Vec4 dr2ijpj =  2*dr*rbj/m2;     // R^2_{ij}/dp_j
      Vec4 dr2ijpi = 0.0;
      Vec4 dr2jipj = 0.0;

      matp[i][j] =  fsky1*dr2ijpj;
      matp[j][i] =  fsky2*dr2jipi;
      matp[i][i] += fsky1*dr2ijpi;
      matp[j][j] += fsky2*dr2jipj;

      matr[i][j] =  -fsky1*dr2ijrj;
      matr[j][i] =  -fsky2*dr2jiri;
      matr[i][i] += -fsky1*dr2ijri;
      matr[j][j] += -fsky2*dr2jirj;

      // Derivative of p0/m and p/p0 term.
      if(optDerivative) {
        Vec4 A1 = facV(i,pk1);
        Vec4 A2 = facV(j,pk2);
	//devV(A1,A2,u1,u2,p1[0],p2[0]);
        devV1 = (optP0dev*A2.e()*p1/p1[0] - A2)/m1;
        devV2 = (optP0dev*A1.e()*p2/p2[0] - A1)/m2;
        matp[j][i] += devV1*rhom[j][i]*bar1*bar2; 
        matp[i][j] += devV2*rhom[i][j]*bar1*bar2;
      }

      if(!withMomDep) continue;

      //distanceP->distanceP();
      //Vec4 pc1 = part[i]->getPcan(optVdot);
      //Vec4 pc2 = part[j]->getPcan(optVdot);
      Vec4 dp = pm1 - pm2;
      double psq = dp.m2Calc();
      double psq1 = psq;
      double psq2 = psq;
      Vec4 dp2ijpi =  2*dp;
      Vec4 dp2ijpj = -2*dp;
      Vec4 dp2jipi = dp2ijpi;
      Vec4 dp2jipj = dp2ijpj;

      if(optMDarg3==2) {
        Vec4 pCM = pm1 + pm2;
        double sInv = pCM.m2Calc();
        double pma = pow2(dp * pCM)/sInv;
        psq1 = psq - optPV * pma;
        psq2 = psq1;
        Vec4 v1 = pm1/pm1[0];
        Vec4 v2 = pm2/pm2[0];
        Vec4 bbi = pCM/pCM[0] - optP0dev*v1;
        Vec4 bbj = pCM/pCM[0] - optP0dev*v2;
        dp2ijpi = 2*( dp - optP0dev*dp[0]*v1 + optPV * pCM[0]/sInv*pma*bbi);
        dp2ijpj = 2*(-dp + optP0dev*dp[0]*v2 + optPV * pCM[0]/sInv*pma*bbj);
        dp2jipi = dp2ijpi;
        dp2jipj = dp2ijpj;

      } else if(optMDarg3==3) {

      // distance squared in the rest frame of particle 2
      double dot4j = dp * p2 / m2;
      psq2 = psq - optPV*dot4j*dot4j;

      // distance squared in the rest frame of particle 1
      double dot4i = (dp * p1)/m1;
      psq1 = psq - optPV*dot4i*dot4i;

      // derivatives
      Vec4 bi = p1/p1.e();
      Vec4 bb = optP0dev * p2.e()*bi - p2;
      dp2ijpi = 2*(dp - optP0dev*dp[0]*bi + bb*dot4j/m2);
      dp2jipi = 2*(dp - optP0dev*dp[0]*bi - bb*dot4i/m1);

      Vec4 bj = p2/p2.e();
      Vec4 bb2 = optP0dev * p1.e()*bj - p1;
      dp2ijpj = 2*(-dp + optP0dev*dp[0]*bj + bb2*dot4j/m2);
      dp2jipj = 2*(-dp + optP0dev*dp[0]*bj - bb2*dot4i/m1);
     }

      double vf1=1.0;
      double vf2=1.0;
      if(optVectorPotential==1) {
        vf1 = pk1 * u2;
        vf2 = pk2 * u1;
      }
      double fmomdi = -wG*potential->devVmd2(psq2,1.0,1.0,1.0,1.0)*rhom[i][j]*vf1;
      double fmomdj = -wG*potential->devVmd2(psq1,1.0,1.0,1.0,1.0)*rhom[j][i]*vf2;
      double fmomei =     potential->devVme2(psq2,1.0,1.0,1.0,1.0)*rhom[i][j]*vf1;
      double fmomej =     potential->devVme2(psq1,1.0,1.0,1.0,1.0)*rhom[j][i]*vf2;

      matp[i][j] +=  fmomdi*dr2ijpj + fmomei*dp2ijpj;
      matp[j][i] +=  fmomdj*dr2jipi + fmomej*dp2jipi;
      matp[i][i] +=  fmomdi*dr2ijpi + fmomei*dp2ijpi;
      matp[j][j] +=  fmomdj*dr2jipj + fmomej*dp2jipj;

      matr[i][j] += -fmomdi*dr2ijrj;
      matr[j][i] += -fmomdj*dr2jiri;
      matr[i][i] += -fmomdi*dr2ijri;
      matr[j][j] += -fmomdj*dr2jirj;

      if(optDerivative) {
        double facm1 = -fmomdj/(wG*vf2);
        double facm2 = -fmomdi/(wG*vf1);
        devV1 = (p1/p1[0] - pk2)/m1;
        devV2 = (p2/p2[0] - pk1)/m2;
        matp[j][i] += facm1*devV1;
        matp[i][j] += facm2*devV2;
      }

    } // end loop over j
  } // end loop over i

  setFreeLagrangeMultiplier();
  vector<double> lambdaf = lambda;
  vector<double> lambdaw(NV-1);

  // take only the diagonal part.
  if(optRQMDevolution==2) {
    for(int i=0; i< NV; i++) lambda[i]=1.0/(pHat*matp[i][i]);

  // full solution.
  } else if(optRQMDevolution==3 || optRQMDevolution==4) {
    vector<std::vector<double> > mat(NV,vector<double>(NV+1,1.0));
    for(int i=0; i< NV; i++) 
    for(int j=0; j< NV; j++) {
      mat[i][j]=matp[j][i]*pHat;
    }
    findLagrangeMultiplier(mat,NV);
    for(int i=0;i<NV;i++) lambda[i]=mat[i][NV];

    // do not assume H_i's are first class [H_i,H_j]=0.
    if(optRQMDevolution==4) {
      vector<std::vector<double> > hh(NV,vector<double>(NV,0.0));
      double hhmax=-1.0;
      for(int i=0; i< NV; i++)
      for(int j=i+1; j< NV; j++) {
      for(int k=0;k<NV;k++) {
        hh[i][j] +=matp[i][k]*matr[j][k] - matr[i][k]*matp[j][k];
      }
      hh[j][i]=-hh[i][j];
      hhmax=max(hhmax,abs(hh[i][j]));
      }

      // Joseph Samuel, PRD26 (1982)3475.
      for(int i=0;i<NV-1;i++) {
	lambdaw[i]=0.0;
        for(int j=0;j<NV-1;j++)
  	  lambdaw[i] += lambda[i]*lambda[j]*hh[i][j];
      }

      /*
      for(int i=0; i< NV; i++) {
	mat[i][NV]=0.0;
        for(int j=0; j< NV; j++)
  	  mat[i][NV] += hh[j][i]*lambda[j];
      }
      for(int i=0; i< NV; i++)
      for(int j=0; j< NV; j++) {
	mat[i][j] = matp[i][j]*pHat;
	if(j<NV-1) mat[i][j] -= matp[i][NV-1]*pHat;
      }
      findLagrangeMultiplier(mat,NV);
      for(int i=0;i<NV-1;i++) lambdaw[i]=mat[i][NV];
      */
    }

  }

    double pm=pHat.mCalc();
  for(int i=0; i< NV; i++) {
    //Vec4 pk1 = optVdot <= 1 ? part[i]->getPkin() : part[i]->getP();
    //forcer[i] =  - lambdaf[i]*pk1;
    Vec4 q1 = part[i]->getR();
    if(optRQMDevolution==4) {
      force[i] -= (lambdaw[i]-lambdaw[NV-2])*pHat;
    }

    for(int j=0; j< NV; j++) {
      force[i]  += lambda[j]*matr[j][i];
      forcer[i] += lambda[j]*matp[j][i];
    }

    //Vec4 qdot= lambdaf[i]*pk1 + forcer[i];
    Vec4 qdot= forcer[i];
    if(qdot.m2Calc()<0.0) {
      double q2=qdot.pAbs();
      double eps=1e-12;
      qdot[1] *= qdot[0]*(1.0-eps)/q2;
      qdot[2] *= qdot[0]*(1.0-eps)/q2;
      qdot[3] *= qdot[0]*(1.0-eps)/q2;
      forcer[i][1] = qdot[1];
      forcer[i][2] = qdot[2];
      forcer[i][3] = qdot[3];
      //cout << " force^2= "<< forcer[i].m2Calc()<<endl;
    }

    double lam0=qdot[0];

    //cout << i << setprecision(16) << " qdot^2~= "<< qdot.mCalc() << " v= "<< qdot.pAbs()/qdot[0] <<endl;
    //cin.get();

    //if(forcer[i].pAbs()/lam0 >1.0) {
    if(qdot.m2Calc() <= 0.0) {
      double fmax=0.0;
      int imax=0;
      for(int j=0;j<NV;j++) {
	double f1=lambda[j]*matp[j][i].pAbs()/lam0;
	if(f1> fmax) {
	   fmax = f1; imax=j;
	}
	//cout << j << " matp= "<< matp[j][i].mCalc() << " pabs= "<< matp[j][i].pAbs() <<endl;
      }
      cout << i << " force= "<< qdot.pAbs()/lam0
	<< " qdot^2= "<< qdot.mCalc()
	<< " force^2= "<< forcer[i].mCalc()
	<< " force0= "<< forcer[i][0]
	<< " force= "<< forcer[i]
       	<< " fmax= "<< fmax << " imax= "<< imax <<endl;
    }
    //double vx=forcer[i][1]+pk[1]*lam;
    //double vy=forcer[i][2]+pk[2]*lam;
    //double vz=forcer[i][3]+pk[3]*lam;
    //double v0=forcer[i][0]+pk[0]*lam;
    //double vv=sqrt(vx*vx+vy*vy+vz*vz)/v0;

    if(optRQMDevolution==4) {
      for(int j=0; j< NV-1; j++) {
        Vec4 q12 = q1 - part[j]->getR();
        forcer[i]  += lambdaw[j]*(q12 - (pHat*q12)*pHat)/pm;
      }
    }

    //if(forcer[i][0]<0.0) {forcer[i]=0.0;} //force[i]=0.0;}
  }

}

void RQMDv::findLagrangeMultiplier(vector<vector<double> >& mat,int nv)
{
  //vector<std::vector<double> > mat(NV,vector<double>(NV+1,1.0));
  //mat.resize(NV);
  //for(int i=0;i<NV;i++) mat[i].resize(NV+1);

  for (int k=0; k<nv; k++) {

    // Initialize maximum value and index for pivot
    //int i_max = k;
    //int v_max = mat[i_max][k];

    // find greater amplitude for pivot if any
    //for (int i = k+1; i < N; i++)
    //  if (abs(mat[i][k]) > v_max) v_max = mat[i][k], i_max = i;

    //if (!mat[k][i_max]) {
    //  cout << " singlular matirx"<<endl;
    //  exit(1);
    //}
    if (!mat[k][k]) {
      cout << " singlular matirx "<< mat[k][k] << endl;
      exit(1);
    }

   // Swap the greatest value row with current row.
   //if (i_max != k) swap_row(mat, k, i_max);

    for (int i=k+1; i<nv; i++) {
      // factor f to set current row kth element to 0,and subsequently remaining kth column to 0.
      double f = mat[i][k]/mat[k][k];

      // subtract fth multiple of corresponding kth row element
      for (int j=k+1; j<=nv; j++) mat[i][j] -= mat[k][j]*f;

     // filling lower triangular matrix with zeros
     mat[i][k] = 0;
    }
  }

  // Start calculating from last equation up to the first.
  for (int i = nv-1; i >= 0; i--) {
    // start with the RHS of the equation
    //lambda[i] = mat[i][nv];

    // Initialize j to i+1 since matrix is upper. triangular.
    for (int j=i+1; j<nv; j++) {
    // subtract all the lhs values except the coefficient of the variable whose value is being calculated
      //lambda[i] -= mat[i][j]*lambda[j];
      mat[i][nv] -= mat[i][j]*mat[j][nv];
    }

    // divide the RHS by the coefficient of the unknown being calculated
    //lambda[i] = lambda[i]/mat[i][i];
    mat[i][nv] /= mat[i][i];
  }

}

} // namespace jam2


