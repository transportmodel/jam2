#!/bin/bash
 
INPUTFILE="jam.inp"

for ((i=$1; i<=$2; i++)); do

WRKSUBDIR=`printf %d $i`
echo $WRKSUBDIR
rm -rf $WRKSUBDIR
mkdir $WRKSUBDIR
cd $WRKSUBDIR

echo "Main:numberOfEvents = 200 ! number of events to generate

Random:setSeed = on
Random:seed = "$(printf %d 0x$(xxd -l 3 -ps -c 10 /dev/urandom))"

Cascade:TimeStepSize = 0.2
Cascade:TimeStep = 200

MeanField:mode=16
MeanField:EoS= 43
MeanField:gaussWidth = 2.0
MeanField:optPotential = 1
MeanField:optRhoMeson=0

MeanField:optDeltaCoupling=4
MeanField:optNStarCoupling=2
MeanField:optDeltaStarCoupling=2

MeanField:optHyperonCoupling=4
MeanField:optLambdaStarCoupling=2
MeanField:optSigmaStarCoupling=2
MeanField:optXiStarCoupling=2

111:mayDecay = off    ! pi0
221:mayDecay = off    ! eta
311:mayDecay = off    ! K0
321:mayDecay = off    ! K+
#333:mayDecay = off    ! phi
3122:mayDecay = off   ! Lambda0
3212:mayDecay = off   ! Sigma0
3112:mayDecay = off   ! Sigma-
3222:mayDecay = off   ! Sigma+
3322:mayDecay = off   ! Xi0
3312:mayDecay = off   ! Xi-
3334:mayDecay = off   ! Omega-

Beams:beamA = 197Au
Beams:beamB = 197Au
Beams:eCM =  4.5
Beams:bmin = 4.6
Beams:bmax = 9.4

Analysis:printFreq = 1
Analysis:collision = on
Analysis:timeDependenceFlow = on
Analysis:timeDependenceDensity = on
Analysis:Potentials = on
Analysis:yCut = 0.5
Analysis:yCutFoward = 0.5
Analysis:yCutMax = 1.5

" > $INPUTFILE

echo $INPUTFILE

time ../jam -v 0 > log &

cd ..

done

