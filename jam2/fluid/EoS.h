#ifndef jam2_fluid_EoS_h
#define jam2_fluid_EoS_h

#include <Pythia8/PythiaStdlib.h>
#include <iostream>
#include <string>
#include <cmath>

namespace jam2 {

using Pythia8::HBARC;

class EoS
{
private:
  double eMin, eMax,bMax, dE,dN;
  bool   isLogScale;
  int    maxE, maxN;
  double **eosTabP, **eosTabT, **eosTabMu, **eosTabSmu, **eosTabLam;
  double **eosTabS, **eosTabD;
  std::string fName;
  int    optPot;
  double BagConst;
public:
   EoS(const std::string& fname, bool logscale=true);
  ~EoS();

private:
  void readTable();
  double lookup(double e,double b,double** eostab) const;
  double lookuplog(double e,double b,double** eostab) const;
  static double rlinter(double x1,double x2,double y1,double y2,double x) {
      return (y2-y1)*(x-x1)/(x2-x1)+y1;
  }
  double getEoSTable(double b, double e, double** eostab, double mass_dimension) const;

public:
  // e [GeV/fm^4] b [1/fm^3]
  double getPressureInGeV(double b,double e) const {
    if(e >= eMax) return (e - 4*BagConst)/3.0;
    return getEoSTable(b, e, eosTabP, 4.0);
  }

  double getCs2GeV(double b, double e) const {
    if(e >= eMax) return 1.0/3.0;
    double p1=getEoSTable(b, e-dE, eosTabP, 4.0);
    double p2=getEoSTable(b, e+dE, eosTabP, 4.0);
    return (p2-p1)/(2*dE);
  }

  // e in GeV/fm^3
  double getTGeV(double b,double e) const {
      return getEoSTable(b, e, eosTabT, 1.0);}

  double getMuBGeV(double b,double e) const {
      return getEoSTable(b, e, eosTabMu, 1.0);}

  double getMuSGeV(double b,double e) const {
      return getEoSTable(b, e, eosTabSmu, 1.0);}

  double getEntropyGeV(double b,double e) const {
      return getEoSTable(b, e, eosTabS, 3.0);}

  double getMultGeV(double b,double e) const {
      return getEoSTable(b,e,eosTabD, 3.0);}


  // e in 1/fm^4
  double getPressure(double b, double e) const {
    if(e*HBARC >= eMax) return (e - 4*BagConst)/3.0;
    return getEoSTable(b, e*HBARC, eosTabP, 4.0)/HBARC;
    //return getPressureInGeV(b,e*HBARC)/HBARC; 
  }

  double getCs(double b, double e) const {
    //return 1.0/sqrt(3.0);}
    // 2022/7/10
    if(e >= eMax) return sqrt(1.0/3.0);
    double p1=getEoSTable(b, (e-dE)*HBARC, eosTabP, 4.0);
    double p2=getEoSTable(b, (e+dE)*HBARC, eosTabP, 4.0);
    return sqrt((p2-p1)/(2*dE));
  }

  double getT(double b,double e) const {
      return getEoSTable(b, e*HBARC, eosTabT, 1.0)/HBARC;}

  double getMuB(double b,double e) const {
      return getEoSTable(b, e*HBARC, eosTabMu, 1.0)/HBARC;}

  double getMuS(double b,double e) const {
      return getEoSTable(b, e*HBARC, eosTabSmu, 1.0)/HBARC;}

  double getEntropy(double b,double e) const {
      return getEoSTable(b, e*HBARC, eosTabS, 3.0);}

  double getMult(double b,double e) const {
      return getEoSTable(b,e*HBARC,eosTabD, 3.0);}

};
} // end namespace jam2
#endif // jam2_fluid_EoS_h
