#ifndef jam2_hadrons_DecayWidthTable_h
#define jam2_hadrons_DecayWidthTable_h

#include <Pythia8/ParticleData.h>
#include <jam2/hadrons/ParticleTable.h>
#include <jam2/hadrons/DecayWidth.h>

namespace jam2 {

class DecayWidthTable
{
private:
  int nMass;
  double minMass,maxMass,dMass;
  std::vector<double> width;
  double wid0;
  ParticleDataEntryPtr part;
public:
  DecayWidthTable(ParticleDataEntryPtr p, DecayWidth* d);
  ~DecayWidthTable() {;}
  double getWid0() const {return wid0;}

  double getWidth(double m) {
    if(nMass==1) return wid0;
    int i=(int)((m-minMass)/dMass);
    if(i>=0 && i<nMass-1) {
      double mi=minMass+i*dMass;
      double s = (m-mi)/dMass;
      return width[i]*(1.0-s) + s*width[i+1];
    } else if(i>=nMass) {
      std::cout << "DecayWidthTable::getWidth mass too large id= "
	  << part->id() << " m= " << m
	  << " maxmass= "<< maxMass
	  << " nMass= "<< nMass
	  <<std::endl;
      exit(1);
      //return width[nMass-1];
    } else {
      std::cout << "DecayWidthTable::getWidth mass too small "<< m
                << " minMass= "<< minMass
		<< " name= "<< part->name()
		<< " id= "<< part->id()
                << " i= "<< i
                << " floor= " << (int)((m-minMass)/dMass)
                <<std::endl;
      return width[0];
    }
  }


};
} // end namespace jam2
#endif
