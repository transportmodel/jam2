#include <DecayWidthTable.h>

namespace jam2 {
using namespace std;

DecayWidthTable::DecayWidthTable(ParticleDataEntryPtr p, DecayWidth* d)
{
    part = p;
    wid0=p->mWidth();
    if(wid0 < 1e-15 || !p->varWidth()) {
      width.push_back(p->mWidth());
      nMass=1;
      minMass=p->m0();
      maxMass=p->m0();
      return;
    }
    nMass=500;
    //maxMass=6.0;
    minMass = p->mMin();
    maxMass = std::max(6.0,p->mMax());
    dMass = (maxMass - minMass) / (nMass - 1);
    width.resize(nMass);

    // make table loop over mass.
    for(int i=0;i<nMass;i++) {
      width[i]=d->getTotalWidth(p,minMass + i*dMass);
    }

}


} // end namespace jam2
