#include <jam2/initcond/Nucleus.h>

namespace jam2 {

using namespace std;
using namespace Pythia8;

Vec4 Nucleus::generateHardSphare() const
{
  double r = rHard * pow(rndPtr->flat(), 1.0/3.0);
  double costhe = 2.0*rndPtr->flat() - 1.0;
  double sinthe = sqrt(max(1.0 - costhe*costhe, 0.0));
  double phi = 2.0*M_PI*rndPtr->flat();

  return Vec4(r*sinthe*cos(phi), r*sinthe*sin(phi), r*costhe);

}

// deuteron
Vec4 Nucleus::generateDeuteron() const
{
  const double gamma = 0.2316;  // [1/fm]
  const double beta = 1.385;    // [1/fm]
  double phi_d =0.0, r=0.0;
  // max of nucleon prob density
  double rwMax = pow(beta-gamma,2);
  do { // Hulthen wave function
    r = 10.*pow(rndPtr->flat(),1.0/3.0);  // trial r, [fm]
    phi_d = (r>1e-3) ? (exp(-gamma*r) - exp(-beta*r))/r : beta-gamma;
    r /= 2.0;         // r in Hulthen wf is relative distance of n and p
  } while(rndPtr->flat()*rwMax > phi_d*phi_d);
  double cx = 1.0-2.0*rndPtr->flat();
  double sx = sqrt(1.0-cx*cx);
  double phi = 2*M_PI*rndPtr->flat();
  return Vec4(r*sx*cos(phi),r*sx*sin(phi),r*cx);
}

void Nucleus::setDeformationParameters()
{
//...generic parameterization of radius and surface thickness
  dRad=1.12*pow(A(),0.333333)-0.86/pow(A(),0.333333);
  dR = 0.54;
  density0 = 0.17;

  // Explicit values for specific nuclei
  // taken from C.W.De Jager et al.,
  // Atom.Data.Nucl.Data Tabl.36, 495 (1987).
  // Deformation parameters taken from Moller et al., 
  // Atomic Data and Nuclear Data Tables, 59, 185-381, (1995). 
  if(A() == 197) {  // Au
    dRad = 6.38;
    dR = 0.535;
    density0 = 0.1695;
    if(optFiniteSize==1) {
      dRad=6.42;
      dR=0.44;
      density0 = 0.1695;
    }
    beta2 = -0.13;
    beta4 = -0.03;
  } else if(A() == 208 || A() == 207) {  // Pb
    dRad = 6.62;
    dR = 0.546;
    density0 = 0.1695;
    if(optFiniteSize==1) {
      dRad = 6.67;
      dR = 0.44;
      density0 = 0.161;
    }
    beta2 = 0.0;
    beta4 = 0.0;
  } else if(A() == 63) {  // Cu
    dRad = 4.20641;
    dR = 0.5877;
    density0 = 0.1686;
    if(optFiniteSize==1) {
      dRad=4.28;
      dR=0.5;
      density0 = 0.1686;
    }
    beta2 = 0.162;
    beta4 = 0.006;
  } else if(A() == 238) { // U
    if(optFiniteSize==1) {
      dRad=6.86;
      dR=0.44;
      density0 = 0.166;
    }
    //P. Filip, R. Lednicky, H. Masui, and N. Xu, Phys. Rev. C 80, 054903 (2009).
    beta2 = 0.28;
    beta4 = 0.093;
  } else {
    cout << "(Nucleus::DeformationParameters) mass number not available"<< endl;
    exit(1);
  }

}

Vec4 Nucleus::generateDeformedNucleus() const
{
  double rmaxCut = dRad + 2.5;
  double rwMax = 1.0/(1.0+exp(-dRad/dR));
  double rad1 = dRad;
  double rwMax1 = rwMax;
  double r=0.0, cx=1.0;
  do {
    // Uniform distribution in a sphere with r = rmaxCut
    r = rmaxCut*pow(rndPtr->flat(),1.0/3.0);
    cx = 1.0-2.0*rndPtr->flat();
    
   // deformation Main axis in z-axis
   // Spherical Harmonics Y^0_2
   double y20 = sqrt(5.0/16.0/M_PI)*(3.0*cx*cx-1.0);

    // Spherical Harmonics Y^0_4
    double y40 = 35.0*cx*cx*cx*cx - 30.0*cx*cx + 3.0;
    y40 *= 3.0/16.0/sqrt(M_PI);

    rad1 =dRad*(1.0 + beta2 * y20 + beta4 * y40);
    rwMax1 =1.0/(1.0+exp(-rad1/dR));
  } while(rndPtr->flat()*rwMax1 > 1.0/(1.0+exp((r-rad1)/dR)));
  
  double sx = sqrt(1.0-cx*cx);
  double phi=2*M_PI*rndPtr->flat();
  double x = r*sx*cos(phi);
  double y = r*sx*sin(phi);
  double z = r*cx;
  
  double ctr,str,phir;
  // Rotate a deformed nucleus randomly
  if(optDeformRot==0) {
        ctr = 1.0-2.0*rndPtr->flat();
        str = sqrt(1.0-ctr*ctr);
        phir=2*M_PI*rndPtr->flat();

  // tip-tip  Rz(0-2pi)Ry(0)
  } else if(optDeformRot==1) {
        ctr=1.0;
        str=0.0;
        phir=2*M_PI*rndPtr->flat();

  // body-body Ry(pi/2)
  } else if(optDeformRot==2) {
        ctr=0.0;
        str=1.0;
        phir=0.0;

  // side-side Rz(pi/2)*Ry(pi/2)
  } else if(optDeformRot==3) {
        ctr=0.0;
        str=1.0;
        phir=0.5*M_PI;
  } else {
    cout << "Nucleus::generateDeformedNuclei:wrong optDeformRot " << optDeformRot <<endl;
    exit(1);
  }

  // Rotate a point by (theta,phi).
  double rot[3][3],pr[3]={x,y,z};
  rot[0][0]=ctr*cos(phir);
  rot[0][1]=-sin(phir);
  rot[0][2]=str*cos(phir);
  rot[1][0]=ctr*sin(phir);
  rot[1][1]=cos(phir);
  rot[1][2]=str*sin(phir);
  rot[2][0]=-str;
  rot[2][1]=0.0;
  rot[2][2]=ctr;

  //pr[0]=x;
  //pr[1]=y;
  //pr[2]=z;

  x=rot[0][0]*pr[0]+rot[0][1]*pr[1]+rot[0][2]*pr[2];
  y=rot[1][0]*pr[0]+rot[1][1]*pr[1]+rot[1][2]*pr[2];
  z=rot[2][0]*pr[0]+rot[2][1]*pr[1]+rot[2][2]*pr[2];

  return Vec4(x,y,z);
}

// This is the same as  Pythia8::GLISSANOModel::generate() except pz().
vector<Nucleon> Nucleus::generate() const
{
  //vector<Nucleon> nucl=GLISSANDOModel::generate();

  int sign = id() > 0? 1: -1;
  int pid = sign*2212;
  int nid = sign*2112;
  vector<Nucleon> nucleons;
  if ( A() == 0 ) {
    nucleons.push_back(Nucleon(id(), 0, Vec4()));
    return nucleons;
  } else if ( A() == 1 ) {
    if ( Z() == 1 ) nucleons.push_back(Nucleon(pid, 0, Vec4()));
    else  nucleons.push_back(Nucleon(nid, 0, Vec4()));
    return nucleons;
  } else if( A() == 2 ) {
    Vec4 pos = generateDeuteron();
    nucleons.push_back(Nucleon(pid,0,pos));
    nucleons.push_back(Nucleon(nid,1,Vec4(-pos.px(),-pos.py(),-pos.pz())));
    return nucleons;
  }

  Vec4 cms;
  vector<Vec4> positions;
  while ( int(positions.size()) < A() ) {
    while ( true ) {
      Vec4 pos;
      if(optSample == 1) 
        pos = generateNucleon();
      else if(optSample == 2) 
        pos = generateHardSphare();
      else
        pos = generateDeformedNucleus();

      bool overlap = false;
      for ( int i = 0, N = positions.size(); i < N && !overlap; ++i )
        if ( (positions[i] - pos).pAbs() < (gaussHardCore ? RhGauss() : Rh()) )
          overlap = true;
      if ( overlap ) continue;
      positions.push_back(pos);
      cms += pos;
      break;
    }
  }

  cms /= A();
  nucleons.resize(A());
  int Np = Z();
  int Nn = A() - Z();
  for ( int i = 0, N= positions.size(); i < N; ++i ) {
    Vec4 pos(positions[i].px() - cms.px(),
             positions[i].py() - cms.py(),
             positions[i].pz() - cms.pz());//This part is the only modification.
    if ( int(rndPtr->flat()*(Np + Nn)) >= Np ) {
      --Nn;
      nucleons[i] = Nucleon(nid, i, pos);
    } else {
      --Np;
      nucleons[i] = Nucleon(pid, i, pos);
    }
  }

  return nucleons;

}


}

