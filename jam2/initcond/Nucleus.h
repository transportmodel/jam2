#ifndef jam2_initcond_Nucleus_h
#define jam2_initcond_Nucleus_h

#include <cmath>
#include <Pythia8/HIUserHooks.h>
#include <Pythia8/Basics.h>

namespace jam2 {

class Nucleus : public Pythia8::GLISSANDOModel
{
protected:
    std::vector<Pythia8::Nucleon> generate() const;
    bool gaussHardCore;
    int  optSample=1;
    int  optFiniteSize=0;
    int  optDeformRot=0;
    double A0,R0,rHard;
    double density0,dR,dRad;
    double beta2,beta4; //deformation parameters

    // working area for deuteron.
    double x0,y0,z0;

public:
    Nucleus() : gaussHardCore(false) {
      R0=0.03;
    }
    virtual ~Nucleus() {
    }
    void setParam(std::string nucl) {
      optFiniteSize = settingsPtr->mode("HeavyIon:optNucleonFiniteSize");
      R0 = settingsPtr->parm("HeavyIon:WSR0");
      A0 = settingsPtr->parm("HeavyIon:A0");
      optSample = settingsPtr->mode("HeavyIon:optSample");
      //std::cout << "A= "<< A()<<std::endl;
      if(optSample >= 3) setDeformationParameters();

      if(nucl=="proj") {
        optDeformRot = settingsPtr->mode("HeavyIon:optProjRotation");
        if(optSample==4) {
          density0 = settingsPtr->parm("HeavyIon:projRho0");
          dRad = settingsPtr->parm("HeavyIon:projRadius");
          dR   = settingsPtr->parm("HeavyIon:projDefuseness");
          beta2 = settingsPtr->parm("HeavyIon:projBeta2");
          beta4 = settingsPtr->parm("HeavyIon:projBeta4");
        }
      } else {
        optDeformRot = settingsPtr->mode("HeavyIon:optTargRotation");
        if(optSample==4) {
          density0 = settingsPtr->parm("HeavyIon:targRho0");
          dRad = settingsPtr->parm("HeavyIon:TargRadius");
          dR   = settingsPtr->parm("HeavyIon:TargDefuseness");
          beta2 = settingsPtr->parm("HeavyIon:TargBeta2");
          beta4 = settingsPtr->parm("HeavyIon:TargBeta4");
        }
      }



      //rHard = 1.123*pow(double(A()),1.0/3.0) - R0;
      rHard = A0*pow(double(A()),1.0/3.0) - R0;
    }
    Pythia8::Vec4 generateHardSphare() const;
    Pythia8::Vec4 generateDeuteron() const;
    void setDeformationParameters();
    Pythia8::Vec4 generateDeformedNucleus() const;
    void initHist();
};

}

#endif // jam2_initcond_Nucleus_h

