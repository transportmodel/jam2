#ifndef jam2_initcond_Expanding_h
#define jam2_initcond_Expanding_h

#include <cmath>
#include <string>
#include <jam2/collision/Collision.h>
#include <jam2/initcond/InitialCondition.h>
#include <jam2/hadrons/JamParticleData.h>
#include <jam2/initcond/Constraints.h>

namespace jam2 {

class Expanding: public InitialCondition
{
protected:
  Constraints *timeCon;
  std::string compFrame;
  double etaMin=-5.0;
  double etaMax=5.0;
  double etaCM=6.0;
  double tau0=0.1;
  double temp=0.5;
  int numberOfParticle=4000;
  int overSample=1;
  double radNucl=5.0;
  double mG=0.0;
  double impactPar=0.0;
  double tStart;
  bool   optCMcorrection=false;
  int     withBox,optPDist;
  double  xBox,yBox,zBox;

public:
  Expanding(Pythia8::Settings* s, JamParticleData* pd,Pythia8::Rndm* r);
  virtual~Expanding();
  void init();
  void generate(Collision* event,int mode=0);
  Pythia8::Vec4 getThermalMomentum(const double em);
  Pythia8::Vec4 setPosition(double eta);
  Pythia8::Vec4 setPositionBox();

};

}

#endif
