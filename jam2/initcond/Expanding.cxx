#include <jam2/initcond/Expanding.h>
//#include <cstdlib>
#include <Pythia8/ParticleData.h>

using namespace std;
using namespace Pythia8;

namespace jam2 {

Expanding::Expanding(Settings* s, JamParticleData* p, Rndm* r)
    : InitialCondition(s,p,r)
{
  tStart = settings->parm("Cascade:timeStart");
  overSample=settings->mode("Cascade:overSample");
  compFrame=settings->word("Beams:compFrame");
  timeCon = new Constraints(s);
}

Expanding::~Expanding()
{
  delete timeCon;
}

void Expanding::init()
{
  optCMcorrection=true;

  // ZPC
  etaMin=-5.0;
  etaMax=5.0;
  tau0=0.1;  // formation time in fm/c
  temp=0.5;  // Temperature in Gev
  numberOfParticle=4000; // initial number of particles
  radNucl=5.0; // radius of tube in fm
  mG=settings->parm("Cascade:gluonMass");      // parton mass
  etaCM=6.0;

  // BUMPS
  temp=2.6;  // Temperature in Gev
  etaMin=-3.0;
  etaMax=3.0;
  etaCM=2.0;
  // dN/dy = (pi*R^2)*(T^3/pi^2)*tau0
  //numberOfParticle=1748*(etaMax-etaMin); // initial number of particles
  numberOfParticle=1821*(etaMax-etaMin); // initial number of particles

  numberOfParticle=settings->mode("BJinit:numberOfParticle");
  mA=numberOfParticle/2;
  mB=numberOfParticle/2;
  etaMax=settings->parm("BJinit:eta");
  etaMin= - etaMax;
  etaCM=settings->parm("BJinit:etaCM");
  temp=settings->parm("BJinit:temperature");
  tau0=settings->parm("BJinit:tau0");
  radNucl=settings->parm("BJinit:transverseRaidus");
  optPDist=settings->mode("BJinit:optPDist");

  withBox=settings->mode("Cascade:box");
  xBox=settings->parm("Cascade:boxLx");
  yBox=settings->parm("Cascade:boxLy");
  zBox=settings->parm("Cascade:boxLz");

}

void Expanding::generate(Collision* event, int mode)
{
  int id=21;
  Pythia8::ParticleDataEntryPtr pg = particleData->particleDataEntryPtr(id);
  impactPar=0.0;

  int ntot = numberOfParticle*overSample;
  vector<Vec4> rProj(ntot), pProj(ntot);
  Vec4 ptot=0.0;
  for(int i=0; i< ntot; i++) {
    if(withBox) {
      if(optPDist==1) {
        //double pt = 6.0*sqrt( rndm->flat());
        //double pt = 6.0;
        double pt = temp;
        double phi = 2*M_PI*rndm->flat();
        pProj[i][1]= pt*cos(phi);
        pProj[i][2]= pt*sin(phi);
        pProj[i][3] = 0.0;
      } else if(optPDist==2) {
        Vec4 pth = getThermalMomentum(mG);
        double phi = 2*M_PI*rndm->flat();
	double pt = sqrt(pth.pAbs2()-pth[3]*pth[3]/4);
        pProj[i][1]= pt*cos(phi);
        pProj[i][2]= pt*sin(phi);
        pProj[i][3] = pth[3]/2;
      } else {
        pProj[i] = getThermalMomentum(mG);
      }
      rProj[i] = setPositionBox();
    } else {
      pProj[i] = getThermalMomentum(mG);
      double eta  = etaMin + (etaMax - etaMin)* rndm->flat();
      pProj[i].bst(0.0,0.0,tanh(eta));
      rProj[i] = setPosition(eta);
      rProj[i][1] += pProj[i][1]/pProj[i][0]*rProj[i][0];
      rProj[i][2] += pProj[i][2]/pProj[i][0]*rProj[i][0];
    }
    ptot += pProj[i];
  }

  pHat = ptot/ptot.mCalc();
  if(optCMcorrection) {
    ptot /= ntot;
    Vec4 ptot2 = 0.0;
    for(int i=0; i< ntot; i++) {
      pProj[i] -= ptot;
      //pProj[i][1] -= ptot[1],pProj[i][2] -= ptot[2];
      pProj[i][0]=sqrt(mG*mG + pProj[i].pAbs2());
      ptot2 += pProj[i];
    }
    pHat = ptot2/ptot2.mCalc();
  }

  vector<double> lambdaA(ntot,0.1),tform(ntot);

  for(int i=0;i<ntot;i++) tform[i]=rProj[i][0];

  bool optPropagateT=true;
  optPropagateT=false;
  // propagate all particles to the same time tau0.
  if(optPropagateT) {
    for(int i=0; i< ntot; i++) {
      rProj[i] += pProj[i]/pProj[i][0]*(tau0-rProj[i][0]);
    }
  }

  // Sorge's time constraints.
  if(event->cascadeModel() !=3 && optCollisionOrdering>=120) {

     //for(int i=0;i<ntot;i++) rProj[i] += pProj[i]/pProj[i][0]*(tau0-rProj[i][0]);
     //for(int i=0;i<ntot;i++) tform[i]=rProj[i][0];

    double eps = timeCon->fixConstraints(rProj,pProj);
    cout << " eps= "<< eps<<endl;
    lambdaA = timeCon->solveConstraints(rProj,pProj,mG);

  for(int i=0;i<ntot;i++) {
    if(rProj[i].m2Calc()>0.0) {
      cout << "timelike? "<< rProj[i].m2Calc() << " eta= "<< 
	0.5*log((pProj[i][4]+pProj[i][3])/(pProj[i][4]-pProj[i][3]))
	<<endl;}
  }

    int it=0, ip=0;
  for(int i=0;i<ntot;i++) {
    if(rProj[i].m2Calc()>0.0) {
      ip++;
    }
  for(int j=i+1;j<ntot;j++) {
    //if(rProj[j].m2Calc()>0.0) continue;
    double dr2=(rProj[i]-rProj[j]).m2Calc();
    if(dr2>0.0) {
      cout << "i= "<< i << " j= "<< j << " dr2= "<< dr2
	<< " r1= "<< rProj[i].m2Calc()
	<< " r2= "<< rProj[j].m2Calc()
	 <<endl;
      it++;
    }
  }
  }
  cout << " it= "<< it << " ip= "<< ip<<endl;
  //if(it>0)  exit(1);

  }

  //cout << "init. cond Expand pHat= "<< pHat;

  if(compFrame =="lab") {

  pHat=0.0;
  //double betz = tanh(etaCM);
  double gam  = cosh(etaCM);
  double uz   = sinh(etaCM);
  for(int i=0; i< ntot; i++) {
    Vec4 p=pProj[i];
    pProj[i][0]=gam*p[0] + uz*p[3];
    pProj[i][3]=gam*p[3] + uz*p[0];
    Vec4 r=rProj[i];
    rProj[i][0]=gam*r[0] + uz*r[3];
    rProj[i][3]=gam*r[3] + uz*r[0];
    tform[i]=rProj[i][0];

    //pProj[i].bst(0.0,0.0,betz,gam);
    //rProj[i].bst(0.0,0.0,betz,gam);
    pHat += pProj[i];
  }
  pHat /= pHat.mCalc();

  }

  event->plist.clear();
  int q0[2]={0,0};
  for(int i=0; i< ntot; i++) {
    EventParticle* cp = new EventParticle(id,mG,rProj[i],pProj[i],pg);
    //cp->tevol(rProj[i][0]);

    // change tau of particle.
    if(optCollisionOrdering > 100) cp->tevol(tStart);
    if(optCollisionOrdering == 101) cp->tevol(pHat*cp->getR());
    else  if(optCollisionOrdering==103)
      cp->tevol(cp->getP()*cp->getR()/cp->getMass());

    cp->setOnShell();
    cp->lambda(timeCon->lambda(cp->getP(),pHat,cp->getMass(), lambdaA[i]));
    cp->setFormationTime(tform[i]);
    if(tform[i]>rProj[i][0]) cp->setConstQuark(q0);
    event->plist.push_back(cp);
  }

  //double eps = event->checkConstraints(event->plist, 0.0,optGij);
  //cout << "eps= "<< eps << " size= "<< event->plist.size() <<endl;
  //cout << "tstart= "<< tStart<<endl;
  //cout << "phat= "<<pHat;
  //cin.get();

  // output initial phase space
  if(outputIni) {
    iniOfs << event->plist.size()<<endl;
    Pythia8::Hist dndy("initial dndy",40,-8.0,8.0);
    double dy=8.0/40;
    for(auto& p: event->plist) {
      Vec4 r = p->getR();
      double y = compFrame =="lab" ? p->rap()-etaCM: p->rap();
      dndy.fill(y,1.0/dy);
      iniOfs << setw(3) << p->getStatus()
              << setw(12) << p->getID()
              << setw(15) << fixed << p->getMass()
              << scientific
              << setw(16) << setprecision(8) << r[0]
              << setw(16) << setprecision(8) << r[1]
              << setw(16) << setprecision(8) << r[2]
              << setw(16) << setprecision(8) << r[3]
              << setw(16) << setprecision(8) << p->getPe()
	      << setw(16) << setprecision(8) << p->getPx()
              << setw(16) << setprecision(8) << p->getPy()
              << setw(16) << setprecision(8) << p->getPz()
	      <<endl;
    }
    dndy.table("dndy_ini.dat");
  }

}

Vec4 Expanding::getThermalMomentum(const double em)
{
  //Bin Zhang, et.al Phys. Rev. C58 (1998) 1175,
  //S. Pratt, Phys. Rev. C89 (2014) 024910.
  //The Erlang distribution can be generated by using convolution generation algorithm.
  // We first generate energy according to p^2*exp(-p/T) for the massless particle.
  double e=0;
  while(1) {
    e = rndm->flat();
    e *= rndm->flat();
    e *= rndm->flat();
    if (e < 0.0) continue;
    e = - temp * log(e);
    if (rndm->flat() > exp((e - sqrt(e*e  + em*em))/temp)) continue;
    break;
  }
  double cost = 2.0*rndm->flat() - 1.0;
  double sint = sqrt(1.0 - cost *cost);
  double phi = 2.0 * M_PI * rndm->flat();
  double px = e * sint * cos(phi);
  double py = e * sint * sin(phi);
  double pz = e * cost;
  double pe = sqrt(e*e + em*em);
  return Vec4(px,py,pz,pe);

}

Vec4 Expanding::setPosition(double eta)
{
  double t = tau0*cosh(eta);
  double z = tau0*sinh(eta);
  double r0 = radNucl;
  double b = impactPar;

  double a0 = r0-b/2;
  double b0 = sqrt(r0*r0 - b*b/4);
  double rr = sqrt( rndm->flat());
  double phi = 2*M_PI*rndm->flat();
  double x = a0*rr*cos(phi);
  double y = b0*rr*sin(phi);
  return Vec4(x,y,z,t);
}

Vec4 Expanding::setPositionBox()
{
  double x  = -xBox/2.0 + xBox * rndm->flat();
  double y  = -yBox/2.0 + yBox * rndm->flat();
  double z  = -zBox/2.0 + zBox * rndm->flat();
  return Vec4(x,y,z,0.0);
}


}// end namespace jam2
