#ifndef jam2_interaction_FixTime_h
#define jam2_interaction_FixTime_h

#include <Pythia8/Basics.h>
#include <jam2/collision/EventParticle.h>

namespace jam2 {

using Pythia8::Vec4;

class FixTime
{
protected:
    Pythia8::Info*  info;
    Pythia8::Settings* settings;
    Pythia8::Vec4 pHat;
    int    optCollisionOrdering;
    double lambdaLag;
    int optVectorPotential,optVdot,optPropagate;

public:
  FixTime(Pythia8::Info* inf,Pythia8::Settings* s);
  virtual ~FixTime();
  void phat(Vec4 p) {pHat=p;}
  Vec4 phat() const {return pHat;}
  void setEvolutionTime(EventParticle* pa,double tc,double ctime,double lam,int proc);

};
}
#endif

