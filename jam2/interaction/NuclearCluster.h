#ifndef jam2_interaction_NuclearCluster_h
#define jam2_interaction_NuclearCluster_h

#include <jam2/collision/EventParticle.h>
#include <jam2/collision/Collision.h>
//#include "MassTable.h"

#include <vector>

namespace jam2 {

using Pythia8::Vec4;

class NuclearCluster
{
private:
  double R0=3.0; // fm
  double P0=0.3; // GeV/c
  double R0s=3.0;
  double P0s=0.3;
  bool   withMeanField=false;
  bool   deleteParticle=true;
  //MassTable* mastbl;
public:
  NuclearCluster(double r0=3.0, double p0=0.3,double r0s=3.0, double p0s=0.3,bool mfield=false, bool dpart=true):
    R0(r0),P0(p0),R0s(r0s),P0s(p0s),withMeanField(mfield),deleteParticle(dpart) {
  cout << "nuclear cluster parameters: R0= "<< R0 << " P0= " << P0<<endl; }
  void setR0(double r) {R0=r;}
  void setP0(double p) {P0=p;}
  void setRP0(double r, double p) {R0=r,P0=p;}
  bool clust(EventParticle* p1, EventParticle* p2);
  //int findCluster(std::list<EventParticle*>& plist);
  int findCluster(Collision* event);
  std::pair<double,double> rpCMsq(EventParticle &p1, EventParticle &p2,double& rr,double &pp);
  EventParticle* setClusterProperty(std::vector<EventParticle*>& part, std::vector<int>& num,
      int& ii, int nt,int isave, int& ibar,Pythia8::Vec4& pf);
  double getMass(int kf);

};
}
#endif
