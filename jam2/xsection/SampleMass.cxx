#include <jam2/xsection/SampleMass.h>
#include <jam2/hadrons/JamStdlib.h>
#include <jam2/hadrons/GaussPoints.h>
#include <jam2/xsection/CrossSection.h>
#include <jam2/xsection/CG.h>

namespace jam2 {

using namespace std;
using Pythia8::ParticleDataEntryPtr;

const double SampleMass::Mnucl=0.9383, SampleMass::Mpion=0.138;
//const int SampleMass::nS=500;
const int SampleMass::nS=100;
const double SampleMass::sMinBB=2*Mnucl+Mpion;
const double SampleMass::sMinMM=3*Mpion;
const double SampleMass::sMinMB=Mnucl+2*Mpion;
const double SampleMass::sMaxBB=7.0;
const double SampleMass::sMaxMM=5.0;
const double SampleMass::sMaxMB=6.0;


SampleMass::SampleMass(Pythia8::Settings *s,string bwfile,JamParticleData* table,StringFlav* fs,Pythia8::Rndm* r)
{
  settings = s;
  jamTable=table; 
  decay=jamTable->getDecayWidth();
  flavSel = fs;
  rndm=r;
  hadronContent = new HadronContent(jamTable->getParticleData(),rndm);

 // Mixing for eta and eta'.
  double theta    = settings->parm("StringFlav:thetaPS");
  double alpha    = (theta + 54.7) * M_PI / 180.;
  fracEtass       = pow2(sin(alpha));
  fracEtaPss      = 1. - fracEtass;

  proton  = jamTable->find(2212);
  neutron = jamTable->find(2112);
  lambda  = jamTable->find(3122);
  sigmam  = jamTable->find(3112);
  sigma0  = jamTable->find(3212);
  sigmap  = jamTable->find(3222);
  xim     = jamTable->find(3312);
  xi0     = jamTable->find(3322);
  omega   = jamTable->find(3334);

  optConstQuarkDiffra=settings->mode("Cascade:optConstQuarkDiffra");
  optQuarkExchange=settings->flag("Cascade:optQuarkExchangeProcess");
  optQuarkAnn = settings->flag("Cascade:optQuarkAnnihilationProcess");
  probQuarkExchange = settings->parm("Cascade:probQuarkExchange");
  probQuarkAnn = settings->parm("Cascade:probQuarkAnnihilation");
  probDiffraction= 1.0 - probQuarkExchange - probQuarkAnn;
  elementRes = settings->parm("Cascade:matrixElementRR");
  probS = settings->parm("Cascade:probStrangeQuarkProduction");

  // Option for BW integration.
  optProb=settings->mode("Cascade:optBWIntegration");
  //optProb=3; // use table
  //optProb=2;// full two-dim integral
  //optProb=1;// use approximate formula
  //optProb=0;// neglect BW integration.
  //optProb=-1;// elastic only
 
  optWidth=settings->mode("Cascade:optBWWidth");
  //optWidth=0; // constant width
  //optWidth=1; // momentum-dependent width
  optBW=settings->mode("Cascade:optBWFunction");
  // optBW=1; relativistic I
  // optBW=2; relativistic II

  //ParticleTable* meson=jamTable->getMeson();
 
  // sigma, eta, omega,...
  ParticleTable* meson = jamTable->getLight0Meson();
  for(int i=0;i<(int)meson->size();i++) {
    mesons.push_back(meson->getParticle(i));
    isoParticle[meson->getParticle(i)]=meson->getParticle(i);
  }

  // pion,rho...
  ParticleTable* meson1 = jamTable->getLight1Meson0();
  ParticleTable* meson1p = jamTable->getLight1Mesonp();
  for(int i=0;i<(int)meson1->size();i++) {
    mesons.push_back(meson1->getParticle(i));
    isoParticle[meson1->getParticle(i)]=meson1->getParticle(i);
    isoParticle[meson1p->getParticle(i)]=meson1->getParticle(i);
  }

  // K*
  ParticleTable *strp = jamTable->getStrMesonp();
  ParticleTable *str0 = jamTable->getStrMeson0();
  for(int i=0;i<(int)strp->size();i++) {
    mesons.push_back(strp->getParticle(i));
    isoParticle[strp->getParticle(i)]=strp->getParticle(i);
    isoParticle[str0->getParticle(i)]=strp->getParticle(i);
  }

  // N*
  baryons.push_back(proton);
  isoParticle[proton]=proton;
  isoParticle[neutron]=proton;
  ParticleTable* pstar=jamTable->getPstar();
  ParticleTable* nstar=jamTable->getNstar();
  for(int i=0;i<(int)pstar->size();i++) {
    baryons.push_back(pstar->getParticle(i));
    isoParticle[pstar->getParticle(i)]=pstar->getParticle(i);
    isoParticle[nstar->getParticle(i)]=pstar->getParticle(i);
  }

  // Delta*
  ParticleTable* dm=jamTable->getDmstar();
  ParticleTable* d0=jamTable->getD0star();
  ParticleTable* dp=jamTable->getDpstar();
  ParticleTable* dpp=jamTable->getDppstar();
  for(int i=0;i<(int)dm->size();i++) {
    baryons.push_back(dm->getParticle(i));
    isoParticle[dm->getParticle(i)]=dm->getParticle(i);
    isoParticle[d0->getParticle(i)]=dm->getParticle(i);
    isoParticle[dp->getParticle(i)]=dm->getParticle(i);
    isoParticle[dpp->getParticle(i)]=dm->getParticle(i);
  }

  // Lambda*
  baryons.push_back(lambda);
  isoParticle[lambda]=lambda;
  ParticleTable* lam=jamTable->getLambda();
  for(int i=0;i<(int)lam->size();i++) {
    baryons.push_back(lam->getParticle(i));
    isoParticle[lam->getParticle(i)]=lam->getParticle(i);
  }

  // Sigma*
  baryons.push_back(sigmam);
  isoParticle[sigmam]=sigmam;
  isoParticle[sigma0]=sigmam;
  isoParticle[sigmap]=sigmam;
  ParticleTable* sm=jamTable->getSmstar();
  ParticleTable* s0=jamTable->getS0star();
  ParticleTable* sp=jamTable->getSpstar();
  for(int i=0;i<(int)sm->size();i++) {
    baryons.push_back(sm->getParticle(i));
    isoParticle[sm->getParticle(i)]=sm->getParticle(i);
    isoParticle[s0->getParticle(i)]=sm->getParticle(i);
    isoParticle[sp->getParticle(i)]=sm->getParticle(i);
  }
 
  // Xi*
  baryons.push_back(xim);
  isoParticle[xi0]=xim;
  ParticleTable *xism=jamTable->getXmstar();
  ParticleTable *xis0=jamTable->getX0star();
  for(int i=0;i<(int)xism->size();i++) {
    baryons.push_back(xism->getParticle(i));
    isoParticle[xism->getParticle(i)]=xism->getParticle(i);
    isoParticle[xis0->getParticle(i)]=xism->getParticle(i);
  }

  baryons.push_back(omega);
  isoParticle[omega]=omega;

  // compute normalization of BW.
  for(auto& h:mesons)  bwNorm[h] = computeNorm(h);
  for(auto& h:baryons) bwNorm[h] = computeNorm(h);

  if(optProb==3) if(!readBWTable(bwfile)) makeBWTable(bwfile);

}

SampleMass::~SampleMass()
{
  for(int i=0;i<(int)BWintSave.size();i++) delete BWintSave[i];
  BWintSave.clear();
  delete hadronContent;
}

bool SampleMass::readBWTable(string fname)
{
  ifstream inFile(fname.c_str(),ios::in);
  if(!inFile.is_open()) {
    cout << "Sample::readBWTable file does not exist " << fname
         << " now making table" << endl;
    return false;
  }


  vector<ParticleDataEntryPtr> hadrons=mesons;
  hadrons.insert(hadrons.end(),baryons.begin(),baryons.end());

  // compute normalization of BW.
  //for(auto& h:hadrons) bwNorm[h] = computeNorm(h);


  std::vector<double> bwint(nS);
  for(int i=0;i<(int)hadrons.size();i++)
  for(int j=i;j<(int)hadrons.size();j++) {
  //if(hadrons[i]->mWidth() <1e-5 && hadrons[j]->mWidth() <1e-5) continue;
    if(hadrons[i]->mWidth() <1e-5 || hadrons[j]->mWidth() <1e-5) continue;
    double sMin=sMinMB, sMax=sMaxMB;
    if(hadrons[i]->isMeson() && hadrons[j]->isMeson()) {
      sMin=sMinMM; sMax=sMaxMM;
    } else if(hadrons[i]->isBaryon() && hadrons[j]->isBaryon()) {
      sMin=sMinBB; sMax=sMaxBB;
    }

    for(int k=0;k<nS;k++) inFile >> bwint[k];

    BWintegral *bw = new BWintegral(sMin,sMax,bwint,hadrons[i]->id(),hadrons[j]->id());
    BWint[make_pair(hadrons[i],hadrons[j])]=bw;
    if(i != j) BWint[make_pair(hadrons[j],hadrons[i])]=bw;
    BWintSave.push_back(bw);
  }

  inFile.close();
  return true;
}

void SampleMass::makeBWTable(string outfile)
{
  ofstream ofs(outfile.c_str());

  vector<ParticleDataEntryPtr> hadrons=mesons;
  hadrons.insert(hadrons.end(),baryons.begin(),baryons.end());

  // compute normalization of BW.
  //for(auto& h:hadrons) bwNorm[h] = computeNorm(h);

  std::vector<double> bwint(nS);
  // loop over hadron pair.
  for(int i=0;i<(int)hadrons.size();i++)
  for(int j=i;j<(int)hadrons.size();j++) {

    // we only make table for double resonances.
    if(hadrons[i]->mWidth() <1e-5 || hadrons[j]->mWidth() <1e-5) continue;

    // set minimum and maximum energy
    double sMin=sMinMB, sMax=sMaxMB;
    if(hadrons[i]->isMeson() && hadrons[j]->isMeson()) {
      sMin=sMinMM; sMax=sMaxMM;
    } else if(hadrons[i]->isBaryon() && hadrons[j]->isBaryon()) {
      sMin=sMinBB; sMax=sMaxBB;
    }
    double dS=(sMax-sMin)/nS;

    // loop over incident energy, output BW result to the file.
    for(int k=0;k<nS;k++) {
      double srt = sMin + dS * k;
      bwint[k] = probBW2(srt,hadrons[i],hadrons[j]);
      ofs << bwint[k] << endl;
    }
    BWintegral *bw = new BWintegral(sMin,sMax,bwint,hadrons[i]->id(),hadrons[j]->id());
    BWint[make_pair(hadrons[i],hadrons[j])]=bw;
    if(i != j) BWint[make_pair(hadrons[j],hadrons[i])]=bw;
    BWintSave.push_back(bw);
  }

}

//***********************************************************************
//...Generate masses according to the Breit-Wigner distribution.
int SampleMass::jamrmas2(int coltype,ParticleDataEntryPtr pd1, ParticleDataEntryPtr pd2,
	int kf1,int kf2,int id1,int id2, int iz1, int iz2, 
	double m1, double m2, bool preHadronA, bool preHadronB,double srt,bool isAnti)
{
  // elastic only
  if(optProb == -1) return 1;

  // currently charmed hadrons are not implemented.
  if(CrossSection::getHeavyQB(kf1)+CrossSection::getHeavyQB(kf2)>0) return 1;

  eCM = srt;
  pout[0] = pd1;
  pout[1] = pd2;
  isRes1= pd1->mWidth()>1e-5 ? true: false;
  isRes2= pd2->mWidth()>1e-5 ? true: false;

  Id[0] = kf1;
  Id[1] = kf2;
  emr[0] = m1;
  emr[1] = m2;
  iZ1 = iz1;
  iZ2 = iz2;
  iD1 = id1;
  iD2 = id2;

  // compute Clepsch-Gordan coefficient for the incomping particle pair.
  pair<int,int> iso1 = isospin(Id[0]);
  pair<int,int> iso2 = isospin(Id[1]);
  int j1=iso1.first, j2=iso2.first;
  int jz1 = iso1.second, jz2=iso2.second;
  jtMax = j1 + j2;
  jtMin = abs(j1 - j2);
  jMz   = jz1 + jz2;

  // cofcgi(j1,j2,j,m1,m2,m) = <j1,j2;m1,m2|j,m>
  cofCG12.clear();
  for(int j=jtMin; j<=jtMax;j +=2) {
   double coef = ClebschGordan::cofcgi( j1,j2, j,jz1,jz2, jMz);
   cofCG12.push_back(coef*coef);
  }

  collPair.clear();

  // diffractive scattering only for preformed hadrons
  if(preHadronA || preHadronB) {
    diffractive(1.0);
  } else {

    if(optQuarkExchange && optQuarkAnn) {
      diffractive(probDiffraction);
      quarkExchangeProcess2(Id[0],Id[1],probQuarkExchange);
      if(coltype!=1) quarkAnnProcess(Id[0],Id[1],probQuarkAnn);
    } else if(optQuarkExchange && !optQuarkAnn) {
      diffractive(0.5);
      quarkExchangeProcess2(Id[0],Id[1],0.5);
    } else if(!optQuarkExchange && optQuarkAnn) {
      diffractive(0.5);
      if(coltype!=1) quarkAnnProcess(Id[0],Id[1],0.5);
    } else if(!optQuarkExchange && !optQuarkAnn) {
      diffractive(1.0);
    }

  }

  double totsp=0.0;
  for(auto& i : collPair) totsp += i.probability();

  if(totsp == 0.0) {
    informChannel(totsp,srt);
    return 1;
  }

  // Select outgoing particles.
  int ntry=0;
  do {
    double  xrand=totsp*rndm->flat();
    double ttp=0.0;
    for(auto& cp : collPair) {
      xrand -= cp.probability();
      ttp += cp.probability();
      if(xrand <= 0.0) {
	pout[0]=cp.particleA();
	pout[1]=cp.particleB();
	Id[0]=cp.id1();
	Id[1]=cp.id2();
	goto L100;
      }
    }
    cout << "SampleMass::  no particles? totsp= " << totsp
      << " ttp= "<< ttp << endl;
    return 1;
L100:

    // Monte Carlo sampling of particle masses.
    if(sample()) break;

  } while(ntry++<100);

  if(ntry==100) {
    cout << "SampleMass: does not converge eCM= " << srt
      << " id1= "<< Id[0]
      << " id2= "<< Id[1]
      << endl;
    return 1;
  }

  if(isAnti) {
    if(pout[0]->hasAnti()) Id[0] *= -1;
    if(pout[1]->hasAnti()) Id[1] *= -1;
  }

  /*
  cout << "srt= "<< srt << " " << pd1->name(kf1) 
	<< " + "<< pd2->name(kf2) 
	<< " ->  " << pout[0]->name(Id[0])
	<< " + " << pout[1]->name(Id[1])
	<<endl;
  cin.get();
  */

  return 0;

}

vector<ParticleDataEntryPtr> SampleMass::
findMeson(ParticleDataEntryPtr pa, int kf0)
{
  //....Mesons.

  ParticleTable* table=jamTable->getMeson();
  vector<ParticleDataEntryPtr> ncount;
  int kfsign= kf0 > 0 ? 1 : -1;
  if(pa->hasAnti()) kfsign=1;

  int kfa=abs(kf0);
  int kf1=(kfa/100) % 10;
  int kf2=(kfa/10)  % 10;
  int kfm=100*kf1+10*kf2;

  //if(kfm == 220 || kfm == 330) kfm=110;

  int itry=0, jtry=0;
  do {
    if(itry++ > 200) {
      cout << "SamplemMass::findMeson infinit loop? " << kf0 <<endl;
      exit(1);
    }

    //9000111
    //int icount=0;
    for(int i9=0;i9<=9; i9+=9) 
    //for(int ir=0;ir<=2; ir++) 
      for(int ir=0;ir<=5; ir++) 
	for(int is=1;is<=7; is+=2) {
	  //int kfmes=(ir*10000+kfm+is)*kfsign;
	  int kfmes=(i9*1000000+ir*10000+kfm+is)*kfsign;
	  //ParticleDataEntry* pr=jamTable->find(kfmes);
	  ParticleDataEntryPtr pr=table->find(kfmes);
	  if(pr == 0) continue;
	  ncount.push_back(pr);
	}

    /*
    if(kfm == 220) {
      kfm=330;
      jtry=1;
    } else if(kfm == 110) {
      kfm=220;
      jtry=2;
    } else if(kfm == 330) {
      kfm=110;
      jtry=3;
    }
    */

  } while (itry <= 2 && jtry >= 1);

  return ncount;

}


// Before call this, specify the incoming particles pout[2].
bool SampleMass::sample()
{    
  double em0[2],gam0[2],gamw[2];
  double mmax[2],mmin[2],bwmax[2],bw[2],ymin[2],ymax[2];
  int iex[2], id[2];

  bool ok=false;
  double wid1=pout[0]->mWidth();
  double wid2=pout[1]->mWidth();
  mmin[0]=pout[0]->m0();
  mmin[1]=pout[1]->m0();
  iex[0]=0; iex[1]=0;
  if(wid1>1e-5) {mmin[0] = pout[0]->mMin(); iex[0]=1;}
  if(wid2>1e-5) {mmin[1] = pout[1]->mMin(); iex[1]=1;}

  mmax[0]=mmin[0];
  mmax[1]=mmin[1];
  //if(iex[0]==1) mmax[0]=min(3.5,ecm-mmin[1]-eKinMin);
  //if(iex[1]==1) mmax[1]=min(3.5,ecm-mmin[0]-eKinMin);
  if(iex[0]==1) mmax[0]=max(mmin[0],eCM-mmin[1]-eKinMin);
  if(iex[1]==1) mmax[1]=max(mmin[1],eCM-mmin[0]-eKinMin);

  if(mmin[0] <= mmax[0] && mmin[1] <= mmax[1]) { ok=true; }

  if(!ok) {
    id[0]=pout[0]->id();
    id[1]=pout[1]->id();
    cout << "SampleMass::ResnanceMass too small srt? srt= " << eCM <<endl;
    cout << " ok= " << ok << endl;
    cout << " id1= " << id[0] << " min1= "<< mmin[0]<< " max= "<< mmax[0]
      << " iex= " << iex[0] <<endl;
    cout << " id2= " << id[1] << " min2= " << mmin[1]<< " max= "<< mmax[1]
      << " iex= " << iex[1] <<endl;
    exit(1);
  }

  if(eCM < mmin[0] + mmin[1] + eKinMin) {
    cout << "sampleResonanceMass too small srt? " << eCM
      << " mmin1= " << mmin[0]
      << " mmin2= " << mmin[1]
      << " id1= " << Id[0]
      << " id2= " << Id[1]
      <<endl;
    exit(1);
  }

  id[0]=pout[0]->id();
  id[1]=pout[1]->id();

  // First compute maximum value.
  for(int jt=0; jt<2;jt++) {
    bwmax[jt]=1.0;
    bw[jt]=1.0;
    ymin[jt]=0.0;
    ymax[jt]=0.0;
    em0[jt]=pout[jt]->m0();
    gam0[jt]=pout[jt]->mWidth();
    emr[jt]=em0[jt];
    if(iex[jt] == 0) continue;
    ymin[jt]=atan((pow2(mmin[jt])-pow2(em0[jt]))/(em0[jt]*gam0[jt]));
    ymax[jt]=atan((pow2(mmax[jt])-pow2(em0[jt]))/(em0[jt]*gam0[jt]));
    //gamw[jt] = decay->getTotalWidth(pout[jt],mmax[jt]);
    //bwmax[jt] = BW(em0[jt],mmax[jt],gamw[jt])/BW(em0[jt],mmax[jt],gam0[jt]);
    gamw[jt] = jamTable->totalWidth(pout[jt],em0[jt]);
    bwmax[jt] = BW(em0[jt],em0[jt],gamw[jt])/BW(em0[jt],em0[jt],gam0[jt]);

    //int pid=jamTable->pid(id[jt]);
    //if(pid == id_nucls) bwmax[jt] *= 2.0;
    //if(pid == id_delts) bwmax[jt] *= 1.5;
    //if(pid == id_delt) bwmax[jt] *= 1.5;
    //if(pid == id_str) bwmax[jt] *= 1.5;
    if(id[jt] == 333) bwmax[jt] *= 3.0;
    if(id[jt] == 313 || id[jt]==323) bwmax[jt] *= 2.0;
  }

  double pf0=PCM(eCM,mmin[0],mmin[1]);
  double bwpmax=bwmax[0]*bwmax[1]*pf0 * 5.0;

  // Generate resonance mass according to Breit-Wigner + phase space.
  int maxtry=10;
  int ntry=0;
  double pf=0.0;
  do {
    do {
      for(int jt=0;jt<2;jt++)
	if(iex[jt] == 1) {
	  // First generate mass according to B-W with constant width.
	  // A(m^2)=d(m^2)/((m^2 - m_R^2)^2 + (m*Gamma)^2)
	  double y=ymin[jt] + rndm->flat()*( ymax[jt] - ymin[jt] );
	  emr[jt] = sqrt( em0[jt]*gam0[jt]*tan(y) + em0[jt]*em0[jt] );
	}
    } while(emr[0]+emr[1]+eKinMin > eCM);

    for(int jt=0;jt<2;jt++)
      if(iex[jt] == 1) {
	double wid = jamTable->totalWidth(pout[jt],emr[jt]);
	bw[jt] = BW(em0[jt],emr[jt],wid)/BW(em0[jt],emr[jt],gam0[jt]);
	//bw[jt] = BW(em0[jt],emr[jt],gamw[jt])/BW(em0[jt],emr[jt],gam0[jt]);
      }

    // Check final phase space.
    pf=PCM(eCM,emr[0],emr[1]);
    if(bwpmax < bw[0]*bw[1]*pf) {
      cout << "SampleMass:bwm= " << bwpmax
	<< " bw= "<< bw[0]*bw[1]*pf
	<< " id1= " << id[0]
	<< " id2= " << id[1]
	<< " srt= " << eCM  << endl;
      bwpmax *= 1.05;
      continue;
    }

    if(ntry++ > maxtry) return false;

  } while (bwpmax*rndm->flat() > bw[0]*bw[1]*pf);

  return true;
}



//***********************************************************************
// Purpose: to find possible hadronic excitation states.
vector<ParticleDataEntryPtr> SampleMass::
jamexpa(ParticleDataEntryPtr pa, int kf0,int id0, int iz0)
{
  vector<ParticleDataEntryPtr> ncount;

  if(pa->isMeson()) {

    return findMeson(pa,kf0);

  } else if(pa->isBaryon()) { 

    if(kf0 < 0) iz0 *= -1;

    if(id0 == id_nucl || id0 == id_nucls
	|| id0 == id_delt || id0 == id_delts) {
      //....Find N* D* channel.

      if(iz0 == -1) {
	ParticleTable* dm=jamTable->getDmstar();
	for(int i=0;i<(int)dm->size();i++) {
	  ncount.push_back(dm->getParticle(i));
	}
      } else if(iz0 == 0) {
	ncount.push_back(neutron);
	ParticleTable* d0=jamTable->getD0star();
	for(int i=0;i<(int)d0->size();i++) {
	  ncount.push_back(d0->getParticle(i));
	}
	ParticleTable* n0=jamTable->getNstar();
	for(int i=0;i<(int)n0->size();i++) {
	  ncount.push_back(n0->getParticle(i));
	}
      } else if(iz0 == 1) {
	ncount.push_back(proton);
	ParticleTable* dp=jamTable->getDpstar();
	for(int i=0;i<(int)dp->size();i++) {
	  ncount.push_back(dp->getParticle(i));
	}
	ParticleTable* ps=jamTable->getPstar();
	for(int i=0;i<(int)ps->size();i++) {
	  ncount.push_back(ps->getParticle(i));
	}
      } else if(iz0 == 2) {
	ParticleTable* dpp=jamTable->getDppstar();
	for(int i=0;i<(int)dpp->size();i++) {
	  ncount.push_back(dpp->getParticle(i));
	}
      }

      //...Lambda/Sigma
    } else if(id0 == id_lambda || id0 == id_lambdas
	|| id0 == id_sigma || id0 == id_sigmas) {

      if(iz0 == -1) {
	ncount.push_back(jamTable->find(3112));
	ParticleTable* table=jamTable->getSmstar();
	for(int i=0;i<(int)table->size();i++) {
	  ncount.push_back(table->getParticle(i));
	}
      } else if(iz0 == 0) {
	ncount.push_back(jamTable->find(3212));
	ParticleTable* table=jamTable->getLambda();
	for(int i=0;i<(int)table->size();i++) {
	  ncount.push_back(table->getParticle(i));
	}
	table=jamTable->getS0star();
	for(int i=0;i<(int)table->size();i++) {
	  ncount.push_back(table->getParticle(i));
	}
      } else if(iz0 == 1) {
	ncount.push_back(jamTable->find(3222));
	ParticleTable* table=jamTable->getSpstar();
	for(int i=0;i<(int)table->size();i++) {
	  ncount.push_back(table->getParticle(i));
	}
      }

      //...Xi*
    } else if(id0 == id_xi || id0 == id_xis) {

      if(iz0 == -1) {
	ncount.push_back(jamTable->find(3312));
	ParticleTable* table=jamTable->getXmstar();
	for(int i=0;i<(int)table->size();i++) {
	  ncount.push_back(table->getParticle(i));
	}
      } else if(iz0 == 0) {
	ncount.push_back(jamTable->find(3322));
	ParticleTable* table=jamTable->getX0star();
	for(int i=0;i<(int)table->size();i++) {
	  ncount.push_back(table->getParticle(i));
	}
      }

    } else {
      ncount.push_back(pa);
    }

  } else {
    ncount.push_back(pa);
  }

  if(ncount.size()==0) {
    cout << "SampleMass:jamexpa no particles ? " << kf0 << " id0= " << id0
      << " iz= "<< iz0
      << endl;
    exit(1);
  }

  return ncount;
}

void SampleMass::quarkContent(int id, std::array<int,3>& iq)
{
  int idabs = abs(id);
  iq[0]=(idabs/1000)  % 10;
  iq[1]=(idabs/100)   % 10;
  iq[2]=(idabs/10)    % 10;
  //int  iq1=idabs         % 10;      // spin

  if(iq[0]==0 && iq[1] != iq[2]) {
    if (iq[1]%2 == 1) std::swap( iq[1], iq[2]);
    if (id > 0) iq[2] = -iq[2];
    else {
      iq[1] = -iq[1];
    }
  } else if(iq[0] ==0) {
    int id2=iq[1]*10 + iq[2];
    if (iq[1] < 3 || id2 == 33) {
      iq[1] = (rndm->flat() < 0.5) ? 1 : 2;
      // eta and eta' can also be s sbar.
      if (id2 == 22 && rndm->flat() < fracEtass)  {iq[1] = 3, iq[2]=-3;}
      if (id2 == 33 && rndm->flat() < fracEtaPss) {iq[1] = 3, iq[2]=-3;}
    }
    iq[2] = -iq[1];

    // anti-baryon
  } else if(id < 0) {
    iq[0]= -iq[0];
    iq[1]= -iq[1];
    iq[2]= -iq[2];
  }
}

void SampleMass::diffractive(double fac)
{
  vector<ParticleDataEntryPtr> pcount1 = jamexpa(pout[0],Id[0],iD1,iZ1);
  vector<ParticleDataEntryPtr> pcount2 = jamexpa(pout[1],Id[1],iD2,iZ2);
  int n1 = pcount1.size();
  int n2 = pcount2.size();
  int kfsign1 = Id[0] > 0 ? 1 : -1;
  int kfsign2 = Id[1] > 0 ? 1 : -1;

  for(int ik1=0;ik1<n1;ik1++) 
    for(int ik2=0;ik2<n2;ik2++) {
      double prob = computeProb(kfsign1,kfsign2,pcount1[ik1],pcount2[ik2]);
      int id1 = (pcount1[ik1]->id())*kfsign1;
      int id2 = (pcount2[ik2]->id())*kfsign2;
      collPair.push_back(CollPair(pcount1[ik1],pcount2[ik2],id1,id2,prob*fac));
    }
}

void SampleMass::quarkExchangeProcess(int id1, int id2, double fac)
{
  std::array<int,3> iq1, iq2;
  quarkContent(id1,iq1);
  quarkContent(id2,iq2);

  vector<pair<int,int> > qpair;
  qpair.clear();

  // quark exchange process.
  for(int i=0; i<3;i++) {
    if(iq1[i]==0) continue;
    for(int j=0; j<3;j++) {
      if(iq2[j]==0) continue;
      if(iq1[i]==iq2[j]) continue;
      if(iq1[i]*iq2[j] < 0) continue;
      qpair.push_back(make_pair(i,j));
    }
  }

  if(qpair.size()==0) return;

  // chose exchange quark pair randomly
  int iq=rndm->flat()*qpair.size();
  int i1 = qpair[iq].first;
  int i2 = qpair[iq].second;
  array<int,3> kq1=iq1;
  array<int,3> kq2=iq2;
  kq1[i1]=iq2[i2];
  kq2[i2]=iq1[i1];

  int q1=kq1[1];
  int qq1=kq1[2];
  int q2=kq2[1];
  int qq2=kq2[2];
  // find diquark
  if(iq1[0] !=0) {
    int j=0;
    int jq1[2]={0,0};
    for(int i=0;i<3;i++) {
      if(i != i1) jq1[j++]=iq1[i];
    }
    int spin=3;
    if(jq1[0] != jq1[1])  if(rndm->flat() <0.5) spin=1;
    qq1= 1000*max(abs(jq1[0]) ,abs(jq1[1]))+ 100*min(abs(jq1[0]),abs(jq1[1]))+spin;
    if(Id[0] <0) qq1 *= -1;
    q1=iq2[i2];
  }
  if(iq2[0] !=0) {
    int j=0;
    int jq2[2]={0,0};
    for(int i=0;i<3;i++) {
      if(i != i2) jq2[j++]=iq2[i];
    }
    int spin=3;
    if(jq2[0] != jq2[1])  if(rndm->flat() <0.5) spin=1;
    qq2= 1000*max(abs(jq2[0]) ,abs(jq2[1]))+ 100*min(abs(jq2[0]),abs(jq2[1]))+spin;
    if(Id[1] <0) qq1 *= -1;
    q2=iq1[i1];
  }

  FlavContainer flav1(q1);
  FlavContainer flav2(qq1);
  int id3=0;
  do {
    id3 = flavSel->combine(flav1, flav2);
  }while(id3==0);


  FlavContainer flav3(q2);
  FlavContainer flav4(qq2);
  int id4=0;
  do {
    id4 = flavSel->combine(flav3, flav4);
  } while(id4==0);

  ParticleDataEntryPtr p3 = jamTable->find(id3);
  ParticleDataEntryPtr p4 = jamTable->find(id4);

  bool error=false;
  if(p3==0 ) {
    error=true;
    cout << "quark exchange wrong id3 id3= "<< id3 << " p3= "<< p3<<endl;
  }
  if(p4==0 ) {
    error=true;
    cout << "quark exchange wrong id4 id4= "<< id4 << " p4= "<< p4<<endl;
  }
  if(p3 !=0 && p4 != 0) {
    int ch0=pout[0]->chargeType(Id[0]) + pout[1]->chargeType(Id[1]);
    int ch1=p3->chargeType(id3) + p4->chargeType(id4);
    if(ch0 != ch1) {
      cout << "quark exchange charge does not conserve ch0= "<< ch0 << " ch1= "<< ch1<<endl;
      error=true;
    }
  }

  if(error) {
    cout << " i1= "<< i1 << " i2= "<<i2<<endl;
    cout << " iq1= "<< iq1[0] << " " << iq1[1] << "  "<< iq1[2]  <<endl;
    cout << " iq2= "<< iq2[0] << " " << iq2[1] << "  "<< iq2[2]  <<endl;
    cout << "q1 = "<< q1 <<  "  qq1= "<< qq1 <<endl;
    cout << "q2 = "<< q2 <<  "  qq2= "<< qq2 <<endl;
    cout << "id1= "<< Id[0] << " id2=  "<< Id[1] << " id3= "<< id3 << " id4= "<< id4<<endl;
   exit(1);
  }

  int iz3=p3->chargeType(id3)/3;
  int iz4=p4->chargeType(id4)/3;
  int pd3=jamTable->pid(id3);
  int pd4=jamTable->pid(id4);

  vector<ParticleDataEntryPtr> pcount3=jamexpa(p3,id3,pd3,iz3);
  vector<ParticleDataEntryPtr> pcount4=jamexpa(p4,id4,pd4,iz4);

  int n3 = pcount3.size();
  int n4 = pcount4.size();
  int kfsign1 = id3 > 0 ? 1 : -1;
  int kfsign2 = id4 > 0 ? 1 : -1;
  for(int ik1=0;ik1<n3;ik1++) 
    for(int ik2=0;ik2<n4;ik2++) {
      double prob = computeProb(kfsign1,kfsign2,pcount3[ik1],pcount4[ik2]);
      int ido1 = (pcount3[ik1]->id())*kfsign1;
      int ido2 = (pcount4[ik2]->id())*kfsign2;
      collPair.push_back(CollPair(pcount3[ik1],pcount4[ik2],ido1,ido2,fac*prob));
      cout << " prob= "<< prob << " fac= "<< fac<<endl;
  }
      cin.get();

}

void SampleMass::quarkExchangeProcess2(int id1, int id2,double fac)
{
  vector<pair<int,int> > qpair;

  int ifla1, iflb1, ifla2,iflb2;
  array<int,2> iq1,iq2;
  int ntry=0;
  do {
  hadronContent->findFlavor(id1,ifla1,iflb1);
  hadronContent->findFlavor(id2,ifla2,iflb2);
  iq1={ifla1,iflb1};
  iq2={ifla2,iflb2};
  bool isDiq1 = (abs(ifla1) < 10 && abs(iflb1) <10) ? false: true;
  bool isDiq2 = (abs(ifla2) < 10 && abs(iflb2) <10) ? false: true;

  // exclude anti-baryon-baryon collision.
  if(isDiq1 && isDiq2 && ifla1*ifla2 < 0) return;

  qpair.clear();
  // quark exchange process.
  for(int i=0; i<2;i++) {
    int iqa=abs(iq1[i]);
    for(int j=0; j<2;j++) {
      int iqb=abs(iq2[j]);
      if(iq1[i]==iq2[j]) continue;
      if(iqa>10 || iqb>10) continue;
      //if(iqa>10 && iqb>10) continue;
      //if(iqa<10 && iqb>10 && iq1[i]*iq2[j] >  0) continue;
      //if(iqa>10 && iqb<10 && iq1[i]*iq2[j] >  0) continue;
      if(iqa<10 && iqb<10 && iq1[i]*iq2[j] <  0) continue;
      qpair.push_back(make_pair(i,j));
    }
  }

  if(++ntry>30) {
    //cout << "SampleMass::quarkExchangeProcess2 no flavor exchange? "
    //  << " id1= "<< id1 << " id2= "<< id2<<endl;
    return;
  }

  } while (qpair.size()==0);

  int nqpair=qpair.size();

  // chose exchange quark pair randomly
  int iq=0;
  if(nqpair==1) {
    int i1 = abs(qpair[0].first);
    int i2 = abs(qpair[0].second);
    if(i1==3 || i2 ==3 ) fac = 0.1;
    if(i1>10 || i2> 10 ) fac = 0.01;
  } else if(nqpair==2) {
    double prob[2]={1.0,1.0};
    for(int i=0;i<2;++i) {
      int i1 = abs(qpair[i].first);
      int i2 = abs(qpair[i].second);
      if(i1==3 || i2 ==3 ) prob[i] = 0.2;
      if(i1>10 || i2> 10 ) prob[i] = 0.01;
    }
    double xq=rndm->flat()*(prob[0]+prob[1]);
    if(xq-prob[0] <= 0.0) iq=0;
    else iq=1;
  } else {

    cout << "quarkExchangeProcess2 qpair.size= "<< nqpair<<endl;
    cout << " iq1= "<< iq1[0] << " " << iq1[1] <<endl;
    cout << " iq2= "<< iq2[0] << " " << iq2[1] <<endl;
    exit(1);
  }

  int i1 = qpair[iq].first;
  int i2 = qpair[iq].second;
  array<int,2> kq1=iq1;
  array<int,2> kq2=iq2;
  kq1[i1]=iq2[i2];
  kq2[i2]=iq1[i1];

  // find hadron id3.
  FlavContainer flav1(kq1[0]);
  FlavContainer flav2(kq1[1]);
  int id3=0;
  int itry=0;
  do {
    id3 = flavSel->combine(flav1, flav2);
    if(itry++>100) {
      cout << "SampleMass::quarkExchangeProcess2 infinite loop? kq1[0]= "<< kq1[0]<< " kq1[1]= "<< kq1[1]<<endl;
    cout << " iq1= "<< iq1[0] << " " << iq1[1] <<endl;
    cout << " iq2= "<< iq2[0] << " " << iq2[1] <<endl;
    cout << " kq1= "<< kq1[0] << " " << kq1[1] <<endl;
    cout << " kq2= "<< kq2[0] << " " << kq2[1] <<endl;
    cout << " id1= "<< id1 << " id2= " << id2 <<endl;
    //cout << " isDiq1= "<< isDiq1 << " isDiq2= "<< isDiq2 << " id1*id2= "<< id1*id2 <<endl;
      exit(1);
    }
  }while(id3==0);


  // find hadron id4.
  FlavContainer flav3(kq2[0]);
  FlavContainer flav4(kq2[1]);
  int id4=0;
  itry=0;
  do {
    id4 = flavSel->combine(flav3, flav4);
    if(itry++>100) {
      cout << "SampleMass:quarkExchangeProcess2 infinite loop? kq2[0]= "<< kq2[0]<< " kq2[1]= "<< kq2[1]<<endl;
    cout << " iq1= "<< iq1[0] << " " << iq1[1] <<endl;
    cout << " iq2= "<< iq2[0] << " " << iq2[1] <<endl;
    cout << " kq1= "<< kq1[0] << " " << kq1[1] <<endl;
    cout << " kq2= "<< kq2[0] << " " << kq2[1] <<endl;
    cout << " id1= "<< id1 << " id2= " << id2 <<endl;
    //cout << " isDiq1= "<< isDiq1 << " isDiq2= "<< isDiq2 << " id1*id2= "<< id1*id2 <<endl;
      exit(1);
    }
  } while(id4==0);

  // find hadrons.
  ParticleDataEntryPtr p3 = jamTable->find(id3);
  ParticleDataEntryPtr p4 = jamTable->find(id4);

  bool error=false;
  if(p3==0 ) {
    error=true;
    cout << "2 quark exchange wrong id3 id3= "<< id3 << " p3= "<< p3<<endl;
  }
  if(p4==0 ) {
    error=true;
    cout << "2 quark exchange wrong id4 id4= "<< id4 << " p4= "<< p4<<endl;
  }
  if(p3 !=0 && p4 != 0) {
    int ch0=pout[0]->chargeType(Id[0]) + pout[1]->chargeType(Id[1]);
    int ch1=p3->chargeType(id3) + p4->chargeType(id4);
    if(ch0 != ch1) {
      cout << "quarkExchangeProcess2::quark exchange charge does not conserve ch0= "<< ch0 << " ch1= "<< ch1<<endl;
      error=true;
    }
  }

  if(error) {
    cout << " i1= "<< i1 << " i2= "<<i2<<endl;
    cout << " iq1= "<< iq1[0] << " " << iq1[1] <<endl;
    cout << " iq2= "<< iq2[0] << " " << iq2[1] <<endl;
    cout << " kq1= "<< kq1[0] << " " << kq1[1] <<endl;
    cout << " kq2= "<< kq2[0] << " " << kq2[1] <<endl;
    cout << "id1= "<< Id[0] << " id2=  "<< Id[1] << " id3= "<< id3 << " id4= "<< id4<<endl;
   exit(1);
  }

  int iz3=p3->chargeType(id3)/3;
  int iz4=p4->chargeType(id4)/3;
  int pd3=jamTable->pid(id3);
  int pd4=jamTable->pid(id4);

  vector<ParticleDataEntryPtr> pcount3=jamexpa(p3,id3,pd3,iz3);
  vector<ParticleDataEntryPtr> pcount4=jamexpa(p4,id4,pd4,iz4);

  int n3 = pcount3.size();
  int n4 = pcount4.size();
  int kfsign1 = id3 > 0 ? 1 : -1;
  int kfsign2 = id4 > 0 ? 1 : -1;
  for(int ik1=0;ik1<n3;ik1++) 
    for(int ik2=0;ik2<n4;ik2++) {
      double prob = computeProb(kfsign1,kfsign2,pcount3[ik1],pcount4[ik2]);
      int ido1 = (pcount3[ik1]->id())*kfsign1;
      int ido2 = (pcount4[ik2]->id())*kfsign2;
      collPair.push_back(CollPair(pcount3[ik1],pcount4[ik2],ido1,ido2,fac*prob));
  }

}

void SampleMass::quarkAnnProcess(int id1,int id2,double fac)
{
  std::array<int,3> iq1, iq2;
  quarkContent(id1,iq1);
  quarkContent(id2,iq2);

  vector<pair<int,int> > qpair;
  qpair.clear();

  // find q-qbar pair.
  for(int i=0; i<3;i++) {
    if(iq1[i]==0) continue;
    for(int j=0; j<3;j++) {
      if(iq2[j]==0) continue;
      if(iq1[i]+iq2[j]==0) {
	qpair.push_back(make_pair(i,j));
      }
    }
  }

  if(qpair.size()==0) return;

  //double probS=0.2;
  int i0=rndm->flat()*qpair.size();
  int i1 = qpair[i0].first;
  int i2 = qpair[i0].second;
  int qann=1;
  if(abs(iq1[i1])==1) {
    if(rndm->flat()< probS) qann=3;
    else qann=2;
  } else if(abs(iq1[i1])==2) {
    if(rndm->flat()< probS)  qann=3;
    else qann=1;
  } else if(abs(iq1[i1])==3) {
    if(rndm->flat()< 0.5) qann=1;
    else qann=2;
  } else {
    double x = rndm->flat();
    if(x< probS)  qann=3;
    else if(x< 0.5*(probS+1.0)) qann=1;
    else qann=2;
  }

  int j1=0, j2=0;
  int jq1[2]={0,0};
  int jq2[2]={0,0};
  int q1=0, q2=0;
  for(int i=0;i<3;i++) {
    if(i==i1) {
      q1 = iq1[i1] > 0 ? qann : -qann;
    } else if(iq1[i] !=0) {
      jq1[j1++]=iq1[i];
    }
    if(i==i2) {
      q2 = iq2[i2] > 0 ? qann : -qann;
    } else if(iq2[i] !=0) {
      jq2[j2++]=iq2[i];
    }
  }

  // make diquark
  int qq1=0, qq2=0;
  if(iq1[0] !=0) {
    int spin=3;
    if(jq1[0] != jq1[1])  if(rndm->flat() <0.5) spin=1;
    qq1= 1000*max(abs(jq1[0]) ,abs(jq1[1]))+ 100*min(abs(jq1[0]),abs(jq1[1]))+spin;
    if(Id[0] <0) qq1 *= -1;
  } else {
    qq1=jq1[0];
  }

  // find hadron id
  FlavContainer flav1(q1);
  FlavContainer flav2(qq1);
  int id3=0;
  do {
    id3 = flavSel->combine(flav1, flav2);
  }while(id3==0);

  // same for particle 2.
  if(iq2[0] !=0) {
    int spin=3;
    if(jq2[0] != jq2[1])  if(rndm->flat() <0.5) spin=1;
    qq2= 1000*max(abs(jq2[0]) ,abs(jq2[1]))+ 100*min(abs(jq2[0]),abs(jq2[1]))+spin;
    if(Id[1]<0) qq2 *= -1;
  } else {
    qq2=jq2[0];
  }

  FlavContainer flav3(q2);
  FlavContainer flav4(qq2);
  int id4=0;
  do {
    id4 = flavSel->combine(flav3, flav4);
  } while(id4==0);

  ParticleDataEntryPtr p3 = jamTable->find(id3);
  ParticleDataEntryPtr p4 = jamTable->find(id4);

  bool error=false;
  if(p3==0 ) {
    error=true;
    cout << " wrong id3 id3= "<< id3 << " p3= "<< p3<<endl;
  }
  if(p4==0 ) {
    error=true;
    cout << " wrong id4 id4= "<< id4 << " p4= "<< p4<<endl;
  }
  if(p3 !=0 && p4 != 0) {
    int ch0=pout[0]->chargeType(Id[0]) + pout[1]->chargeType(Id[1]);
    int ch1=p3->chargeType(id3) + p4->chargeType(id4);
    if(ch0 != ch1) {
      cout << " charge does not conserve ch0= "<< ch0 << " ch1= "<< ch1<<endl;
      error=true;
    }
  }

  if(error) {
    cout << " i1= "<< i1 << " i2= "<<i2<<endl;
    cout << " iq1= "<< iq1[0] << " " << iq1[1] << "  "<< iq1[2]  <<endl;
    cout << " iq2= "<< iq2[0] << " " << iq2[1] << "  "<< iq2[2]  <<endl;
    cout << " ann= "<< qann <<endl;;
    cout << "q1 = "<< q1 << " jq1= "<< jq1[0] << " "<< jq1[1] << "  qq1= "<< qq1 <<endl;
    cout << "q2 = "<< q2 << " jq2= "<< jq2[0] << " "<< jq2[1] << "  qq2= "<< qq2 <<endl;
    cout << "id1= "<< Id[0] << " id2=  "<< Id[1] << " id3= "<< id3 << " id4= "<< id4<<endl;
   exit(1);
  }

  int iz3=p3->chargeType(id3)/3;
  int iz4=p4->chargeType(id4)/3;
  int pd3=jamTable->pid(id3);
  int pd4=jamTable->pid(id4);

  vector<ParticleDataEntryPtr> pcount3=jamexpa(p3,id3,pd3,iz3);
  vector<ParticleDataEntryPtr> pcount4=jamexpa(p4,id4,pd4,iz4);

  int n3 = pcount3.size();
  int n4 = pcount4.size();
  int kfsign1 = id3 > 0 ? 1 : -1;
  int kfsign2 = id4 > 0 ? 1 : -1;
  for(int ik1=0;ik1<n3;ik1++) 
    for(int ik2=0;ik2<n4;ik2++) {
      double prob = computeProb(kfsign1,kfsign2,pcount3[ik1],pcount4[ik2]);
      if(prob>0.0) {
        int ido1 = (pcount3[ik1]->id())*kfsign1;
        int ido2 = (pcount4[ik2]->id())*kfsign2;
        collPair.push_back(CollPair(pcount3[ik1],pcount4[ik2],ido1,ido2,fac*prob));

        //cout << " kf1= " << Id[0] << " kf2= " << Id[1];
        //cout << "  " <<pout[0]->name(Id[0])  << " " << pout[1]->name(Id[1]);
        //cout << "  " <<pcount3[ik1]->name(ido1)  << " " << pcount4[ik2]->name(ido2) << endl;
      }
  }

}

double SampleMass::computeProb(int sgn1,int sgn2,ParticleDataEntryPtr p1, ParticleDataEntryPtr p2)
{
  int id3=p1->id()*sgn1;
  int id4=p2->id()*sgn2;
  // exclude elastic collision.
  if((p1->mWidth()<1e-5 && Id[0]==id3) && (p2->mWidth()<1e-5 && Id[1]==id3)) return 0.0;

  // check energy.
  double mmin1= p1->mWidth()>1e-5 ? p1->mMin() : p1->m0();
  double mmin2= p2->mWidth()>1e-5 ? p2->mMin() : p2->m0();

  double mm1=p1->mMin();
  double mm2=p2->mMin();
  if((mmin1!=mm1 || mmin2!=mm2)||(mm1 > p1->m0() || mm2> p2->m0())) {
    cout << "SampleMass:computeProb mmin strange "<<endl;
    cout << " id1= " << id3 << " width= "<< p1->mWidth()<< " mmin= "<< mm1 << " m0= "<< p1->m0()<<endl;
    cout << " id2= " << id4 << " width= "<< p2->mWidth()<< " mmin= "<< mm2 << " m0= "<< p2->m0()<<endl;
    exit(1);
  }
  if(eCM < mmin1 + mmin2 + eKinMin) return 0.0;

  // compute Clepsch-Gordan coefficient for the incomping particle pair.
  pair<int,int> iso3 = isospin(id3);
  pair<int,int> iso4 = isospin(id4);
  int j3=iso3.first, j4=iso4.first;
  int jz3 = iso3.second, jz4=iso4.second;

  // cofcgi(j1,j2,j,m1,m2,m) = <j1,j2;m1,m2|j,m>
  double faci=0.0;
  for(int j=jtMin,i=0; j<=jtMax;j +=2,i++) {
   double coef = ClebschGordan::cofcgi( j3,j4, j,jz3,jz4, jMz);
   faci += cofCG12[i]*coef*coef;
  }
  if(jz3+jz4 != jMz || faci==0.0) return 0.0;

  double pbw=0.0;
  if(optProb==3) // use table
    pbw = probBW3(eCM,p1,p2);
  else if(optProb==0 || optProb==1)  // simplified method
    pbw = probBW1(eCM,p1,p2);
  else
    pbw = probBW2(eCM,p1,p2);

  /*
  cout << "ecm= "<< eCM<<  " id3= "<< p1->name() << " id4= "<< p2->name() <<endl;
  cout <<"bw1= "<< pbw << " BW2- "<< probBW2(eCM,p1,p2) <<endl;
    cout << " id1= " << id3 << " width= "<< p1->mWidth()<< " mmin= "<< mm1 << " m0= "<< p1->m0()<<endl;
    cout << " id2= " << id4 << " width= "<< p2->mWidth()<< " mmin= "<< mm2 << " m0= "<< p2->m0()<<endl;
  cin.get();
  */

  if(pbw <= 0.0) {
    cout << "computeProb pbw<0? "<<pbw<<endl;
    cout << " id1= " << id3 << " width= "<< p1->mWidth()<< " mmin= "<< mm1 << " m0= "<< p1->m0()<<endl;
    cout << " id2= " << id4 << " width= "<< p2->mWidth()<< " mmin= "<< mm2 << " m0= "<< p2->m0()<<endl;
    return 0.0;
  }

  double fac= id3 == id4 ? 0.5 : 1.0;

  // Spin factor
  int ispin1=p1->spinType();
  int ispin2=p2->spinType();

  // set matrix element for resonance production
  bool isRes3= p1->mWidth()>1e-5 ? true: false;
  bool isRes4= p2->mWidth()>1e-5 ? true: false;
  double elm=1.0;
  if((isRes1 || isRes2) && (isRes3 || isRes4)) elm=elementRes;

  return elm * faci * fac * ispin1*ispin2 * pbw;

}

// approximate BW integral.
double SampleMass::probBW1(double srt, ParticleDataEntryPtr p1, ParticleDataEntryPtr p2)
{
  if(optProb==0) {
    double em1=p1->m0();     // pole mass
    double em2=p2->m0();     // pole mass
    if(srt < em1+em2 + eKinMin) {
      em1=p1->mMin();
      em2=p2->mMin();
    }
    if(srt > em1 + em2 + eKinMin) return PCM(srt,em1,em2);
    return 0.0;
  }

  double em0[2], gam0[2],emin[2],emax[2], bw[2], ymin[2],ymax[2];
  int iex[2];
  ParticleDataEntryPtr pv[2];
  pv[0]=p1;
  pv[1]=p2;

  for(int jt=0; jt<2; jt++) {
    em0[jt]=pv[jt]->m0();     // pole mass
    gam0[jt]=pv[jt]->mWidth();
    emin[jt]=em0[jt];
    emax[jt]=emin[jt];
    iex[jt]=0;
    if(gam0[jt]> 1e-5) {
      iex[jt]=1;
      emin[jt]=pv[jt]->mMin();
    }
  }

  if(srt < emin[0] + emin[1] + eKinMin) return 0.0;

  for(int jt=0; jt<2; jt++)
  if(iex[jt] == 1) {
    emax[jt]=max(emin[jt],srt-emin[1-jt]-eKinMin);
  }
  if(emin[0] > emax[0] || emin[1] > emax[1]) return 0.0;

  double pf0=PCM(srt,emin[0],emin[1]);
  if(srt > em0[0]+em0[1]) pf0=PCM(srt,em0[0],em0[1]);

  for(int jt=0; jt<2; jt++) {
    bw[jt]=1.0;
    if(iex[jt] >=1) {
      ymin[jt]=atan((pow2(emin[jt])-pow2(em0[jt]))/(em0[jt]*gam0[jt]));
      ymax[jt]=atan((pow2(emax[jt])-pow2(em0[jt]))/(em0[jt]*gam0[jt]));
      bw[jt]=(ymax[jt]-ymin[jt])/M_PI;
    }
  }
  if(bw[0]<=0 || bw[1]<=0) return 0.0;

  return bw[0]*bw[1]*pf0;
}

// Use table.
double SampleMass::probBW3(double srt,ParticleDataEntryPtr pv1,ParticleDataEntryPtr pv2)
{
  double gam1=pv1->mWidth();
  double gam2=pv2->mWidth();
  if(gam1<1e-5 && gam2 <1e-5) {
    double em1=pv1->m0();     // pole mass
    double em2=pv2->m0();     // pole mass
    if(srt > em1 + em2 + eKinMin) return PCM(srt,em1,em2);
    return 0.0;

  } else if (gam1 >= 1e-5 && gam2  < 1e-5) {
    return bwint(pv1, srt, pv2->m0());

  } else if (gam1 < 1e-5 && gam2  >= 1e-5) {
    return bwint(pv2, srt, pv1->m0());

  } else {
    ParticleDataEntryPtr p1 = isoParticle[pv1];
    ParticleDataEntryPtr p2 = isoParticle[pv2];
    if(p1==0 || p2==0) {
      cout << "SampleMass::probBW3 pv1= "<< pv1->id() << " pv2= "<< pv2->id()<<endl;
      exit(1);
    }
    // integral reading from the table.
    return BWint[make_pair(p1,p2)]->getBW(srt);
  }

}

// compute BW integrals
double SampleMass::probBW2(double srt,ParticleDataEntryPtr pv1,ParticleDataEntryPtr pv2)
{
  double gam1=pv1->mWidth();
  double gam2=pv2->mWidth();
  if(gam1<1e-5 && gam2 <1e-5) {
    double em1=pv1->m0();     // pole mass
    double em2=pv2->m0();     // pole mass
    if(srt > em1 + em2 + eKinMin) return PCM(srt,em1,em2);
    return 0.0;
  } else if (gam1 >= 1e-5 && gam2  < 1e-5) {
    return bwint(pv1, srt, pv2->m0());
  } else if (gam1 < 1e-5 && gam2  >= 1e-5) {
    return bwint(pv2, srt, pv1->m0());
  } else {
    return bwint2(pv1,pv2,srt);
  }

}

double SampleMass::BreitWigner(ParticleDataEntryPtr p,double emd)
{
  double em0=p->m0();
  double gam = optWidth > 0 ? jamTable->totalWidth(p,emd) : p->mWidth();
  if(optBW==2)  {
    return 2.0/M_PI*emd*emd*gam/(pow2(emd*emd-em0*em0)+pow2(emd*gam));
  } else {
    return 2.0/M_PI*emd*em0*gam/(pow2(emd*emd-em0*em0)+pow2(em0*gam));
  }
}

// Integration of Breit-Wigner 
double SampleMass::bwint(ParticleDataEntryPtr p, double srt,double em2)
{
  double em0=p->m0();
  double gamr=p->mWidth();
  double emin=p->mMin();
  if(srt < emin + em2 + eKinMin) return 0.0;

  double emax=srt-em2;
  //double gr=em0*gamr;
  //double ymax=2*atan((emax*emax-em0*em0)/gr);
  //double ymin=2*atan((emin*emin-em0*em0)/gr);
  double ymax=2*atan(2*(emax-em0)/gamr);
  double ymin=2*atan(2*(emin-em0)/gamr);
  double dy=ymax-ymin;

  if(dy<=0.0) return 0.0;

  double ds=0.0;
  for(int i=0;i<NGP38;i++) {
    double y=ymin+xg38f[i]*dy;
    //double emd=sqrt(tan(0.5*y)*gr+ em0*em0);
    //double dc=gr/(1+cos(y))/(2*emd);
    double emd=tan(0.5*y)*gamr/2 + em0;
    double dc= gamr/( 0.25*gamr*gamr + pow2(emd - em0) );
    double bw=BreitWigner(p,emd);
    double pf=PCM(srt,emd,em2);
    double dw=wg38f[i]*dy;
    ds += bw*pf*dw/dc;
  }

  return ds/bwNorm[isoParticle[p]];

}

// Integration of two-Breit-Wigner 
double SampleMass::bwint2(ParticleDataEntryPtr p1,ParticleDataEntryPtr p2,double srt)
{
  double emin1=p1->mMin();
  double emin2=p2->mMin();
  if(srt < emin1 + emin2 + eKinMin) return 0.0;

  double emax1=srt-emin2;
  double emax2=srt-emin1;
  if(srt<=emin1+emin2) return 0.0;
  if(emax1<=emin1) return 0.0;
  if(emax2<=emin2) return 0.0;

  double ds=0.0;
  double em0=p1->m0();
  double gamr=p1->mWidth();
  double emax=srt-emin2;
  //double gr=em0*gamr;
  //double ymax=2*atan((emax*emax-em0*em0)/gr);
  //double ymin=2*atan((emin1*emin1-em0*em0)/gr);
  double ymax=2*atan(2*(emax-em0)/gamr);
  double ymin=2*atan(2*(emin1-em0)/gamr);

  double dy=ymax-ymin;
  if(dy<=0.0) return 0.0;

  for(int i=0;i<NGP38;i++) {
    double y=ymin+xg38f[i]*dy;
    //double emd=sqrt(tan(0.5*y)*gr+ em0*em0);
    //double dc=gr/(1+cos(y))/(2*emd);
    double emd=tan(0.5*y)*gamr/2 + em0;
    double dc= gamr/( 0.25*gamr*gamr + pow2(emd - em0) );
    double bw=BreitWigner(p1,emd);
    double bw2=bwint(p2,srt,emd);
    double dw=wg38f[i]*dy;
    ds += bw*bw2*dw/dc;
  }

  return ds/bwNorm[isoParticle[p1]];

  //------------------------------------------------------
  // This is only for check
  ds=0.0;
  em0=p2->m0();
  gamr=p2->mWidth();
  emax=srt-emin1;
  ymax=2*atan(2*(emax-em0)/gamr);
  ymin=2*atan(2*(emin2-em0)/gamr);
  dy=ymax-ymin;
  if(dy<=0.0) return 0.0;

  for(int i=0;i<NGP38;i++) {
    double y=ymin+xg38f[i]*dy;
    double emd=tan(0.5*y)*gamr/2 + em0;
    double dc= gamr/( 0.25*gamr*gamr + pow2(emd - em0) );
    double bw=BreitWigner(p2,emd);
    double bw2=bwint(p1,srt,emd);
    double dw=wg38f[i]*dy;
    ds += bw*bw2*dw/dc;
  }

}

double SampleMass::computeNorm(ParticleDataEntryPtr& p)
{
  double em0=p->m0();
  double gamr=p->mWidth();
  if(gamr < 1e-5) return 1.0;
  double emin=p->mMin();
  double ymin=2*atan(2*(emin-em0)/gamr);
  double ymax=M_PI;
  double dy = ymax - ymin;
  if(dy<=0.0) {
    cout << "dy<0? "<< dy << " id= "<< p->id() <<endl;
    exit(1);
  }

  double ds=0.0;
  for(int i=0;i<NGP38;i++) {
    double y=ymin+xg38f[i]*dy;
    double emd=tan(0.5*y)*gamr/2 + em0;
    double dc= gamr/( 0.25*gamr*gamr + pow2(emd - em0) );
    double bw=BreitWigner(p,emd);
    ds += bw*wg38f[i]*dy/dc;
  }
  return ds;

}

pair<int,int> SampleMass::isospin(int id)
{
  //static int charge[]={0,-1,2,-1,2,-1,2};
  int idabs = abs(id);
  int iq1=(idabs/1000)  % 10;
  int iq2=(idabs/100)   % 10;
  int iq3=(idabs/10)    % 10;
  //int  iq1=idabs  % 10;      // spin

  int sgn = id > 0 ? 1 : -1;
  //int ch=charge[iq1]+charge[iq2]+charge[iq3];
  int idnew=iq1*100+iq2*10+iq3;
  int it=0, iz=0;
  switch (idnew) {
    case 221: {// p or delta+
      int pid=jamTable->pid(id);
      if(pid==id_nucl || pid==id_nucls) {
	it=1; iz=sgn;
      } else if(pid==id_delt || pid==id_delts) {
	it=3; iz=sgn;
      } else {
	cout << " something is wrong id= "<< id<<endl;
	exit(1);
      }
      break;
	      }

    case 211:  {// n or delta0
      int pid=jamTable->pid(id);
      if(pid==id_nucl || pid==id_nucls) {
	it=1; iz=-sgn;
      } else if(pid==id_delt || pid==id_delts) {
	it=3; iz=-sgn;
      } else {
	cout << " something is wrong id= "<< id<<endl;
	exit(1);
      }
      break;
	       }
    case 222: it=3; iz=3*sgn;break;// Delta++
    case 111: it=3; iz=-3*sgn;break;// Delta-

    case 213: case 312: it=0; iz=0;break; // Lambda
    case 311: it=2; iz=-2*sgn;break; // Sigma-
    case 321: it=2; iz=0;break; // Sigma0
    case 322: it=2; iz=2*sgn;break; // Sigma+
    case 331: it=1; iz=-sgn;break; // Xi-
    case 332: it=1; iz=sgn;break; // Xi0
    case 333: it=0; iz=0;break; // Omega

    case 21: it=2;iz=2*sgn;break;   // pi+/-
    case 11: it=2;iz=0;break;              // pi0
    case 22:case 33:case 44: case 55: it=0;iz=0;break;
    case 31: it=1;iz=-sgn;break;   // K0
    case 32: it=1;iz=sgn;break; // K+-
    case 41: it=1;iz=-sgn;break;
    case 42: it=1;iz=sgn;break;
    default:
     cout << "unknown code id= "<<id<<endl;
     exit(1);
  }

  return {it,iz};

}

void SampleMass::checkCG(int sgn1,int sgn2,ParticleDataEntryPtr p1, ParticleDataEntryPtr p2)
{
  pair<int,int> iso1 = isospin(Id[0]);
  pair<int,int> iso2 = isospin(Id[1]);
  int j1=iso1.first, j2=iso2.first;
  int jz1 = iso1.second, jz2=iso2.second;

  int id3=p1->id()*sgn1;
  int id4=p2->id()*sgn2;
  pair<int,int> iso3 = isospin(id3);
  pair<int,int> iso4 = isospin(id4);
  int j3=iso3.first, j4=iso4.first;
  int jz3 = iso3.second, jz4=iso4.second;

    cout << "computeProb jmz= "<< jz3+jz4 << " jMz= "<< jMz<< endl;
    cout << pout[0]->name() << " + " << pout[1]->name() << " -> "
         << p1->name() << " + " << p2->name()
	 <<endl;
    cout << " id1= "<< Id[0] << " id2= "<< Id[1]
      << " id3= "<< id3 << " id4= "<< id4
      <<endl;
    cout << "j1= "<< j1 << " j2= "<< j2 <<endl;
    cout << "jz1= "<< jz1 << " jz2= "<< jz2<<endl;
    cout << "j3= "<< j3 << " j4= "<< j4 <<endl;
    cout << "jz3= "<< jz3 << " jz4= "<< jz4<<endl;
    double cgi=0.0;
    for(int j=jtMin,i=0; j<=jtMax;j +=2,i++) {
     double c12 = ClebschGordan::cofcgi( j1,j2, j,jz1,jz2, jMz);
     double coef = ClebschGordan::cofcgi( j3,j4, j,jz3,jz4, jMz);
     cgi += coef*coef*cofCG12[i];
      cout << "j= "<< j << " jmz= "<< jMz
	<< " cg12= "<< cofCG12[i] << " cg34= "<< coef*coef << " c12= "<< c12*c12 <<endl;
    }
    cout << " cgi= "<< cgi<<endl;
}

void SampleMass::informChannel(double totsp,double srt)
{
    cout << "jamrmas2 totsp=0 " << totsp 
      << " n= " << collPair.size()
      << " srt= " << srt <<endl;
    cout << " kf1= " << Id[0] << " kf2= " << Id[1] << endl;
    cout << "  " <<pout[0]->name(Id[0])  << " " << pout[1]->name(Id[1]) << endl;
    cout << " m1= " << emr[0] << " m2= " << emr[1] << endl;


      int kfsign1 = Id[0] > 0 ? 1 : -1;
      int kfsign2 = Id[1] > 0 ? 1 : -1;

    int imin=-1;
    double mmin=10000.;
    int i=0;
    for(auto& cp : collPair) {
      ParticleDataEntryPtr p1=cp.particleA();
      ParticleDataEntryPtr p2=cp.particleB();
      if(Id[0]==p1->id() && Id[1]==p2->id()) continue;
      double mmin1= p1->mWidth()>1e-5 ? p1->mMin() : p1->m0();
      double mmin2= p2->mWidth()>1e-5 ? p2->mMin() : p2->m0();
      if(mmin1+mmin2<mmin) {
	mmin = mmin1+mmin2;
	imin=i;
      }
      i++;

      int sgn1= p1->hasAnti() ? kfsign1:1;
      int sgn2= p2->hasAnti() ? kfsign2:1;
      cout <<  p1->name(sgn1) 
	<< " "<< p2->name(sgn2) 
	<< " id1= "<< p1->id()
	<< " id2= "<< p2->id()
	<< " m1= " << mmin1
	<< " m2= " << mmin2
	<< " min1= " << p1->mMin()
	<< " mmin2= " << p2->mMin()
	<< " m01= " << p1->m0()
	<< " m02= " << p2->m0()
	<< " m1+m2= " << mmin1+mmin2+eKinMin
	<< " prob_min= " << collPair[imin].probability()
 	<< " prob= " << cp.probability()
        //<< " prob2= " << computeProb(kfsign1,kfsign2,p1,p2)
	<<endl;
    }

    ParticleDataEntryPtr p1=collPair[imin].particleA();
    ParticleDataEntryPtr p2=collPair[imin].particleB();
    double mmin1= p1->mWidth()>1e-5 ? p1->mMin() : p1->m0();
    double mmin2= p2->mWidth()>1e-5 ? p2->mMin() : p2->m0();
    cout <<  p1->name() 
	<< "  "<< p2->name() 
	<< " id1= "<< p1->id()
	<< " id2= "<< p2->id()
	<< " m1= " << mmin1
	<< " m2= " << mmin2
	<< " m1+m2= " << mmin1+mmin2+eKinMin
	<< " prob= " << collPair[imin].probability()
	<<endl;

     checkCG(kfsign1,kfsign2,p1, p2);
}

} // end namespace jam2
