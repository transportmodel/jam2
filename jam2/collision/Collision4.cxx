#include <vector>
#include <iterator>
#include <algorithm>
#include <cmath>
#include <cfloat>
#include <iostream>
#include <algorithm>

#include <jam2/collision/Collision4.h>
#include <jam2/collision/InterType.h>
#include <jam2/collision/DecayList.h>
#include <jam2/collision/WallList.h>
#include <jam2/xsection/XsecTable.h>

namespace jam2 {

using namespace std;

//  4: Stochastic method (local ensemble method). Small time step is needed.

void Collision4::clear()
{
  cascCell->clear();
  clearPlist();
}

void Collision4::init(const InitialCondition* initcnd)
{
  cascCell = new CascCell(settings);
  cascCell->init(initcnd);
  cascBox = cascCell->boxes();
}

Collision4::Collision4(Pythia8::Settings* s, JamParticleData* jp,CrossSection* in,Pythia8::Rndm* r)
	: Collision(s,jp,in,r)
{
  optBoxBoundary=settings->mode("Cascade:boxBoundary");  // =0: boxes at the edge are infinitely large  =1: finite box
  optColl=settings->mode("Cascade:optStochasticMethod");

}

Collision4::~Collision4()
{
  delete cascCell;
}

void Collision4::wallCollisionTime(EventParticle* p,int opt)
{
  Vec4 r=p->getR();
  Vec4 v=p->velocity(optPropagate);
  double xbox = v[1] >0.0 ? xBox/2 : -xBox/2;
  double ybox = v[2] >0.0 ? yBox/2 : -yBox/2;
  double zbox = v[3] >0.0 ? zBox/2 : -zBox/2;
  double tx = v[1] !=0.0 ? (xbox - r[1])/v[1] : 1e+10;
  double ty = v[2] !=0.0 ? (ybox - r[2])/v[2] : 1e+10;
  double tz = v[3] !=0.0 ? (zbox - r[3])/v[3] : 1e+10;
  //double tw = std::min({tx,ty,tz}) +1e-6;
  //return {tw + r[0],i};

  int i=1;
  double tw = tx;
  if(ty < tw) {tw = ty; i=2;}
  if(tz < tw) {tw = tz; i=3;}
  p->setTWall(tw);
  p->lastColl(i);
  return;

}

void Collision4::wallCollision(EventParticle* p)
{
  double t0=p->getT();
  double tw=p->tWall();
  tw = tw > 0.0 ? tw - 1e-5 : tw + 1e-5;
  p->updateR(tw,optPropagate);

         if(cascCell->boxPosition(p->getR()) == -1) {
	   cout << " after wall coll out side! r= "<< p->getR();
	 }

  // flip the momentum.
  int i=p->lastColl();
  double pi = p->getP(i);
  p->setP(-pi,i);

  return;

  if(tw < 0.0 && p->getT() < t0) {
    p->updateR(t0,optPropagate);
         if(cascCell->boxPosition(p->getR()) == -1) {
	   cout << " after wall coll out side2! r= "<< p->getR();
	 }
  }

}

// =========================================================================//
//...Purpose: to make collision list into collision predictor arrays.
void Collision4::makeCollisionList(double itime, double gtime)
{
  interList.clear();
  initialTime=itime;
  finalTime=gtime;
  setLambdas();

  // assign a box to a particle and check wall collision and decay.
  for(const auto& i1 : plist ) {

    // Periodic boundary condition.
  if(withBox==1) {
    Vec4 r = i1->getR();
    r[0] = r.e();
    r[1] = modulo(r.px() + xBox/2, xBox) - xBox/2;
    r[2] = modulo(r.py() + yBox/2, yBox) - yBox/2;
    r[3] = modulo(r.pz() + zBox/2, zBox) - zBox/2;
    i1->setR(r);

    // reflection by the wall.
  }else if(withBox==2) {
    wallCollisionTime(i1,1);
  }

    if(optBoxBoundary>0) {
    if(cascCell->boxPosition(i1->getR()) == -1) {
      cout << " makeCollisionList outside ? " << i1->getR()<<endl;
      exit(1);
    }
    }

    // find the box for this particle.
    CascBox *box = cascCell->box(i1->getR());
    i1->addBox(box);
  }

  plist.clear();

  // Loop over all boxes.
  for(const auto& box : cascBox) searchCollisionPair(box);

  predictedColl = cascCell->numberOfCollision();
  numberOfCollision=0;

}

void Collision4::searchCollisionPair(CascBox* box)
{
  auto& pl = box->getParticles();
  int nv=pl.size();
  if(nv==0) return;

  //double dt0 = settings->parm("Cascade:TimeStepSize");
  //double v = settings->parm("MeanField:stepVelocity");
  //double stime = settings->parm("MeanField:dtExpandStartTime");
  //double dt = dt0 + v*std::max(0.0,iniTime-stime);

  // compute dt and dV and factors for the collision probability.
  dtNow = finalTime - initialTime;
  volNow = cascCell->volume(initialTime,*box);
  //facSig=dtNow/volNow*MB2FMSQ/overSample;  // 0.1 is the conversion factor from mb to fm^2
  facSig=1.0/volNow*MB2FMSQ/overSample;  // 0.1 is the conversion factor from mb to fm^2
  if(optColl==1) facSig *= nv-1;
  else if(optColl==2) facSig *= (nv-1)/2.0;

  vector<EventParticle*> part;
  for(auto& p : pl) part.push_back(p);
  vector<bool> iscoll(nv,false);

  // only decay
  if(nv==1) {
    if(decayOn) checkDecay(part,iscoll);
    return;
  }

  // pick randomly nv/2 collision pair.
  if(optColl==1) {

  int ncol=nv/2;
  vector<int> colp(nv);
  for(int i=0;i<nv;i++) colp[i]=i;
  vector<pair<int,int> > colpair(ncol);
  int nn=nv;
  for(int i=0;i<ncol;i++) {
    int ic= rndm->flat()*nn;
    colpair[i].first=colp[ic];
    colp.erase(cbegin(colp)+ic);
    nn--;
    ic= rndm->flat()*nn;
    colpair[i].second=colp[ic];
    colp.erase(cbegin(colp)+ic);
    nn--;
  }

  // loop over collision pair.
  for(auto& cp : colpair) {
    hit22(part[cp.first], part[cp.second]);
  }

  // pick randomly nv collision pair.
  } else if(optColl==2) {

    int ncol=nv;
    int nn=nv*(nv-1)/2;
    vector<pair<int,int> > clpair(nn),colpair(ncol);
    int ic=0;
    for(int i=0;i<nv;i++) 
    for(int j=i+1;j<nv;j++) {
      clpair[ic++]={i,j};
    }
    for(int i=0;i<ncol;i++) {
      int j= rndm->flat()*nn;
      colpair[i]=clpair[j];
    }

    // loop over the selected collision pair.
    for(auto& cp : colpair) {
      if(iscoll[cp.first] || iscoll[cp.second]) continue;
      if(hit22(part[cp.first], part[cp.second])) {
        iscoll[cp.first]=true;
        iscoll[cp.second]=true;
      }
    }

  // Search for all possible collision pair.
  } else {

    for(int i1=0;i1<nv;i1++) {
      if(iscoll[i1]) continue;
      for(int i2=i1+1;i2<nv;i2++) {
        if(iscoll[i2]) continue;
        if(hit22(part[i1],part[i2])) {
         iscoll[i1]=true;
         iscoll[i2]=true;
         break;
        }
      }
    }

  }
  
  if(decayOn) checkDecay(part,iscoll);

}

void Collision4::checkDecay(vector<EventParticle*>& part, vector<bool>& iscoll)
{
  int nv=part.size();
  for(int i=0;i<nv;i++) {
    if(iscoll[i]) continue;
    ParticleDataEntryPtr pd=part[i]->getParticleDataEntry();
    if ( !pd->canDecay() || !pd->mayDecay() ) continue;
    double m=part[i]->getMass();
    double e=part[i]->getE0();
    double width = jamParticleData->totalWidth(pd,m)/Pythia8::HBARC*m/e;
    double P12=exp(-dtNow*width);
    double r = rndm->flat();
    if(r > P12) {
      interList.push_back(new DecayList(part[i],part[i]->getT()-log(r)/width));
      iscoll[i]=true;
    }
  }
}

bool Collision4::hit22(EventParticle* i1, EventParticle* i2)
{
  if(i1->getNColl()*i2->getNColl() == 1 ) return false;
  int icltyp = collisionType(i1, i2);
  if(icltyp==0) return false;
  Vec4 p1=i1->getP();
  Vec4 p2=i2->getP();
  double m1sq=max(0.0,p1.m2Calc());
  double m2sq=max(0.0,p2.m2Calc());
  double m1 = sqrt(m1sq);
  double m2 = sqrt(m2sq);
  double s = (p1+p2).m2Calc();
  double srt = sqrt(s);
  if(srt < m1 + m2 + minKinEnergy) return false;
  double pr=sqrt((s-(m1+m2)*(m1+m2))*(s-(m1-m2)*(m1-m2)))/(2*srt);
  CollisionPair cpair = CollisionPair(icltyp,i1,i2,srt,pr);

  //double ct = dtNow*rndm->flat();
  //double tcol1 = i1->getT() + ct;
  //double tcol2 = i2->getT() + ct;
  //double dt = finalTime - initialTime;

  double tcol1 = max(i1->getT(),i2->getT());
  if(tcol1 > finalTime) return false;
  double dt = finalTime - tcol1; 
  tcol1 += dt*rndm->flat();
  double tcol2 = tcol1;
  double ct = tcol1;

   if(tcol1 > i1->tWall()) {
     wallCollision(i1);
     //return false;
   }
   if(tcol2 > i2->tWall()) {
     wallCollision(i2);
     //return false;
   }

  double qfac1 = i1->getTf() > tcol1 ? i1->qFactor() : 1.0;
  double qfac2 = i2->getTf() > tcol2 ? i2->qFactor() : 1.0;
  cpair.qFactor(qfac1,qfac2);
  double sig = xsection->sigma(cpair)*qfac1*qfac2;
  //double vrel=sqrt(pow2(p1*p2)-m1sq*m2sq)/(p1[0]*p2[0]);
  double vrel=sqrt(pow2(s -m1sq - m2sq) -4*m1sq*m2sq)/(2*p1[0]*p2[0]);
  double P22=vrel*sig*facSig*dt;

  if(P22>1.0) {
      cout << "P22= "<< P22
       	<< " tnow= "<< initialTime
       	<< " vrel= "<< vrel
	<< " sig= "<< sig
	<< " dt= "<< dtNow
	<< " vol= "<< volNow
	<< " eta1= "<< i1->getR().rap()
	<< " eta2= "<< i2->getR().rap()
       	<< " dt/vol= "<< dtNow/volNow
       	<< " facsig= "<< facSig
	<<endl;
      exit(1);
  }

  if(rndm->flat() > P22) return false;

  interList.push_back(new TwoBodyInterList(cpair,i1,i2,ct,tcol1,tcol2,0.0));
  return true;

}


//***********************************************************************
//...Purpose: to search decay and collision arrays to find next event
InterList* Collision4::findNextCollision()
{
  if(interList.size() <= 0 ) return 0;
  return interList.back();
}

//...Remove collisions including i1,i2.
void Collision4::removeInteraction2(InterList* inter)
{
  EventParticle* i1 = inter->getParticle(0);
  EventParticle* i2 = inter->getParticle(1);
  CascBox* box1= i1->box();
  CascBox* box2= i2->box();

  if(box1==0 || box2==0) { 
    cout << "Collision4::removeInteraction2 box1= "<< box1 << " box2= "<< box2<<endl;
    exit(1);
  }

  box1->eraseParticle(i1);
  box2->eraseParticle(i2);

}

void Collision4::removeInteraction(EventParticle* i1)
{
  i1->box()->eraseParticle(i1);
}

//***********************************************************************
//...Purpose: to update collision array.
void Collision4::collisionUpdate(InterList* inter)
{
  int np = inter->getNumberOfInComing();
  if(np == 1 ) {
    numberOfDecay++;
    auto&& i1 = inter->getParticle(0);
    i1->box()->eraseParticle(i1);

    if(inter->getParticle(1) != nullptr) {
      cout << "collisonUpdate strange "<<endl;
      exit(1);
    }

  } else if(np ==2) {
    numberOfCollision++;
    removeInteraction2(inter);
  } else {
    cout << "Collision4::collisionUpdate wrong np np= "<< np<< endl;
    exit(1);
  }

  delete inter;
  //interList.erase(interList.end()-1);
  interList.pop_back();

  if(pnew.size()==0) return;

  collisionUpdate();

}

void Collision4::collisionUpdate()
{
  // Find next new collisions for newly produced particles.
  for(auto&& ip : pnew) {

  if(withBox==1) {
    Vec4 r = ip->getR();
    r[0] = r.e();
    r[1] = modulo(r.px() + xBox/2, xBox) - xBox/2;
    r[2] = modulo(r.py() + yBox/2, yBox) - yBox/2;
    r[3] = modulo(r.pz() + zBox/2, zBox) - zBox/2;
    ip->setR(r);
  }//else if(withBox==2) {
    //wallCollisionTime(ip,2);

    // check if this particle is outside the cell.
    if(optBoxBoundary >0) {
      if(cascCell->boxPosition(ip->getR())==-1) {
      Vec4 r=ip->getR();
      cout << "Collision4::collisionUpdate particle is produced outside box r = "<< r;
      exit(1);
      }
    }

    // find a box
    CascBox *box = cascCell->box(ip->getR());

    // This particle will be a fluid after formation time.
    if(ip->getStatus()== -1200) {
      ip->addBox(box);
      continue;
    }

    // put this new particle into a box after search collision
    // to avoid collision between newly produced particles.
    ip->addBox(box);

  }

  pnew.clear();

}

// This is called when particles are created from fluid.
//----------------------------------------------------------------------------
void Collision4::collisionUpdate(std::vector<EventParticle*>& outgoing,
	double itime, double gtime)
{
  initialTime=itime;
  finalTime=gtime;

  for(int i=0; i<(int)outgoing.size();i++) {
    pnew.push_back(outgoing[i]);
  }

  // collision search between newly created particle and old particles.
  collisionUpdate();

}

void Collision4::cancelCollision(InterList* inter)
{
  // reset decay time.
  /*
  if(inter->getNumberOfInComing() == 1) {
    EventParticle* p = inter->getParticle(0);
    double m=p->getMass();
    double e=p->getE0();
    double tdec = p->lifetime() + jamParticleData->lifeTime(p->getParticleDataEntry(),m,e);
    p->setLifeTime(tdec);
    // Convert decay time to the evolution parameter.
    if(optCollisionOrdering > 100) tdec = p->t2tau(tdec,optPropagate);
    if(tdec < finalTime) p->box()->setInterList(new DecayList(p,tdec));
  }
  */

  delete inter;
  //interList.erase(interList.begin());
  interList.pop_back();

}

// This is not used.
bool Collision4::checkNextCollisionTime(TwoBodyInterList* inter,double dtau1,double dtau2,
    bool preHadronA,bool preHadronB)
{
  EventParticle* i1=inter->getParticle(0);
  EventParticle* i2=inter->getParticle(1);
  CascBox *box1 = i1->box();
  CascBox *box2 = i2->box();

  if (!preHadronA && !preHadronB && box1 == box2) {
    for (const auto& neighbor: box1->getNeighbors2())
      if (neighbor->checkNextCollisionTime(static_cast<InterList*>(inter), i1, dtau1, i2, dtau2))
	return true;
    return false;
  }

  if (!preHadronA) {
    for(const auto& neighbor: box1->getNeighbors2())
      if (neighbor->checkNextCollisionTime(static_cast<InterList*>(inter), i1, dtau1))
	return true;
  }

  if (!preHadronB) {
    for(const auto& neighbor: box2->getNeighbors2())
      if (neighbor->checkNextCollisionTime(static_cast<InterList*>(inter), i2, dtau2))
	return true;
  }

  return false;
}

// ctime is the global evolution time.
void Collision4::propagate(double ctime, int opt, int step)
{
  for(auto& b : cascBox) {
    if(b->getParticles().size()>0) {
    plist.splice(plist.end(), b->getParticles());
    }
  }
  if(step==1) return;

  for(auto& p : plist) {
    if(p->getT() < ctime) p->updateR(ctime,optPropagate);
    //p->updateR(ctime,optPropagate);

    p->tevol(ctime);
    p->clearBox();
    if(withBox==2) {
      wallCollisionTime(p,1);
      if(p->tWall() <0.0)  {wallCollision(p);
         if(cascCell->boxPosition(p->getR()) == -1) {
	   cout << " AFTER wall coll r= "<< p->getR();
	   exit(1);
	 }
      }
    }
  }

}

bool Collision4::doPauliBlocking(InterList* inter,
	vector<EventParticle*>& outgoing,int opt)
{
  int np = inter->getNumberOfInComing();
  EventParticle* i1 = inter->getParticle(0);
  EventParticle* i2 = np == 2 ? inter->getParticle(1):i1;

  for(const auto& ip : outgoing) {
    int idp = ip->getID();
    if(idp != 2212 && idp != 2112) continue;
    double ctime = ip->getT();
    if(ctime < ip->getTf()) continue; // not formed
    CascBox *box = cascCell->box(ip->getR());
    double phase = 0.0;
    for(const auto& neighbor : box->getNeighbors2())
      phase += neighbor->phaseSpace(i1,i2,ip,ctime,opt);

    // Loop over all boxes.
    //for(const auto& box : cascBox) phase += box->phaseSpace(i1,i2,ip,ctime,opt);

   if(pauliC*phase/overSample > rndm->flat()) return true; // Pauli-blocked.
  }

  // No Pauli blocking.
  return false;
}

} // end namespace jam2
