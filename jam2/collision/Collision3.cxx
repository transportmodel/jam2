#include <vector>
#include <iterator>
#include <algorithm>
#include <cmath>
#include <cfloat>
#include <iostream>
#include <algorithm>

#include <jam2/collision/Collision3.h>
#include <jam2/collision/InterType.h>
#include <jam2/collision/DecayList.h>
#include <jam2/collision/WallList.h>
#include <jam2/xsection/XsecTable.h>

// Box is used. interList is stored in each box.

namespace jam2 {

using namespace std;

void Collision3::clear()
{
  cascCell->clear();
  clearPlist();
}

void Collision3::init(const InitialCondition* initcnd)
{
  cascCell = new CascCell(settings);
  cascCell->init(initcnd);
  cascBox = cascCell->boxes();
  if(optCollisionOrdering >= 120) {
    timeConstraints = new TimeConstraints(settings);
  }
}

Collision3::Collision3(Pythia8::Settings* s, JamParticleData* jp,CrossSection* in,Pythia8::Rndm* r)
	: Collision(s,jp,in,r)
{
  optBoxBoundary=settings->mode("Cascade:boxBoundary");  // =0: boxes at the edge are infinitely large  =1: finite box

}

Collision3::~Collision3()
{
  delete cascCell;
  delete timeConstraints;
}

// =========================================================================//
//...Purpose: to make collision list into collision predictor arrays.
void Collision3::makeCollisionList(double itime, double gtime)
{
  //for(auto& box : cascBox) box->cleanInterList();

  initialTime=itime;
  finalTime=gtime;

  // assign a box to a particle and check wall collision and decay.
  for(const auto& i1 : plist ) {

    if(optBoxBoundary>0) {
    if(cascCell->boxPosition(i1->getR()) == -1) {
      cout << " makeCollisionList outside ? r= " << i1->getR();
      cout << " p= "<< i1->getP();
      exit(1);
    }
    }

    CascBox *box = cascCell->box(i1->getR());
    i1->addBox(box);
    //checkWallCollision(*i1,*box);

  }

  // Set time constraints.
  if(timeConstraints) {
    for(const auto& box : cascBox) {
      timeConstraints->setParticle(box->getParticles());
      timeConstraints->setConstraints(initialTime, pHat);
    }
  } else {
    setLambdas();
  }

  // Check wall collision
  for(const auto& i1 : plist) checkWallCollision(*i1,*i1->box());

  // Check collision and decay for each box.
  for(const auto& box : cascBox) searchCollisionPair(box);

  predictedColl = cascCell->numberOfCollision();
  numberOfCollision=0;
  plist.clear();

}

void Collision3::searchCollisionPair(CascBox* box)
{
  auto& pl = box->getParticles();
  EventParIt i0 = ++pl.begin();
  for(EventParIt i1 = i0; i1 != pl.end(); ++i1)
  for(EventParIt i2 = pl.begin(); i2 != i1; ++i2) {
    TwoBodyInterList* it = hit(*i1,*i2);
    if(it !=nullptr) box->setInterList(it);
 }

  // search for collision in the neighbor grid.
  for(auto& neighbor : box->getNeighbors1()) {
    auto const& pl2=neighbor->getParticles();
    for(auto& p1:pl)
    for(auto& p2:pl2) {
      TwoBodyInterList* it = hit(p1,p2);
      if(it !=nullptr) box->setInterList(it);
    }
  }

}

void Collision3::checkBox(InterList* inter, int opt)
{
  //InterList *inter= cascCell->findNextCollision();
  if(inter==0) return;
  EventParticle* i1 = inter->getParticle(0);
  EventParticle* i2 = inter->getParticle(1);
  CascBox* box1= i1->box();
  CascBox *box2=box1;
  if(i2 !=0) box2= i2->box();
  if(box1==0 || box2==0) { 
    cout << "Collision3::checkBox box1= "<< box1 << " box2= "<< box2<<endl;
    cout << " opt= "<< opt<<endl;
    cout << " i1= "<< i1 << " i2= "<< i2<<endl;
    cout << " id1 "<< i1->getID() << " id2= "<< i2->getID() <<endl;
    exit(1);
  }
}

//***********************************************************************
//...Purpose: to search decay and collision arrays to find next event
InterList* Collision3::findNextCollision()
{
  /*
  InterList *inter= cascCell->findNextCollision();
  if(inter==0) return inter;

  EventParticle* i1 = inter->getParticle(0);
  EventParticle* i2 = inter->getParticle(1);
  CascBox* box1= i1->box();
  CascBox *box2=box1;
  if(i2 !=0) box2= i2->box();
  if(box1==0 || box2==0) { 
    cout << "Collision3::findNextCollision box1= "<< box1 << " box2= "<< box2<<endl;
    exit(1);
  }
  */


  return cascCell->findNextCollision();


  if(cascBox.size() < plist.size()) {

    return cascCell->findNextCollision();

  } else {

  InterList* inter=0;
  double lastCollTime=1e+25;
  for(const auto& p : plist) {
    InterList* i = p->box()->sortInterList();
    if(i==nullptr) continue;
    if(i->getCollisionOrderTime() < lastCollTime) {
      lastCollTime = i->getCollisionOrderTime();
      inter = i;
    }
  }
  return inter;

  }
}

void Collision3::checkWallCollision(EventParticle& ip, CascBox& box)
{
  Vec4 r = ip.getR();
  Vec4 v = ip.velocity(optPropagate);
  double tw = cascCell->wallCollisionTime(box,r,v);
  double tauw = optCollisionOrdering > 100 ? ip.t2tau(tw,optPropagate) : tw;

  //if(tw<=r[0] || tw < initialTime) {
  //if(tw<r[0] || tauw <= ip.tevol()) {
  //if(tw<r[0]) {
  if(tw-r[0] < 1e-10) {
    if(tw > 1e+5) {
      ip.setTWall(finalTime+1.0);
      return;
    }
    Vec4 r1 = r;
    r1 += v*(tw - r[0]);
    r1[0]= tw;
    cout << "Collision3::checkWallCollision tw strange t-t0 "<< setprecision(20) << tw-r[0]<<endl;
    cout << " tauw= "<< tauw << " tau now = "<< ip.tevol()<<endl;
    cout << " tw= "<< tw <<endl;
    cout << " t-t0 "<< scientific << setprecision(20) << tw-r[0]<<endl;
    cout << scientific << setprecision(9) << " tw = "<< tw
          << " t0 = "<< r[0]
          << " ini t = "<< initialTime <<endl;
    cout << "p = "<<  &ip << " id= "<< ip.getID()<<endl;
    cout << " v= "<< v ;
    cout << "r0= "<< r <<endl;
    cout << "r1= "<< r1 <<endl;
    cout << "p= "<< ip.getP() <<endl;
    ip.setTWall(finalTime+1.0);
    if(tw >= initialTime) return;
    exit(1);
    return;
  }

  if(withBox==0 && optBoxBoundary>0) {
    Vec4 r1 = r;
    r1 += v*(tw - r[0]);
    r1[0]= tw;
    if(cascCell->boxPosition(r1)==-1) {
    cout << " going outside of wall? tw = "<< tw
      << " r0= "<< r
      << " r1= "<< r1 << endl;
    cout << " v= "<< v;
    cout << " p= "<< ip.getP();
    cout << " ipos orig= "<< cascCell->boxPosition(r) <<endl;
    exit(1);
    }
  }

  //double tauw=tw;
  // in case of the option of tau ordering
  if(optTau) {
    double zc= r[3]+v[3]*(tw-r[0]);
    if(tw*tw < zc*zc) {
      cout << "Collision3:checkWallCollision tw= "<< tw << " zc= "<< zc<<endl;
      cout << "t0= "<< r[0] << " z0= "<< r[3] << " v= "<< v[3] <<endl;
      cout << " dt "<< tw - r[0] << " dt*v= " << (tw-r[0])*v[3] <<endl;
      cout << " t0 "<< r[0]  << " zv= " << r[3]*v[3] <<endl;
      //exit(1);
    }
    if(abs(tw) >= abs(zc)) {
      tw = sqrt(tw*tw - zc*zc);
    }
  }

  // set time of wall collision.
  //ip.setTWall(tauw);
  ip.setTWall(tw);

  if(decayOn) {
    double taud = ip.lifetime();
    if(optCollisionOrdering > 100) taud = ip.t2tau(taud,optPropagate);

    if(optTau && taud < 1e+10) {
      double zd= r[3]+v[3]*(taud-r[0]);
      if(abs(taud) < abs(zd)) {
        cout << "Collision3:checkWallCollision taud= "<< taud << " zd= "<< zd <<endl;
      } else {
        taud = sqrt(taud*taud - zd*zd);
      }
    }

    if(tauw < taud) {
      if(tw >= CascCell::infLength) return;
      if(tauw < finalTime) box.setInterList(new WallList(&ip,tauw,tw));
    } else if(taud < finalTime) {
      box.setInterList(new DecayList(&ip,taud));
    }

  } else {
    if(tw >= CascCell::infLength) return;
    if(tauw < finalTime) box.setInterList(new WallList(&ip,tauw,tw));
  }

}

void Collision3::wallCollision(InterList& inter)
{
  numberOfWallCollision++;
  EventParticle* i1=inter.getParticle(0);

  // The time a particle hits the wall.
  double tw=i1->tWall();
  //double tw = optCollisionOrdering > 100? tau2t(tw,i1) : tw;

  // update particle position.
  CascBox *box0= i1->box();
  i1->updateR(tw,optPropagate);

  // set ordering time.
  i1->tevol(inter.getCollisionOrderTime());

  // update last collision time.
  //i1->setVertex();
  i1->setTimeLastColl();

  // Check boundary for the box simulation for the periodic boundary condition.
  if(withBox) {
    Vec4 r = i1->getR();
    r[0] = r.e();
    r[1] = modulo(r.px() + xBox/2, xBox) - xBox/2;
    r[2] = modulo(r.py() + yBox/2, yBox) - yBox/2;
    r[3] = modulo(r.pz() + zBox/2, zBox) - zBox/2;
    i1->setR(r);
  }

  // find a new box.
  CascBox* box1= cascCell->box(i1->getR());
  i1->changeBox(box1);
  box0->cleanInterList(i1);

  if(i1->box()==0) {
    cout << "wallCollision box= "<< i1->box() << " box1= "<< box1<<endl;
    exit(1);
  }

  // find a new wall collision time.
  checkWallCollision(*i1,*box1);

  // find a new collision parter. 
  searchCollisionPair(i1,box1);

}

void Collision3::searchCollisionPair(EventParticle* p1, CascBox* box)
{
  // find collision with particles including the neighbor grid.
  for(const auto& neighbor : box->getNeighbors2()) {
    // loop over all particle in this box.
    for(const auto& p2:neighbor->getParticles()) {
      TwoBodyInterList* it = hit(p1,p2);
      if(it !=nullptr) box->setInterList(it);
    }
  }

}

//...Remove collisions including i1,i2.
void Collision3::removeInteraction2(InterList* inter)
{
  EventParticle* i1 = inter->getParticle(0);
  EventParticle* i2 = inter->getParticle(1);
  CascBox* box1= i1->box();
  CascBox* box2= i2->box();
  if(box1==0 || box2==0) { 
    cout << "Collision3::removeInteraction2 box1= "<< box1 << " box2= "<< box2<<endl;
    exit(1);
  }

  for(auto& neighbor : box1->getNeighbors2()) {
    neighbor->cleanInterList(i1,i2);
  }

  if(box1 != box2) {
    for(auto& neighbor : box2->getNeighbors2()) neighbor->cleanInterList(i1,i2);
  }

  //i1->removeBox();
  box1->eraseParticle(i1);

  //i2->removeBox();
  box2->eraseParticle(i2);

}

void Collision3::removeInteraction(EventParticle* i1)
{
  for(auto& neighbor : i1->box()->getNeighbors2()) {
    neighbor->cleanInterList(i1);
  }
  //i1->removeBox();
  i1->box()->eraseParticle(i1);
}

//***********************************************************************
//...Purpose: to update collision array.
void Collision3::collisionUpdate(InterList* inter)
{
  int np = inter->getNumberOfInComing();
  if(np == 1 ) {
    numberOfDecay++;
    auto&& i1 = inter->getParticle(0);
    removeInteraction(i1);

    //for(auto& neighbor : i1->box()->getNeighbors2()) {
    //  neighbor->cleanInterList(i1);
    //}
    //i1->removeBox();
    //i1->box()->eraseParticle(i1);

    if(inter->getParticle(1) != nullptr) {
      cout << "collisonUpdate strange "<<endl;
      exit(1);
    }

  } else if(np ==2) {
    numberOfCollision++;
    removeInteraction2(inter);
  } else {
    cout << "Collision3::collisionUpdate wrong np np= "<< np<< endl;
    exit(1);
  }

  if(pnew.size()==0) return;

  // Search for new collisions for outgoing particles.
  collisionUpdate();

}

void Collision3::collisionUpdate()
{
  // Find next new collisions for newly produced particles.
  for(auto&& ip : pnew) {

    // Check boundary for the box simulation for the periodic boundary condition.
    if(withBox) {
      Vec4 r = ip->getR();
      r[0] = r.e();
      r[1] = modulo(r.px() + xBox/2, xBox) - xBox/2;
      r[2] = modulo(r.py() + yBox/2, yBox) - yBox/2;
      r[3] = modulo(r.pz() + zBox/2, zBox) - zBox/2;
      ip->setR(r);
    }

    // check if this particle is outside the cell.
    if(optBoxBoundary >0) {
      if(cascCell->boxPosition(ip->getR())==-1) {
      Vec4 r=ip->getR();
      cout << "Collision3::collisionUpdate particle is produced outside box r = "<< r;
      exit(1);
      }
    }

    // find a box
    CascBox *box = cascCell->box(ip->getR());

    // search wall collision and particle decay time.
    checkWallCollision(*ip,*box);

    // This particle will be a fluid after formation time.
    if(ip->getStatus()== -1200) {
      ip->addBox(box);
      continue;
    }

    // search collisions
    searchCollisionPair(ip,box);

    // put this new particle into a box after search collision
    // to avoid collision between newly produced particles.
    ip->addBox(box);

  if(ip->box()==0) {
    cout << "Collision3::collisionUpate box= "<< box<<endl;
    exit(1);
  }
    if(box->checkBox()) {
      cout << " box= "<< box <<endl;
      exit(1);
    }

  }

  pnew.clear();

  /*
  // merge pnew into plist.
  for(auto& p:pnew) {
    plist.push_front(p);
    p->setPlistIt(plist.begin());
  }
  pnew.clear();
  */

}

// This is called when particles are created from fluid.
//----------------------------------------------------------------------------
void Collision3::collisionUpdate(std::vector<EventParticle*>& outgoing,
	double itime, double gtime)
{
  initialTime=itime;
  finalTime=gtime;

  for(int i=0; i<(int)outgoing.size();i++) {
    pnew.push_back(outgoing[i]);
  }

  EventParIt i0 = ++pnew.begin();
  // Loop over newly produced particles.
  for(EventParIt i1 = i0; i1 != pnew.end(); ++i1) {
    CascBox *box = cascBox[cascCell->inside((*i1)->getR())];
    for(EventParIt i2 = pnew.begin(); i2 != i1; ++i2) {
      // if i2 is far away from i1 we should not search collision between i1 and i2.
      if(box->isNeighbor(**i2)) {
	TwoBodyInterList* it = hit(*i1,*i2);
        if(it !=nullptr) box->setInterList(it);
      }
    }
  }

  // collision search between newly created particle and old particles.
  collisionUpdate();

}

void Collision3::cancelCollision(InterList* inter)
{
  EventParticle* p = inter->getParticle(0);

  // reset decay time.
  if(inter->getNumberOfInComing() == 1) {
    double m=p->getMass();
    double e=p->getE0();
    double tdec = p->lifetime() + jamParticleData->lifeTime(p->getParticleDataEntry(),m,e);
    p->setLifeTime(tdec);

    /*
      if(optCollisionOrdering==101) {
      double lam= p->lambda();
      double lam2= 1.0/(pHat*p->getPkin());
      if(abs(lam-lam2)>1e-9) {
	cout << "Collision3::cancelCollision  lam= "<< lam << " lam2= " << lam2
	  << " diff= "<< lam-lam2
	  <<endl;
	exit(1);
      }
      }
      */

    // Convert decay time to the evolution parameter.
    if(optCollisionOrdering > 100) tdec = p->t2tau(tdec,optPropagate);
    if(tdec < finalTime) p->box()->setInterList(new DecayList(p,tdec));

  } else {

      /*
      EventParticle* p2 = inter->getParticle(1);
      if(optCollisionOrdering==101) {
      double lam= p2->lambda();
      double lam2= 1.0/(pHat*p2->getPkin());
      if(abs(lam-lam2)>1e-9) {
	cout <<scientific << setprecision(16) << "Collision3::cancelCollision 2  lam= "<< lam << " lam2= " << lam2
	  << " diff= "<< lam-lam2
	  <<endl;
	exit(1);
      }
      }
      */
  }


  p->box()->removeInter(inter);
  return;

}

// This is not used.
bool Collision3::checkNextCollisionTime(TwoBodyInterList* inter,double dtau1,double dtau2,
    bool preHadronA,bool preHadronB)
{
  //double tc1 = inter->getCollisionTime(0);
  //double tc2 = inter->getCollisionTime(1);
  EventParticle* i1=inter->getParticle(0);
  EventParticle* i2=inter->getParticle(1);
  CascBox *box1 = i1->box();
  CascBox *box2 = i2->box();

  if (!preHadronA && !preHadronB && box1 == box2) {
    for (const auto& neighbor: box1->getNeighbors2())
      if (neighbor->checkNextCollisionTime(static_cast<InterList*>(inter), i1, dtau1, i2, dtau2))
	return true;
    return false;
  }

  if (!preHadronA) {
    for(const auto& neighbor: box1->getNeighbors2())
      if (neighbor->checkNextCollisionTime(static_cast<InterList*>(inter), i1, dtau1))
	return true;
  }

  if (!preHadronB) {
    for(const auto& neighbor: box2->getNeighbors2())
      if (neighbor->checkNextCollisionTime(static_cast<InterList*>(inter), i2, dtau2))
	return true;
  }

  return false;
}

// ctime is the global evolution time.
void Collision3::propagate(double ctime, int opt, int step)
{
  if(opt != optPropagate) {
    cout << "Collision3::propagate opt= "<< opt << " optpro= "<< optPropagate<<endl;
    exit(1);
  }

  for(auto& b : cascBox) {
    if(b->getParticles().size()>0) {
    plist.splice(plist.end(), b->getParticles());
    }
  }
  if(step==1) return;

  for(auto& p : plist) {
    if(ctime <= p->tevol()) continue;

  //Vec4 r = p->getR();
  //Vec4 v = p->velocity(optPropagate);
  //double tw = cascCell->wallCollisionTime(*p->box(),r,v);
  //double tauw = optCollisionOrdering > 100 ? p->t2tau(tw,optPropagate) : tw;
  //if(tauw < ctime) {
  //  cout << " tw= "<< tw  << " tauw= "<< tauw << " ctime= "<< ctime<<endl;
  //  cout << "p= "<< p->getP() << " r= "<< p->getR();
  //}

      if(optCollisionOrdering > 100) {
	p->updateRtau(ctime,opt);
      } else {
        p->updateR(ctime,opt);
	p->tevol(ctime);
      }

      /*
      double t = optCollisionOrdering > 100 ? p->tau2t(ctime,optPropagate) : ctime;
      p->updateR(t,optPropagate);
      p->tevol(ctime);
      p->clearBox();
      */

      //Vec4 pk = optPropagate == 0 ? p->getP() : p->getPkin();
      /*
	cout << " ctime= "<< ctime << " tau= "<< p->tevol();
	cout <<scientific  << " t= "<< t  << " t0= "<< p->getT() << " tf= "<< p->getTf() << " tlast= "<< p->TimeLastColl();
	  cout << " lam= "<< p->lambda() << " phat*p= "<< 1/(pHat*pk) << " v0= "<< p->lambda()*pk[0]
	    << " v1= "<< pk[0]/(pHat*pk)
	    <<endl;
	    */

      /*
      if(optCollisionOrdering==101) {
      double lam= p->lambda();
      double lam2= 1.0/(pHat*pk);
      if(abs(lam-lam2)>1e-9) {
	Vec4 pc=p->getP();
	double m= p->getMass();
	double meff= p->getEffectiveMass();
	double pe = sqrt(m*m+pc.pAbs2());
	cout<< scientific << setprecision(8) << "Collision3::propagate  lam= "<< lam
	  << " lam2= " << lam2
	  << " lam3= " << 1.0/(pHat*p->getP())
	  << " id= "<< p->getID()
	  << " q= "<< p->qFactor()
	  << " S= "<< p->pots()
	  << " m= "<< m
	  << " m*= "<< meff
	  <<endl;
	cout << scientific << "pk= "<< pk[0];
	cout << scientific << " p= "<< pc[0]
	  << " diff= "<< pk[0]-pc[0] <<endl;
	cout << " p0*= "<< sqrt(meff*meff+pk.pAbs2())<<endl;
	cout << " p0= "<< sqrt(m*m+pc.pAbs2())<<endl;
	cout << " lam0= "<< 1.0/pe<<endl;
	exit(1);
      }
      }
      */

  if(optBoxBoundary >0 && cascCell->boxPosition(p->getR()) == -1) {
    cout << "Collision3::propagate outside? lambda= "<< p->lambda()
      << " id= "<< p->getID()
      << endl
      << " r= "<< p->getR()
      << " p= "<< p->getP();
    exit(1);
  }

    //}
  }

  //cout << " pHat= "<< pHat;

  //for(auto& b : cascBox) b->clearParticle();

}

bool Collision3::doPauliBlocking(InterList* inter,
	vector<EventParticle*>& outgoing,int opt)
{
  /*
  for(auto& b : cascBox) {
    plist.insert(plist.end(),b->getParticles().begin(),b->getParticles().end());
  }
  bool block= Collision::doPauliBlocking(inter,outgoing,opt);
  plist.clear();
  return  block;
  */

  int np = inter->getNumberOfInComing();
  EventParticle* i1 = inter->getParticle(0);
  EventParticle* i2 = np == 2 ? inter->getParticle(1):i1;

  for(const auto& ip : outgoing) {
    int idp = ip->getID();
    if(idp != 2212 && idp != 2112) continue;
    double ctime = ip->getT();
    if(ctime < ip->getTf()) continue; // not formed
    CascBox *box = cascCell->box(ip->getR());
    double phase = 0.0;
    for(const auto& neighbor : box->getNeighbors2())
      phase += neighbor->phaseSpace(i1,i2,ip,ctime,opt);

    // Loop over all boxes.
    //for(const auto& box : cascBox) phase += box->phaseSpace(i1,i2,ip,ctime,opt);


    //cout << " phase = "<< pauliC*phase <<endl;
    //cin.get();
   if(pauliC*phase/overSample > rndm->flat()) return true; // Pauli-blocked.
  }

  // No Pauli blocking.
  return false;
}

} // end namespace jam2
