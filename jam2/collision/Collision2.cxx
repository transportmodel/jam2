#include <vector>
#include <iterator>
#include <algorithm>
#include <cmath>
#include <cfloat>
#include <iostream>
#include <algorithm>

#include <jam2/collision/Collision2.h>
#include <jam2/collision/InterType.h>
#include <jam2/collision/DecayList.h>
//#include <jam2/collision/WallList.h>
#include <jam2/xsection/XsecTable.h>

// Cluster is used.

namespace jam2 {

using namespace std;

void Collision2::clear()
{
  //cascCell->clear();
  clearPlist();
}

void Collision2::init(const InitialCondition* initcnd)
{
  optSeparable=settings->mode("Cascade:optSeparable");
  cWidth = settings->parm("Cascade:gaussWidth");
  double dc = settings->parm("Cascade:clusterSeparation");

  // parameter for the cluster separation in fm^2
  distMinSq=dc*dc;
  //distMinSq=5.*5.;
  //distMinSq=2.5*2.5;
}

Collision2::Collision2(Pythia8::Settings* s, JamParticleData* jp,CrossSection* in,Pythia8::Rndm* r)
	: Collision(s,jp,in,r)
{
  //optBoxBoundary=settings->mode("Cascade:boxBoundary");  // =0: boxes at the edge are infinitely large  =1: finite box

}

Collision2::~Collision2()
{
  cout <<"average time constraints eps = "<< epsAve/nEpsAve<<endl;
}

void Collision2::findCluster()
{
  pHat=pHatSave;
  vector<EventParticle*> part(plist.begin(),plist.end());
  for(const auto& c : clusterList) delete c;
  clusterList.clear();
  int nv=part.size();
  if(nv==0) return;

  // Save initial total momentum and baryon number.
  Vec4 ptot0=0.0;
  for(auto& p: part) {
    ptot0 += p->getP();
  }

  // Start coalescence 
  vector<int> mscl(nv,1);
  vector<int> num(nv);
  for(int i=0;i<nv;i++) num[i]=i;

  int  nclst=0; // number of cluster
  int  icheck=0;
  // Find nuclear cluster
  for(int i=0;i<nv-1;i++) {
    int j0=icheck+1;
    int i1=num[i];
    Vec4 x1=part[i1]->getR();
    //Vec4 p1=part[i1]->getP();
    for(int j=j0;j<nv;j++) {
      int i2=num[j];

      // Check if two-particles is close enough in coordinate space.
      // relative momentum in the two-body c.m. frame.
      Vec4 x2=part[i2]->getR();
      Vec4 dx  = x1 - x2;
      double dqsq = -dx.m2Calc();

      //Vec4 p2=part[i2]->getP();
      //Vec4 pcm = p1 + p2;
      //dqsq = dqsq + pow2(dx*pcm)/pcm.m2Calc();

      //if(dqsq < distMinSq) {
      if(dqsq >=0.0 && dqsq < distMinSq) {
	int lp=num[icheck+1];
	num[icheck+1]=i2;
	num[j]=lp;
	icheck++;
	mscl[nclst]++;
      }
    }
    if(icheck == i) {
      nclst++;
      icheck++;
    }
  }
  nclst++;

  //cout << " number of cluster= "<< nclst<<endl;

  int nn=0;
  int j=0;
  // Loop over nuclear clusters.
  for(int ic=0;ic<nclst;ic++) {
    int nt=mscl[ic]; // number of particle in the cluster
    //cout << " particle in the cluster= "<< nt<<endl;
    nn += nt;
    CCluster *cluster = new CCluster(settings);
    // loop over particles in this cluster.
    for(int i=0;i<nt;i++) {
      cluster->add(part[num[j]]);
      j++;
    } // end loop over particle in the cluster
    clusterList.push_back(cluster);
    //cluster->setConstraints(optSeparable,initialTime,pHatSave);
  }  // end cluster loop

  plist.clear();
  part.clear();

  for(const auto& c : clusterList) {
    double eps = c->setConstraints(optSeparable,initialTime,pHatSave);
    epsAve += eps;
    nEpsAve++;
  }
    

  /*
  Vec4 ptot=0.0;
  for(const auto& c : clusterList) {
    auto& pl = c->getParticles();
    for(auto p:pl) ptot += p->getP();
  }
  cout << "nc= "<< nclst << " optSep= "<< optSeparable<<endl;
  cout << " ptot= "<< ptot0-ptot;
  */

}

// =========================================================================//
//...Purpose: to make collision list into collision predictor arrays.
void Collision2::makeCollisionList(double itime, double gtime)
{
  // Set initial and final time for searching two-body collisions.
  initialTime=itime;
  finalTime=gtime;

  // find nuclear clusters.
  findCluster();

  // total number of collisions.
  predictedColl = 0;

  // Loop over all clusters.
  for(const auto& c : clusterList) {
    predictedColl += searchCollisionPair(c);
  }
  numberOfCollision=0;

}

// Search collision pair and decay time in the cluster.
int Collision2::searchCollisionPair(CCluster* cluster)
{
  int ncoll=0;

  // get particle number of the cluster
  auto& pl = cluster->getParticles();

  // Loop over particle pairs in the cluster.
  EventParIt i0 = ++pl.begin();
  for(EventParIt i1 = i0; i1 != pl.end(); ++i1)
  for(EventParIt i2 = pl.begin(); i2 != i1; ++i2) {
    TwoBodyInterList* it = hit(*i1,*i2);
    if(it !=nullptr) {
      cluster->setInterList(it);
      ++ncoll;
    }
 }

  // set decay time.
  if(decayOn) {
    for(auto& p: pl) {
      double ct=p->lifetime();
      if(optTau) {
        Vec4 v = p->velocity(optPropagate);
        double zc= p->getZ() +v[3]*(ct-p->getT());
        if(abs(ct) < abs(zc)) {
          cout << "Collision2:makeCollisionList tc= "<< ct << " zc= "<< zc<<endl;
          exit(1);
        }
        ct = sqrt(ct*ct - zc*zc);
      }
      if(optCollisionOrdering > 100) ct = p->t2tau(ct,optPropagate);
      if(ct < finalTime) cluster->setInterList(new DecayList(p,ct));
    }
  }

  // total number of predicted collision.
  return ncoll;

}

void Collision2::searchCollisionPair(EventParticle* p1)
{
  auto& pl = clusterNow->getParticles();

  int ncol=0;
  // find collision with particles including the neighbor grid.
  for(const auto& p2 : pl) {
      TwoBodyInterList* it = hit(p1,p2);
      if(it !=nullptr) {
	clusterNow->setInterList(it);
	ncol++;
      }
    }
}

//***********************************************************************
//...Purpose: to search decay and collision arrays to find next event
InterList* Collision2::findNextCollision()
{
  InterList* inter=nullptr;
  clusterNow=nullptr;
  lastCollisionTime=1e+35;

  // Loop over clusters
  for(const auto& c : clusterList) {
    InterList* i = c->sortInterList();
    if(i==nullptr) continue;
    if(i->getCollisionOrderTime() < lastCollisionTime) {
      lastCollisionTime = i->getCollisionOrderTime();
      inter = i;
      clusterNow = c;
    }
  }
  if(optSeparable > 0 && clusterNow !=nullptr) {
    pHat = clusterNow->phat();
  }
  return inter;
}


//...Remove collisions including i1,i2.
void Collision2::removeInteraction2(InterList* inter)
{
  EventParticle* i1 = inter->getParticle(0);
  EventParticle* i2 = inter->getParticle(1);

  clusterNow->cleanInterList(i1,i2);
  clusterNow->eraseParticle(i1);
  clusterNow->eraseParticle(i2);

}

void Collision2::removeInteraction(EventParticle* i1)
{
  clusterNow->cleanInterList(i1);
  clusterNow->eraseParticle(i1);
}

//***********************************************************************
//...Purpose: to update collision array.
void Collision2::collisionUpdate(InterList* inter)
{
  int np = inter->getNumberOfInComing();
  if(np == 1 ) {
    numberOfDecay++;
    auto&& i1 = inter->getParticle(0);
    removeInteraction(i1);

    if(inter->getParticle(1) != nullptr) {
      cout << "collisonUpdate strange "<<endl;
      exit(1);
    }

  } else if(np ==2) {
    numberOfCollision++;
    removeInteraction2(inter);
  } else {
    cout << "Collision2::collisionUpdate wrong np np= "<< np<< endl;
    exit(1);
  }

  if(pnew.size()==0) return;

  int optSetConstraintsAfterColl=0;
  if(optSetConstraintsAfterColl) {
    double coltime=inter->getCollisionOrderTime();
    clusterNow->setConstraints(optSeparable,coltime,pHatSave);
  }

  collisionUpdate();

}

void Collision2::collisionUpdate()
{
  // Find next new collisions for newly produced particles.
  for(auto&& ip : pnew) {

    // This particle will be a fluid after formation time.
    if(ip->getStatus()== -1200) {
      clusterNow->add(ip);
      continue;
    }

    // search collisions
    searchCollisionPair(ip);

    // put this new particle into a cluster after search collision
    // to avoid collision between newly produced particles.
    clusterNow->add(ip);

  }

    // check decay.
  if(decayOn) {
    for(auto&& ip : pnew) {
      double ct=ip->lifetime();
      if(optTau) {
        Vec4 v = ip->velocity(optPropagate);
        double zc= ip->getZ() +v[3]*(ct-ip->getT());
        if(abs(ct) < abs(zc)) {
          cout << "Collision2::collisionUpdate fluid tc= "<< ct << " zc= "<< zc<<endl;
          exit(1);
        }
        ct = sqrt(ct*ct - zc*zc);
      }
      if(optCollisionOrdering > 100) ct = ip->t2tau(ct,optPropagate);
      if(ct<finalTime) clusterNow->setInterList(new DecayList(ip,ct));
    }
  }

  pnew.clear();

}

// This is called when particles are created from fluid.
//----------------------------------------------------------------------------
void Collision2::collisionUpdate(std::vector<EventParticle*>& outgoing,
	double itime, double gtime)
{
  cout << "Warning! Collision2::collisionUpdate is not completed! do not used."<<endl;
  exit(1);

  initialTime=itime;
  finalTime=gtime;

  for(int i=0; i<(int)outgoing.size();i++) {
    pnew.push_back(outgoing[i]);
  }

  /* should be modified....

  EventParIt i0 = ++pnew.begin();
  // Loop over newly produced particles.
  for(EventParIt i1 = i0; i1 != pnew.end(); ++i1) {
    CascBox *box = cascBox[cascCell->inside((*i1)->getR())];
    for(EventParIt i2 = pnew.begin(); i2 != i1; ++i2) {
      // if i2 is far away from i1 we should not search collision between i1 and i2.
      if(box->isNeighbor(**i2)) {
	TwoBodyInterList* it = hit(*i1,*i2);
        if(it !=nullptr) box->setInterList(it);
      }
    }
  }

  // collision search between newly created particle and old particles.
  collisionUpdate();
  */

}


void Collision2::cancelCollision(InterList* inter)
{
  EventParticle* p = inter->getParticle(0);

  // reset decay time.
  if(inter->getNumberOfInComing() == 1) {
    double m=p->getMass();
    double e=p->getE0();
    double tdec = p->lifetime() + jamParticleData->lifeTime(p->getParticleDataEntry(),m,e);
    p->setLifeTime(tdec);

      if(optTau) {
        Vec4 v = p->velocity(optPropagate);
        double zc= p->getZ() +v[3]*(tdec-p->getT());
        if(abs(tdec) < abs(zc)) {
          cout << "Collision2::cancelCollision tdec= "<< tdec << " zc= "<< zc <<endl;
          exit(1);
        }
        tdec = sqrt(tdec*tdec - zc*zc);
      }
    // Convert decay time to the evolution parameter.
    if(optCollisionOrdering > 100) tdec = p->t2tau(tdec,optPropagate);
    if(tdec < finalTime) clusterNow->setInterList(new DecayList(p,tdec));
  }

  clusterNow->removeInter(inter);
  return;

}

// This is not used.
bool Collision2::checkNextCollisionTime(TwoBodyInterList* inter,double dtau1,double dtau2,
    bool preHadronA,bool preHadronB)
{
  //double tc1 = inter->getCollisionTime(0);
  //double tc2 = inter->getCollisionTime(1);
  EventParticle* i1=inter->getParticle(0);
  EventParticle* i2=inter->getParticle(1);
  //CascBox *box1 = i1->box();
  //CascBox *box2 = i2->box();

  if (!preHadronA && !preHadronB) {
    if (clusterNow->checkNextCollisionTime(static_cast<InterList*>(inter), i1, dtau1, i2, dtau2))
      return true;
    return false;
  }

  if (!preHadronA) {
      if (clusterNow->checkNextCollisionTime(static_cast<InterList*>(inter), i1, dtau1))
	return true;
  }

  if (!preHadronB) {
      if (clusterNow->checkNextCollisionTime(static_cast<InterList*>(inter), i2, dtau2))
	return true;
  }

  return false;
}

// ctime is the global evolution time.
void Collision2::propagate(double ctime, int opt, int step)
{
  // Add particles in the current cluster to the plist.
  for(auto& c : clusterList)
    plist.splice(plist.end(),c->getParticles());

  // in case there is no time step.
  if(step==1) return;

  // propagate all particles up to the 'ctime'.
  for(auto& p : plist) {
    if(ctime <= p->tevol()) continue;
      if(optCollisionOrdering > 100) {
	p->updateRtau(ctime,opt);
      } else {
        p->updateR(ctime,opt);
	p->tevol(ctime);
      }
  }

}

bool Collision2::doPauliBlocking(InterList* inter,vector<EventParticle*>& outgoing,int opt)
{
  int np = inter->getNumberOfInComing();
  EventParticle* i1 = inter->getParticle(0);
  EventParticle* i2 = np == 2 ? inter->getParticle(1):i1;

  for(const auto& ip : outgoing) {
    int idp = ip->getID();
    if(idp != 2212 && idp != 2112) continue;
    double ctime = ip->getT();
    if(ctime < ip->getTf()) continue; // not formed
    double phase = 0.0;
    phase += clusterNow->phaseSpace(i1,i2,ip,ctime,opt);
    if(pauliC*phase/overSample > rndm->flat()) return true; // Pauli-blocked.
  }

  // No Pauli blocking.
  return false;
}

} // end namespace jam2
