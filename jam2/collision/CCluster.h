#ifndef jam2_collision_CCluster_h
#define jam2_collision_CCluster_h

#include <vector>
#include <list>
#include <array>
#include <memory>
#include <algorithm>
#include <Pythia8/Settings.h>

namespace jam2 {

class EventParticle;
class InterList;
class InterListContainer;

class CCluster
{
private:
  Pythia8::Settings* settings;
  std::list<EventParticle*> particles;
  std::unique_ptr<InterListContainer> m_interList; // PImpl
  int     withBox;
  double  xBox,yBox,zBox;
  double gWidth,pauliR, pauliP;
  int optCollisionOrdering, optPropagate;
  int optSeparable;
  Pythia8::Vec4 pHat,pHatC;
  double cWidth;
  double aSmall;
  int optGij=1;
  int optG;
  int maxIt;
  static const double epsG;
  Pythia8::vector<double> chi;
  std::vector<std::vector<double> > mat;
  std::vector<Pythia8::Vec4> rSave;
  double distMinSq;
  std::vector<double> pW4;

public:
  CCluster(Pythia8::Settings* s);
  ~CCluster();

  //void setPhat(double tau);
  double setConstraints(int optSep,double tau,const Pythia8::Vec4& phat);
  double makeConstraintMatrix(double tau);
  double setTimeConstraints(double tau);
  void solveConstraints(double tau);
  double makeConstraints(double tau);
  double makeConstraints2(double tau);
  Pythia8::Vec4 phat() const {return pHat;}

  std::list<EventParticle*>& getParticles()  {return particles;}

  std::list<EventParticle*>::iterator add(EventParticle* p) {
    particles.push_front(p);
    return particles.begin();
  }
  void removeParticle(std::list<EventParticle*>::iterator p) {
    particles.erase(p);
  }
  void removeParticle(EventParticle* ip) {
    particles.erase(std::find(particles.begin(),particles.end(),ip));
  }
  void eraseParticle(EventParticle* ip);
  void clearParticle();

  void setInterList(InterList* inter);
  void cleanInterList(EventParticle* i1, EventParticle* i2);
  void cleanInterList(EventParticle* i1);
  void removeInterList(EventParticle* i1);
  int cleanInterList();
  void removeInter(InterList* inter);
  InterList* sortInterList();
  void printInterList();
  int interListSize();
  bool checkNextCollisionTime(InterList const* inter,
    EventParticle const* p1, double dtau1,
    EventParticle const* p2 = nullptr, double dtau2 = 0.0) const;

  double phaseSpace(EventParticle* i1, EventParticle* i2, EventParticle* ip,double ctime,int opt);
  void gaussianElimination(double tau);
  double w4UL(double dtau);

  //void searchCollisionPair();
  //void searchCollisionPair(EventParticle* p1);
};


} // end namespace jam2
#endif
