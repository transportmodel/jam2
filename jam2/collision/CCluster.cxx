#include <jam2/collision/CCluster.h>
#include <jam2/collision/TimeOrder.h>
#include <jam2/collision/EventParticle.h>
#include <jam2/collision/InterList.h>
#include <Pythia8/Basics.h>
#include <jam2/hadrons/JamStdlib.h>

#include <cstddef>
#include <ostream>
#include <algorithm>
#include <vector>

namespace jam2 {

const double CCluster::epsG=1e-9;
//const double CCluster::epsG=0.0;

template<typename InterListRange>
static bool checkNextCollisionTime(InterListRange& range, InterList const* inter, EventParticle const* p1, double dtau1, EventParticle const* p2, double dtau2) {
  if (p2) {
    for (InterList const* iw : range) {
      if(iw == 0 || iw == inter) continue;
      if(iw->getParticle(0) == p1 && iw->getCollisionTime(0) < dtau1) return true;
      if(iw->getParticle(1) == p1 && iw->getCollisionTime(1) < dtau1) return true;
      if(iw->getParticle(0) == p2 && iw->getCollisionTime(0) < dtau2) return true;
      if(iw->getParticle(1) == p2 && iw->getCollisionTime(1) < dtau2) return true;
    }
  } else {
    for (InterList const* iw : range) {
      if(iw == 0 || iw == inter) continue;
      if(iw->getParticle(0) == p1 && iw->getCollisionTime(0) < dtau1) return true;
      if(iw->getParticle(1) == p1 && iw->getCollisionTime(1) < dtau1) return true;
    }
  }
  return false;
}

// default (jam2_collision_CCluster_InterListContainerType == 1)

class InterListContainer {
  typedef std::vector<InterList*> buffer_type;
  buffer_type m_data;
  buffer_type m_buff;
  std::size_t m_sorted_size = 0;
  std::size_t stat_sort = 0, stat_ninter = 0, stat_rmmiss = 0;

public:
  std::size_t size() const {
    return m_data.size();
  }

  void push(InterList* inter) {
    stat_ninter++;
    m_data.push_back(inter);
  }

  void print(std::ostream& ostr) const {
    for(auto& inter : m_data)
      ostr << " i= "<< inter << " t= "<< inter->getCollisionOrderTime()<< std::endl;
  }

private:
  void sort() {
    if (m_sorted_size == m_data.size()) return;
    stat_sort++;
    auto const beg = m_data.begin();
    auto const mid = beg + m_sorted_size;
    auto const end = m_data.end();
    std::sort(mid, end, TimeOrderDescend());
    if (m_sorted_size) {
      m_buff.resize(m_data.size());
      std::merge(beg, mid, mid, end, m_buff.begin(), TimeOrderDescend());
      m_data.swap(m_buff);
    }
    m_sorted_size = m_data.size();
  }

public:
  InterList* next() {
    if (m_data.empty()) return nullptr;
    this->sort();
    return m_data.back();
  }

  void clean(EventParticle const* p1, EventParticle const* p2 = nullptr) {
    this->sort();
    if (p2) {
      m_data.erase(
	std::remove_if(m_data.begin(), m_data.end(), [p1, p2](InterList* const entry) {
	  EventParticle* const ip1 = entry->getParticle(0);
	  EventParticle* const ip2 = entry->getParticle(1);
	  if (ip1 == p1 || ip2 == p1 || ip1 == p2 || ip2 == p2) {
	    delete entry;
	    return true;
	  }
	  return false;
	}), m_data.end());
    } else {
      m_data.erase(
	std::remove_if(m_data.begin(), m_data.end(), [p1](InterList* const entry) {
	  EventParticle* const ip1 = entry->getParticle(0);
	  EventParticle* const ip2 = entry->getParticle(1);
	  if (ip1 == p1 || ip2 == p1) {
	    delete entry;
	    return true;
	  }
	  return false;
	}), m_data.end());
    }
    m_sorted_size = m_data.size();
  }

  bool remove(InterList* inter) {
    if (m_data.empty()) return false;

    // "inter" is probably the last element because the interactions are
    // processed from earlier ones.
    if (inter == m_data.back()) {
      delete inter;
      if (m_sorted_size == m_data.size()) m_sorted_size--;
      m_data.pop_back();
      return true;
    }

    // When "inter" is not found at the end, do a binary search.
    stat_rmmiss++;
    this->sort();
    for (auto it = std::lower_bound(m_data.begin(), m_data.end(), inter, TimeOrderDescend());
	 it != m_data.end(); ++it)
      {
	if (*it != inter) continue;
	delete inter;
	m_data.erase(it);
	m_sorted_size--;
	return true;
      }

    return false;
  }

  std::size_t clear() {
    std::size_t const original_size = m_data.size();
    for(InterList const* inter : m_data) delete inter;
    m_data.clear();
    m_sorted_size = 0;
    return original_size;
  }

  bool checkNextCollisionTime(InterList const* inter, EventParticle const* p1, double dtau1, EventParticle const* p2, double dtau2) const {
    return jam2::checkNextCollisionTime(m_data, inter, p1, dtau1, p2, dtau2);
  }

  ~InterListContainer() {
    this->clear();
    // if (stat_ninter)
    //   std::cerr << "ninter=" << stat_ninter << " sort=" << stat_sort << " nmiss=" << stat_rmmiss << std::endl;
  }
};

CCluster::CCluster(Pythia8::Settings* s): settings(s) 
{
  m_interList = std::make_unique<InterListContainer>();

  withBox=settings->mode("Cascade:box");
  xBox=settings->parm("Cascade:boxLx");
  yBox=settings->parm("Cascade:boxLy");
  zBox=settings->parm("Cascade:boxLz");

  optCollisionOrdering = settings->mode("Cascade:optCollisionOrder");
  int optVectorPotential=settings->mode("MeanField:optVectorPotential");
  int optVdot=settings->mode("MeanField:optVdot");
  optPropagate=0;
  if(optVectorPotential==1 && optVdot==0) optPropagate=1;

  double dc = settings->parm("Cascade:clusterSeparation");
  distMinSq=dc*dc;
  maxIt=settings->mode("Cascade:timeFixMaxIteration");
  optGij=settings->mode("Cascade:optGij");
  cWidth=settings->parm("Cascade:gaussWidth");
  gWidth=settings->parm("MeanField:gaussWidth");
  aSmall=0.01;
  aSmall=0.0;

  if(settings->mode("Cascade:PauliBlocking")==1) {
    pauliR = 1.0/(2*gWidth);
    pauliP = 2.0*gWidth/(HBARC*HBARC);
  // Fusimi function.
  } else {
    pauliR = 1.0/(4*gWidth);
    pauliP = gWidth/(HBARC*HBARC);
  }
}

CCluster:: ~CCluster()
{
  if (m_interList->size() > 0) {
    cout << "CCluster::~CCluster interlist size = "<< m_interList->size()
      << " particle = "<< particles.size()
      <<endl;
      printInterList();
      exit(1);
  }

  for (auto& p : particles) p = 0;
  particles.clear();
}

void CCluster::eraseParticle(EventParticle* ip) 
{
  delete ip;
  particles.erase(std::find(particles.begin(),particles.end(),ip));
}

void CCluster::clearParticle()
{
  particles.clear();
}


double CCluster::setConstraints(int optSep,double tau, const Vec4& phat)
{
  optSeparable=optSep;

  // projector for the overall center-of-mass system (not this cluster.)
  pHat = phat;

  // projector for this cluster system.
  if(particles.size()>1) {
    pHatC=0.0;
    for(const auto& p : particles) pHatC += p->getP();
    double pp=pHatC.mCalc();
    pHatC /= pp;
    //cout << " pHatC= "<< pHatC << scientific <<" pp= "<< pp << " nc= "<< particles.size() <<endl;
  } else {
    double m=particles.front()->getEffectiveMass();
    if(m > 1e-6) {
      pHatC = particles.front()->getP()/m;
    } else {
      //pHatC = phat;
      pHatC = Vec4(0.0,0.0,0.0,1.0);
    }
  }

  double epsave=0.0;
  // Sorge's time constraints
  if(optCollisionOrdering >= 120) {
    epsave += setTimeConstraints(tau);
    solveConstraints(tau);

  } else {
    for(auto& p : particles) {
      // no cluster separability
      if(optSeparable==0) p->lambda(1.0/(p->getP()*pHat));

      // impose cluster separability 
      else if(optSeparable>=1) p->lambda(1.0/(p->getP()*pHatC));

      // set time of particles
      if(optSeparable==2) p->setT((dot3(p->getR(),pHatC)+tau)/pHatC[0]);
      else if(optSeparable==3) p->tevol((p->getR()*pHatC));
      //cout << " t= "<< p->getT() << " z= "<< p->getZ()<< " vz= "<< p->getPz()*p->lambda() <<endl;
    if(p->lambda()<0.0) {
    cout << "nc= "<< particles.size() << " phatc= "<< pHatC <<endl;
    cout << "p= "<< p->getP();
    cout << " m= "<< p->getP().mCalc()<<endl;
    cin.get();
    }

    }
  }

  return epsave;
}

double CCluster::setTimeConstraints(double tau)
{
  rSave.clear();
  for(const auto& p:particles) {
    rSave.push_back(p->getR());
  }

  double epsAve=0.0;
  optG=0;
  pW4.assign(particles.size(),0.0);
  double eps=1e+10,eps1=1e+10;
  bool isback=false;
  for(int it=0;it<maxIt;it++) {
    eps1 = makeConstraints(tau);
    //eps1 = makeConstraints2(tau);
   //cout << it << " tau= "<< tau  << " CCluster::setTimeConstraints eps= "<< eps1 << " nv= "<< particles.size() <<endl;
    /*
    if(eps1 > eps || isinf(eps1)) {
      isback=true;
      //optG=1; 
      break;
    }
    */

    eps=eps1;
    if(eps1 < 1e-5) break;
  } // loop over iteration

  //if(eps > 1e-5 && particles.size()>2)
  //cout << "CCluster::setTimeConstrains fails eps= " << eps1 << " at tau= "<< tau << " NV= "<< particles.size() <<endl;

  epsAve += eps;

  /*
  int i=0;
  for(auto p:particles) {
    double t1=p->getT();
    double dt = t1 - rSave[i][0];
    cout << "dt= " << dt<<endl;
      i++;
  }
  cin.get();
  */

  if(isback) {
    int i=0;
    for(auto p:particles) {
      p->setT(rSave[i][0]);
      i++;
    }
  }

  return epsAve;

}

double CCluster::makeConstraintMatrix(double tau)
{
  int NV=particles.size();
  chi.assign(NV,0.0);
  mat.assign(NV,vector<double>(NV+1,0.0));

  Vec4 R=0.0;
  int i1=0;
  auto pend=--particles.end();
  for(auto pa1=particles.begin(); pa1 != particles.end(); ++pa1,++i1) {
    Vec4 r1=(*pa1)->getR();
    Vec4 p1=(*pa1)->getP();
    R += r1;
    chi[i1] += aSmall*((pHat*r1) - tau);
    mat[i1][i1] += aSmall*pHat[0];
    int i2=NV-1;
    for(auto pa2=pend; pa2 !=pa1; --pa2,--i2) {
      Vec4 r2=(*pa2)->getR();
      Vec4 p2=(*pa2)->getP();
      Vec4 r12=r1-r2;
      double q12=r12*r12;
      //if(q12 > distMinSq) continue;
      if(q12 > 0.0) continue;
      Vec4 u12=p1+p2;
      u12 /= u12.mCalc();
      double ur12=u12*r12;
      double G12=1.0, dq0=u12[0];
      double qt12 = (q12 - ur12*ur12)/(4*cWidth);
      Vec4 uij = u12;
      double urij=ur12;
      if(optG==1) {
        r12=rSave[i1]-rSave[i2];
	ur12=u12*r12;
        qt12 = (r12*r12 - ur12*ur12)/(4*cWidth);
      }
      q12 /= (4*cWidth);
      if(optGij==1) {
        G12= exp(q12)/abs(q12-epsG);
        dq0=G12*(2*r12[0]/(4*cWidth)*ur12*(1.-1./(q12-epsG))+uij[0]);
      } else if(optGij==2) {
        G12= exp(q12)/(q12-epsG);
        dq0=G12*(2*r12[0]/(4*cWidth)*ur12*(1.-1./(q12-epsG))+uij[0]);
      } else if(optGij==3) {
        G12= exp(q12);
        dq0=G12*(2*r12[0]/(4*cWidth)*ur12+uij[0]);
      } else if(optGij==4 || optGij==5) {
        G12= optGij==1 ? exp(qt12)/(qt12-epsG) : exp(qt12)/abs(qt12-epsG);
        double q0 = r12[0] - ur12*u12[0];
        dq0=G12*(2*q0/(4*cWidth)*ur12*(1.-1./(qt12-epsG))+uij[0]);
      } else if(optGij==6) {
        G12= exp(qt12);
        double q0 = r12[0] - ur12*u12[0];
        dq0=G12*(2*q0/(4*cWidth)*ur12+uij[0]);
      }
      chi[i1]  += G12*urij;
      chi[i2]  -= G12*urij;
      mat[i1][i1] += dq0;
      mat[i2][i2] += dq0;
      mat[i1][i2] = -dq0;
      mat[i2][i1] = -dq0;
      if(isnan(chi[i1]) || isnan(chi[i2]) || isnan(dq0)) {
	cout << " chi1= " << chi[i1] << " chi2= "<< chi[i2]
	  << " G12= "<< G12 << " urij= "<< urij
	  << " qt12= " << qt12
	  << " q12= " << q12
	  <<endl;
	cout <<"r1= "<< r1;
	cout <<"r2= "<< r2;
	cout <<"p1= "<< p1;
	cout <<"p2= "<< p2;
	exit(1);
      }
    }
  }

  Vec4 Uc = optSeparable == 1 ? pHat : pHatC;
  chi[NV-1]=(Uc*R)/NV - tau;
  mat[NV-1][NV]= -chi[NV-1];

  double eps=0.0;
  for(int k=0;k<NV;k++) {
    //if(abs(mat[k][k])<1e-15) mat[k][k]=1.0;
    mat[NV-1][k]=Uc[0]/NV;
    mat[k][NV] = -chi[k];
    //eps += abs(chi[k]);
    eps += chi[k];
    //cout << k << scientific << " chi= "<< chi[k] << " mat= "<< mat[k][k] << endl;
  }

  if(isinf(eps)) {
    i1=0;
  for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
    cout<< " r= "<< (*pa)->getR();
  }
  for(int i=0;i<NV;i++) if(isinf(chi[i])) cout << " chi= "<< chi[i]<<endl;
  cout<<"CCluster::makeConstraints eps? "<< eps <<endl;
  return 1e+10;
  }

  return eps/NV;

}

double CCluster::makeConstraints(double tau)
{
  int NV=particles.size();
  if(NV==1) {
    EventParticle *p = particles.front();
    p->setT((dot3(p->getR(),pHat)+tau)/p->getPe());
    return 0.0;
  } else if(NV==2) {
    EventParticle *p1 = particles.front();
    EventParticle *p2 = particles.back();
    Vec4 u=p1->getP() + p2->getP();
    u /= u.mCalc();
    p1->setT( (tau+dot3(p1->getR(),u))/u[0] );
    p2->setT( (tau+dot3(p2->getR(),u))/u[0] );
    return 0.0;
  }

//-------------------------------------------------------------------------------------------
  double eps=10;
  bool isW4=0;
  // W4UL method
  if(isW4) {
    double dtau=0.5,tmax=0.0;
    do {
      //double eps0 = makeConstraintMatrix(tau);
      tmax=w4UL(dtau);
      dtau *=0.5;
      cout <<"W4UL eps= "<< eps << " tmax= "<< tmax << " dtau= "<< dtau<<endl;
    } while(tmax>1.0);

  } else {
    eps = makeConstraintMatrix(tau);
    gaussianElimination(tau);
  }

  return eps/NV;
}

//-------------------------------------------------------------------------------------------

double CCluster::w4UL(double dtau)
{
  int NV=particles.size();
  vector<double> pW40=pW4;

  // UL decomposition.
  for(int k=NV-1;k>=1;k--) {
    double pivot = mat[k][k];
    int l=k;
    for(int j=k-1;j>=0;j--)
    if(abs(pivot) < abs(mat[j][k]) ) {
      l = j; pivot = mat[j][k];
    }
    if(l != k) {
      for(int j=0;j<=NV;j++) {
	swap(mat[k][j],mat[l][j]);
        swap(pW4[k],pW4[l]);
      }
    }
    // end of pivotting

    for(int j=k-1; j>=0;j--) mat[k][j] /= pivot;
    for(int i=k-1; i>=0;i--)
    for(int j=k-1; j>=0;j--)
      mat[i][j] -=  (mat[i][k] * mat[k][j]);
  }

  // forward substitution.
  for(int k=0;k<NV;k++) {
    for(int j=0;j<k;j++) {
      pW40[k] -= mat[k][j] * pW40[j];
    }
  }

  // backward substitution. 
  for(int k=NV-1;k>=0;k--) {
    for(int j=k+1;j<NV;j++) {
      mat[k][NV] -= mat[k][j] * mat[j][NV];
    }
    mat[k][NV] /= mat[k][k];
  }

  int i1=0;
  double tmax=0.0;
  for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
    tmax = max(tmax,dtau*pW40[i1]);
    (*pa)->addT(dtau * pW40[i1]);
    pW4[i1] = dtau*mat[i1][NV] + (1.-2*dtau)*pW4[i1];
  }

  return tmax;
}

void CCluster::gaussianElimination(double tau)
{
  int NV=particles.size();
  // fix time by Newton method using Gaussian elimination method.
  for (int k=0; k<NV; k++) {

    // Initialize maximum value and index for pivot
    //int i_max = k;
    //double v_max = mat[i_max][k];
    // find greater amplitude for pivot if any
    //for (int i = k+1; i < NV; i++)
      //if (abs(mat[i][k]) > v_max) v_max = mat[i][k], i_max = i;

    //if (!mat[k][i_max]) {
    if (!mat[k][k]) {
      cout << "CCluster::makeConstraints singlular matirx "<< mat[k][k] << " at k= "<< k
       	<< " nc= "<< NV << " tau= "<< tau << endl;
      mat[k][k]=1.0;
    }
   // Swap the greatest value row with current row.
   //if (i_max != k) for (int k=0; k<=N; k++) swap(mat[i][k],mat[j][k]);

    for (int i=k+1; i<NV; i++) {
      double f = mat[i][k]/mat[k][k];
      for (int j=k+1; j<=NV; j++) mat[i][j] -= mat[k][j]*f;
      mat[i][k] = 0;
    }
  }
  double dtt=0.0;
  // Start calculating from last equation up to the first.
  for (int i = NV-1; i >= 0; i--) {
    for (int j=i+1; j<NV; j++) {
      mat[i][NV] -= mat[i][j]*mat[j][NV];
    }
    mat[i][NV] /= mat[i][i];
    dtt += abs(mat[i][NV]);
  }
  double alpha=1.0;
  dtt /= NV;
  if(dtt > 0.6) alpha = 0.6/dtt;

  int i1=0;
  for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
    (*pa)->addT(alpha * mat[i1][NV]);
    if(isnan(mat[i1][NV])) {
    cout << " dt= "<< mat[i1][NV]<<endl;
    exit(1);
    }
  }

}

// change |r| to satisfy the time constraints.
double CCluster::makeConstraints2(double tau)
{
  int NV=particles.size();
  if(NV==1) {
    EventParticle *p = particles.front();
    p->setT((dot3(p->getR(),pHat)+tau)/p->getPe());
    return 0.0;
  } else if(NV==2) {
    EventParticle *p1 = particles.front();
    EventParticle *p2 = particles.back();
    Vec4 u=p1->getP() + p2->getP();
    u /= u.mCalc();
    p1->setT( (tau+dot3(p1->getR(),u))/u[0] );
    p2->setT( (tau+dot3(p2->getR(),u))/u[0] );
    return 0.0;
  }

  chi.assign(NV,0.0);
  mat.assign(NV,vector<double>(NV+1,0.0));

  Vec4 R=0.0;
  int i1=0;
  auto pend=--particles.end();
  for(auto pa1=particles.begin(); pa1 != particles.end(); ++pa1,++i1) {
    Vec4 r1=(*pa1)->getR();
    Vec4 p1=(*pa1)->getP();
    R += r1;
    chi[i1] += aSmall*((pHat*r1) - tau);
    mat[i1][i1] += aSmall*pHat[0];
    int i2=NV-1;
    for(auto pa2=pend; pa2 !=pa1; --pa2,--i2) {
      Vec4 r2=(*pa2)->getR();
      Vec4 p2=(*pa2)->getP();
      Vec4 r12=r1-r2;
      Vec4 u12=p1+p2;
      u12 /= u12.mCalc();
      double ur12=u12*r12;
      double G12=1.0, dq0=u12[0];
      double qt12 = (r12*r12 - ur12*ur12)/(4*cWidth);
      Vec4 uij = u12;
      double urij=ur12;

      if(optG==1) {
        r12=rSave[i1]-rSave[i2];
	ur12=u12*r12;
        qt12 = (r12*r12 - ur12*ur12)/(4*cWidth);
      }
      double q12=r12*r12;
      //if(q12 > distMinSq) continue;
      if(q12 > 0.0) continue;
      q12 /= (4*cWidth);
      double qabs=r12.pAbs();
      Vec4  qhat = r12/qabs;
      qhat[0]=0.0;
      if(optGij==1) {
        G12= exp(q12)/abs(q12-epsG);
        double q0 = 2*qabs;
        dq0=G12*(q0/(4*cWidth)*ur12*(1.-1./(q12-epsG))+uij*qhat);
      } else if(optGij==2) {
        G12= exp(q12)/(q12-epsG);
        double q0 = 2*qabs;
        dq0=G12*(q0/(4*cWidth)*ur12*(1.-1./(q12-epsG))+uij*qhat);
      } else if(optGij==3) {
        G12= exp(q12);
        double q0 = 2*qabs;
        dq0=G12*(q0/(4*cWidth)*ur12+uij*qhat);
      } else if(optGij==4 || optGij==5) {
          G12= optGij==1 ? exp(qt12)/(qt12-epsG) : exp(qt12)/abs(qt12-epsG);
          double q0 = 2*qabs - ur12*(qhat*u12);
          dq0=G12*(q0/(4*cWidth)*ur12*(1.-1./(qt12-epsG))+uij*qhat);
      } else if(optGij==6) {
        G12= exp(qt12);
        double q0 = 2*qabs - ur12*(qhat*u12);
        dq0=G12*(q0/(4*cWidth)*ur12+uij*qhat);
      }
      chi[i1]  += G12*urij;
      chi[i2]  -= G12*urij;
      mat[i1][i1] += dq0;
      mat[i2][i2] += dq0;
      mat[i1][i2] = -dq0;
      mat[i2][i1] = -dq0;
    }
  }

  Vec4 Uc = optSeparable == 1 ? pHat : pHatC;
  chi[NV-1]=(Uc*R)/NV - tau;
  mat[NV-1][NV]= -chi[NV-1];

  double eps=0.0;
  i1=0;
  for(auto pa1=particles.begin(); pa1 != particles.end(); ++pa1,++i1) {
  //for(int k=0;k<NV;k++) {
    if(abs(mat[i1][i1])<1e-15) mat[i1][i1]=1.0;
      Vec4  qhat = (*pa1)->getR();
      qhat /= qhat.pAbs();
      qhat[0]=0.0;
    mat[NV-1][i1]=Uc*qhat/NV;
    mat[i1][NV] = -chi[i1];
    eps += abs(chi[i1]);
    //cout << k << scientific << " chi= "<< chi[k] << " mat= "<< mat[k][k] << endl;
  }

  // fix time by Newton method.
  for (int k=0; k<NV; k++) {

    // Initialize maximum value and index for pivot
    //int i_max = k;
    //double v_max = mat[i_max][k];
    // find greater amplitude for pivot if any
    //for (int i = k+1; i < NV; i++)
      //if (abs(mat[i][k]) > v_max) v_max = mat[i][k], i_max = i;

    //if (!mat[k][i_max]) {
    if (!mat[k][k]) {
      cout << "CCluster::makeConstraints2 singlular matirx "<< mat[k][k] << " at k= "<< k
       	<< " nc= "<< NV << " tau= "<< tau << endl;
      //for(int k1=0;k1<NV;k1++) cout << k1 << " matkk= "<< mat[k1][k1] << " chi= "<< chi[k1] <<endl;
      auto pa1= particles.begin();
      for(int i=0;i<k;i++) pa1++;
      Vec4 r1=(*pa1)->getR();
      Vec4 p1=(*pa1)->getP();
	i1=0;
      for(auto pa2=particles.begin(); pa2 !=particles.end(); ++pa2,++i1) {
        Vec4 r2=(*pa2)->getR();
        Vec4 p2=(*pa2)->getP();
        Vec4 u12=p1+p2;
        u12 /= u12.mCalc();
        Vec4 r12=r1-r2;
      double ur12=u12*r12;
      double qt12 = (r12*r12 - ur12*ur12);
	cout << " mat= " << i1 << scientific << " " << mat[k][i1]
	  << " qt12= "<< qt12
	  << " G12= " << exp(qt12/(4*cWidth))*4*cWidth/qt12
	  << " t1= "<< r1[0]
	  << " t2= "<< r2[0]
	      << endl;
      }
      //mat[k][k]=1.0;
      //exit(1);
      return 0.0;
    }
   // Swap the greatest value row with current row.
   //if (i_max != k) for (int k=0; k<=N; k++) swap(mat[i][k],mat[j][k]);

    for (int i=k+1; i<NV; i++) {
      double f = mat[i][k]/mat[k][k];
      for (int j=k+1; j<=NV; j++) mat[i][j] -= mat[k][j]*f;
      mat[i][k] = 0;
    }
  }
  // Start calculating from last equation up to the first.
  for (int i = NV-1; i >= 0; i--) {
    for (int j=i+1; j<NV; j++) {
      mat[i][NV] -= mat[i][j]*mat[j][NV];
    }
    mat[i][NV] /= mat[i][i];
  }

  if(eps/NV>10.0) return eps/NV;

  i1=0;
  for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
    Vec4 r = (*pa)->getR();
    Vec4 rhat = r/r.pAbs();
    r += mat[i1][NV]*rhat;
    (*pa)->addR(r);
    //cout << " dt= "<< mat[i1][NV]<<endl;
  }

  return eps/NV;
}


void CCluster::solveConstraints(double tau)
{
  int NV=particles.size();
  if(NV ==1) {
    double m=particles.front()->getEffectiveMass();
    if(m==0.0) m=1.0;
    particles.front()->lambda(1.0/m);
    return;
  } else if(NV==2) {
    EventParticle *p1 = particles.front();
    EventParticle *p2 = particles.back();
    Vec4 u=p1->getP() + p2->getP();
    u /= u.mCalc();
    p1->lambda( 1.0/(u*p1->getP()) );
    p2->lambda( 1.0/(u*p2->getP()) );
    return;
  }

  //chi.assign(NV,0.0);
  mat.assign(NV,vector<double>(NV+1,0.0));

  Vec4 R=0.0;
  auto pend=--particles.end();
  int i1=0;
  for(auto pa1=particles.begin(); pa1 !=particles.end(); ++pa1,++i1) {
    Vec4 r1=(*pa1)->getR();
    Vec4 p1=(*pa1)->getP();
    R += r1;
    mat[i1][i1] += aSmall*2*(pHat*p1);
    int i2=NV-1;
    for(auto pa2=pend; pa2 !=pa1; --pa2,--i2) {
      //if(chi[i2]>1.0) continue;
      Vec4 r2=(*pa2)->getR();
      Vec4 p2=(*pa2)->getP();
      Vec4 u12=p1+p2;
      u12 /= u12.mCalc();
      Vec4 r12=r1-r2;
      double q12=r12*r12;
      //if(q12 > distMinSq) continue;
      if(q12 > 0.0) continue;
      double ur12=u12*r12;
      double G12=1.0;
      double dq1=2*p1*u12;
      double dq2=2*p2*u12;
      double qt12 = (q12 - ur12*ur12)/(4*cWidth);
      Vec4 uij = u12;
      q12 /= (4*cWidth);
      if(q12 > 10.0) continue;
      if(optGij==1) {
        G12= exp(q12)/abs(q12-epsG);
        dq1=G12*(2/(4*cWidth)*ur12*(1.-1./(q12-epsG))*2*(r12*p1) + 2*(p1*uij));
        dq2=G12*(2/(4*cWidth)*ur12*(1.-1./(q12-epsG))*2*(r12*p2) + 2*(p2*uij));
      } else if(optGij==2) {
        G12= exp(q12)/(q12-epsG);
        dq1=G12*(2/(4*cWidth)*ur12*(1.-1./(q12-epsG))*2*(r12*p1) + 2*(p1*uij));
        dq2=G12*(2/(4*cWidth)*ur12*(1.-1./(q12-epsG))*2*(r12*p2) + 2*(p2*uij));
      } else if(optGij==3) {
        G12= exp(q12);
        dq1=G12*(2/(4*cWidth)*ur12*2*(r12*p1) + 2*(p1*uij));
        dq2=G12*(2/(4*cWidth)*ur12*2*(r12*p2) + 2*(p2*uij));
      } else if(optGij==4 || optGij==5) {
        G12= optGij==1 ? exp(qt12)/(qt12-epsG): exp(qt12)/abs(qt12-epsG);
        Vec4 q = r12 - ur12*u12;
        dq1=G12*(2/(4*cWidth)*ur12*(1.-1./(qt12-epsG))*2*(q*p1) + 2*(p1*uij));
        dq2=G12*(2/(4*cWidth)*ur12*(1.-1./(qt12-epsG))*2*(q*p2) + 2*(p2*uij));
      } else if(optGij==6) {
        G12= exp(qt12);
        Vec4 q = r12 - ur12*u12;
        dq1=G12*(2/(4*cWidth)*ur12*2*(p1*q)+2*(p1*uij));
        dq2=G12*(2/(4*cWidth)*ur12*2*(p2*q)+2*(p2*uij));
      }
      //chi[i1]  += G12*urij;
      //chi[i2]  -= G12*urij;

      // for Newton method.
      mat[i1][i1] += dq1;
      mat[i2][i2] += dq2;
      mat[i1][i2] = -dq2;
      mat[i2][i1] = -dq1;
      if(isnan(dq1) || isnan(dq2)) {
	cout << "dq1 = "<< dq1 << " dq2= "<< dq2 <<endl;
	cout << "G12= "<< G12 << " q12= "<< q12 <<endl;
	cout << "r1= " << r1;
	cout << "r2= " << r2;
	exit(1);
      }
    }
  }
  Vec4 Uc = optSeparable == 1 ? pHat : pHatC;
  //double eps=0.0;
  i1=0;
  for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
    if(abs(mat[i1][i1])<1e-15) mat[i1][i1]=1.0;
    mat[NV-1][i1]=2*((*pa)->getP()*Uc)/NV;
    //eps += abs(chi[i1]);
    //cout << i1 << " sovle "<< scientific << " chi= "<< chi[i1] << endl;
  }

  //chi[NV-1]=(Uc*R)/NV - tau;
  if(aSmall) for(int i=0;i<NV;i++) mat[NV-1][i]= 1.0;
  mat[NV-1][NV]= 1.0;

  // fix time by Newton method.
  for (int k=0; k<NV; k++) {
    if (!mat[k][k]) {
      cout << "CCluster::solveConstraints singlular matirx "<< mat[k][k]  <<" at k= "<< k << endl;
      cout << " tau= "<< tau << " NV= "<< NV <<endl;
      i1=0;
      //for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
      //cout << i1 << " mat= "<< mat[i1][i1] << " lambda= "<< (*pa)->lambda() <<endl;}
      mat[k][k]=1.0;
    }
    for (int i=k+1; i<NV; i++) {
      double f = mat[i][k]/mat[k][k];
      for (int j=k+1; j<=NV; j++) mat[i][j] -= mat[k][j]*f;
      mat[i][k] = 0;
    }
  }
  // Start calculating from last equation up to the first.
  for (int i = NV-1; i >= 0; i--) {
    for (int j=i+1; j<NV; j++) {
      mat[i][NV] -= mat[i][j]*mat[j][NV];
    }
    mat[i][NV] /= mat[i][i];
  }

  i1=0;
  for(auto pa=particles.begin(); pa !=particles.end(); ++pa,++i1) {
    (*pa)->lambda(2.0*mat[i1][NV]);

    if(mat[i1][NV]<=0.0) {
      cout <<"CCluster lambda<0? Backward propagation? "<< mat[i1][NV]<<endl;
    //(*pa)->lambda(1.0/(pHat*(*pa)->getP()));

    //double m = (*pa)->getMass() !=0.0 ? (*pa)->getMass() : 1.0;
    //(*pa)->lambda(1.0/m);
    //(*pa)->lambda(abs(2.0*mat[i1][NV]));
    (*pa)->lambda(1e-8);
    }
    //float inf = std::numeric_limits<float>::infinity();
    if(isinf((*pa)->lambda())) {
      cout <<"CCluster lambda inf? "<< mat[i1][NV] << " id= "<< (*pa)->getID() <<endl;
      exit(1);
    }
      
    if(isnan((*pa)->lambda())) {
      cout << "lambda= "<< (*pa)->lambda()<<endl;
      exit(1);
    }

    //cout << " lambda= "<< (*pa)->lambda() << " tau= "<< (*pa)->tevol()
    //   << " t= "<< (*pa)->getT() << " z= "<< (*pa)->getZ()
    //   << " vz= "<< (*pa)->getPz()*2.0*mat[i1][NV]
    //  <<endl;
  }
  //cout << "SolveConst eps= "<< eps << " nc= "<< particles.size() <<endl;
  //cin.get();

  return;
}


InterList* CCluster::sortInterList() { return m_interList->next(); }

int CCluster::interListSize() { return m_interList->size(); }

void CCluster::setInterList(InterList* inter) { m_interList->push(inter); }

void CCluster::printInterList() { m_interList->print(std::cout); }

void CCluster::cleanInterList(EventParticle* i1, EventParticle* i2) {
  m_interList->clean(i1, i2);
}

void CCluster::removeInterList(EventParticle* i1) { m_interList->clean(i1); }

void CCluster::cleanInterList(EventParticle* i1)
{
  //for(auto& b : neighbors2)  b->removeInterList(i1);
  removeInterList(i1);
}

int CCluster::cleanInterList() { return m_interList->clear(); }

void CCluster::removeInter(InterList* inter)
{
  if (!m_interList->remove(inter)) {
    std::cout << "CCluster not find inter "<< inter <<std::endl;
    std::exit(1);
  }
}

bool CCluster::checkNextCollisionTime(InterList const* inter, EventParticle const* p1, double dtau1, EventParticle const* p2, double dtau2) const {
  return m_interList->checkNextCollisionTime(inter, p1, dtau1, p2, dtau2);
}

double CCluster::phaseSpace(EventParticle* i1, EventParticle* i2, EventParticle* ip,double ctime,int opt)
{
  Vec4 r = ip->getR();
  Vec4 p = ip->getP();
  int idp = ip->getID();

  // Loop over all particles. 
  double phase = 0.0;
  for(const auto& i3 : particles) {
      if(i1 == i3) continue; // exclude incoming particle 1.
      if(i2 == i3) continue; // exclude incoming particle 2.
      if(idp != i3->getID()) continue;  // not a nucleon.
      if(ctime < i3->getT()) continue; // not formed
      if(ctime < i3->getTf()) continue; // not formed
      Vec4 r3 = i3->propagate(ctime,opt);
      Vec4 p3 = i3->getP();
      Vec4 dr = r - r3;
      Vec4 dp = p - p3;
      dr[0]=0.0;

      if(withBox) {
        dr[1] = modulo(dr[1] + xBox/2, xBox) - xBox/2;
        dr[2] = modulo(dr[2] + yBox/2, yBox) - yBox/2;
        dr[3] = modulo(dr[3] + zBox/2, zBox) - zBox/2;
      }

      double s = m2(p,p3);
      Vec4 P = p+p3;
      double dr2 = dr.m2Calc() - pow2(dr*P)/s;
      double dp2 = dp.m2Calc() - pow2(dp*P)/s;
      phase += exp(pauliR*dr2 + pauliP*dp2);
  }
  return phase;
}

} // end of namespace jam2
