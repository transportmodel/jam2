#ifndef jam2_collision_Collision2_h
#define jam2_collision_Collision2_h

#include <list>
#include <vector>
#include <set>
#include <fstream>
#include <jam2/collision/EventParticle.h>
#include <jam2/collision/TimeOrder.h>
#include <jam2/collision/InterList.h>
#include <jam2/xsection/CrossSection.h>
#include <Pythia8/Settings.h>
#include <jam2/collision/Collision.h>
#include <jam2/collision/CCluster.h>
//#include <jam2/collision/CascBox.h>
//#include <jam2/collision/CascCell.h>

namespace jam2 {

typedef std::list<EventParticle*>::iterator EventParIt;

class Collision2 : public Collision
{
private:
  double distMinSq=5.0*5.0;
  std::vector<CCluster*> clusterList;
  CCluster *clusterNow=0;
  int optSeparable=1;
  double cWidth;

public:
    Collision2(Pythia8::Settings* s,JamParticleData* jp, CrossSection* in,Pythia8::Rndm* r);
    ~Collision2();
  void clear();
  void findCluster();
  void init(const InitialCondition* initc);
  void makeCollisionList(double itime,double gtime);
  InterList* findNextCollision();
  void collisionUpdate();
  void collisionUpdate(InterList* inter);
  void collisionUpdate(vector<EventParticle*>& outgoing,double itime,double gtime);
  void cancelCollision(InterList* inter);
  bool checkNextCollisionTime(TwoBodyInterList* inter,double dtau1,double dtau2,
                 bool preHadronA,bool preHadronB);
  int  searchCollisionPair(CCluster* c);
  void searchCollisionPair(EventParticle* p);

  void propagate(double ctime,int opt,int step=2);
  bool doPauliBlocking(InterList* inter,vector<EventParticle*>& outgoing,int opt);
  void removeInteraction(EventParticle* i1);

private:
  void removeInteraction2(InterList* inter);

};
}
#endif
